<?php  
  session_start(); 
  error_reporting(0);
  $hoy = date("Ymd");
  //20210701
  if($hoy>='20210704'){
    header('Location:resultados_nuevo.php');
  }else{
    // header('Location:resultados_nuevo.php');

  } ?>
  

<!doctype html>
<html lang="es">
<head>
<!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>

  <!-- <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" /> -->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<title>LABORATORIO CORREGIDORA</title>
<!-- <link rel="stylesheet" href="estilo.css" /> -->

<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
<link rel="manifest" href="img/favicon/site.webmanifest">
<link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
<meta name="msapplication-TileColor" content="#000000">
<meta name="theme-color" content="#ffffff">
<!-- favicon -->

<!-- <meta name="Description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/>
<meta property="og:url" content="https://almecmexico.com"/>
<meta property="og:title" content="ALMEC"/>
<meta property="og:image" content="https://almecmexico.com/img/almec-share.jpg"/>
<meta property="og:description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/> -->

<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-691396176');
</script> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-152013601-2');
</script> -->
<!-- end Global site tag (gtag.js) - Google Analytics -->

</head>
<body>
  <?php include('header.php'); ?>
  <section id="home_resultado">
    <div class="cont_imagen_fondo"></div>
    <div class="cont-opciones-resultados">
      <div class="cont-menu-interno">

        <a class="link-resultados" href="https://norte75.resultados.nojoch.net/" target="_blank">
          <div class="menu-resultados-cuadro">
            <div class="borde-opcion">
              <div class="icono-menu-cuadro">
                <img class="icono-svg" src="img/svg/pacientes.svg">
              </div>
              <div class="titulo-menu-cuadro">
                <p>PACIENTES</p>
              </div>
            </div>
          </div>
        </a>

        <a class="link-resultados" href="https://corregidora.resultados.nojoch.net/" target="_blank">
          <div class="menu-resultados-cuadro">
            <div class="borde-opcion">
              <div class="icono-menu-cuadro">
                <img class="icono-svg" src="img/svg/doctores.svg">
              </div>
              <div class="titulo-menu-cuadro">
                <p>DOCTORES</p>
              </div>
            </div>
          </div>
        </a>

        <a class="link-resultados" href="https://corregidora.resultados.nojoch.net/" target="_blank">
          <div class="menu-resultados-cuadro">
            <div class="borde-opcion">
              <div class="icono-menu-cuadro">
                <img class="icono-svg" src="img/svg/laboratorios.svg">
              </div>
              <div class="titulo-menu-cuadro">
                <p>LABORATORIOS E INSTITUCIONES</p>
              </div>
            </div>
          </div>
        </a>

      </div>

      <!-- <p class="flex nota_res">NOTA: En caso de consultarlos desde dispositivos APPLE(iPhone, iPad, Mac) entrar mediante navegador Chrome <img class="logos_navegadores" src="img/svg/logo_chrome.svg" alt="Logo Navegador"> , opera <img class="logos_navegadores" src="img/svg/logo_opera.svg" alt="Logo Navegador"> , explorer <img class="logos_navegadores" src="img/svg/logo_explorer.svg" alt="Logo Navegador"> o Firefox <img class="logos_navegadores" src="img/svg/logo_firefox.svg" alt="Logo Navegador"> ya que no es compatible con Safari.</p> -->

    </div>



    <!-- footer movil -->

    <div class="footer-mov footer-mov-resultados">
      <div class="cont_footer_mov">
        <p><a href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de privacidad</a></p>
        <p><a href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a></p>
        <p><a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a></p>
        <p><span class="menu-el link-a" op="vid-mov" style="font-family: m-regular!important; font-size: 16px!important; cursor: pointer!important;">Contáctanos</span></p>
        <p style="text-align: center;">Permiso de Publicidad: 203301201A0872</p>
        <p><a class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a></p>
      </div>
    </div>

    <!-- end footer movil -->

  </section>
  <div class="div-footer footer-resultados">
    <div class="footer-cell">
     <a href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de Privacidad</a>   &nbsp;    /    &nbsp; <a href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a>   &nbsp;    /    &nbsp;  <a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a>    &nbsp;  /   &nbsp;    <span class="menu-el" op="sucursales" style="cursor: pointer; ">Contáctanos</span>
    </div>
    <div class="footer-cell">
      <p style="text-align: center;">Permiso de Publicidad: 203301201A0872</p>
    </div>
    <div class="footer-cell" style="text-align:right">
      <a class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a>
    </div>
  </div>


  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>

  <script type="text/javascript">

    // $(".notificacion").click(function(){
    //   console.log("medidas de ancho = "+screen.width);
    //   if (screen.width < 600) {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid_mov";
    //   }else{
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid";
    //   }

    // });

    // $(".citas_covid").click(function(){
    //   window.location = "https://www.laboratoriocorregidora.com.mx/citas/";
    // });

    $(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function(){

      window.location = "resultados.php";

    });

    $(document).ready(function(){
      cargar_carrito_header();
    });

    // $(".notificacion").css("z-index","100");

    //$(".op_guardias").addClass("menu_activo_desktop");

    $(document).on("click", ".menu-el", function(){
      let opcion = $(this).attr("op");
      switch(opcion){
        case "servicios":
          localStorage.setItem("opcion_animate", "servicios");
          window.location = "/";
          break;
        case "preguntas":
          localStorage.setItem("opcion_animate", "preguntas");
          window.location = "/";
          break;
        case "sucursales":
          localStorage.setItem("opcion_animate", "sucursales");
          window.location = "/";
          break;
        case "serv-mov":
          localStorage.setItem("opcion_animate", "serv-mov");
          window.location = "/";
          break;
        case "preg-mov":
          localStorage.setItem("opcion_animate", "preg-mov");
          window.location = "/";
          break;
        case "vid-mov":
          localStorage.setItem("opcion_animate", "vid-mov");
          window.location = "/";
          break;
        default:
          break;
      }
    });

    function myFunction(x) {
      x.classList.toggle("change");
      if(document.getElementById("contenidoMenu").style.display == "none"){
        document.getElementById("contenidoMenu").style.display = "";
        $("#contenidoMenu").animate({
          height: '100vh'
        }, "slow");
      }else{
        $("#contenidoMenu").animate({
          height: '0'
        }, "slow", function(){ document.getElementById("contenidoMenu").style.display = "none"; });
      }
    }

  </script>
</body>
</html>
