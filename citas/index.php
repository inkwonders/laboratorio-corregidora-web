<?php

  date_default_timezone_set('America/Mexico_city');

  error_reporting(E_ALL);
  ini_set('display_errors', '1');

  require_once "controladores/plantilla.controlador.php";
  require_once "controladores/cita.controlador.php";
	require_once "controladores/enfermedades.controlador.php";
  require_once "controladores/sintomas.controlador.php";
  require_once "controladores/validaciones.controlador.php";
  require_once "controladores/ubicaciones.controlador.php";
  require_once "controladores/pruebas.controlador.php";

  require_once "modelos/cita.modelo.php";
	require_once "modelos/enfermedades.modelo.php";
  require_once "modelos/sintomas.modelo.php";
  require_once "modelos/ubicaciones.modelo.php";
  require_once "modelos/pruebas.modelo.php";


	// $plantilla = new ControladorPlantilla();
	// $plantilla -> plantilla();

  include "vistas/plantilla.php";
