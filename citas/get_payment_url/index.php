<?php
header("Content-Type: application/json");
date_default_timezone_set('America/Mexico_city');
require_once $_SERVER['DOCUMENT_ROOT']."/citas/modelos/conexion.php";

$request = json_decode(file_get_contents('php://input'));

$analysis_type = [
    'antigenos' => 1029,
    'pcr'       => 9023
];

$confirmation = hash("adler32", file_get_contents('php://input'));
$no_confirmacion = substr($confirmation, 0, 4)."-".substr($confirmation, 4, 7);
// exit(explode(" ", $request->time->start_datetime)[1]);




foreach ($request->patients as $patient) {
    // Creación o verificación de la existencia del usuario ---------------------------------------
    $stmt = Conexion::conectar()->prepare(
        "SELECT *
            FROM users

            WHERE email = :email"
    );

    $stmt->bindParam(":email", $patient->email, PDO::PARAM_STR);
    $stmt->execute();

    $user = $stmt->fetchAll();

    // Si no existe se registra
    if (count($user) == 0) {
        $stmt = Conexion::conectar()->prepare(
            "INSERT
                INTO `users` (`name`, `email`, `email_verified_at`, `created_at`)

                VALUES (:patient_name, :email, '0000-00-00 00:00:00', '".date("Y-m-d H:i:s")."')"
        );

        $stmt->bindParam(":patient_name",   $patient->patient_name, PDO::PARAM_STR);
        $stmt->bindParam(":email",          $patient->email,        PDO::PARAM_STR);

        if (!$stmt->execute()) {
            exit(json_encode([
                "success"   => false,
                "msg"       => "Ocurrió un error, inténtelo más tarde por favor.",
            ]));
        }
    }


    // Creación del registro del paciente ---------------------------------------------------------
    $clave = hash("adler32", $patient->email);

    $stmt = Conexion::conectar()->prepare(
        "INSERT
            INTO `pacientes` (
                `user_id`,
                `clave`,
                `nombre`,
                `apellido_paterno`,
                `apellido_materno`,
                `nombre_completo`,
                `genero`,
                `fecha_nacimiento`,
                `calle`,
                `colonia`,
                `no_exterior`,
                `no_interior`,
                `ubicacion_id`,
                `cp`,
                `tel_celular`,
                `tel_casa`,
                `paciente_email`,
                `medico_nombre`,
                `medico_email`,
                `razon_social`,
                `rfc`,
                `domicilio_fiscal`,
                `cfdis_id`,
                `regimen_fiscal_id`,
                `otro_sintoma`,
                `otra_enfermedad`,
                `contacto_persona_positivo`,
                `antiviral`,
                `influenza`,
                `otro_prueba`,
                `fecha_alta`,
                `pasaporte`,
                `es`,
                `en`,
                `created_at`
            )

            VALUES (
                (SELECT id FROM users WHERE users.email = :paciente_email LIMIT 1),
                '$clave',
                :nombre,
                :apellido_paterno,
                :apellido_materno,
                :nombre_completo,
                :genero,
                :fecha_nacimiento,
                :calle,
                :colonia,
                :no_exterior,
                :no_interior,
                :ubicacion_id,
                :cp,
                :tel_celular,
                :tel_casa,
                :paciente_email,
                :medico_nombre,
                :medico_email,
                :razon_social,
                :rfc,
                :domicilio_fiscal,
                :cfdis_id,
                :regimen_fiscal_id,
                :otro_sintoma,
                :otra_enfermedad,
                :contacto_persona_positivo,
                :antiviral,
                :influenza,
                :otro_prueba,
                :fecha_alta,
                :pasaporte,
                :es,
                :en,
                '".date("Y-m-d H:i:s")."'
            )"
    );

    $full_name = $patient->patient_name." ".$patient->patient_father_last_name." ".$patient->patient_mother_last_name;
    $gender = ($patient->gender == "male"? 1 : 2);
    $contacto_persona_positivo = ($patient->question_a? 1 : 0);
    $antiviral = ($patient->question_b? 1 : 0);
    $influenza = ($patient->question_c? 1 : 0);
    $fecha_alta = date("Y-m-d H:i:s");
    $es = (in_array(1, $patient->languages)? 1 : 0);
    $en = (in_array(2, $patient->languages)? 1 : 0);

    $stmt->bindParam(":nombre",                     $patient->patient_name,             PDO::PARAM_STR);
    $stmt->bindParam(":apellido_paterno",           $patient->patient_father_last_name, PDO::PARAM_STR);
    $stmt->bindParam(":apellido_materno",           $patient->patient_mother_last_name, PDO::PARAM_STR);
    $stmt->bindParam(":nombre_completo",            $full_name,                         PDO::PARAM_STR);
    $stmt->bindParam(":genero",                     $gender,                            PDO::PARAM_INT);
    $stmt->bindParam(":fecha_nacimiento",           $patient->birthdate,                PDO::PARAM_STR);
    $stmt->bindParam(":calle",                      $patient->street,                   PDO::PARAM_STR);
    $stmt->bindParam(":colonia",                    $patient->colony,                   PDO::PARAM_STR);
    $stmt->bindParam(":no_exterior",                $patient->no_ext,                   PDO::PARAM_STR);
    $stmt->bindParam(":no_interior",                $patient->no_int,                   PDO::PARAM_STR);
    $stmt->bindParam(":ubicacion_id",               $patient->municipality_id,          PDO::PARAM_INT);
    $stmt->bindParam(":cp",                         $patient->zip_code,                 PDO::PARAM_STR);
    $stmt->bindParam(":tel_celular",                $patient->cellphone,                PDO::PARAM_STR);
    $stmt->bindParam(":tel_casa",                   $patient->phone,                    PDO::PARAM_STR);
    $stmt->bindParam(":paciente_email",             $patient->email,                    PDO::PARAM_STR);
    $stmt->bindParam(":medico_nombre",              $patient->med_name,                 PDO::PARAM_STR);
    $stmt->bindParam(":medico_email",               $patient->med_email,                PDO::PARAM_STR);
    $stmt->bindParam(":razon_social",               $patient->business_name,            PDO::PARAM_STR);
    $stmt->bindParam(":rfc",                        $patient->rfc,                      PDO::PARAM_STR);
    $stmt->bindParam(":domicilio_fiscal",           $patient->tax_residence,            PDO::PARAM_STR);
    $stmt->bindParam(":cfdis_id",                   $patient->cfdi_use_id,              PDO::PARAM_INT);
    $stmt->bindParam(":regimen_fiscal_id",          $patient->tax_id,                   PDO::PARAM_INT);
    $stmt->bindParam(":otro_sintoma",               $patient->other_symptoms,           PDO::PARAM_STR);
    $stmt->bindParam(":otra_enfermedad",            $patient->other_disease,            PDO::PARAM_STR);
    $stmt->bindParam(":contacto_persona_positivo",  $contacto_persona_positivo,         PDO::PARAM_INT);
    $stmt->bindParam(":antiviral",                  $antiviral,                         PDO::PARAM_INT);
    $stmt->bindParam(":influenza",                  $influenza,                         PDO::PARAM_INT);
    $stmt->bindParam(":otro_prueba",                $patient->other_reasons,            PDO::PARAM_STR);
    $stmt->bindParam(":fecha_alta",                 $fecha_alta,                        PDO::PARAM_STR);
    $stmt->bindParam(":pasaporte",                  $patient->passport,                 PDO::PARAM_STR);
    $stmt->bindParam(":es",                         $es,                                PDO::PARAM_INT);
    $stmt->bindParam(":en",                         $en,                                PDO::PARAM_INT);

    if (!$stmt->execute()) {
        exit(json_encode([
            "success"   => false,
            "msg"       => "Ocurrió un error, inténtelo más tarde por favor.",
        ]));
    }


    // Adición de las enfermedades de cada paciente -----------------------------------------------
    foreach ($patient->chronic_diseases as $disease) {
        $stmt = Conexion::conectar()->prepare(
            "INSERT
                INTO `enfermedades_pacientes` (
                    `paciente_id`,
                    `enfermedad_id`,
                    `created_at`
                )

                VALUES (
                    (SELECT id FROM pacientes WHERE pacientes.user_id = (
                        SELECT id FROM users WHERE users.email = :paciente_email LIMIT 1
                    ) LIMIT 1),
                    :enfermedad_id,
                    '".date("Y-m-d H:i:s")."'
                )"
        );

        $stmt->bindParam(":paciente_email", $patient->email,    PDO::PARAM_STR);
        $stmt->bindParam(":enfermedad_id",  $disease,           PDO::PARAM_INT);

        if (!$stmt->execute()) {
            exit(json_encode([
                "success"   => false,
                "msg"       => "Ocurrió un error, inténtelo más tarde por favor.",
            ]));
        }
    }


    // Adición de las enfermedades de cada paciente -----------------------------------------------
    foreach ($patient->test_reasons as $prueba) {
        $stmt = Conexion::conectar()->prepare(
            "INSERT
                INTO `pruebas_pacientes` (
                    `paciente_id`,
                    `prueba_id`,
                    `created_at`
                )

                VALUES (
                    (SELECT id FROM pacientes WHERE pacientes.user_id = (
                        SELECT id FROM users WHERE users.email = :paciente_email LIMIT 1
                    ) LIMIT 1),
                    :prueba_id,
                    '".date("Y-m-d H:i:s")."'
                )"
        );

        $stmt->bindParam(":paciente_email", $patient->email,    PDO::PARAM_STR);
        $stmt->bindParam(":prueba_id",      $prueba,            PDO::PARAM_INT);

        if (!$stmt->execute()) {
            exit(json_encode([
                "success"   => false,
                "msg"       => "Ocurrió un error, inténtelo más tarde por favor.",
            ]));
        }
    }


    // Adición de los síntomas de cada paciente ---------------------------------------------------
    foreach ($patient->symptoms as $symptom) {
        $stmt = Conexion::conectar()->prepare(
            "INSERT
                INTO `sintomas_pacientes` (
                    `paciente_id`,
                    `sintoma_id`,
                    `created_at`
                )

                VALUES (
                    (SELECT id FROM pacientes WHERE pacientes.user_id = (
                        SELECT id FROM users WHERE users.email = :paciente_email LIMIT 1
                    ) LIMIT 1),
                    :sintoma_id,
                    '".date("Y-m-d H:i:s")."'
                )"
        );

        $stmt->bindParam(":paciente_email", $patient->email,    PDO::PARAM_STR);
        $stmt->bindParam(":sintoma_id",     $symptom,           PDO::PARAM_INT);

        if (!$stmt->execute()) {
            exit(json_encode([
                "success"   => false,
                "msg"       => "Ocurrió un error, inténtelo más tarde por favor.",
            ]));
        }
    }


    // Creación de la cita ------------------------------------------------------------------------
    $stmt = Conexion::conectar()->prepare(
        "INSERT
            INTO `citas` (
                `horario_id`,
                `paciente_id`,
                `fecha_seleccionada`,
                `no_confirmacion`,
                `tipo_pago`,
                `pagado`,
                `costo`,
                `tipo_prueba`
            )

            VALUES (
                (SELECT id FROM horarios_citas WHERE hora_inicial = :hora_inicial LIMIT 1),
                (SELECT id FROM pacientes WHERE pacientes.user_id = (
                    SELECT id FROM users WHERE users.email = :paciente_email LIMIT 1
                ) LIMIT 1),
                :fecha_seleccionada,
                :no_confirmacion,
                1,
                0,
                (SELECT precio FROM analisis WHERE clave = :clave_prueba LIMIT 1),
                :tipo_prueba
            )"
    );

    $hora_inicial = explode(" ", $request->time->start_datetime)[1];
    $clave_prueba = $analysis_type[$request->test];
    $tipo_prueba = $request->test == "pcr"? "1" : "2";

    $stmt->bindParam(":hora_inicial", $hora_inicial, PDO::PARAM_STR);
    $stmt->bindParam(":paciente_email", $patient->email, PDO::PARAM_STR);
    $stmt->bindParam(":fecha_seleccionada", $request->time->date, PDO::PARAM_STR);
    $stmt->bindParam(":no_confirmacion", $no_confirmacion, PDO::PARAM_STR);
    $stmt->bindParam(":clave_prueba", $clave_prueba, PDO::PARAM_STR);
    $stmt->bindParam(":tipo_prueba", $tipo_prueba, PDO::PARAM_STR);

    if (!$stmt->execute()) {
        exit(json_encode([
            "success"   => false,
            "msg"       => "Ocurrió un error, inténtelo más tarde por favor.",
        ]));
    }
}


$stmt = Conexion::conectar()->prepare(
    "SELECT * FROM analisis WHERE clave = :clave_prueba LIMIT 1"
);

$clave_prueba = $analysis_type[$request->test];

$stmt->bindParam(":clave_prueba", $clave_prueba, PDO::PARAM_STR);
$stmt->execute();

$analysis = $stmt->fetch();


$texto_nombre = $analysis['nombre'];
$precio = $analysis['precio'];
$num_pacientes = count($request->patients);
$precio_tot = $precio * $num_pacientes;

$discount   = $precio_tot * 8/100;

$precio_tot = $precio_tot - $discount;

$horainicial = $request->time->start_time;
$horafinal = $request->time->end_time;


exit(json_encode([
    "success"               => true,
    "url"                   => "/pp/solicitud_pago_mostrar.php?confirmacion=$no_confirmacion&email=&fecha=".explode(" ", $request->time->start_datetime)[1]."&tipo=$texto_nombre&precio=$precio&total=$precio_tot&numpacientes=$num_pacientes&horainicial=$horainicial&horafinal=$horafinal",
    "confirmation_number"   => $no_confirmacion
]));
