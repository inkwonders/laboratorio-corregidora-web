<?php

    date_default_timezone_set('America/Mexico');

    error_reporting(E_ALL);
    ini_set('display_errors', '1');

?>

<!doctype html>
<html lang="es">
<head>

<!-- <meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" /> -->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<title>LABORATORIO CORREGIDORA | TEST COVID</title>

<meta name="description" content="Laboratorio de Análisis clínicos en Querétaro">
<meta name="robots" content="all" />
<meta name="laboratorio corregidora" content="laboratoriocorregidora.com.mx" />
<meta name="distribution" content="global"/>
<meta name="keywords" content="Mejor laboratorio en Querétaro
Laboratorios en Querétaro
Laboratorios Corregidora
Corregidora
Juriquilla Laboratorio
Milenio Juriquilla
Pueblo Nuevo Laboratorio
Jurica Laboratorio
Tecnológico Laboratorio
Mejor laboratorio de Querétaro
Laboratorio de análisis clínicos Querétaro
Resultados laboratorio corregidora
Laboratorio acreditado Querétaro
Mejor laboratorio de Querétaro
Análisis Laboratorio Corregidora
Costos Laboratorio Corregidora
Laboratorios Corregidora
Laboratorios Querétaro certificado
Laboratorios Querétaro acreditado
Laboratorio especialista en toma de muestras difíciles
Laboratorio Querétaro alta tecnología
Laboratorio con amplio menú de pruebas
Laboratorio con más años de experiencia
Laboratorio confiable
Resultados exactos y precisos laboratorio
Laboratorios Vázquez Mellado
Toma de muestras a domicilio laboratorio
Laboratorio con servicio fines de semana
Laboratorio con personal competente
Laboratorio con servicio los sábados y domingos
Sucursales laboratorio corregidora
Costos análisis laboratorio corregidora
Laboratorios en Querétaro ISO
Laboratorio corregidora sucursales
Laboratorio con costos accesibles en Querétaro
Laboratorio con costos económicos en Querétaro
Laboratorio con costos bajos en Querétaro
Laboratorio entrega rápido resultados
Laboratorio resultados confiables
Laboratorio entrega resultados el mismo día
Laboratorio con mayor prestigio en Querétaro
Laboratorio más recomendado en Querétaro
Laboratorio con el mejor control de calidad
Laboratorio de análisis clínicos con mayor calidad
Laboratorio de calidad
Laboratorio con excelencia en los resultados
Laboratorio con resultados confiables
Laboratorio personal especialista en toma de muestra pediátrica
Análisis Clínicos
Análisis de sangre
Análisis de orina
Análisis generales
Análisis para infección
Análisis de laboratorio
Análisis  de VIH
Análisis de prueba de embarazo
Menú de análisis de laboratorio
Menú de análisis clínicos
Análisis clínicos
Análisis check up" />

<link href='../js/fullcalendar-4.4.0/packages/core/main.css' rel='stylesheet' />
<link href='../js/fullcalendar-4.4.0/packages/daygrid/main.css' rel='stylesheet' />
<link href='../js/fullcalendar-4.4.0/packages/interaction/main.css' rel='stylesheet' />
<link href='../js/fullcalendar-4.4.0/packages/timegrid/main.css' rel='stylesheet' />

<link rel="stylesheet" href="../estilo.css" />
<link rel="stylesheet" href="../estilo_add.css" />

<link rel="stylesheet" type="text/css" href="../slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="../slick/slick-theme.css"/>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
<link rel="manifest" href="../img/favicon/site.webmanifest">
<link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#cbf202">
<meta name="msapplication-TileColor" content="#000000">
<meta name="theme-color" content="#ffffff">
<!-- favicon -->

<!-- <meta name="Description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/>
<meta property="og:url" content="https://almecmexico.com"/>
<meta property="og:title" content="ALMEC"/>
<meta property="og:image" content="https://almecmexico.com/img/almec-share.jpg"/>
<meta property="og:description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/> -->

<!-- Start of inkwonders Zendesk Widget script
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=e2bbc2bc-7b8b-478b-98a7-dcf05c450748"> </script>
End of inkwonders Zendesk Widget script -->

<!-- Start of labcorregidorachat Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=a4e70b89-6ec3-4ab7-b3d9-447e3d21424a"> </script>
<!-- End of labcorregidorachat Zendesk Widget script -->

<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-691396176');
</script> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-152013601-2');
</script> -->
<!-- end Global site tag (gtag.js) - Google Analytics -->

</head>
<body>
<body>

    <!--<header id="header">
        <div id="menu">
            <div class="menu-contender">
            <ul class="menu-nav" id="menu-slide">
                <li class="logo_menu"><a href="index.php"><img class="logo-head" src="img/svg/logo-nuevo.svg" id="logo-ss" /></a></li>
                <li id="bServicios" class="menu-el menu-serv" op='servicios'><span>SERVICIOS</span></li>
                <li id="bPreguntas" class="menu-el menu-preg" op='preguntas'><span>FAQ’s</span></li>
                <li id="bSucursal" class="menu-el menu-suc" op='sucursales'><span>SUCURSALES</span></li>
            </ul>
            </div>
            <div id="boton-res">
            <div class="inter-res">
                QUIERO VER<br />
                MIS RESULTADOS
            </div>
            </div>
        </div>

        </header>-->

        <header>

            <div id="menu">

            <div class="cont_logo">
                <a href="index.php"><img class="logo-head" src="../img/svg/logo-nuevo.svg" id="logo-ss" /></a>
            </div>

            <ul class="menu-nav" id="menu-slide">
                <li id="bServicios" class="menu-el menu-serv" op='servicios'><span>SERVICIOS</span></li>
                <li id="analisis" class="menu-el menu-preg" op='analisis'> <a href="../analisis.php"><span>ANÁLISIS</span></a></li>
                <li id="bPreguntas" class="menu-el menu-preg" op='preguntas'><span>FAQ’s</span></li>
                <li id="bSucursal" class="menu-el menu-suc" op='sucursales'><span>SUCURSALES</span></li>
            </ul>

            <div class="cont_opciones_ext">

                <div class="opcion_ext">
                    <a class="no_padding" href="https://www.facebook.com/Laboratorios-Corregidora-108767437596443" target="_blank"><img class="img_menu" src="../img/svg/fa.svg" alt="facebook"></a>
                    <a class="no_padding" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank"><img class="img_menu" src="../img/svg/em.svg" alt="email"></a>
                    <a class="no_padding no_link_menu" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank" style="justify-content: space-around;display: flex;align-items: center;"><img class="img_menu" src="../img/svg/wa.svg" alt="whatsapp">
                    <span style="padding-left: 5px;">442 120 62 15</span></a>
                </div>

                <div class="opcion_ext">
                    <img class="img_menu img_tel" src="../img/svg/tel.svg" alt="teléfono">
                    <div class="cont_telefonos_menu">
                        <a href="tel:4421201052" class="no_link_menu">442 212 10 52</a>
                        <!-- <a href="tel:4421201901" class="no_link_menu">442 212 19 01</a> -->
                    </div>
                </div>

                <div class="opcion_ext">
                <a href="covid.php" class="no_link_menu" ><p class="opc_menu_desktop op_covid" op="covid">INFORMACIÓN <br>COVID-19</p></a>
                </div>

                <div class="opcion_ext">
                <a href="../guardias.php" class="no_link_menu"><p class="opc_menu_desktop op_guardias" op="guardias">SERVICIOS <br>DE GUARDIAS</p></a>
                </div>

                <div id="boton-res">
                <img class="img_pdf" src="../img/svg/pdf.svg" alt="">
                <div class="inter-res">
                    QUIERO VER<br />
                    MIS RESULTADOS
                </div>
                </div>

            </div>



            </div>

        </header>

        <div class="menu-bar">
        <div class="container" onclick="myFunction(this)" id="container">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
        <a href="index.php"><img src="../img/svg/logo-nuevo.svg" id="logo-movil" class="logo-mov-g"></a>
        </div>
        <div id="contenidoMenu" style="display: none;">
            <div class="menu-conten">
            <span class="menu-el" id="btn-proy-mov" op="serv-mov">SERVICIOS</span><br><br>
            <span class="menu-el" id="btn-cons-mov" op="analisis-mov"><a style="color: #194271;" href="../analisis.php"> ANÁLISIS </a></span><br><br>
            <span class="menu-el" id="btn-cons-mov" op="preg-mov">FAQ’s</span><br><br>
            <span class="menu-el" id="btn-acab-mov" op="vid-mov">SUCURSALES</span><br><br>
            <a href="covid.php" class="no_link_menu" ><span class="menu-gua opc_menu_desktop op_covid" id="bGuardias" op="covid">INFORMACIÓN COVID-19</span></a><br><br>
            <a href="guardias.php" class="no_link_menu"><span class="menu-gua opc_menu_desktop op_guardias" id="bGuardias" op="guardias">GUARDIAS</span></a><br><br>
            <span class="menu-res" id="btn-resl-mov">QUIERO VER MIS RESULTADOS</span><br><br><br>
            <span class="menu-el" id="btn-tel-mov"><a href="tel:442 212 1052" class="menu-tel" style="margin-right:5px; color: #194271;">442 212 1052</a>  </span><br><br>
            <!--<img src="img/icon-usa.svg" class="icon-li" id="btn-en"/>
            <img src="img/icon-mex.svg" class="icon-li" id="btn-es"/>-->
            <div class="opcion_ext_movil">
                    <a class="no_padding" href="https://www.facebook.com/Laboratorios-Corregidora-108767437596443" target="_blank"><img class="img_menu_movil" src="../img/svg/fa.svg" alt="facebook"></a>
                    <a class="no_padding" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank"><img class="img_menu_movil" src="../img/svg/em.svg" alt="email"></a>
                    <a class="no_padding" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank"><img class="img_menu_movil" src="../img/svg/wa.svg" alt="whatsapp"></a>
            </div>
            </div>
        </div>

<!-- //aqui -->
    <div class="cont_testcovid">

        <div class="cont_interno_testcovid">

            <div class="cont_pasos">

                <div class="paso">
                    <p class="txt_paso">1/ TEST</p>
                </div>

                <div class="paso paso_activo">
                    <p class="txt_paso">2/ CITA</p>
                </div>

                <div class="paso">
                    <p class="txt_paso">3/ PAGO</p>
                </div>

            </div>

            <div class="cont_titulo_testcovid">

                <p class="titulo_testcovid">PRUEBA DE PCR PARA COVID-19 (SARS-Cov-2) <br>
                    EN MUESTRA NASO FARINGEA
                </p>

            </div>

            <div class="cont_datosgenerales_paciente">

                <p class="titulo_testcovid">CALENDARIO</p>

                <div class="cont_interno_form_datosgenerales_paciente">
                    <div class="calendario_cont">
                        <div id="calendario_secc" class="calendario_secc"></div>

                        <input type='hidden'name='hora_seleccionada' value='' id="hora_seleccionada"> <!-- hora seleccionada input hidden -->

                        <div class="horarios_calendario">
<!--
                            <div class="parte_reservacion">
                                <div class="cont_head">
                                    <span class="titulo_testcovid t_horario">Mañana</span>
                                    <span class="subtitulo_testcovid margen_tiempo">07:30 - 09:30 am</span>
                                </div>
                                <div class="seleccion_horas manana">
                                    <span name="1" context="seleccion_hora">07:30 am</span>
                                    <span name="2" context="seleccion_hora">08:00 am</span>
                                    <span name="3" context="seleccion_hora">08:30 am</span>
                                    <span name="4" context="seleccion_hora">09:00 am</span>
                                    <span name="5" context="seleccion_hora">09:30 am</span>
                                    <span name="6" context="seleccion_hora" disabled>10:00 am</span>
                                    <span name="7" context="seleccion_hora" disabled>10:30 am</span>
                                    <span name="8" context="seleccion_hora" disabled>11:00 am</span>
                                </div>
                            </div>
                            <div class="parte_reservacion">
                                <div class="cont_head">
                                    <span class="titulo_testcovid t_horario">Tarde</span>
                                    <span class="subtitulo_testcovid margen_tiempo">04:00 - 05:00 pm</span>
                                </div>
                                <div class="seleccion_horas tarde">
                                    <span name="1" context="seleccion_hora">04:00 am</span>
                                    <span name="2" context="seleccion_hora">04:30 am</span>
                                    <span name="3" context="seleccion_hora">05:00 am</span>
                                    <span name="4" context="seleccion_hora" disabled>05:30 am</span>
                                    <span name="5" context="seleccion_hora" disabled>06:00 am</span>
                                    <span name="6" context="seleccion_hora" disabled>06:30 am</span>
                                    <span name="7" context="seleccion_hora" disabled>07:00 am</span>
                                    <span name="8" context="seleccion_hora" disabled>07:30 am</span>
                                </div>
                            </div> -->

                        </div>

                        <div class="cont_centrado oculto boton_continuar_cal">
                            <button type="button" name="continuar">Continuar</button>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <div class="div-footer">
      <div class="footer-cell">
        <a href="../pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de Privacidad</a>   &nbsp;   /  &nbsp;  <a href="../pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a>  &nbsp;    /  &nbsp;   <a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a>   &nbsp;  /    &nbsp;  <span class="menu-el" op="sucursales" style="cursor: pointer; ">Contáctanos</span>
      </div>
      <div class="footer-cell">
        <p style="text-align: center;">Permiso de Publicidad: 203301201A0872</p>
      </div>
      <div class="footer-cell" style="text-align:right">
        <a class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a>
      </div>
    </div>
    <div class="footer-mov">
      <div class="cont_footer_mov">
        <p><a href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de privacidad</a></p>
        <p><a href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a></p>
        <p><a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a></p>
        <p><span class="menu-el link-a" op="vid-mov" style="font-family: m-regular!important; font-size: 16px!important; cursor: pointer!important;">Contáctanos</span></p>
        <p style="text-align: center;">Permiso de Publicidad: 203301201A0872</p>
        <p><a class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a></p>
      </div>
    </div>

  <script type="text/javascript" src="../js/jquery.js"></script>


  <script src='../js/fullcalendar-4.4.0/packages/core/main.js'></script>
  <script src='../js/fullcalendar-4.4.0/packages/daygrid/main.js'></script>
  <script src='../js/fullcalendar-4.4.0/packages/interaction/main.js'></script>
  <script src='../js/fullcalendar-4.4.0/packages/timegrid/main.js'></script>
  <script src='../js/fullcalendar-4.4.0/packages/core/locales/es.js'></script>



  <script type="text/javascript">


    var idioma="es";
    todo_dia = 'Día ent.';
    textos_botones = {
        today: 'Hoy',
        month: 'Mes',
        week: 'Semana',
        day: 'Día',
        list: 'Lista'
    };

    texto_ev_lim = 'más';

    $(".seleccion_horas span").click(function() {
      if(!$(this).attr("disabled")) {
          let context = $(this).attr("context");
          $('.seleccion_horas span[context="'+context+'"]').removeAttr("select");
          $(this).attr("select", "select");
      }
    });

    document.addEventListener('DOMContentLoaded', function(){

        var calendarEl = document.getElementById('calendario_secc');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            locale: idioma,
            allDayText: todo_dia,
            plugins: ['interaction', 'dayGrid', 'timeGrid'],
            defaultDate: '<?php echo date("Y-m-d"); ?>',
            defaultView: 'dayGridMonth',
            selectable: true,
            visibleRange: {
                start: '<?php echo date("Y-m-d"); ?>'
            },
            header: {
                left: 'title',
                center: '',
                right: 'prev,today,next'
            },
            //editable: false,
            /*timeFormat: {
              agenda: 'H(:mm)' //h:mm{ - h:mm}'
            },*/
            titleFormat: {
                month: 'short',
                year: 'numeric',
                day: 'numeric'
            },
            columnHeaderFormat: {
                weekday: 'short',
                day: 'numeric',
                omitCommas: true
            },
            slotLabelFormat:{
                hour: '2-digit',
                minute: '2-digit',
                hour12: true
            },//se visualizara de esta manera 01:00 AM en la columna de horas
            eventTimeFormat:{
                hour: '2-digit',
                minute: '2-digit',
                hour12: true
            },
            buttonText: textos_botones,
            height: 'parent',
            //contentHeight: 'auto',
            //eventLimit: false,
            eventLimitText: texto_ev_lim,
            eventMouseEnter: function(info){
                var eventObj = info.event;

                console.log('Event: ' + info.event.title);
                console.log('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
                console.log('View: ' + info.view.type);
            },
            dragScroll: false,
            nowIndicator: true,
            events: 'vistas/cargatabla.php',
            eventRender: function(info) {
                  // $("td[data-date]").click(function(){
                  //     let dia_seleccionado = $(this).attr("data-date");
                  //     alert(dia_seleccionado);
                  // });
                //var tooltip = new Tooltip(info.el, {
                  //title: info.event.extendedProps.description,
                  //placement: 'top',
                  //trigger: 'hover',
                  //container: 'body'
                //});
                //var tit = info.getElementsByClassName("fc-title").text;
                //info.getElementsByClassName("fc-title").setAttribute("data-tooltip",tit);
                //$(info.el).tooltip({
                  //content: "Awesome title!",
                  //position: 'left'
                //});
            },
            dateClick: function(info) {
                obtener_horario(info.dateStr);
            },
            // select: function(info) {
            //     alert('selected ' + info.startStr + ' to ' + info.endStr);
            // },
            eventClick: function(info) {
            /*alert('Event: ' + info.event.title);
            alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
            alert('View: ' + info.view.type);*/

            // id_evento = info.event.id;
            // alert(id_evento);
              // clave = info.event.extendedProps['clave'];
              // colegio = info.event.extendedProps['colegio'];
              // hash = info.event.extendedProps['hash'];

                if(info.event.allDay == true){

                }else{

                }


            // console.log(info.view.type);
            // console.log(info.event.id);
            // console.log(info.event.title);
            // console.log(info.event.start);
            //
            // console.log(info.event.allDay);
            // console.log(info.event.extendedProps['hourStart']);
            // console.log(info.event.extendedProps['hourEnded']);
            // console.log(info.event.extendedProps['hash']);



            // change the border color just for fun
            //info.el.style.borderColor = 'red';
            },
            eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

               /*alert(
                   event.title + " was moved " +
                   dayDelta + " days and " +
                   minuteDelta + " minutes."
               );

               if (allDay) {
                   alert("Event is now all-day");
               }else{
                   alert("Event has a time-of-day");
               }

               if (!confirm("Are you sure about this change?")) {
                   revertFunc();
               }*/

                alert("se movio el evento");

            }
        });

        calendar.render();
        // $("td[data-date]").click(function(){
        //     let dia_seleccionado = $(this).attr("data-date");
        //     alert(dia_seleccionado);
        // });
    });

    function obtener_horario(fecha) {
        $.ajax({
            url:"ajax/horario.ajax.php",
            type:"POST",
            data:{'fecha': fecha},
            beforeSend: function(){
            },
            success: function(response){
                if (!document.querySelector("[data-date='"+fecha+"'].fc-other-month") && !document.querySelector("[data-date='"+fecha+"'].fc-past")) {
                    $(".horarios_calendario").html(response);
                    $(".seleccionado").removeClass("seleccionado");
                    // let seleccion = $("[data-date='"+fecha+"'] span");
                    // seleccion.addClass("seleccionado");
                    $("#estilo_seleccion").html("[data-date='"+fecha+"'] span { background-color: #007af6 !important; color: #fff !important; }");
                    $(".seleccion_horas span").click(function() {
                        if(!$(this).attr("disabled")) {
                            let context = $(this).attr("context");
                            $('.seleccion_horas span[context="'+context+'"]').removeAttr("select");
                            $(this).attr("select", "select");
                        }
                    });
                    // alert(1);
                }
            }
        });
    }

    $(document).on("click","span[selectbox]",function() {
      let context = $(this).attr("context");
      $('span[context="'+context+'"]').attr("selectbox", "false");
      $(this).attr("selectbox", "true");
    });

    $(document).on("click","span[checkbox]",function() {
      $(this).attr("checkbox", ($(this).attr("checkbox") == "true"? "false" : "true"));
    });


    $(".pdf_boton").click(function(){
      // window.open("pdf/comunicado.pdf","_blank");
      window.open("pdf/rt-pcr_para_la_deteccion_de_covid-19.pdf","_blank");
    });


    $(document).on("click",".seleccion_hora",function(){
      console.log("click");
      let hora = $(this).attr("value");
      $("#hora_seleccionada").val(hora);
      $(".boton_continuar_cal").removeClass("oculto");
    });

  </script>
  <style id="estilo_seleccion" media="screen"></style>
  <style media="screen">
      .fc-other-month span {
          background-color: transparent !important;
          color: #194271 !important;
      }
      /* .fc-past span {
          background-color: transparent !important;
          color: #194271 !important;
      } */
      .fc-day-top.fc-past {
          opacity: 0.3;
      }
</style>
</body>
</html>
