<div class="cont_testcovid no_visible" id="cont_cita">

    <div class="cont_interno_testcovid">

        <div class="cont_pasos2">

            <div class="paso paso_click">
                <p class="txt_paso">1/ TIPO PRUEBA</p>
            </div>

            <div class="paso paso_activo">
                <div class="circulo sp">
                    <span class="sp --i:1"></span>
                    <span class="sp --i:2"></span>
                </div>
                <p class="txt_paso">2/ TEST</p>
            </div>

            <div class="paso">
                <p class="txt_paso">3/ CONFIRMACION</p>
            </div>

        </div>

        <div class="cont_titulo_testcovid">

            <p class="titulo_testcovid nombre_tipo_prueba">
            </p>

        </div>

        <div class="cont_datosgenerales_paciente">

            <p class="titulo_testcovid">CALENDARIO</p>

            <div class="cont_interno_form_datosgenerales_paciente">
                <div class="calendario_cont">
                    <div id="calendario_secc" class="calendario_secc"></div>

                    <input type='hidden'name='hora_seleccionada' value='' hora_final="" id="hora_seleccionada"> <!-- hora seleccionada input hidden -->
                    <input type="hidden" name="dia_seleccionado" value="" id="dia_seleccionado">
                    <div class="horarios_calendario">
                    </div>

                    <div class="cont_centrado oculto boton_continuar_cal">
                        <button type="button" name="continuar" id="continuar">Continuar</button>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


<script type="text/javascript" src="../js/jquery.js"></script>


<script src='vistas/assets/plugins/fullcalendar-4.4.0/packages/core/main.js'></script>
<script src='vistas/assets/plugins/fullcalendar-4.4.0/packages/daygrid/main.js'></script>
<script src='vistas/assets/plugins/fullcalendar-4.4.0/packages/interaction/main.js'></script>
<script src='vistas/assets/plugins/fullcalendar-4.4.0/packages/timegrid/main.js'></script>
<script src='vistas/assets/plugins/fullcalendar-4.4.0/packages/core/locales/es.js'></script>



<script type="text/javascript">


var idioma="es";
todo_dia = 'Día ent.';
textos_botones = {
    today: 'Hoy',
    month: 'Mes',
    week: 'Semana',
    day: 'Día',
    list: 'Lista'
};

texto_ev_lim = 'más';

$(".seleccion_horas span").click(function() {
  if(!$(this).attr("disabled")) {
      let context = $(this).attr("context");
      $('.seleccion_horas span[context="'+context+'"]').removeAttr("select");
      $(this).attr("select", "select");
  }
});

// document.addEventListener('DOMContentLoaded', function(){

    var calendarEl = document.getElementById('calendario_secc');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        locale: idioma,
        allDayText: todo_dia,
        plugins: ['interaction', 'dayGrid', 'timeGrid'],
        defaultDate: '<?php echo date("Y-m-d"); ?>',
        defaultView: 'dayGridMonth',
        selectable: true,
        header: {
            left: 'title',
            center: '',
            right: 'prev,today,next'
        },
        //editable: false,
        /*timeFormat: {
          agenda: 'H(:mm)' //h:mm{ - h:mm}'
        },*/
        titleFormat: {
            month: 'short',
            year: 'numeric',
            day: 'numeric'
        },
        columnHeaderFormat: {
            weekday: 'short',
            day: 'numeric',
            omitCommas: true
        },
        slotLabelFormat:{
            hour: '2-digit',
            minute: '2-digit',
            hour12: true
        },//se visualizara de esta manera 01:00 AM en la columna de horas
        eventTimeFormat:{
            hour: '2-digit',
            minute: '2-digit',
            hour12: true
        },
        buttonText: textos_botones,
        height: 'parent',
        //contentHeight: 'auto',
        //eventLimit: false,
        eventLimitText: texto_ev_lim,
        eventMouseEnter: function(info){
            var eventObj = info.event;

            console.log('Event: ' + info.event.title);
            console.log('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
            console.log('View: ' + info.view.type);
        },
        dragScroll: false,
        nowIndicator: true,
        events: 'vistas/cargatabla.php',
        eventRender: function(info) {
              // $("td[data-date]").click(function(){
              //     let dia_seleccionado = $(this).attr("data-date");
              //     alert(dia_seleccionado);
              // });
            //var tooltip = new Tooltip(info.el, {
              //title: info.event.extendedProps.description,
              //placement: 'top',
              //trigger: 'hover',
              //container: 'body'
            //});
            //var tit = info.getElementsByClassName("fc-title").text;
            //info.getElementsByClassName("fc-title").setAttribute("data-tooltip",tit);
            //$(info.el).tooltip({
              //content: "Awesome title!",
              //position: 'left'
            //});
        },
        dateClick: function(info) {
            obtener_horario(info.dateStr);
            $("#dia_seleccionado").val(""+info.dateStr);
        },
        // select: function(info) {
        //     alert('selected ' + info.startStr + ' to ' + info.endStr);
        // },
        eventClick: function(info) {
        /*alert('Event: ' + info.event.title);
        alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
        alert('View: ' + info.view.type);*/

        // id_evento = info.event.id;
        // alert(id_evento);
          // clave = info.event.extendedProps['clave'];
          // colegio = info.event.extendedProps['colegio'];
          // hash = info.event.extendedProps['hash'];

            if(info.event.allDay == true){

            }else{

            }


        // console.log(info.view.type);
        // console.log(info.event.id);
        // console.log(info.event.title);
        // console.log(info.event.start);
        //
        // console.log(info.event.allDay);
        // console.log(info.event.extendedProps['hourStart']);
        // console.log(info.event.extendedProps['hourEnded']);
        // console.log(info.event.extendedProps['hash']);



        // change the border color just for fun
        //info.el.style.borderColor = 'red';
        },
        eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

           /*alert(
               event.title + " was moved " +
               dayDelta + " days and " +
               minuteDelta + " minutes."
           );

           if (allDay) {
               alert("Event is now all-day");
           }else{
               alert("Event has a time-of-day");
           }

           if (!confirm("Are you sure about this change?")) {
               revertFunc();
           }*/

            alert("se movio el evento");

        }
    });

    calendar.render();
    // $("td[data-date]").click(function(){
    //     let dia_seleccionado = $(this).attr("data-date");
    //     alert(dia_seleccionado);
    // });
// });

function obtener_horario(fecha) {
    $.ajax({
        url:"ajax/horario.ajax.php",
        type:"POST",
        data:{'fecha': fecha},
        beforeSend: function(){
        },
        success: function(response){
          if (!document.querySelector("[data-date='"+fecha+"'].fc-other-month") && !document.querySelector("[data-date='"+fecha+"'].fc-past")) {
              $(".horarios_calendario").html(response);
              $(".seleccionado").removeClass("seleccionado");
              // let seleccion = $("[data-date='"+fecha+"'] span");
              // seleccion.addClass("seleccionado");
              $("#estilo_seleccion").html("[data-date='"+fecha+"'] span { background-color: #007af6 !important; color: #fff !important; }");
              $(".seleccion_horas span").click(function() {
                  if(!$(this).attr("disabled")) {
                      let context = $(this).attr("context");
                      $('.seleccion_horas span[context="'+context+'"]').removeAttr("select");
                      $(this).attr("select", "select");
                  }
              });
              // alert(1);
          }
        }
    });
}

$(document).on("click","span[selectbox]",function() {
  let context = $(this).attr("context");
  $('span[context="'+context+'"]').attr("selectbox", "false");
  $(this).attr("selectbox", "true");
});

$(document).on("click","span[checkbox]",function() {
  $(this).attr("checkbox", ($(this).attr("checkbox") == "true"? "false" : "true"));
});


$(".pdf_boton").click(function(){
  // window.open("pdf/comunicado.pdf","_blank");
  window.open("pdf/rt-pcr_para_la_deteccion_de_covid-19.pdf","_blank");
});


$(document).on("click",".seleccion_hora",function(){
  console.log("click "+$(this).attr("hora_final"));
  let hora = $(this).attr("value");
  let hora_final= $(this).attr("hora_final");
  $("#hora_seleccionada").val(hora);
  $("#hora_seleccionada").attr("hora_final",hora_final);
  $(".boton_continuar_cal").removeClass("oculto");
});

</script>
<style id="estilo_seleccion" media="screen"></style>
<style media="screen">
  .fc-other-month span {
      background-color: transparent !important;
      color: #194271 !important;
  }
  .fc-day-top.fc-past {
      opacity: 0.3;
  }
</style>
