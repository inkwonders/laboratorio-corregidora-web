<?php

    $confirmacion = $_POST["confirmacion"];

    $fecha = $_POST["fecha"];

    $pacientes = $_REQUEST['pacientes'];

    $precio = $_REQUEST['precio'];

    $email = $_REQUEST['email'];

    $hora_inicial = $_REQUEST['hora_formato'];

    $hora_final = $_REQUEST['hora_final'];



    $obj_paciente = json_decode($_REQUEST['pacientes']);
    $precio = $_REQUEST['precio'];

    $num_pacientes = sizeof($obj_paciente);
    $total = ((int) $num_pacientes * (int)$precio);

?>

<div class="cont_testcovid" id="cont_test">

    <div class="cont_interno_testcovid">

        <div class="cont_pasos2">

            <div class="paso btn_vistas" op="test">

                <p class="txt_paso">1/ TEST</p>

            </div>

            <div class="paso btn_vistas" op="cita" id="btn_activar">
                <p class="txt_paso">2/ CITA</p>
            </div>

            <div class="paso paso_activo btn_vistas" op="pago">
                 <div class="circulo sp">
                    <span class="sp --i:1"></span>
                    <span class="sp --i:2"></span>
                </div>
                <p class="txt_paso">3/ CONFIRMACIÓN</p>
            </div>

        </div>

        <div class="cont_forma_pago">

            <p class="titulo_testcovid">
                Paga tu prueba en línea y obtén un 8% de descuento <br> <span style="font-size: 12px;">* No aceptamos pagos con tarjetas American Express.</span>
            </p>
            <br>
            <p class="pregunta">Recuerda que hasta no realizar el pago y obtener tu número de confirmación tu cita no será agendada</p>


        </div>

        <form id="form_tipo_pago" method="POST">

            <div class="opciones_genero">
                <span class="label_input cont_selectbox">
                    <span>Estoy de acuerdo</span>
                    <span selectbox="true" value="si" context="pago"></span>
                </span>
                <!-- <span class="label_input cont_selectbox">
                    <span>Pago al momento de la toma</span>
                    <span selectbox="false" value="no" context="pago"></span>
                </span> -->

                <input id="valor_pago" type="hidden" name="pago" value="si">
                <input id="no_confirmacion" type="hidden" name="confirmacion" value="<?php echo $confirmacion; ?>">
                <input id="email_paciente" type="hidden" name="email" value="<?php echo $email; ?>">
                <input id="fecha_cita" type="hidden" name="fecha" value="<?php echo $fecha; ?>">
                <input id="precio" type="hidden" name="precio" value="<?php echo $precio; ?>">
                <input id="num_pacientes"type="hidden" name="num_pacientes" value="<?php echo $num_pacientes; ?>">
                <input id="total"type="hidden" name="total" value="<?php echo $total; ?>">

                <input id="hora_inicial"type="hidden" name="hora_inicial" value="<?php echo $hora_inicial; ?>">
                <input id="hora_final"type="hidden" name="hora_final" value="<?php echo $hora_final; ?>">

            </div>

            <br>

            <div class="centrado">
                <button type="button" id="boton_siguiente_pago">IR AL PAGO</button>

            </div>


        </form>



    </div>

</div>
