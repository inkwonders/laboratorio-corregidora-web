<?php
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado

    function asset($filename, $key = "file") {
        $manifest_path = '../dist/.vite/manifest.json';

        if (file_exists($manifest_path)) {
            $manifest = json_decode(file_get_contents($manifest_path), TRUE);
        } else {
            $manifest = [];
        }

        $route = "";
        if (array_key_exists($filename, $manifest)) {
            $result = ($manifest[$filename][$key]);
            if (gettype($result) == "string") {
                $route = "../dist/$result";
            } else {
                $route = "../dist/".$result[0];
            }
        } else {
            $route = "../dist/".$filename;
        }

        return $route;
    }
?>
<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <title>LABORATORIO CORREGIDORA | TEST COVID</title>

    <meta name="description" content="Laboratorio de Análisis clínicos en Querétaro">
    <meta name="robots" content="all" />
    <meta name="laboratorio corregidora" content="laboratoriocorregidora.com.mx" />
    <meta name="distribution" content="global"/>
    <meta name="keywords" content="Mejor laboratorio en Querétaro
Laboratorios en Querétaro
Laboratorios Corregidora
Corregidora
Juriquilla Laboratorio
Milenio Juriquilla
Pueblo Nuevo Laboratorio
Jurica Laboratorio
Tecnológico Laboratorio
Mejor laboratorio de Querétaro
Laboratorio de análisis clínicos Querétaro
Resultados laboratorio corregidora
Laboratorio acreditado Querétaro
Mejor laboratorio de Querétaro
Análisis Laboratorio Corregidora
Costos Laboratorio Corregidora
Laboratorios Corregidora
Laboratorios Querétaro certificado
Laboratorios Querétaro acreditado
Laboratorio especialista en toma de muestras difíciles
Laboratorio Querétaro alta tecnología
Laboratorio con amplio menú de pruebas
Laboratorio con más años de experiencia
Laboratorio confiable
Resultados exactos y precisos laboratorio
Laboratorios Vázquez Mellado
Toma de muestras a domicilio laboratorio
Laboratorio con servicio fines de semana
Laboratorio con personal competente
Laboratorio con servicio los sábados y domingos
Sucursales laboratorio corregidora
Costos análisis laboratorio corregidora
Laboratorios en Querétaro ISO
Laboratorio corregidora sucursales
Laboratorio con costos accesibles en Querétaro
Laboratorio con costos económicos en Querétaro
Laboratorio con costos bajos en Querétaro
Laboratorio entrega rápido resultados
Laboratorio resultados confiables
Laboratorio entrega resultados el mismo día
Laboratorio con mayor prestigio en Querétaro
Laboratorio más recomendado en Querétaro
Laboratorio con el mejor control de calidad
Laboratorio de análisis clínicos con mayor calidad
Laboratorio de calidad
Laboratorio con excelencia en los resultados
Laboratorio con resultados confiables
Laboratorio personal especialista en toma de muestra pediátrica
Análisis Clínicos
Análisis de sangre
Análisis de orina
Análisis generales
Análisis para infección
Análisis de laboratorio
Análisis  de VIH
Análisis de prueba de embarazo
Menú de análisis de laboratorio
Menú de análisis clínicos
Análisis clínicos
Análisis check up" />

    <link href='vistas/assets/plugins/fullcalendar-4.4.0/packages/core/main.css' rel='stylesheet' />
    <link href='vistas/assets/plugins/fullcalendar-4.4.0/packages/daygrid/main.css' rel='stylesheet' />
    <!-- <link href='vistas/assets/plugins/fullcalendar-4.4.0/packages/interaction/main.css' rel='stylesheet' /> -->
    <link href='vistas/assets/plugins/fullcalendar-4.4.0/packages/timegrid/main.css' rel='stylesheet' />

    <!-- <link rel="stylesheet" href="vistas/assets/css/estilo.css" /> -->
    <!-- <link rel="stylesheet" href="vistas/assets/css/estilo_add.css" /> -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="vistas/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="vistas/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="vistas/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="vistas/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="vistas/assets/favicon/safari-pinned-tab.svg" color="#cbf202">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">

    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

    <link rel="stylesheet" type="text/css" href="<?=asset("modules/app.css"); ?>" />
    <link rel="stylesheet" type="text/css" href="<?=asset("modules/app.js", "css"); ?>" />
</head>
<body>
<body>
    <?php include "vistas/header.php"; ?>

    <div class="w-full max-[980px]:min-h-[calc(100vh-5rem)] bg-[#e5e5e5] min-h-[calc(100vh-6rem)] flex justify-center p-0 lg:p-6">
        <div id="citas" class="w-full max-w-7xl"></div>
    </div>

    <?php include "vistas/footer.php"; ?>

    <script type="text/javascript" src="vistas/assets/js/jquery.js"></script>
    <script type="text/javascript" src="vistas/assets/js/sweat_alert.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="vistas/assets/js/script.js?v=1"></script>
    <script type="module" src="<?=asset("modules/app.js"); ?>"></script>
    <script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/jquery-ui.min.js"></script>
</body>
</html>
