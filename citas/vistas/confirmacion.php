<?php

require_once "../controladores/sintomas.controlador.php";
require_once "../modelos/sintomas.modelo.php";
require_once "../controladores/enfermedades.controlador.php";
require_once "../modelos/enfermedades.modelo.php";
require_once "../controladores/pruebas.controlador.php";
require_once "../modelos/pruebas.modelo.php";

$obj_paciente = json_decode($_REQUEST['obj_paciente']);
$precio = $_REQUEST['precio'];
$tipo_prueba = $_REQUEST['tipo_prueba'];

function mostrar_sintomas($sintomas_paciente){

  $cont_sintomas=0;
  $resultado="";

  foreach (($sintomas_paciente->array_sintomas) as $key => $value) {

    if($cont_sintomas!=0){
        $resultado.=", ";
    }
    $re = ControladorSintomas::ctrConsultaNombreSintoma("'{$value->id}'");
    $resultado.= "<span >".$re[0]."</span>";
    $cont_sintomas++;

  }

  if($cont_sintomas!=0 && $sintomas_paciente->sintoma!=""){
    $resultado.= "<span >, ".$sintomas_paciente->sintoma."</span>";
  }else {
    $resultado.= "<span >".$sintomas_paciente->sintoma."</span>";
  }

  if(sizeof($re) > 1){
    $resultado.= ".";
  }else{
    $resultado = "Sin síntomas.";
  }

  return $resultado;

}

function mostrar_enfermedades($obj_paciente){

  $cont_sintomas=0;
  $resultado="";

  foreach (($obj_paciente->array_cronicas) as $key => $value) {

    if($cont_sintomas>1){
        $resultado.= ", ";
    }
    $re = ControladorEnfermedades::ctrConsultaNombreEnfermedades($value);
    $resultado.= "<span >".$re[0]."</span>";
    $cont_sintomas++;

  }

  if($cont_sintomas!=0 && $obj_paciente->enfermedad!=""){
    $resultado.= "<span >, ".$obj_paciente->enfermedad."</span>";
  }else {
    $resultado.= "<span >".$obj_paciente->enfermedad."</span>";
  }

  if(sizeof($re) > 1){
    $resultado.= ".";
  }else{
    $resultado = "Sin enfermedades.";
  }

  return $resultado;
}

function mostrar_motivos($obj_paciente){

  $cont_sintomas=0;
  $resultado="";

  foreach (($obj_paciente->array_pruebas) as $key => $value) {

    if($cont_sintomas!=0){
      $resultado.= ", ";
    }

    $re = ControladorPruebas::ctrConsultaNombrePruebas($value);
    $resultado.= "<span > ".$re[0]."</span>";
    $cont_sintomas++;

  }

  if($cont_sintomas!=0 && $obj_paciente->prueba!=""){
    $resultado.= "<span >, ".$obj_paciente->prueba."</span>";
  }else {
    $resultado.= "<span >".$obj_paciente->prueba."</span>";
  }

  if(sizeof($re) > 1){
    $resultado.= ".";
  }else{
    $resultado = "Sin motivos.";
  }

  return $resultado;

}

$num_pacientes = sizeof($obj_paciente);
$total = $precio * (int)$num_pacientes;

?>

<style media="screen">
    .div-footer {
        top: 0;
    }
    .mensaje_contenido {
        overflow-y: hidden;
        height: auto;
        min-height: inherit;
    }
    span {
        overflow-wrap: anywhere;
    }
    .mensaje_estr {
        padding-top: 90px;
    }
    html, body {
        height: auto;
        min-height: 100%;
    }
    .conten_texto {
        min-height: calc(100vh - 277px);
        justify-content: center;
        font-size: 18px;
    }
    .cont_interno_msg {
        padding: 0;
    }
    .contenedor_principal_cita {
        padding-top: 100px;
    }
    #boton_siguiente_test {
        width: 230px;
    }
    @media (max-width:980px){
        .footer-mov {
            margin-top: 0;
        }
    }
    .centrado_cuadro_conf{
      position: relative;
      width: 65%;
    }
    .centrado_boton{
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 35%;
    }
    .cont_top_confirmacion{
    position: relative;
    width: 60%;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    flex-direction: column;
    }
    .boton_pagar_online{
    width: auto;
    height: 60px;
    font-family: m-semibold;
    font-size: 22px;
    color: #fff;
    background-color: #007af6;
    border: 0;
    border-radius: 30px;
    cursor: pointer;
    padding: 0 20px;
    }
    .centrado_cuadro_conf{
    position: relative;
    width: 100%;
    display: flex;
    }
</style>
<section class="mensaje_contenido">

    <div class="cont_interno_msg">
        <div class="mensaje_estr">

            <div class="conten_texto">
                <div class="centrado_cuadro_conf">

                  <div class="cont_top_confirmacion">
                    <span>Usted agendó un cita para el día <i class="fas fa-calendar-alt"></i> <span class="azul_sel bold_c" style="font-size: 24px;"><?php echo $_POST['fecha_formato']; ?></span></span>
                    <span>Tipo prueba: <span class="azul_sel bold_c"><?php  echo $tipo_prueba; ?></span></span>
                    <span>Horario: <i class="far fa-clock"></i> <span class="azul_sel bold_c" style="font-size: 24px;"><?php echo $_POST['hora_formato']." - ".$_POST['hora_final']; ?></span></span>
                    <span class="">Número de Confirmación: <i class="fas fa-barcode"></i> <span class="azul_sel bold_c" style="font-size: 24px;"><?php echo $_POST['confirmacion']; ?></span></span>
                    <span class="">Número de Pacientes: <i class="fas fa-users"></i> <span class="azul_sel bold_c" style="font-size: 24px;"><?php echo $num_pacientes; ?></span></span>
                    <span class="">Total: <span class="azul_sel bold_c" style="font-size: 24px;">$<?php echo number_format($total,2); ?></span></span>
                  </div>

                </div>

                <br><br>

                <section class="ac-container" style="margin-top: 20px;">
                    <?php $pos = 1; ?>
                    <?php foreach($obj_paciente as $paciente): ?>
                    <div>
                        <input id="ac-<?php echo $pos; ?>" name="accordion-1" type="radio" <?php echo $pos == 1 ? 'checked' : '' ?>>
                        <label for="ac-<?php echo $pos; ?>"><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <?php echo $paciente->nombre." ".$paciente->paterno." ".$paciente->materno; ?></span> <span><i class="fas fa-angle-right"></i></span></label>
                        <article>
                            <span class="pad_top total"><span class="azul_sel">Teléfono: </span> <?php  echo $paciente->celular; ?></span>
                            <span class="total"><span class="azul_sel">Email: </span> <?php  echo $paciente->email; ?></span>
                            <span class="total"><span class="azul_sel">Sintomas: </span><?php echo mostrar_sintomas($paciente); ?></span>
                            <span class="total"><span class="azul_sel">Enfermedades crónicas: </span><?php echo mostrar_enfermedades($paciente); ?></span>
                            <span class="total"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?:  </span><?php  echo $paciente->contacto ? "Si" : "No"; ?></span>
                            <span class="total"><span class="azul_sel">¿Está tomando algún antiviral?:  </span><?php  echo $paciente->antiviral ? "Si" : "No"; ?></span>
                            <span class="total"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><?php  echo $paciente->influenza ? "Si" : "No"; ?></span>
                            <span class="total"><span class="azul_sel">Porque se realiza esta prueba: </span><?php  echo mostrar_motivos($paciente); ?></span>
                        </article>
                    </div>
                    <?php $pos++; ?>
                    <?php endforeach; ?>

			          </section>

                <section>
                <br><br>
                  <span class="pad_top"><span class="azul_sel" style="font-size: 24px;">Dirección y link de ubicación de la sucursal a la que deben acudir para realizar la prueba: </span> Avenida Prolongación Constituyentes No. 32 Oriente Calle Avenida Marqués de la Villa del Villar del Aguila, Altos del Marques, 76140 Santiago de Querétaro, Qro. <br><br> <a href="https://goo.gl/maps/ibT9atD4xPkXeAxy8" target="_blank" style="font-size: 24px; color: cadetblue;"> https://goo.gl/maps/ibT9atD4xPkXeAxy8 </a><br><br><br><br></span>

                  <span class="pad_top"><span class="azul_sel" style="font-size: 24px;">Indicaciones de como deben presentarse para realizar la prueba:  </span></span>
                  <ul style='text-align: left;'>
                      <li class="pad_top">PCR: Acudir con ayuno mínimo de 4 horas de alimentos sólidos. No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra, esta prueba se debe de hacer a partir del día 3 en adelante en que el paciente presenta los síntomas o que estuvo en contacto directo con una persona COVID- 19 Positivo.</li>
                      <li class="pad_top">ANTIGENOS: No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra. esta prueba debe de hacerse durante los primeros 7 días de haberse presentado por lo menos dos síntomas sugestivos a COVID-19.</li>
                  </ul>
                  <br><br>
                </section>

                <section class="cont_email_enviar">
                    <input type="text" hidden name="fecha_formato" id="fecha_formato" value="<?php echo $_POST['fecha_formato']; ?>">
                    <input type="text" hidden name="hora_formato" id="hora_formato" value="<?php echo  $_POST['hora_formato']; ?>">
                    <input type="text" hidden name="hora_final" id="hora_final" value="<?php echo $_POST['hora_final']; ?>">
                    <input type="text" hidden name="confirmacion" id="confirmacion" value="<?php echo $_POST['confirmacion']; ?>">
                    <input type="text" hidden name="num_pacientes" id="num_pacientes" value="<?php echo $num_pacientes; ?>">
                    <input type="text" hidden name="total" id="total" value="<?php echo $total; ?>">
                    <input type="text" hidden name="pacientes" id="pacientes" value='<?php var_dump($obj_paciente); ?>'>
                    <input type="text" hidden name="precio" id="precio" value="<?php echo $precio; ?>">
                    <span style="width: 100%; text-align: left;">Ingresa un correo electronico para mandar la información de confirmación de la cita:</span>
                    <input placeholder="Ingresar correo" type="email" class="input_style" name="email_confirmacion" id="email_confirmacion" oninput="valida_email_enviar()" required style="width:100%!important;">
                    <span class="error_correo" style="color:red"></span>
                    <button type="button" class="boton_enviar_correo" disabled>ENVIAR CORREO</button>
                </section>

            </div>

        </div>

    </div>

</section>
