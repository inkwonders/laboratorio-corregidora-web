var obj_paciente = [];
var hora_formato;
var hora_final_formato;
var fecha_formato;
var lista_pacientes = [];

var tipo;
var precio;
var es, en;


var tipo_numero;

$(document).ready(function () {

	$(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function () {
		window.location = "../resultados.php";
	});


	$(document).on("click", "#boton_siguiente_test", function () {
		ir_a('#cont_cita');
		$("#boton_siguiente_test").addClass("no_visible")
		tipo = $(".nombre_tipo_prueba").text();
		tipo_estudio = $(".nombre_tipo_prueba").attr('prueba');
		localStorage.setItem('ti', tipo);

		// aqui va el precio 

		$.ajax({
			url: "vistas/includes/pago.php",
			type: 'POST',
			data: {
				'tipo_estudio': tipo_estudio
			},
			beforeSend: function () {},
			success: function (response) {
				console.log(response)

				precio = response;
				$(".nombre_tipo_prueba").attr("precio", precio);
				window.location = '../pp/solicitud_pago_mostrar.php?confirmacion=' + confirmacion + '&email=' + email + '&fecha=' + fecha + '&tipo=' + tipo + '&precio=' + precio + '&total=' + total + '&numpacientes=' + num_pacientes + '&horainicial=' + hora_inicial + '&horafinal=' + hora_final;
			}
		});

		// precio = $(".nombre_tipo_prueba").attr("precio");
		tipo_numero = $(".nombre_tipo_prueba").attr("prueba");
		$(".nombre_tipo_prueba").text(tipo);
	})

	$(document).on("click", ".boton_eliminar_paciente", function () {
		let elem = $(this).attr("nodo");
		$(".nodo" + elem).remove();
		lista_pacientes.splice(elem, 1);
		$(".contenedor_pacientes").empty();
		lista_pacientes.forEach(function (valor) {

			let nodos = parseInt(document.getElementById("contenedor_pacientes").childNodes.length);

			$(".contenedor_pacientes").append("<div class='nodo" + nodos + "' style='display:flex; justify-content: space-between; align-items:center; padding-top: 5px;'><p class='nombre_paciente'>Paciente: " + valor.nombre + " " + valor.paterno + " " + valor.materno + "</p><div class='boton_eliminar_paciente' nodo='" + nodos + "' style='cursor:pointer; width: 30px; background-color: red; border-radius: 5px; display:flex; justify-content:center; align-items:center; padding:10px;'><img  src='vistas/assets/img/svg/delete.svg' alt=' style='padding-left: 10px;' width='20' height='20'/></div></div>");

		});

		if (lista_pacientes.length > 0) {
			$("#boton_siguiente_test").removeClass("no_visible")
		} else {
			$("#boton_siguiente_test").addClass("no_visible")
		}

	})

	$(".boton_agregar_paciente").click(function () {
		$(".modal_4").removeClass("no_visible");
		$("#boton_cancelar").removeClass("no_visible");

		$("#boton_agregar_paciente").removeClass("no_visible");
		$(".spinner_validar").addClass("no_visible");
	})

	$(".boton_opcion_prueba").click(function () {
		let tipo_prueba = $(this).attr("tipo");
		// console.log("tipo prueba", tipo_prueba)
		$(".cont_tipo_prueba_principal, .titulo_tipo_prueba, .pruebas").addClass("no_visible");
		$(".cont_nombre_tipo_prueba").removeClass("no_visible");
		if (tipo_prueba == "pcr") {
			$(".cont_nombre_tipo_prueba").html("<p class='titulo_testcovid nombre_tipo_prueba'  prueba='1'>PRUEBA DE PCR PARA COVID-19 (SARS-Cov-2) <br> \
													EN MUESTRA NASO FARINGEA\
												</p>");
		} else {
			$(".cont_nombre_tipo_prueba").html("<p class='titulo_testcovid nombre_tipo_prueba'  prueba='2'>PRUEBA DE ANTÍGENOS PARA COVID-19 (SARS-Cov-2) <br> </p>");
		}

		$(".boton_add_paciente").removeClass("no_visible");

		$(".paso1").removeClass("paso_activo");
		$(".paso2").addClass("paso_activo");
		$(".circulo_paso1").addClass("no_visible");
		$(".circulo_paso2").removeClass("no_visible");

	});

	$("#form_registro_cita").submit(function (e) {

		e.preventDefault();

		$("#boton_cancelar").addClass("no_visible");

		let datos = $(this).serialize();

		console.log(datos)

		/** ajax form **/

		$.ajax({
			url: "vistas/includes/validaciones.php",
			data: datos,
			type: 'POST',
			beforeSend: function () {
				$("#boton_agregar_paciente").addClass("no_visible");
				$(".spinner_validar").removeClass("no_visible");
			},
			success: function (response) {

				response = JSON.parse(response);

				obj_paciente = response;

				if (obj_paciente['status'] == true) {

					$("input").removeClass("input_error");

					lista_pacientes.push(obj_paciente);

					cerrar_modal();

					$(".contenedor_pacientes").empty();

					lista_pacientes.forEach(function (valor) {

						let nodos = parseInt(document.getElementById("contenedor_pacientes").childNodes.length);

						$(".contenedor_pacientes").append("<div class='nodo" + nodos + "' style='display:flex; justify-content: space-between; align-items:center; padding-top: 5px;'><p class='nombre_paciente'>Paciente: " + valor.nombre + " " + valor.paterno + " " + valor.materno + "</p><div class='boton_eliminar_paciente' nodo='" + nodos + "' style='cursor:pointer; width: 30px; background-color: red; border-radius: 5px; display:flex; justify-content:center; align-items:center; padding:10px;'><img  src='vistas/assets/img/svg/delete.svg' alt=' style='padding-left: 10px;' width='20' height='20'/></div></div>");

					});

					if (lista_pacientes.length > 0) {
						$("#boton_siguiente_test").removeClass("no_visible")
					} else {
						$("#boton_siguiente_test").addClass("no_visible")
					}

					$("#form_registro_cita")[0].reset();

					/* reset genero */
					$("span[selectbox='true']").attr("selectbox", "false");
					$('span[value="mujer"]').attr("selectbox", "true");
					$(".opciones_genero input").val("mujer");

					/* reset sintomas */
					$(".sintomas_itm input").val(0);
					$("span[checkbox='true']").attr("checkbox", "false");

					/*reset preguntas */
					$(".opciones input:not([name='cont_pruebas'])").val("no");

					$("span[value='si']").attr("selectbox", "false");
					$("span[value='no']").attr("selectbox", "true")

					/*reset pruebas */
					$(".cont_input_testcovid .sintomas_itm input").val(0);
					//$("input[name='cont_pruebas']").val(0);

					/*reset idioma */
					$(".cont_input_testcovid .idioma_int input").val(0);
					$("input[name='idioma_es']").val(1);
					$("span[value='idioma_es']").attr("checkbox", "true");

				} else {
					abrir_modal("Favor de llenar los campos obligatorios marcados con un *", "Aceptar");
					obj_paciente['array_bool']['nombre'] ? console.log("valido") : $("input[name='nombre_paciente']").addClass("input_error");
					obj_paciente['array_bool']['paterno'] ? console.log("valido") : $("input[name='apellido_paterno_paciente']").addClass("input_error");
					obj_paciente['array_bool']['materno'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['fecha_nacimiento'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['calle'] ? console.log("valido") : $("input[name='calle_paciente']").addClass("input_error");
					obj_paciente['array_bool']['colonia'] ? console.log("valido") : $("input[name='colonia_paciente']").addClass("input_error");
					obj_paciente['array_bool']['no_interior'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['no_exterior'] ? console.log("valido") : $("input[name='no_exterior_paciente']").addClass("input_error");
					obj_paciente['array_bool']['estado'] ? console.log("valido") : $("input[name='v']").addClass("input_error");
					obj_paciente['array_bool']['municipio'] ? console.log("valido") : $("input[name='municipio_paciente']").addClass("input_error");
					obj_paciente['array_bool']['codigo_postal'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['celular'] ? console.log("valido") : $("input[name='numero_celular_paciente']").addClass("input_error");
					obj_paciente['array_bool']['telefono'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['email'] ? console.log("valido") : $("input[name='email_paciente']").addClass("input_error");
					obj_paciente['array_bool']['nombre_medico'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['email_medico'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['rfc'] ? console.log("valido") : $("input[name='rfc']").addClass("input_error");
					obj_paciente['array_bool']['domicilio_fiscal'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['sintoma'] ? console.log("valido") : $("input[name='cont_sintomas']").addClass("input_error");
					obj_paciente['array_bool']['enfermedad'] ? console.log("valido") : console.log("no valido");
					obj_paciente['array_bool']['prueba'] ? console.log("valido") : $("input[name='cont_pruebas']").addClass("input_error");
					ir_a('#cont_test');
				}

			},
			error: function () {
				$("#boton_agregar_paciente").removeClass("no_visible");
				$(".spinner_validar").addClass("no_visible");
			}
		});


	});

	$(document).on('click', '#continuar', function () {

		const fecha = $("#dia_seleccionado").val();

		let meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

		let arr_fecha = fecha.split('-');

		let year = arr_fecha[0];
		let month = meses[parseInt(arr_fecha[1])];
		let day = arr_fecha[2];

		fecha_formato = day + " de " + month + " de " + year;

		const hora = $("#hora_seleccionada").val();
		const hora_final = $("#hora_seleccionada").attr("hora_final");

		let hora_arreglo = hora.split(':');
		let hora_final_arreglo = hora_final.split(':');
		let horas = hora_arreglo[0];
		let horas_f = hora_final_arreglo[0];
		let minutos = hora_arreglo[1];
		let minutos_f = hora_final_arreglo[1];
		let datos_hora = obten_formato_hora(horas);
		let datos_hora_final = obten_formato_hora(horas_f);
		let hora_nueva = datos_hora[0];
		let hora_final_nueva = datos_hora_final[0];
		let formato_hora = datos_hora[1];
		let formato_hora_final = datos_hora_final[1];


		hora_formato = hora_nueva + ":" + minutos + " " + formato_hora;
		hora_final_formato = hora_final_nueva + ":" + minutos_f + " " + formato_hora_final;

		abrir_modal_confirmacion(fecha_formato + ' en el horario ' + hora_formato + ' - ' + hora_final_formato);

	});

	$(document).on('click', '#btn_confirmar_cita', function () {
		console.log('algo');
		insertarPaciente(hora_formato, hora_final_formato, tipo, precio, tipo_numero);
	});

	$(document).on('click', "#form_tipo_pago span[selectbox]", function () {

		let context = $(this).attr("context");

		$('#form_tipo_pago span[context="' + context + '"]').attr("selectbox", "false");
		$(this).attr("selectbox", "true");
		$("#form_tipo_pago #valor_pago").val($(this).attr("value"));


	});


	$(document).on("click", "#form_tipo_pago #boton_siguiente_pago", function () {

		let pago = $('#form_tipo_pago input[name="pago"]').val();
		let confirmacion = $('#form_tipo_pago input[name="confirmacion"]').val();
		let fecha = $('#form_tipo_pago input[name="fecha"]').val();
		let email = $('#form_tipo_pago input[name="email"]').val();

		let precio = $('#form_tipo_pago input[name="precio"]').val();
		let num_pacientes = $('#form_tipo_pago input[name="num_pacientes"]').val();

		let hora_inicial = $('#form_tipo_pago input[name="hora_inicial"]').val();
		let hora_final = $('#form_tipo_pago input[name="hora_final"]').val();

		let descuento = (precio * 0.08);
		let precio_final = precio - descuento;
		let total = precio_final * num_pacientes;

		let tipo_pago;


		if (pago == "si") {

			tipo_pago = 1;

			if (precio_final == 2392) {
				precio_final = 2390;
				total = precio_final * num_pacientes;
			}

			$.ajax({
				url: "vistas/includes/update_tipo_pago.php",
				type: 'POST',
				data: {
					'no_confirmacion': confirmacion,
					'tipo': tipo_pago,
					'precio': precio_final
				},
				beforeSend: function () {},
				success: function (response) {
					console.log("confirmacion")
					console.log(confirmacion)
					window.location = '../pp/solicitud_pago_mostrar.php?confirmacion=' + confirmacion + '&email=' + email + '&fecha=' + fecha + '&tipo=' + tipo + '&precio=' + precio + '&total=' + total + '&numpacientes=' + num_pacientes + '&horainicial=' + hora_inicial + '&horafinal=' + hora_final;
				}
			});

		} else {

			tipo_pago = 0;

			$.ajax({
				url: "vistas/includes/update_tipo_pago.php",
				type: 'POST',
				data: {
					'no_confirmacion': confirmacion,
					'tipo': tipo_pago,
					'precio': precio
				},
				beforeSend: function () {},
				success: function (response) {
				}
			});

			$.ajax({
				url: "vistas/confirmacion.php",
				type: 'POST',
				async: 'false',
				data: {
					'obj_paciente': JSON.stringify(lista_pacientes),
					'fecha_formato': fecha_formato,
					'hora_formato': hora_formato,
					'hora_final': hora_final_formato,
					'confirmacion': confirmacion,
					'precio': precio,
					'tipo_prueba': tipo,
				},
				beforeSend: function () {
					$(".contenedor_principal_cita").css("height", "100%");
					$(".contenedor_principal_cita").css({
						"display": "flex",
						"justify-content": "center",
						"align-items": "center"
					});
					$(".contenedor_principal_cita").html("<p class='carga_registro subtitulo_testcovid' style='text-align: center;'><i class='fas fa-atom fa-3x text-blue spinner' style='color:#007af6;'></i><br>Registrando cita...</p>");
				},
				success: function (response) {

					$(".contenedor_principal_cita").html(response);

				}
			});

		}

	});


	function insertarPaciente(hora_formato, hora_final_formato, tipo, precio, tipo_numero,email) {
		$.ajax({
			url: "ajax/datos_paciente.ajax.php",
			headers: {
				contentType: "application/json; charset=utf-8"
			},
			type: 'POST',
			data: {
				'obj_paciente': JSON.stringify(lista_pacientes),
				'dia_seleccionado': $("#dia_seleccionado").val(),
				'hora_seleccionada': $("#hora_seleccionada").val(),
				'hora_final_seleccionada': $("#hora_seleccionada").attr("hora_final"),
				'hora_formato': hora_formato,
				'precio': precio,
				'tipo': tipo,
				'tipo_numero': parseInt(tipo_numero)
			},
			beforeSend: function () {

			},
			success: function (response) {

				console.log('respuestass')
				console.log(response)

				cerrar_modal();

				if (response.some(item => item.status != "ok"))
					abrir_modal_error_cita()
				else {

					let hora_formato = response[0].horario;
					let confirmacion = response[0].no_confirmacion;
					let fecha = response[0].fecha_seleccionada;

					$.ajax({
						url: "vistas/tipo_pago.php",
						type: 'POST',
						data: {
							'pacientes': JSON.stringify(lista_pacientes),
							'fecha_formato': fecha_formato,
							'hora_formato': hora_formato,
							'hora_final': hora_final_formato,
							'confirmacion': confirmacion,
							'email': email,
							'fecha': fecha,
							'precio': precio
						},
						beforeSend: function (response) {
							$(".contenedor_principal_cita").css({
								"display": "flex",
								"justify-content": "center",
								"align-items": "center"
							});
							$(".contenedor_principal_cita").html("<p class='carga_registro subtitulo_testcovid' style='text-align: center;'><i class='fas fa-atom fa-3x text-blue spinner' style='color:#007af6;'></i><br>Registrando cita...</p>");
						},
						success: function (response) {
							$(".contenedor_principal_cita").html(response);
						}
					});

				}
			},
			error: function (response) {
				console.log('respuestassSSS')
					console.log(response)
			}
		});
	}


	$(document).on('click', "span[selectbox]", function () {
		let context = $(this).attr("context");
		$('span[context="' + context + '"]').attr("selectbox", "false");
		$(this).attr("selectbox", "true");
		$("input[name='" + context + "']").val($(this).attr("value"));
	});

	$(document).on('click', "span[checkbox]", function () {
		let check = $(this);

		let cambio_attr = check.attr("checkbox");

		if (cambio_attr == 'false') {

			check.attr("checkbox", "false");

		} else {

			check.attr("checkbox", "true");
		}

		let nombre = check.attr("value");
		let orden = check.attr("order");

		$("input[name='" + nombre + "']").val((check.attr("checkbox") == "true" ? orden : "0"));
	});

	$(document).on('change', "#selectEstado", function () {

		$("#selectMunicipio").html("");

		$("input[name='estado_paciente']").val($(this).val());

		$.ajax({
			url: "ajax/ubicaciones.ajax.php",
			type: "POST",
			data: {
				'idEstado': $(this).val()
			},
			beforeSend: function () {

			},
			success: function (response) {
				$("#selectMunicipio").append(response);

			}
		});

	});

	$(document).on('change', "#selectMunicipio", function () {

		$("input[name='municipio_paciente']").val($(this).val());

	});

	$(document).on('change', "#cfdi_paciente", function () {

		$("input[name='cfdi_paciente']").val($(this).val());

	});

	$(document).on('change', "#regimen_fiscal", function () {

		$("input[name='regimen_fiscal']").val($(this).val());

	});

	$(document).on("click", ".menu-el", function () {

		let opcion = $(this).attr("op");

		let ruta = filename();

		switch (opcion) {
			case "serv-mov":
				localStorage.setItem("opcion_animate", "serv-mov");
				window.location = "https://laboratoriocorregidora.com.mx/index.php";
				break;
			case "preg-mov":
				localStorage.setItem("opcion_animate", "preg-mov");
				window.location = "https://laboratoriocorregidora.com.mx/index.php";
				break;
			case "vid-mov":
				localStorage.setItem("opcion_animate", "vid-mov");
				window.location = "https://laboratoriocorregidora.com.mx/index.php";
				break;
			default:
				break;
		}

	});

	$(document).on("click", ".boton_enviar_correo", function () {

		let email = $('#email_confirmacion').val();
		let pacientes = $("#pacientes").val();
		let hora_formato = $("#hora_formato").val();
		let fecha_formato = $("#fecha_formato").val();
		let hora_final = $("#hora_final").val();
		let confirmacion = $("#confirmacion").val();
		let num_pacientes = $("#num_pacientes").val();
		let total = $("#total").val();

		$.ajax({
			url: "vistas/includes/envio_email.php",
			type: 'POST',
			data: {
				'obj_paciente': JSON.stringify(lista_pacientes),
				'precio': precio,
				'email': email,
				'fecha_formato': fecha_formato,
				'hora_formato': hora_formato,
				'hora_final': hora_final,
				'confirmacion': confirmacion,
				'num_pacientes': num_pacientes,
				'total': total
			},
			beforeSend: function (response) {
				$(".cont_email_enviar").html("<p class='carga_registro subtitulo_testcovid' style='text-align: center;'><i class='fas fa-atom fa-3x text-blue spinner' style='color:#007af6;'></i><br>Enviando correo...</p>");
			},
			success: function (response) {
				if (response == "ok") {
					$(".cont_email_enviar").html("<span class='azul_sel bold_c' style='font-size: 24px;'><i class='fas fa-check-circle'></i> El correo se envio correctamente</span>");
				} else if (response == "error") {
					alert("error  de envio de correo");
				}
			}
		});

	});

});

function filename() {
	var rutaAbsoluta = self.location.href;
	var posicionUltimaBarra = rutaAbsoluta.lastIndexOf("/");
	var rutaRelativa = rutaAbsoluta.substring(posicionUltimaBarra + "/".length, rutaAbsoluta.length);
	return rutaRelativa;
}

function cerrar_modal() {
	$(".modal_1").addClass("no_visible");
	$(".modal_2").addClass("no_visible");
	$(".modal_3").addClass("no_visible");
	$(".modal_4").addClass("no_visible");
}

function abrir_modal(mensaje, boton) {
	$(".txt_modal").html(mensaje);
	$(".boton_modal").html(boton);
	$(".modal_1").removeClass("no_visible");
}

function abrir_modal_confirmacion(mensaje) {
	$("#txt_modal_confirmacion").html(mensaje);
	$(".modal_2").removeClass("no_visible");
}

function abrir_modal_error_cita() {
	$(".modal_3").removeClass("no_visible");
}

function ir_a(contenedor) {
	$(".cont_testcovid").addClass("no_visible");
	$(contenedor).removeClass("no_visible");
	$("#boton_agregar_paciente").removeClass("no_visible");
	$(".spinner_validar").addClass("no_visible");
}

function obten_formato_hora(hora) {
	let hora_nueva = "";
	let formato = "";
	let arreglo = new Array();
	switch (hora) {
		case '01':
			hora_nueva = '01';
			formato = 'am';
			break;
		case '02':
			hora_nueva = '02';
			formato = 'am';
			break;
		case '03':
			hora_nueva = '03';
			formato = 'am';
			break;
		case '04':
			hora_nueva = '04';
			formato = 'am';
			break;
		case '05':
			hora_nueva = '05';
			formato = 'am';
			break;
		case '06':
			hora_nueva = '07';
			formato = 'am';
			break;
		case '07':
			hora_nueva = '07';
			formato = 'am';
			break;
		case '08':
			hora_nueva = '08';
			formato = 'am';
			break;
		case '09':
			hora_nueva = '09';
			formato = 'am';
			break;
		case '10':
			hora_nueva = '10';
			formato = 'am';
			break;
		case '11':
			hora_nueva = '11';
			formato = 'am';
			break;
		case '12':
			hora_nueva = '12';
			formato = 'pm';
			break;
		case '13':
			hora_nueva = '01';
			formato = 'pm';
			break;
		case '14':
			hora_nueva = '02';
			formato = 'pm';
			break;
		case '15':
			hora_nueva = '03';
			formato = 'pm';
			break;
		case '16':
			hora_nueva = '04';
			formato = 'pm';
			break;
		case '17':
			hora_nueva = '05';
			formato = 'pm';
			break;
		case '18':
			hora_nueva = '06';
			formato = 'pm';
			break;
		case '19':
			hora_nueva = '07';
			formato = 'pm';
			break;
		case '20':
			hora_nueva = '08';
			formato = 'pm';
			break;
		case '21':
			hora_nueva = '09';
			formato = 'pm';
			break;
		case '22':
			hora_nueva = '10';
			formato = 'pm';
			break;
		case '23':
			hora_nueva = '11';
			formato = 'pm';
			break;
		case '24':
			hora_nueva = '12';
			formato = 'am';
			break;
	}
	arreglo.push(hora_nueva);
	arreglo.push(formato);
	return arreglo;
}

function myFunction(x) {

	x.classList.toggle("change");

	if (document.getElementById("contenidoMenu").style.display == "none") {

		document.getElementById("contenidoMenu").style.display = "";

		$("#contenidoMenu").animate({
			height: '100vh',
			top: '0px'
		}, "slow");

	} else {

		$("#contenidoMenu").animate({
			height: '0'
		}, "slow", function () {
			document.getElementById("contenidoMenu").style.display = "none";
		});

	}

}

function valida_email_enviar() {
	console.log($('#email_confirmacion').val());
	var txt_correo = $('#email_confirmacion').val();
	var correo_formato = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	if (txt_correo.length == 0 || !txt_correo.trim()) {
		$('.error_correo').html('Campo requerido');
		$('.boton_enviar_correo').attr('disabled')
	} else if (txt_correo.length < 2) {
		$('.error_correo').html('Ingresar más de un caracter');
		$('.boton_enviar_correo').attr('disabled')
	} else if (!correo_formato.test(txt_correo)) {
		$('.error_correo').html('Correo electronico invalido');
		$('.boton_enviar_correo').attr('disabled')
	} else {
		$('.boton_enviar_correo').removeAttr('disabled')
		$('.error_correo').html('');
	}

}