<?php
require_once "../controladores/sintomas.controlador.php";
require_once "../modelos/sintomas.modelo.php";
require_once "../controladores/enfermedades.controlador.php";
require_once "../modelos/enfermedades.modelo.php";
require_once "../controladores/pruebas.controlador.php";
require_once "../modelos/pruebas.modelo.php";
$paciente_dat = $_REQUEST['obj_paciente'];
$fecha_formato = base64_decode($_POST['fecha_formato']);
$hora_formato = base64_decode($_POST['hora_formato']);
$confirmacion = base64_decode($_POST['confirmacion']);
$obj_paciente=json_decode(base64_decode($paciente_dat));
print_r($obj_paciente->array_cronicas);
print_r($obj_paciente->array_pruebas);
function mostrar_sintomas($obj_paciente){
  $cont_sintomas=0;
  $resultado="";
  foreach (($obj_paciente->array_sintomas) as $key => $value) {
    if($cont_sintomas!=0){
        $resultado.=", ";
    }
    $re = ControladorSintomas::ctrConsultaNombreSintoma($value->sk_sintoma);

    $resultado.= "<span >".$re[0]."</span>";
    $cont_sintomas++;
    $re="";
  }
  if($cont_sintomas!=0 && $obj_paciente->sintoma!=""){
    $resultado.= "<span >, ".$obj_paciente->sintoma."</span>";
  }else {
    $resultado.= "<span >".$obj_paciente->sintoma."</span>";
  }
  $resultado.= ".";
    return $resultado;
}
function mostrar_enfermedades($obj_paciente){
  $cont_sintomas=0;
  $resultado="";
  foreach (($obj_paciente->array_cronicas) as $key => $value) {
    if($cont_sintomas!=0){
        $resultado.= ", ";
    }
    $re = ControladorEnfermedades::ctrConsultaNombreEnfermedades($value);
    $resultado.= "<span >".$re[0]."</span>";
    $cont_sintomas++;
    $re="";
  }
  if($cont_sintomas!=0 && $obj_paciente->enfermedad!=""){
    $resultado.= "<span >, ".$obj_paciente->enfermedad."</span>";
  }else {
    $resultado.= "<span >".$obj_paciente->enfermedad."</span>";
  }
  $resultado.= ".";
    return $resultado;
}
function mostrar_motivos($obj_paciente){
  $cont_sintomas=0;
  $resultado="";
  foreach (($obj_paciente->array_pruebas) as $key => $value) {
    if($cont_sintomas!=0){
        $resultado.= ", ";
    }
    $re = ControladorPruebas::ctrConsultaNombrePruebas($value);
    $resultado.= "<span >".$re[0]."</span>";
    $cont_sintomas++;
    $re="";
  }
  if($cont_sintomas!=0 && $obj_paciente->prueba!=""){
    $resultado.= "<span >, ".$obj_paciente->prueba."</span>";
  }else {
    $resultado.= "<span >".$obj_paciente->prueba."</span>";
  }
  $resultado.= ".";
  return $resultado;
}
 ?>
 <style media="screen">
     .div-footer {
         top: 0;
     }
     .mensaje_contenido {
         overflow-y: hidden;
         height: auto;
         min-height: inherit;
     }
     span {
         overflow-wrap: anywhere;
     }
     .mensaje_estr {
         padding-top: 90px;
     }
     html, body {
         height: auto;
         min-height: 100%;
     }
     .conten_texto {
         min-height: calc(100vh - 277px);
         justify-content: center;
     }
     .cont_interno_msg {
         padding: 0;
     }
     .contenedor_principal_cita {
         padding-top: 100px;
     }
     #boton_siguiente_test {
         width: 230px;
     }
 </style>
<section class="mensaje_contenido">
    <div class="cont_interno_msg">
        <div class="mensaje_estr">
            <!-- <div class="conten_image">
                <img class="logo-image" src="../img/svg/logo-nuevo.svg" id="logo-ss">
            </div> -->
            <div class="conten_texto">
                <span>Usted agendó un cita para el día <span class="azul_sel bold_c"><?php echo $fecha_formato." a las ".$hora_formato; ?></span></span>
                <span>Paciente: <span class="azul_sel bold_c"><?php echo $obj_paciente->nombre." ".$obj_paciente->paterno." ".$obj_paciente->materno; ?></span></span>
                <span class="pad_top">Número de Confirmación: <span class="azul_sel bold_c"><?php echo $confirmacion; ?></span></span>
                <span class="pad_top"><span class="azul_sel">Tel.</span> <?php echo $obj_paciente->celular; ?></span>
                <span><span class="azul_sel">Mail.</span> <?php echo $obj_paciente->email; ?></span>
                <span class="pad_top"><span class="azul_sel">Sintomas: </span><?php
                echo mostrar_sintomas($obj_paciente);
                ?></span>
                <span class="pad_top"><span class="azul_sel">Enfermedades cronicas: </span><?php
                echo mostrar_enfermedades($obj_paciente);
                ?></span>
                <span class="pad_top"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?:  </span><span><?php echo (($obj_paciente->contacto)==1)?"SI":"NO" ?></span></span>
                <span class="pad_top"><span class="azul_sel">¿Está tomando algún antiviral?:  </span><span><?php echo (($obj_paciente->antiviral)==1)?"SI":"NO" ?></span></span>
                <span class="pad_top"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><span><?php echo (($obj_paciente->influenza)==1)?"SI":"NO" ?></span></span>
                <span class="pad_top">
                    <span class="azul_sel">Porque se realiza esta prueba: </span>
                    <?php
                        echo mostrar_motivos($obj_paciente);
                    ?>
                </span>
                <span class="pad_top">
                    <button type="submit" name="continuar" id="boton_siguiente_test" onclick="reneviar_correo('<?php echo $paciente_dat; ?>', '<?php echo $_POST['fecha_formato']; ?>', '<?php echo $_POST['hora_formato']; ?>', '<?php echo $_POST['confirmacion']; ?>');">Reenviar correo</button>
                </span>
            </div>
            <!-- <div class="conten_image"></div> -->
        </div>
    </div>
</section>
<script type="text/javascript">
    // let acceso = true;
    // function reneviar_correo(obj_paciente, fecha_formato, hora_formato, confirmacion){
    //     if (acceso) {
    //         acceso = false;
    //         $.ajax({
    //             url: "vistas/reenvio.php",
    //             type: 'POST',
    //             data:{
    //                 'obj_paciente':JSON.stringify(obj_paciente),
    //                 'fecha_formato' : fecha_formato,
    //                 'hora_formato' : hora_formato,
    //                 'confirmacion' : confirmacion
    //             },
    //             beforeSend: function(response){
    //                 console.log("beforeSend");
    //                 console.log(obj_paciente);
    //                 console.log(confirmacion);
    //             },
    //             success: function(response){
    //
    //                 $(".contenedor_principal_cita").html(response);
    //                 $(".modal_4").removeClass("no_visible");
    //                 acceso = true;
    //
    //             }
    //         });
    //     }
    // }
</script>
<?php

$nombre_paciente = $obj_paciente->nombre." ".$obj_paciente->paterno." ".$obj_paciente->materno;
$telefono_paciente = $obj_paciente->celular;
$mail_paciente = $obj_paciente->email;
$fecha_cita = $fecha_formato." a las ".$hora_formato;
$no_confirmacion = $confirmacion;

$telefono_admin = "442 123 456 789";
$correo_admin = "m.lecuona@gmail.com";


$nombre = "Yetzin";
$apellidos = "Ramírez Rivera";
$correo = "yetzin@inkwonders.com";
$telefono = "442 123 456 789";




$to = $mail_paciente;

// $to_admin = "contacto@laboratoriocorregidora.com.mx";
$to_admin = "giorocha.tamps18@gmail.com";
$to_admin = "yetzin@inkwonders.com";

$subject = "Sitio web de Laboratorio Corregidora";



$message = '<html>
    <head>
        <meta charset="utf-8">
        <title>Se agendó la cita</title>
        <style media="screen">
        .contenedor * {
            box-sizing: border-box;
            font-family: arial;
        }
        .contenedor {
            box-sizing: border-box;
            width: 600px;
            max-width: 100%;
            background-color: #e6e6e6;
            margin-top: 50px;
            padding: 17px;
        }
        .mensaje {
            width: 100%;
            background-color: #f2f2f2;
            display: inline-flex;
            flex-direction: column;
            align-items: center;
            text-align: center;
            font-size: 18px;
            margin: 0;
            padding: 5%;
            color: #194271;
        }
        .logo_head {
            width: 90%;
            height: auto;
        }
        .texto_msg, .manifiesto_txt {
            margin-top: 80px;
        }
        .texto_confirmacion {
            font-size: 20px;
        }
        .texto_confirmacion, .texto_numero {
            margin-top: 40px;
        }
        .link_aviso {
            text-decoration: none;
            margin-top: 30px;
            font-size: 16px;
            color: #194271;
        }
        .numero_confirmacion {
            color: #007af6;
            font-weight: bold;
            font-size: 25px;
        }
        .manifiesto_txt {
            color: #848484;
            font-size: 13px;
        }
        .numero_tel {
            font-weight: bold;
        }
        .numero_tel a {
            text-decoration: none;
            color: #194271;
        }
        span {
            overflow-wrap: anywhere;
        }
    </style>
    </head>
    <body>
        <div class="contenedor">
            <table class="mensaje">
                <tr>
                    <td>
                        <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_msg">Usted agendó un cita para el día '.$fecha_cita.'</span>
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_msg">'.$obj_paciente->nombre." ".$obj_paciente->paterno." ".$obj_paciente->materno.'</span>
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_confirmacion">Número de Confirmación: <span class="numero_confirmacion">'.$no_confirmacion.'</span></span>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top">
                            <span class="azul_sel">Sintomas: </span>
                            <b>
                                '.mostrar_sintomas($obj_paciente).'
                            </b>
                        </span>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">Enfermedades cronicas: </span>
                            <b>
                                '.mostrar_enfermedades($obj_paciente).'
                            </b>
                        </span>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?:  </span><b>'.((($obj_paciente->contacto)==1)?"SI":"NO").'</b></span>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">¿Está tomando algún antiviral?:  </span><b>'.((($obj_paciente->antiviral)==1)?"SI":"NO").'</b></span>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><b>'.((($obj_paciente->influenza)==1)?"SI":"NO").'</b></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">Porque se realiza esta prueba: </span>
                            <b>
                                '.mostrar_motivos($obj_paciente).'
                            </b>
                        </span>
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">442 2121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>

';



$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com>' . "\r\n";
// $headers .= 'Bcc: iloyola@merlion.com.mx' . "\r\n";



if(mail($to,$subject,$message,$headers)){



    // echo "correcto";



}else{



    // echo "incorrecto";



}
?>
