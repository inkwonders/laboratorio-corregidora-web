<div class="cont_testcovid" id="cont_test">
    <div class="cont_interno_testcovid">
        <a rel="noreferrer" class="wa-contenedor" href="https://wa.link/b0x4ly" target="_blank">
            <div class="wa-contenedor__icon"></div>
            <div class="wa-contenedor__banner">
                <span class="wa-contenedor__texto">¿Ayuda?</span>
                <div class="wa-contenedor__barra"></div>
            </div>
        </a>
        <div class="cont_pasos">
            <div class="paso paso_activo btn_vistas paso1" op="test">
                <div class="circulo sp circulo_paso1">
                    <span class="sp --i:1"></span>
                    <span class="sp --i:2"></span>
                </div>
                <p class="txt_paso">1/ TIPO PRUEBA</p>
            </div>
            <div class="paso btn_vistas paso2" op="cita" id="btn_activar">
                <div class="circulo sp circulo_paso2 no_visible">
                    <span class="sp --i:1"></span>
                    <span class="sp --i:2"></span>
                </div>
                <p class="txt_paso">2/ TEST</p>
            </div>
            <div class="paso btn_vistas paso3" op="pago">
                <p class="txt_paso">3/ CONFIRMACIÓN</p>
            </div>
        </div>
        <div class="cont_titulo_testcovid titulo_tipo_prueba">
            <p class="titulo_testcovid">SELECCIONA EL TIPO DE PRUEBA</p>
        </div>
        <div class="cont_pasos pruebas">
            <div class="boton_opcion_prueba" tipo="pcr">PCR</div>
            <div class="boton_opcion_prueba" tipo="antigenos">ANTÍGENOS</div>
        </div>

        <br>
        <br>
        <br>

        <div>
            <p class="titulo_testcovid" style="margin-top: 7%;">DEBERÁ ACUDIR A SU TOMA DE MUESTRA EN EL LABORATORIO MATRIZ (MARQUÉS)</p>
        </div>
        <div class="cont_titulo_testcovid no_visible cont_nombre_tipo_prueba">
            <p class="titulo_testcovid nombre_tipo_prueba"></p>
        </div>
        <div class="cont_boton_agregar_paciente boton_add_paciente no_visible">
            <div class="boton_agregar_paciente" tipo="pcr">AGREGAR PACIENTE <img width="20" height="20" src="vistas/assets/img/svg/paciente.svg" alt="" style="padding-left: 10px;"></div>
        </div>
        <div class="contenedor_pacientes" id="contenedor_pacientes"></div>
        <button type="button" name="continuar" id="boton_siguiente_test" class="no_visible">SIGUIENTE</button>
    </div>
</div>

<div class="modal modal_4 no_visible">
    <div class="card_modal">
        <div>
            <p class="titulo_testcovid">FORMULARIO</p>
        </div>
        <div class="cruz_cerrar" onclick="cerrar_modal();"></div>
        <form id="form_registro_cita" method="POST">
            <div class="cont_datosgenerales_paciente">
                <div class="cont_interno_form_datosgenerales_paciente">
                    <span class="subtitulo_testcovid">DATOS GENERALES DEL PACIENTE</span>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Nombre o Nombres *</p>
                            <input type="text" class="input_style" name="nombre_paciente" required maxlength="40">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Apellido paterno * </p>
                            <input type="text" class="input_style tam_95" name="apellido_paterno_paciente" required maxlength="50">
                        </div>
                        <div class="cont_input_testcovid padding_top_20_movil">
                            <p class="label_input">Apellido materno </p>
                            <input type="text" class="input_style tam_95" name="apellido_materno_paciente" maxlength="50">
                        </div>
                    </div>
                    <div class="fila_input input_genero">
                        <span class="label_input">Género *</span>
                        <div class="opciones_genero">
                            <span class="label_input cont_selectbox">
                                <span>Masculino</span>
                                <span selectbox="false" value="hombre" context="genero"></span>
                            </span>
                            <span class="label_input cont_selectbox">
                                <span>Femenino</span>
                                <span selectbox="true" value="mujer" context="genero"></span>
                            </span>
                            <input type="hidden" name="genero" value="mujer">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Fecha de nacimiento *</p>
                            <input id="datepicker1" type="text" class="input_style" name="fecha_nacimiento_paciente" value="" required readonly>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Calle * </p>
                            <input type="text" class="input_style" name="calle_paciente" required maxlength="200">
                        </div>
                        <div class="cont_input_testcovid padding_top_20_movil">
                            <p class="label_input">Colonia *</p>
                            <input type="text" class="input_style" name="colonia_paciente" required maxlength="100">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">No. Exterior * </p>
                            <input type="text" class="input_style" name="no_exterior_paciente" required maxlength="10">
                        </div>
                        <div class="cont_input_testcovid padding_top_20_movil">
                            <p class="label_input">No. Interior </p>
                            <input type="text" class="input_style" name="no_interior_paciente" maxlength="10">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Estado * </p>

                            <select class="input_style estado sel_inpt" name="" id="selectEstado" required>
                                <option value="0" disabled selected>Seleccione estado</option>
                                <?php
                                $consulta_estados = ControladorUbicacion::ctrConsultaEstado();
                                foreach ($consulta_estados as $key => $value) {
                                    echo " <option value=" . $value['idEstado'] . " >" . $value['nombreEstado'] . "</option>";
                                }
                                ?>
                            </select>
                            <input type="hidden" class="input_style" name="estado_paciente">
                        </div>
                        <div class="cont_input_testcovid padding_top_20_movil">
                            <p class="label_input">Municipio *</p>

                            <select class="input_style municipio sel_inpt" name="" id="selectMunicipio" required>
                                <option value="0" disabled selected>Seleccione Municipio</option>
                                    
                            </select>
                            <input type="hidden" class="input_style" name="municipio_paciente">
                        </div>
                        <div class="cont_input_testcovid padding_top_20_movil">
                            <p class="label_input">Codigo Postal </p>
                            <input type="text" class="input_style" name="codigo_postal_paciente" maxlength="5">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Pasaporte</p>
                            <input type="text" class="input_style" name="pasaporte" maxlength="100">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Número de Celular * </p>
                            <input type="text" class="input_style" name="numero_celular_paciente" required maxlength="10">
                        </div>
                        <div class="cont_input_testcovid padding_top_20_movil">
                            <p class="label_input">Número Telefónico </p>
                            <input type="text" class="input_style" name="numero_telefonico_paciente" maxlength="10">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">E-mail *</p>
                            <input type="text" class="input_style" name="email_paciente" required maxlength="100">
                        </div>
                    </div>
                </div>
            </div>
            <div class="info_medico">
                <div class="cont_interno_form_datosgenerales_paciente">
                    <span class="subtitulo_testcovid">INFORMACIÓN DEL MÉDICO</span>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Nombre</p>
                            <input type="text" class="input_style" name="nombre_medico" maxlength="200">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">E-mail </p>
                            <input type="text" class="input_style" name="email_medico" maxlength="100">
                        </div>
                    </div>
                </div>
            </div>
            <div class="info_medico">
                <div class="cont_interno_form_datosgenerales_paciente">
                    <span class="subtitulo_testcovid">DATOS DE FACTURACIÓN</span>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Razón Social </p>
                            <input type="text" class="input_style" name="razon_social" maxlength="25">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">RFC </p>
                            <input type="text" class="input_style" name="rfc" maxlength="15">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Domicilio Fiscal, incluyendo código postal</p>
                            <input type="text" class="input_style" name="domicilio_fiscal" maxlength="300">
                        </div>
                    </div>

                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Régimen fiscal </p>
                            <select class="input_style municipio sel_inpt" name="" id="regimen_fiscal" required>
                                <option value="0" disabled selected>Régimen Fiscal</option>

                                <?php
                                $consulta_regimen = ControladorUbicacion::ctrRegimenFiscal();
                                foreach ($consulta_regimen as $key => $value) {
                                    echo " <option value=" . $value['id'] . " >" . $value['codigo'] ." - ". $value['descripcion'] . "</option>";
                                }
                                
                                ?>
                            </select>
                            <input type="hidden" class="input_style" name="regimen_fiscal">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Uso del CFDI </p>
                            <select class="input_style municipio sel_inpt" name="" id="cfdi_paciente" required>
                                <option value="0" disabled selected>CFDI</option>
                    
                                <?php
                                $consulta_cfdi = ControladorUbicacion::ctrCfdi();
                                foreach ($consulta_cfdi as $key => $value) {
                                    echo " <option value=" . $value['id'] . " >" . $value['codigo'] ." - ". $value['descripcion'] . "</option>";
                                }
                                
                                ?>
                            </select>
                            <input type="hidden" class="input_style" name="cfdi_paciente">
                        </div>
                    </div>
                </div>
            </div>
            <div class="info_sintomas">
                <div class="cont_interno_form_datosgenerales_paciente">
                    <span class="subtitulo_testcovid sintomas_title">¿Qué síntomas has tenido?</span>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <?php
                            $consulta_sintomas = ControladorSintomas::ctrConsultaSintomas();
                            $cont_sintomas = 0;
                            foreach ($consulta_sintomas as $key => $value) {
                                echo " <div class='sintomas_itm'>
                                    <span checkbox='false' value='sintoma_" . $key . "'  order='" . $value['orden'] . "' context='sintomas'></span>
                                    <span class='label_input txt_sintoma'>" . $value["nombre"] . "</span>
                                    <input type='hidden' name='sintoma_" . $key . "' value='0'>
                                </div>";
                                $cont_sintomas = $key + 1;
                            }
                            echo "<input type='hidden' name='cont_sintomas' value='" . $cont_sintomas . "'>";
                            ?>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Otro</p>
                            <input type="text" class="input_style" name="otro_sintoma" maxlength="250">
                        </div>
                    </div>
                </div>
            </div>
            <div class="info_enfermedad">
                <div class="cont_interno_form_datosgenerales_paciente">
                    <span class="subtitulo_testcovid sintomas_title">¿Padece usted de alguna enfermedad crónica? </span>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <?php
                            $consulta_enfermedades = ControladorEnfermedades::ctrConsultaEnfermedades();
                            $cont_cronicas = 0;
                            foreach ($consulta_enfermedades as $key => $value) {
                                if ($cont_cronicas > 0) {
                                    echo "<div class='sintomas_itm'>
                                        <span checkbox='false' value='cronica_" . $key . "' order='" . $value['orden'] . "' context='enfermedad'></span>
                                        <span class='label_input txt_sintoma'>" . $value["nombre"] . "</span>
                                        <input type='hidden' id='cronica_" . $key . "' name='cronica_" . $key . "'  value='0'>
                                    </div>";
                                }
                                $cont_cronicas = $key + 1;
                            }
                            echo "<input type='hidden' name='cont_cronicas' value='" . $cont_cronicas . "'>";
                            ?>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">Otra</p>
                            <input type="text" class="input_style" name="otra_enfermedad_cronica" maxlength="250">
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?</p>
                            <div class="opciones">
                                <span class="label_input cont_selectbox">
                                    <span selectbox="false" value="si" context="contacto_covid"></span>
                                    <span>Sí</span>
                                </span>
                                <span class="label_input cont_selectbox">
                                    <span selectbox="true" value="no" context="contacto_covid"></span>
                                    <span>No</span>
                                </span>
                                <input type="hidden" name="contacto_covid" value="no">
                            </div>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">¿Está tomando algún antiviral?</p>
                            <div class="opciones">
                                <span class="label_input cont_selectbox">
                                    <span selectbox="false" value="si" context="antiviral"></span>
                                    <span>Sí</span>
                                </span>
                                <span class="label_input cont_selectbox">
                                    <span selectbox="true" value="no" context="antiviral"></span>
                                    <span>No</span>
                                </span>
                                <input type="hidden" name="antiviral" value="no">
                            </div>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">¿Se vacunó contra la Influenza el último año? *</p>
                            <div class="opciones">
                                <span class="label_input cont_selectbox">
                                    <span selectbox="false" value="si" context="influenza"></span>
                                    <span>Sí</span>
                                </span>
                                <span class="label_input cont_selectbox">
                                    <span selectbox="true" value="no" context="influenza"></span>
                                    <span>No</span>
                                </span>
                                <input type="hidden" name="influenza" value="no">
                            </div>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">¿Porque se realiza esta prueba? </p>
                            <div class="opciones">
                                <div class="cont_input_testcovid">
                                    <?php
                                    $cont_pruebas = 0;
                                    $consulta_pruebas = ControladorPruebas::ctrConsultaPruebas();
                                    foreach ($consulta_pruebas as $key => $value) {
                                        echo "<div class='sintomas_itm'>
                                                    <span checkbox='false' value='prueba_" . $key . "' order='" . $value['orden'] . "' context='enfermedad'></span>
                                                    <span class='label_input txt_sintoma'>" . $value["nombre"] . "</span>
                                                    <input type='hidden' id='pruebas_" . $key . "' name='prueba_" . $key . "'  value='0'>
                                                </div>";
                                        $cont_pruebas = $key + 1;
                                        // $cont_pruebas = $cont_pruebas + 1;
                                    }
                                    echo "<input type='hidden' name='cont_pruebas' value='" . $cont_pruebas . "'>";
                                    ?>
                                </div>
                            </div>
                            <div class="cont_input_testcovid">
                                <p class="label_input">Otro</p>
                                <input type="text" class="input_style" name="por_que_prueba" maxlength="250">
                            </div>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="label_input">¿En que idioma desea recibir sus estudios? * puede marcar ambas casillas</p>
                            <div class="opciones">
                                <div class="cont_input_testcovid">
                                    <div class='idioma_int'>
                                        <span checkbox='true' value='idioma_es' order='1' context='idioma'></span>
                                        <span class='label_input txt_sintoma'>Español</span>
                                        <input type='hidden' id='idioma_es' name='idioma_es' value='1'>
                                    </div>
                                </div>
                                <div class="cont_input_testcovid">
                                    <div class='idioma_int'>
                                        <span checkbox='false' value='idioma_en' order='1' context='idioma'></span>
                                        <span class='label_input txt_sintoma'>Ingles</span>
                                        <input type='hidden' id='idioma_en' name='idioma_en' value='0'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fila_input">
                        <div class="cont_input_testcovid">
                            <p class="subtitulo_testcovid red">* Campos obligatorios</p>
                        </div>
                    </div>
                    <div class="spinner_validar no_visible">
                        <p class='carga_registro titulo_sipinner' style='text-align: center;'><i class='fas fa-atom fa-2x text-blue spinner' style='color:#007af6;'></i><br>Validando datos...</p>
                    </div>
                    <div class="cont_botones_modal">
                        <button type="submit" name="continuar" id="boton_agregar_paciente">AGREGAR</button>
                        <button type="button" name="cancelar" id="boton_cancelar" onclick="cerrar_modal();">CERRAR</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
