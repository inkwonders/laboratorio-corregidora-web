<div class="cont_testcovid" id="cont_test">

    <div class="cont_interno_testcovid">

        <div class="cont_pasos2">

            <div class="paso paso_activo btn_vistas" op="test">
                <div class="circulo sp">
                    <span class="sp --i:1"></span>
                    <span class="sp --i:2"></span>
                </div>
                <p class="txt_paso">1/ TIPO PRUEBA</p>

            </div>

            <div class="paso btn_vistas" op="cita" id="btn_activar">
                <p class="txt_paso">2/ TEST</p>
            </div>

            <div class="paso btn_vistas" op="pago">
                <p class="txt_paso">3/ CONFIRMACIÓN</p>
            </div>

        </div>

        <div class="cont_titulo_testcovid titulo_tipo_prueba">

            <p class="titulo_testcovid">SELECCIONA EL TIPO DE PRUEBA</p>

        </div>

        <div class="cont_pasos pruebas">

            <div class="boton_opcion_prueba" tipo="pcr">PCR</div>
            <div class="boton_opcion_prueba" tipo="antigenos">ANTÍGENOS</div>

        </div>

    </div>

</div>
