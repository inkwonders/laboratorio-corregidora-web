<footer id="footer" class="flex flex-col items-center w-full px-4 py-8 text-base text-white shadow bg-blue-lab-claro lg:text-lg xl:text-xl 2xl:text-2xl">
    <div class="flex w-full max-w-[94rem] flex-col items-center justify-center gap-6 py-3 sm:py-6 lg:items-start lg:flex-row lg:justify-evenly">
        <div class="col-span-1">
            <a href="/">
                <img src="/modules/img/logos/logo-lab-blanco.svg" alt="Logo" class="mx-auto w-52 lg:w-60">
            </a>
        </div>

        <div class="flex flex-col items-center col-span-1 lg:gap-3 lg:items-start">
            <div class="flex items-center mb-2">
                <img src="/modules/img/svg/whatsapp-blanco.svg" alt="WhatsApp" class="w-4 mr-2 lg:w-5 xl:w-6">
                <p><a href="https://api.whatsapp.com/send/?phone=524421206215">442 120 6215 (Solo Mensajes)</a></p>
            </div>
            <div class="flex items-center">
                <img src="/modules/img/svg/telefono-blanco.svg" alt="Teléfono" class="w-4 mr-2 lg:w-5 xl:w-6">
                <p><a href="tel:4422121052">442 212 1052</a></p>
            </div>
        </div>

        <div class="flex items-center gap-x-7">
            <a title="Laboratorios-Corregidora-108767437596443" href="https://www.facebook.com/Laboratorios-Corregidora-108767437596443" target="_blank">
                <img src="/modules/img/svg/facebook-blanco.svg" alt="phone" class="w-3">
            </a>
            <a title="@laboratoriocorregidora3382" href="https://www.youtube.com/@laboratoriocorregidora3382" target="_blank">
                <img src="/modules/img/svg/youtube-blanco.svg" alt="phone" class="w-7">
            </a>
        </div>

        <div class="flex flex-col items-center col-span-1 space-y-2 font-rob-medium lg:items-start">
            <button type="button" class="menu-el" op="servicios">Servicios</button>
            <button type="button" class="menu-el" op="nosotros">Nosotros</button>
            <!-- <a href="#">Bolsa de Trabajo</a> -->
        </div>

        <button op="preguntas" class="flex flex-col items-center col-span-1 space-y-2 cursor-pointer menu-el font-rob-medium lg:items-start">
            <span>FAQ’S</span>
            <span>Ayuda</span>
        </button>
    </div>

    <div class="w-full mt-6 mb-0 border-t-2 border-white border-opacity-50 lg:hidden"></div>

    <div class="flex w-full max-w-[94rem] flex-col items-center justify-center gap-3 pt-6 text-center lg:text-base xl:text-lg 2xl:text-xl lg:pt-16 lg:gap-5 xl:gap-6 2xl:gap-7 lg:flex-row">
        <span>Laboratorio Corregidora <?=date("Y"); ?></span>
        <span class="hidden lg:inline">/</span>
        <span>Permiso de Publicidad: 203301201A0872</span>
        <span class="hidden lg:inline">/</span>
        <span><a href="/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">Aviso de Privacidad</a></span>
        <span class="hidden lg:inline">/</span>
        <span><a href="https://inkwonders.com" target="_blank">Desarrollado por INK WONDERS</a></span>
    </div>
</footer>
