<?php

	error_reporting(E_ALL);
	ini_set('display_errors', '1');

  include('pagoliga.php');

  $obj_paciente = json_decode($_REQUEST['obj_paciente']);

  $cliente_correo = $obj_paciente->email;

  $confirmacion = $_POST['confirmacion'];
  $monto = $_POST['monto'];

  $datosAdicionaless = "";

  $liga = generarLiga($monto,$confirmacion,$cliente_correo,$datosAdicionaless);

?>

<section class="pago oculto">

	<div class="titulo_galeria">
		PAGO
	</div>

	<div class="fondo_pagos"></div>

	<section class="cont_forma_pago_api">
			<div style="text-align:center"><h1 class="titulos titulo_pago">REALIZA TU PAGO</h1><br /><span class="txt_pago">Captura tus datos de pago y haz clic en el botón PAGAR.<br /><br />No salgas de esta página hasta no ver la confirmación de pago, puede tardar algunos segundos.</span></div>
			<div style="text-align: center; margin-bottom: 0px; margin-top: 20px">
				<div style="display:inline-block; border: 5px solid #F00; padding: 20px 25px; text-align:center">
					<h1 class="mensaje_pago">Si tu pago no pudo ser procesado,<br />comunícate con tu banco<br />para habilitar pagos en línea</h1>
				</div>
			</div>
	</section>
	<section class="noventa" style="text-align:center">

		<iframe src="<?php echo $liga ?>" width="520px" height="680px" frameborder="0" scrolling="no" seamless style="margin-top:20px"></iframe>

	</section>

</section>
