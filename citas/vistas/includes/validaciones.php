<?php

	error_reporting(E_ALL);
	ini_set("display_errors", 1);

	require_once "../../controladores/plantilla.controlador.php";
	require_once "../../controladores/cita.controlador.php";
	require_once "../../controladores/enfermedades.controlador.php";
	require_once "../../controladores/sintomas.controlador.php";
	require_once "../../controladores/validaciones.controlador.php";
	require_once "../../controladores/ubicaciones.controlador.php";
	require_once "../../controladores/pruebas.controlador.php";
	require_once "../../modelos/cita.modelo.php";
	require_once "../../modelos/enfermedades.modelo.php";
	require_once "../../modelos/sintomas.modelo.php";
	require_once "../../modelos/ubicaciones.modelo.php";
	require_once "../../modelos/pruebas.modelo.php";

	$nombre=$_POST['nombre_paciente'];
	$paterno=$_POST['apellido_paterno_paciente'];
	$materno=$_POST['apellido_materno_paciente'];
	$fecha_nacimiento=$_POST['fecha_nacimiento_paciente'];
	$calle=$_POST['calle_paciente'];
	$colonia=$_POST['colonia_paciente'];
	$no_exterior=$_POST['no_exterior_paciente'];
	$no_interior=$_POST['no_interior_paciente'];
	$estado=$_POST['estado_paciente'];
	$municipio=$_POST['municipio_paciente'];
	$codigo_postal=$_POST['codigo_postal_paciente'];
	$celular=$_POST['numero_celular_paciente'];
	$telefono=$_POST['numero_telefonico_paciente'];
	$email=$_POST['email_paciente'];
	$nombre_medico=$_POST['nombre_medico'];
	$email_medico=$_POST['email_medico'];
	$rs=$_POST['razon_social'];
	$regimen_fiscal=$_POST['regimen_fiscal'];
	$cfdi_paciente=$_POST['cfdi_paciente'];
	$ref=$_POST['rfc'];
	$domicilio_fiscal=$_POST['domicilio_fiscal'];
	$sintoma=$_POST['otro_sintoma'];
	$enfermedad_cronica=$_POST['otra_enfermedad_cronica'];
	$prueba=$_POST['por_que_prueba'];
	$cont_cronicas=$_POST['cont_cronicas'];
	$cont_sintomas=$_POST['cont_sintomas'];
	$cont_pruebas=$_POST['cont_pruebas'];
	$genero=($_POST['genero']=="mujer")? "2" : "1";
	$contacto=($_POST['contacto_covid']=="no") ? "0" : "1";
	$antiviral=($_POST['antiviral']=="no")? "0" : "1";
	$influenza=($_POST['influenza']=="no")? "0" : "1";
	$array_cronicas=array();
	$array_sintomas=array();
	$array_pruebas=array();
	$pasaporte = $_POST['pasaporte'];

	$es = $_POST['idioma_es'];
	$en = $_POST['idioma_en'];

	for($i = 0; $i < $cont_sintomas; $i++){
		$val_s = $_POST['sintoma_'.$i];
		if($val_s != "0"){
			array_push($array_sintomas,ControladorSintomas::ctrConsultaSkSintoma($val_s));
		}
	}

	for($i = 1; $i < $cont_cronicas; $i++){
		$val_c = $_POST['cronica_'.$i];
		if($val_c != "0"){
			array_push($array_cronicas,ControladorEnfermedades::ctrConsultaSkEnfermedades($val_c));
		}
	}

	for($j = 0;$j < $cont_pruebas; $j++){
		$val_p = (int)$_POST['prueba_'.$j];
		if($val_p != 0){
			array_push($array_pruebas,ControladorPruebas::ctrConsultaSkPruebas($val_p));
		}
	}

	$bool_nombre=ControladorValidacion::ctrValidarNombres($nombre,1);
	$bool_paterno=ControladorValidacion::ctrValidarNombres($paterno,1);
	$bool_materno=ControladorValidacion::ctrValidarNombres($materno,0);
	$bool_fecha_nacimiento=ControladorValidacion::ctrValidarFechaNacimiento($fecha_nacimiento,1);
	$bool_calle=(ControladorValidacion::ctrValidaLongitud($calle,200))?ControladorValidacion::ctrValidarCampoObligatorio($calle,1):false;
	$bool_colonia=(ControladorValidacion::ctrValidaLongitud($colonia,100))?ControladorValidacion::ctrValidarCampoObligatorio($colonia,1):false;
	$bool_interior=ControladorValidacion::ctrValidaLongitud($no_interior,10);
	$bool_exterior=ControladorValidacion::ctrValidaLongitud($no_exterior,10);
	$bool_municipio=ControladorValidacion::ctrValidarSelect($municipio,1);
	$bool_estado=ControladorValidacion::ctrValidarSelect($estado,1);
	$bool_CP=(ControladorValidacion::ctrValidarCampoObligatorio($codigo_postal,0))?ControladorValidacion::ctrValidaLongitud($codigo_postal,5):false;
	$bool_celular=ControladorValidacion::ctrValidarTelefono($celular,1);
	$bool_telefono=ControladorValidacion::ctrValidarTelefono($telefono,0);
	$bool_email=ControladorValidacion::ctrValidarCorreo($email,1);
	$bool_nombre_medico=ControladorValidacion::ctrValidarNombres($nombre_medico,0);
	$bool_email_medico=ControladorValidacion::ctrValidarCorreo($email_medico,0);
  	$bool_rfc=($ref=='')?true:ControladorValidacion::ctrValidarRFC($ref);
	$bool_dom_fisc=ControladorValidacion::ctrValidaLongitud($domicilio_fiscal,300);
	$bool_sintoma=ControladorValidacion::ctrValidaLongitud($sintoma,250);
	$bool_enfermedad=ControladorValidacion::ctrValidaLongitud($enfermedad_cronica,250);
	$bool_prueba=ControladorValidacion::ctrValidaLongitud($prueba,250);

 	if($bool_nombre &&$bool_paterno && $bool_materno  && $bool_fecha_nacimiento  && $bool_calle && $bool_colonia && $bool_exterior && $bool_interior && $bool_estado && $bool_municipio && $bool_CP && $bool_celular  && $bool_telefono  && $bool_email  && $bool_nombre_medico && $bool_email_medico && $bool_rfc && $bool_dom_fisc && $bool_sintoma && $bool_enfermedad && $bool_prueba){

	 	$skUbicacion=ControladorUbicacion::ctrConsultaSkUbicacion($estado,$municipio);

		$response= array(
			'status'			=> true,
			'nombre'			=> $nombre,
			'paterno'			=> $paterno,
			'materno'			=> $materno,
			'fecha_nacimiento'	=> $fecha_nacimiento,
			'calle'				=> $calle,
			'colonia'			=> $colonia,
			'no_interior'		=> $no_interior,
			'no_exterior'		=> $no_exterior,
			'ubicacion'			=> $skUbicacion,
			'codigo_postal'		=> $codigo_postal,
			'celular'			=> $celular,
			'telefono'			=> $telefono,
			'email'				=> $email,
			'nombre_medico'		=> $nombre_medico,
			'email_medico'		=> $email_medico,
			'rs'				=> $rs,
			'rfc'				=> $ref,
			'domicilio_fiscal'	=> $domicilio_fiscal,
			'sintoma'			=> $sintoma,
			'enfermedad'		=> $enfermedad_cronica,
			'prueba'			=> $prueba,
			'genero'			=> $genero,
			'contacto'			=> $contacto,
			'influenza'			=> $influenza,
			'antiviral'			=> $antiviral,
			'array_sintomas'	=> $array_sintomas,
			'array_cronicas'	=> $array_cronicas,
			'array_pruebas'		=> $array_pruebas,
			'es' 				=> $es,
			'en' 				=> $en,
			'pasaporte'         => $pasaporte,
			'regimen_fiscal'	=> $regimen_fiscal,
			'cfdi_paciente'		=> $cfdi_paciente,
		);

	}else{

		$bool_response=array(
			'nombre'			=> $bool_nombre,
			'paterno'			=> $bool_paterno,
			'materno'			=> $bool_materno,
			'fecha_nacimiento'	=> $bool_fecha_nacimiento,
			'calle'				=> $bool_calle,
			'colonia'			=> $bool_colonia,
			'no_interior'		=> $bool_interior,
			'no_exterior'		=> $bool_exterior,
			'estado'			=> $bool_estado,
			'municipio'			=> $bool_municipio,
			'codigo_postal'		=> $bool_CP,
			'celular'			=> $bool_celular,
			'telefono'			=> $bool_telefono,
			'email'				=> $bool_email,
			'nombre_medico'		=> $bool_nombre_medico,
			'email_medico'		=> $bool_email_medico,
			'rs'				=> $rs,
			'rfc'				=> $bool_rfc,
			'domicilio_fiscal'	=> $bool_dom_fisc,
			'sintoma'			=> $bool_sintoma,
			'enfermedad'		=> $bool_enfermedad,
			'prueba'			=> $bool_prueba,
			'pasaporte'         => $pasaporte,
			'regimen_fiscal'	=> $regimen_fiscal,
			'cfdi_paciente'		=> $cfdi_paciente,

		);

		$response= array(
			'status'      => false,
			'array_bool'  => $bool_response,
		);

	}

  	echo json_encode($response, JSON_FORCE_OBJECT);
