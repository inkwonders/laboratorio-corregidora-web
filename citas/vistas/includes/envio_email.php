<?php

    require_once "../../controladores/sintomas.controlador.php";
    require_once "../../modelos/sintomas.modelo.php";
    require_once "../../controladores/enfermedades.controlador.php";
    require_once "../../modelos/enfermedades.modelo.php";
    require_once "../../controladores/pruebas.controlador.php";
    require_once "../../modelos/pruebas.modelo.php";

    $pacientes = json_decode($_REQUEST['obj_paciente']);
    $precio = $_REQUEST['precio'];
    $email = $_REQUEST['email'];
    $num_pacientes = $_REQUEST['num_pacientes'];
    $total = $_REQUEST['total'];

    function mostrar_sintomas($sintomas_paciente){
        $cont_sintomas=0;
        $resultado="";
        foreach (($sintomas_paciente->array_sintomas) as $key => $value) {
            if($cont_sintomas!=0){
                $resultado.=", ";
            }
            $re = ControladorSintomas::ctrConsultaNombreSintoma("'{$value->sk_sintoma}'");
            $resultado.= "<span class='numero_tel'>".$re[0]."</span>";
            $cont_sintomas++;
        }
        if($cont_sintomas!=0 && $sintomas_paciente->sintoma!=""){
            $resultado.= "<span class='numero_tel'>, ".$sintomas_paciente->sintoma."</span>";
        }else {
            $resultado.= "<span class='numero_tel'>".$sintomas_paciente->sintoma."</span>";
        }
        if(sizeof($re) > 1){
            $resultado.= ".";
        }else{
            $resultado = "<span class='numero_tel'>Sin síntomas.</span>";
        }
        return $resultado;
    }

    function mostrar_enfermedades($obj_paciente){
        $cont_sintomas=0;
        $resultado="";
        foreach (($obj_paciente->array_cronicas) as $key => $value) {
            if($cont_sintomas>1){
                $resultado.= ", ";
            }
            $re = ControladorEnfermedades::ctrConsultaNombreEnfermedades($value);
            $resultado.= "<span class='numero_tel'>".$re[0]."</span>";
            $cont_sintomas++;
        }
        if($cont_sintomas!=0 && $obj_paciente->enfermedad!=""){
            $resultado.= "<span class='numero_tel'>, ".$obj_paciente->enfermedad."</span>";
        }else {
            $resultado.= "<span class='numero_tel'>".$obj_paciente->enfermedad."</span>";
        }
        if(sizeof($re) > 1){
            $resultado.= ".";
        }else{
            $resultado = "<span class='numero_tel'>Sin enfermedades.</span>";
        }
        return $resultado;
    }

    function mostrar_motivos($obj_paciente){
        $cont_sintomas=0;
        $resultado="";
        foreach (($obj_paciente->array_pruebas) as $key => $value) {
            if($cont_sintomas!=0){
                $resultado.= ", ";
            }
            $re = ControladorPruebas::ctrConsultaNombrePruebas($value);
            $resultado.= "<span class='numero_tel'>".$re[0]."</span>";
            $cont_sintomas++;
        }
        if($cont_sintomas!=0 && $obj_paciente->prueba!=""){
            $resultado.= "<span class='numero_tel'>, ".$obj_paciente->prueba."</span>";
        }else {
            $resultado.= "<span class='numero_tel'>".$obj_paciente->prueba."</span>";
        }
        if(sizeof($re) > 1){
            $resultado.= ".";
        }else{
            $resultado = "<span class='numero_tel'>Sin motivos.</span>";
        }
        return $resultado;
    }

    $mail1 = false;
    $mail2 = false;

    $fecha_cita = $_POST['fecha_formato'];
    $hora_inicial=$_POST['hora_formato'];
    $hora_final=$_POST['hora_final'];
    $no_confirmacion = $_POST['confirmacion'];

    $to = $email;
    $to_admin = "contacto@laboratoriocorregidora.com.mx";

    $subject = "Reservación Laboratorio Corregidora";

    $message .= '<html>
        <head>
            <meta charset="utf-8">
            <title>Se agendó la cita</title>
            <style media="screen">
            .contenedor * {
                box-sizing: border-box;
                font-family: arial;
            }
            .contenedor {
                box-sizing: border-box;
                width: 600px;
                max-width: 100%;
                background-color: #e6e6e6;
                margin-top: 50px;
                padding: 17px;
            }
            .mensaje {
                width: 100%;
                background-color: #f2f2f2;
                display: inline-flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
                font-size: 18px;
                margin: 0;
                padding: 5%;
                color: #194271;
            }
            .logo_head {
                width: 90%;
                height: auto;
            }
            .texto_msg, .manifiesto_txt {
                margin-top: 10px;
            }
            .texto_confirmacion {
                font-size: 20px;
            }
            .texto_confirmacion, .texto_numero {
                margin-top: 40px;
            }
            .link_aviso {
                text-decoration: none;
                margin-top: 30px;
                font-size: 16px;
                color: #194271;
            }
            .numero_confirmacion {
                color: #007af6;
                font-weight: bold;
                font-size: 25px;
            }
            .manifiesto_txt {
                color: #848484;
                font-size: 13px;
            }
            .numero_tel {
                font-weight: bold;
            }
            .numero_tel a {
                text-decoration: none;
                color: #194271;
            }
            span {
                overflow-wrap: anywhere;
            }
            .izq{
                width: 100%;
                text-align: left;
            }
            .colorgris{
                background-color: #f2f2f2;
                padding: 5%;
                color: #194271;
            }
            .txt_fecha_hora{
                font-size: 18px;
                color: #194271;
            }
        </style>
        </head>
        <body>
            <div class="contenedor">
                <table class="colorgris">
                    <tr class="mensaje">
                        <td>
                            <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                            <br><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_msg izq txt_fecha_hora">Usted agendó una cita para el día '.$fecha_cita.'</span>
                            <br>
                            <span class="texto_msg izq txt_fecha_hora">Horario: '.$hora_inicial." - ".$hora_final.'</span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_confirmacion izq">Número de Confirmación: <span class="numero_confirmacion">'.$no_confirmacion.'</span></span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">'.$num_pacientes.'</span></span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_confirmacion izq">Total: <span class="numero_confirmacion">$'.$total.'</span></span>
                            <br><br>
                        </td>
                    </tr>';

                    $bandera = 0;
                    
                    foreach($pacientes as $paciente){
                        if($bandera == 0){
                            $message.= '<hr>';
                        }
                        $message .= '<tr>
                                        <td>
                                            <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">'. $paciente->nombre.' '.$paciente->paterno.' '.$paciente->materno .'</span></span></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">'.  $paciente->celular .'</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">'.  $paciente->email .'</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">Sintomas: </span>'. mostrar_sintomas($paciente) .'</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">Enfermedades crónicas: </span>'. mostrar_enfermedades($paciente) .'</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?:  </span><span class="numero_tel">'.($paciente->contacto ? 'Si' : 'No').'</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">¿Está tomando algún antiviral?:  </span><span class="numero_tel">'.($paciente->antiviral ? 'Si' : 'No').'</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><span class="numero_tel">'.($paciente->influenza ? 'Si' : 'No').'</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="pad_top"><span class="azul_sel">Porque se realiza esta prueba: </span>'.  mostrar_motivos($paciente) .'</span>
                                        </td>
                                    </tr><hr>';
                 
                                    $bandera++;
                    }
                    
        $message .='
                    <tr>
                        <td>
                            <span class="texto_numero"><br>Dirección y link de ubicación de la sucursal a la que deben acudir para realizar la prueba: <span class="numero_tel">Avenida Prolongación Constituyentes No. 32 Oriente Calle Avenida Marqués de la Villa del Villar del Aguila, Altos del Marques, 76140 Santiago de Querétaro, Qro.</span> <br><br> <a href="https://goo.gl/maps/ibT9atD4xPkXeAxy8" target="_blank">https://goo.gl/maps/ibT9atD4xPkXeAxy8</a> </span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_numero"><br><br>Indicaciones de como deben presentarse para realizar la prueba: </span> <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul> 
                                <li style="text-align: left;">PCR: Acudir con ayuno mínimo de 4 horas de alimentos sólidos. No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra, esta prueba se debe de hacer a partir del día 3 en adelante en que el paciente presenta los síntomas o que estuvo en contacto directo con una persona COVID- 19 Positivo.</li>

                                <br> 

                                <li style="text-align: left;">ANTIGENOS: No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra. esta prueba debe de hacerse durante los primeros 7 días de haberse presentado por lo menos dos síntomas sugestivos a COVID-19.</li> <br>
                            </ul> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">442 2121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                            <br><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                        </td>
                    </tr>
                </table>
            </div>
        </body>
    </html>';

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com>' . "\r\n";
    // $headers .= 'Bcc: iloyola@merlion.com.mx' . "\r\n";

    if(mail($to,$subject,$message,$headers)){
       $mail1 = true;
    }

    $message = "";

    $message .= '<html>
        <head>
            <meta charset="utf-8">
            <title>Se agendó la cita</title>
            <style media="screen">
            .contenedor * {
                box-sizing: border-box;
                font-family: arial;
            }
            .contenedor {
                box-sizing: border-box;
                width: 600px;
                max-width: 100%;
                background-color: #e6e6e6;
                margin-top: 50px;
                padding: 17px;
            }
            .mensaje {
                width: 100%;
                background-color: #f2f2f2;
                display: inline-flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
                font-size: 18px;
                margin: 0;
                padding: 5%;
                color: #194271;
            }
            .logo_head {
                width: 90%;
                height: auto;
            }
            .texto_msg, .manifiesto_txt {
                margin-top: 10px;
            }
            .texto_confirmacion {
                font-size: 20px;
            }
            .texto_confirmacion, .texto_numero {
                margin-top: 40px;
            }
            .link_aviso {
                text-decoration: none;
                margin-top: 30px;
                font-size: 16px;
                color: #194271;
            }
            .numero_confirmacion {
                color: #007af6;
                font-weight: bold;
                font-size: 25px;
            }
            .manifiesto_txt {
                color: #848484;
                font-size: 13px;
            }
            .numero_tel {
                font-weight: bold;
            }
            .numero_tel a {
                text-decoration: none;
                color: #194271;
            }
            span {
                overflow-wrap: anywhere;
            }
            .izq{
                width: 100%;
                text-align: left;
            }
            .colorgris{
                background-color: #f2f2f2;
                padding: 5%;
                color: #194271;
            }
            .txt_fecha_hora{
                font-size: 18px;
                color: #194271;
            }
        </style>
        </head>
        <body>
            <div class="contenedor">
                <table class="colorgris">
                    <tr class="mensaje">
                        <td>
                            <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                            <br><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_msg txt_fecha_hora izq">Se agendó una cita para el día '.$fecha_cita.'</span>
                            <br>
                            <span class="texto_msg txt_fecha_hora izq">Horario: '.$hora_inicial." - ".$hora_final.'</span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_confirmacion izq">Número de Confirmación: <span class="numero_confirmacion">'.$no_confirmacion.'</span></span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">'.$num_pacientes.'</span></span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="texto_confirmacion izq">Total: <span class="numero_confirmacion">$'.$total.'</span></span>
                            <br><br>
                        </td>
                    </tr>';

                    $bandera = 0;
                    
                    foreach($pacientes as $paciente){

                        if($bandera == 0){
                            $message.= '<hr>';
                        }
                        $message .= '<tr>
                                            <td>
                                                    <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">'. $paciente->nombre.' '.$paciente->paterno.' '.$paciente->materno .'</span></span></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">'.  $paciente->celular .'</span></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">'.  $paciente->email .'</span></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">Sintomas: </span>'. mostrar_sintomas($paciente) .'</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">Enfermedades crónicas: </span>'. mostrar_enfermedades($paciente) .'</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?:  </span><span class="numero_tel">'.($paciente->contacto ? 'Si' : 'No').'</span></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">¿Está tomando algún antiviral?:  </span><span class="numero_tel">'.($paciente->antiviral ? 'Si' : 'No').'</span></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><span class="numero_tel">'.($paciente->influenza ? 'Si' : 'No').'</span></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pad_top"><span class="azul_sel">Porque se realiza esta prueba: </span>'.  mostrar_motivos($paciente) .'</span>
                                                </td>
                                            </tr><hr>';              
                    }
                    $bandera++;

        $message .='
                    <tr>
                        <td>
                            <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">442 2121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                            <br><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                        </td>
                    </tr>
                </table>
            </div>
        </body>
    </html>';

    if(mail($to_admin,$subject,$message,$headers)){
        $mail2=true;
    }

    if($mail1 && $mail2){
        echo "ok";
    }else{
        echo "error";
    }

?>