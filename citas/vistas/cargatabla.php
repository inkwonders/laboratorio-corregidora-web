<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

  header('Content-Type: application/json');

  require_once "../controladores/cita.controlador.php";
  require_once "../modelos/cita.modelo.php";

  $datos_array = array();
  $fecha_init = date("Y-m-d");
  $new_fecha_init = strtotime ('-100 year' , strtotime($fecha_init)); //Se resta un año menos
  $fecha_init_new = date('Y-m-d',$new_fecha_init);

  $miArray = array(
      'id'=> '0',
      'title'=> 'inicializador',
      'start'=> $fecha_init_new.'T12:00:00',
      'allDay'=> false,
      'end'=> $fecha_init_new.'T13:00:00',
      'extendedProps'=> array(
        'hourStart' => '12:00:00',
        'hourEnded' => '13:00:00'
      )
  );

  array_push($datos_array, $miArray);

  //if($consulta_citas["feccha_seleccionada"] > 0){

    $dia = true;

    $consulta_citas = ControladorCita::ctrConsultaCita();

    foreach($consulta_citas as $key => $value){

      $consulta_horario = ControladorCita::ctrConsultaHorario($value["fk_horario"]);

      $miArray = array(
        // 'id'=> $sk_agenda,
        'title'=> $value["sk_cita"],
        'start'=> $consulta_horario["hora_inicial"]."T12:00:00",
        'allDay'=> $dia,
        'end'=> $consulta_horario["hora_final"]."T12:00:00",
        'extendedProps'=> array(
          'hourStart' => '07:00',
          'hourEnded' => '08:00'
        )
      );

      array_push($datos_array, $miArray);

    }

  //}

  echo json_encode($datos_array);
