<?php

  require_once "../admin/modelos/conexion.php";

  class ModeloUbicaciones{

    static public function mdlConsultaEstado($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT nombreEstado,id,idEstado FROM $tabla GROUP BY nombreEstado");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlConsultaMunicipio($tabla,$estado){
      
      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE idEstado='$estado' ORDER BY idMunicipio");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }
    static public function mdlConsultaSkUbicacion($tabla,$estado,$municipio){

      $stmt = Conexion::conectar()->prepare("SELECT id FROM $tabla WHERE idEstado=$estado and idMunicipio=$municipio ");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlConsultaCfdi($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT id,codigo,descripcion FROM $tabla ");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlRegimenFiscal($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT id,codigo,descripcion FROM $tabla ");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }


  }
 ?>
