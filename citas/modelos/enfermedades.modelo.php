<?php

  require_once "../admin/modelos/conexion.php";

  class ModeloEnfermedades{

    static public function mdlConsultaEnfermedades($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY orden ASC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlConsultaSkEnfermedades($tabla,$enfermedad){

      $stmt = Conexion::conectar()->prepare("SELECT id FROM $tabla where orden=$enfermedad");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }
    static public function mdlConsultaNombreEnfermedades($tabla,$enfermedad){

      $stmt = Conexion::conectar()->prepare("SELECT nombre FROM $tabla where id='".$enfermedad."' ");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }
    static public function mdlConsultaEmfermedadesPaciente($tabla,$sk_paciente){

      $stmt = Conexion::conectar()->prepare("SELECT enfermedad_id FROM $tabla WHERE paciente_id='".$sk_paciente."'");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }
    static public function ctrConsultaNombreEmfermedad($tabla,$sk_emfermedad){

      $stmt = Conexion::conectar()->prepare("SELECT nombre FROM $tabla where id = '".$sk_emfermedad."' ");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }
  }
