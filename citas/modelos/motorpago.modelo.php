<?php

  require_once "conexion.php";

  class ModeloPago{

    static public function mdlConsultaConfiguracion($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

  }
