<?php

  require_once "../admin/modelos/conexion.php";

  class ModeloSintomas{

    static public function mdlConsultaSintomas($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

      $stmt -> execute();

      return $stmt -> fetchAll();

    }

    static public function mdlConsultaSkSintomas($tabla,$sintoma){

      $stmt = Conexion::conectar()->prepare("SELECT id FROM $tabla where orden=$sintoma");

      $stmt -> execute();

      return $stmt -> fetch();

    }

    static public function mdlConsultaNombreSintomas($tabla,$sintoma){

      $stmt = Conexion::conectar()->prepare("SELECT nombre FROM $tabla where id = $sintoma");

      $stmt -> execute();

      return $stmt -> fetch();

    }

    static public function mdlConsultaSintomaPaciente($tabla,$sk_paciente){

      $stmt = Conexion::conectar()->prepare("SELECT sintoma_id FROM $tabla where paciente_id = $sk_paciente");

      $stmt -> execute();

      return $stmt -> fetch();

    }
  }
