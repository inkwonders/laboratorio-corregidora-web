<?php

  require_once "conexion.php";

  class ModeloUser{

    static public function mdlVerficaUser($tabla, $email){

        $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) AS total, id FROM $tabla WHERE email = :email");

        $stmt->bindParam(":email", $email, PDO::PARAM_STR);

        $stmt -> execute();

        $total = $stmt -> fetch();

        if((int)$total['total'] > 0){
            return $total['id'];
        }else{
            return "error";
        }

    }

    static public function mdlInsertaUser($tabla, $nombre_completo, $email){

        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id, name, email, email_verified_at, password, two_factor_secret, two_factor_recovery_codes, remember_token, current_team_id, profile_photo_path, created_at, updated_at) VALUES(0, :nombre, :email, 0, '', '', '', '', '', '', NULL, NULL)");

        $stmt->bindParam(":nombre", $nombre_completo, PDO::PARAM_STR);
        $stmt->bindParam(":email", $email, PDO::PARAM_STR);

        if($stmt -> execute()){
            return "ok";
        }else{
            return "error";
    }

  }

}