<?php
// echo "lol";
// require_once "../admin/modelos/conexion.php";

// require_once($_SERVER['DOCUMENT_ROOT']."/admin/modelos/conexion.php");
// echo "ok";

class ModeloCita
{

    static public function mdlUpdateTipoPago($tabla, $tipo_pago, $no_confirmacion, $precio)
    {

    $final = (int)$tipo_pago;

    // if($final == 1){
    //   $precio = 2390;
    // }

    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET tipo_pago = :tipo_pago, costo = :precio, updated_at = CURRENT_TIMESTAMP  WHERE no_confirmacion = :no_confirmacion");
    $stmt->bindParam(":tipo_pago",  $final, PDO::PARAM_INT);
    $stmt->bindParam(":no_confirmacion", $no_confirmacion, PDO::PARAM_STR);
    $stmt->bindParam(":precio", $precio, PDO::PARAM_STR);

    if ($stmt->execute()) {
        return "ok";
    } else {
        return "error";
    }

    $stmt = null;
    }

    static public function ConsultaPacienteEnCita($tabla, $no_confirmacion)
    {

    $stmt = Conexion::conectar()->prepare("SELECT paciente_id FROM $tabla WHERE no_confirmacion = '$no_confirmacion'");

    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    static public function mdlConsultaCita($tabla)
    {

    $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha_seleccionada >= CURDATE()");

    $stmt->execute();

    return $stmt->fetchAll();
    }

    static public function mdlConsultaHorario($tabla, $fk_horario)
    {

    $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id = '$fk_horario'");

    $stmt->execute();

    return $stmt->fetch();
    }

    static public function mdlConsultaHorarioDisponible($tabla, $nombre_dia) {
        if ($nombre_dia == "Sabado") {
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE dia_seman = '2' ORDER BY orden");
        } else {
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE dia_seman = '1' ORDER BY orden");
        }

        // echo "[lol_1]";
        $stmt->execute();
        // echo "[lol_2]";

        // exit(json_encode($stmt->fetchAll()));
        return $stmt->fetchAll();
    }

    static public function mdlConsultaHorarioDisponibleLugares($tabla1, $tabla2, $sk_horario, $nombre_dia, $fecha)
    {

    if ($nombre_dia == "Sabado") {

        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla1 WHERE id = '$sk_horario' and
            (SELECT count(fecha_seleccionada) as t FROM $tabla2 as c WHERE horario_id = '$sk_horario' ) < '15' ORDER BY orden");
    } else {

        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla1 WHERE id = '$sk_horario' and
            (SELECT count(fecha_seleccionada) as t FROM $tabla2 as c WHERE horario_id = '$sk_horario' ) < '15' ORDER BY orden");
    }

    $stmt->execute();

    return $stmt->fetch();
    }
    static public function mdlConsultaValidacionHorario($tabla1, $tabla2, $sk_horario, $nombre_dia, $fecha)
    {
        // echo "[$tabla1, $tabla2, $sk_horario, $nombre_dia, $fecha]";
        $stmt = Conexion::conectar()->prepare("SELECT count(fecha_seleccionada) as t FROM $tabla2 as c WHERE horario_id = '$sk_horario' AND fecha_seleccionada='$fecha'");

        $stmt->execute();

        return $stmt->fetch();
    }

    static public function ConsultaSkHorario($tabla1, $dia_sem, $hora)
    {

    $stmt = Conexion::conectar()->prepare("SELECT id FROM $tabla1 WHERE hora_inicial = '$hora' AND dia_seman = '$dia_sem' ");

    $stmt->execute();

    return $stmt->fetch();
    }

    static public function VerificarCitasHorario($tabla1, $sk_horario, $fecha)
    {

    $stmt = Conexion::conectar()->prepare("SELECT count(*) AS num_citas FROM $tabla1 WHERE horario_id = '$sk_horario' AND fecha_seleccionada = '$fecha'");

    $stmt->execute();

    return $stmt->fetch();
    }

    static public function uuid()
    {

    $stmt = Conexion::conectar()->prepare("SELECT UUID()");

    $stmt->execute();

    return $stmt->fetch();
    }

    static public function clave_unica()
    {

    $length = 10;

    $code = md5(uniqid(rand(), true));

    return substr($code, 0, $length);
    }

    static public function mdlBuscarCita($tabla, $no_confirmacion)
    {

    $stmt = Conexion::conectar()->prepare("SELECT horario_id, fecha_seleccionada, created_at, updated_at, SUM(costo) as monto, tipo_prueba FROM $tabla WHERE no_confirmacion = '$no_confirmacion'");

    $stmt->execute();

    return $stmt->fetch();
    }

    // nuevo 
    static public function ConsultaPrecio($tabla, $estudio)
    {

    $stmt = Conexion::conectar()->prepare("SELECT precio FROM $tabla WHERE clave = '$estudio'");

    $stmt->execute();

    return $stmt->fetch();
    }
}
