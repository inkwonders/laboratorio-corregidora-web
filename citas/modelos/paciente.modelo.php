<?php

  require_once "conexion.php";

  class ModeloPaciente{

      static public function insertarPaciente($tabla, $arreglo_datos, $id_user){

        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id,user_id,clave,nombre,apellido_paterno,apellido_materno,nombre_completo,genero,fecha_nacimiento,calle,colonia,
        no_exterior,no_interior,ubicacion_id,cp,tel_celular,tel_casa,paciente_email,medico_nombre,medico_email,razon_social,rfc,domicilio_fiscal,cfdis_id,regimen_fiscal_id,otro_sintoma,
      otra_enfermedad,contacto_persona_positivo,antiviral,influenza,otro_prueba,pasaporte,es,en,created_at) VALUES (0,:user_id,:clave,:nombre,:apellido_paterno,:apellido_materno,:nombre_completo,:genero,:fecha_nacimiento,:calle,:colonia,
      :no_exterior,:no_interior,:fk_ubicacion,:cp,:tel_celular,:tel_casa,:paciente_email,:medico_nombre,:medico_email,:rs, :rfc,:domicilio_fiscal,:cfdis_id,:regimen_fiscal_id,:otro_sintoma,
    :otra_enfermedad,:contacto_persona_positivo,:antiviral,:influenza,:otro_prueba,:pasaporte,:es,:en, NULL)");

	$stmt->bindParam(":user_id", $id_user, PDO::PARAM_STR);    
        $stmt->bindParam(":clave", $arreglo_datos["clave"], PDO::PARAM_STR);        
        $stmt->bindParam(":nombre", $arreglo_datos["nombre"], PDO::PARAM_STR);
        $stmt->bindParam(":apellido_paterno", $arreglo_datos["apellido_paterno"], PDO::PARAM_STR);
        $stmt->bindParam(":apellido_materno", $arreglo_datos["apellido_materno"], PDO::PARAM_STR);
        $stmt->bindParam(":nombre_completo", $arreglo_datos["nombre_completo"], PDO::PARAM_STR);
        $stmt->bindParam(":genero", $arreglo_datos["genero"], PDO::PARAM_INT);
        $stmt->bindParam(":fecha_nacimiento", $arreglo_datos["fecha_nacimiento"], PDO::PARAM_STR);
        $stmt->bindParam(":calle", $arreglo_datos["calle"], PDO::PARAM_STR);
        $stmt->bindParam(":colonia", $arreglo_datos["colonia"], PDO::PARAM_STR);
        $stmt->bindParam(":no_exterior", $arreglo_datos["no_exterior"], PDO::PARAM_STR);
        $stmt->bindParam(":no_interior", $arreglo_datos["no_interior"], PDO::PARAM_STR);
        $stmt->bindParam(":fk_ubicacion", $arreglo_datos["fk_ubicacion"], PDO::PARAM_INT);
        $stmt->bindParam(":cp", $arreglo_datos["cp"], PDO::PARAM_STR);
        $stmt->bindParam(":tel_celular", $arreglo_datos["tel_celular"], PDO::PARAM_STR);
        $stmt->bindParam(":tel_casa", $arreglo_datos["tel_casa"], PDO::PARAM_STR);
        $stmt->bindParam(":paciente_email", $arreglo_datos["paciente_email"], PDO::PARAM_STR);
        $stmt->bindParam(":pasaporte", $arreglo_datos["pasaporte"], PDO::PARAM_STR);
        $stmt->bindParam(":medico_nombre", $arreglo_datos["medico_nombre"], PDO::PARAM_STR);
        $stmt->bindParam(":medico_email", $arreglo_datos["medico_email"], PDO::PARAM_STR);
        $stmt->bindParam(":rs", $arreglo_datos["rs"], PDO::PARAM_STR);
        $stmt->bindParam(":rfc", $arreglo_datos["rfc"], PDO::PARAM_STR);
        $stmt->bindParam(":domicilio_fiscal", $arreglo_datos["domicilio_fiscal"], PDO::PARAM_STR);
        $stmt->bindParam(":cfdis_id", $arreglo_datos["cfdi_paciente"], PDO::PARAM_STR);
        $stmt->bindParam(":regimen_fiscal_id", $arreglo_datos["regimen_fiscal"], PDO::PARAM_STR);
        $stmt->bindParam(":otro_sintoma", $arreglo_datos["otro_sintoma"], PDO::PARAM_STR);
        $stmt->bindParam(":otra_enfermedad", $arreglo_datos["otra_enfermedad"], PDO::PARAM_STR);
        $stmt->bindParam(":contacto_persona_positivo", $arreglo_datos["contacto_persona_positivo"], PDO::PARAM_INT);
        $stmt->bindParam(":antiviral", $arreglo_datos["antiviral"], PDO::PARAM_INT);
        $stmt->bindParam(":influenza", $arreglo_datos["influenza"], PDO::PARAM_INT);
        $stmt->bindParam(":otro_prueba", $arreglo_datos["otro_prueba"], PDO::PARAM_STR);
        $stmt->bindParam(":es", $arreglo_datos["es"], PDO::PARAM_INT);
        $stmt->bindParam(":en", $arreglo_datos["en"], PDO::PARAM_INT);


        if($stmt -> execute()){

            return "ok";

        }else{

            return "INSERT INTO $tabla (id,user_id,clave,nombre,apellido_paterno,apellido_materno,nombre_completo,genero,fecha_nacimiento,calle,colonia,
            no_exterior,no_interior,ubicacion_id,cp,tel_celular,tel_casa,paciente_email,medico_nombre,medico_email,razon_social,rfc,domicilio_fiscal,cfdis_id,regimen_fiscal_id,otro_sintoma,
            otra_enfermedad,contacto_persona_positivo,antiviral,influenza,otro_prueba,pasaporte,es,en,created_at) VALUES ('',".$id_user.", ".$arreglo_datos["clave"].", ".$arreglo_datos["nombre"].", ".$arreglo_datos["apellido_paterno"].", ".$arreglo_datos["apellido_materno"].",".$arreglo_datos["nombre_completo"].",".$arreglo_datos["genero"].",".$arreglo_datos["fecha_nacimiento"].",".$arreglo_datos["calle"].",".$arreglo_datos["colonia"].",
            ".$arreglo_datos["no_exterior"].",".$arreglo_datos["no_interior"].",".$arreglo_datos["fk_ubicacion"].",".$arreglo_datos["cp"].",".$arreglo_datos["tel_celular"].",".$arreglo_datos["tel_casa"].",".$arreglo_datos["paciente_email"].",".$arreglo_datos["medico_nombre"].",".$arreglo_datos["rs"].",".$arreglo_datos["rfc"].",".$arreglo_datos["domicilio_fiscal"].",".$arreglo_datos["cfdi_paciente"].",".$arreglo_datos["regimen_fiscal"].",".$arreglo_datos["otro_sintoma"].",
            ".$arreglo_datos["otra_enfermedad"].",".$arreglo_datos["contacto_persona_positivo"].",".$arreglo_datos["antiviral"].",".$arreglo_datos["influenza"].",".$arreglo_datos["otro_prueba"].",".$arreglo_datos["pasaporte"].",".$arreglo_datos["es"].",".$arreglo_datos["en"].",NULL)";
        }

    }

    static public function insertarCita($tabla, $datos){
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id,horario_id,paciente_id,fecha_seleccionada,no_confirmacion,costo,tipo_prueba,created_at) VALUES (0,:fk_horario,:fk_paciente,:fecha_seleccionada,:no_confirmacion,:precio,:tipo,CURRENT_TIMESTAMP)");

        $stmt->bindParam(":fk_horario", $datos["fk_horario"], PDO::PARAM_STR);
        $stmt->bindParam(":fk_paciente", $datos["fk_paciente"], PDO::PARAM_STR);
        $stmt->bindParam(":fecha_seleccionada", $datos["fecha_seleccionada"], PDO::PARAM_STR);
        $stmt->bindParam(":no_confirmacion", $datos["no_confirmacion"], PDO::PARAM_STR);
        $stmt->bindParam(":precio", $datos["precio"], PDO::PARAM_STR);
        $stmt->bindParam(":tipo", $datos["tipo"], PDO::PARAM_INT);

        if ($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }
    }

      // static public function InsertDatosGeneral($tabla, $item1, $item2, $item3, $valor2, $valor3){

      //   $query = "INSERT INTO $tabla ($item1,$item2,$item3) VALUES ('', :$item2,:$item3)";

      //   $stmt = Conexion::conectar()->prepare($query);

      //   $stmt->bindParam(":$item2", $valor2, PDO::PARAM_INT);
      //   $stmt->bindParam(":$item3", $valor3, PDO::PARAM_STR);

      //   if($stmt -> execute()){

      //     return "1";

      //   }else{

      //     return "2";

      //   }

      // }

    static public function InsertDatosGeneral($tabla, $id_paciente, $valor, $item3) {
        $query = "INSERT INTO $tabla (id, paciente_id, $item3, created_at) VALUES (0, :paciente_id, :valor, NULL)";
        $stmt = Conexion::conectar()->prepare($query);

        $stmt->bindParam(":paciente_id", $id_paciente, PDO::PARAM_INT);
        $stmt->bindParam(":valor", $valor, PDO::PARAM_INT);

        if ($stmt->execute()) {
            return "1";
        } else {
            return "2";
        }
    }



    static public function ConsultaPaciente($tabla,$sk_paciente){
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id = '$sk_paciente'");
        $stmt->execute();

        return $stmt -> fetch();
    }

    static public function consultaIdPaciente($tabla,$clave_paciente){
        $stmt = Conexion::conectar()->prepare("SELECT id FROM $tabla WHERE clave = :clave");
        $stmt->bindParam(":clave", $clave_paciente, PDO::PARAM_STR);

        $stmt -> execute();

        return $stmt -> fetch();
    }

    static public function consultaIdCita($tabla, $id_paciente){
        $stmt = Conexion::conectar()->prepare("SELECT id FROM $tabla WHERE paciente_id = '$id_paciente'");
        $stmt -> execute();

        return $stmt -> fetch();
    }
}
