<?php

  require_once "conexion.php";

  class ModeloPpagos{

    static public function mdlConsultaPagos($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

  }
