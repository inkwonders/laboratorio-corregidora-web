<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

date_default_timezone_set('America/Mexico_City');

require_once "controladores/paciente.controlador.php";
require_once "controladores/cita.controlador.php";
require_once "controladores/enfermedades.controlador.php";
require_once "controladores/sintomas.controlador.php";
require_once "controladores/pruebas.controlador.php";

require_once "modelos/paciente.modelo.php";
require_once "modelos/cita.modelo.php";
require_once "modelos/enfermedades.modelo.php";
require_once "modelos/sintomas.modelo.php";
require_once "modelos/pruebas.modelo.php";

include "controladores/motorpago.controlador.php";
include "modelos/motorpago.modelo.php";

if (!isset($_REQUEST["strResponse"])) {
    exit();
}

include('vistas/AESCrypto.php');
require_once "modelos/conexion.php";
require_once "../ajax/dbcon.php";

// $conf = MotorPago::ctrConsultaConfiguracion();
// $key = $conf["semilla_aes"];

//Respuesta
$originalString = $_REQUEST['strResponse'];

//Decodificar
$key = 'DD33CEF1A4A0DDF130996A2BD052486D';
$decryptedString = AESCrypto::desencriptar($originalString, $key);

//Cargar string desencriptado
$xml = simplexml_load_string($decryptedString);

$idPago = $xml->reference;
echo $idPago . " - ";

$logFile = fopen("log-pagos.txt", 'a') or die("Error creando archivo");
fwrite($logFile, "\n".date("d/m/Y H:i:s")."respuesta xml: ".$xml." |||||") or die("Error escribiendo en el archivo");
fclose($logFile);

/** datos de tarjeta **/

$tipo_tarjeta = $xml->cc_type;
$cuatro_digitos_tarjeta = $xml->cc_number;

/** end datos de tarjeta **/

$correo = $xml->email;


if(isset($xml->foliocpagos) ){
    $foliocpagos = $xml->foliocpagos;
}
else{
    $foliocpagos = null;
}

$total = $xml->amount;
$total = number_format((float)$total, 2);

$response = $xml->response;

$fecha_cita = $xml->datos_adicionales->data[2]->value;

$horas = $xml->datos_adicionales->data[3]->value;
$array_horas = explode("/", $horas);
$hora_inicial = $array_horas[0];
$hora_final = $array_horas[1];

$num_pacientes = $xml->datos_adicionales->data[4]->value;
$tipo_pago = $xml->datos_adicionales->data[5]->value;

$message = "";

if ($tipo_pago != '2') {
    $tipo_compra = 1;
} else {
    $tipo_compra = 2;
}

// datos para response inicia
$no_confirmacion_guardar = $xml->reference;
$total_guardar = $xml->amount;
$respuesta_guardar = $xml->response;
$folio_autorizacion_guardar = $xml->auth;
$hora_guardar = $xml->time;

$fecha_directa = explode("/",$xml->date);
$fecha_armada = $fecha_directa[0]."-".$fecha_directa[1]."-".$fecha_directa[2];
$fecha_guardar = date('Y/m/d', strtotime($fecha_armada));

$datos_tarjeta_explode = explode("/", $xml->cc_type);
$tipo_tarjeta_guardar = $datos_tarjeta_explode[0];
$banco_tarjeta_guardar = $datos_tarjeta_explode[1];
$banco_emisor_tarjeta_guardar = $datos_tarjeta_explode[2];
$tarjeta_guardar = $xml->cc_mask;
// datos para response termina

$sql_guardar = "INSERT INTO response_tarjeta(response,tipo,numero_confirmacion,tipo_pago,tarjeta_digitos,autorizacion,tipo_transaccion,importe,fecha,hora,tipo_tarjeta,tarjeta,banco_emisor, foliocpagos, estatus, created_at) VALUES ('$originalString', '$tipo_compra', '$no_confirmacion_guardar' , 'Contado', '$tarjeta_guardar', '$folio_autorizacion_guardar', 'E-COMMERCE', '$total_guardar','$fecha_guardar', '$hora_guardar', '$banco_tarjeta_guardar', '$tipo_tarjeta_guardar', '$banco_emisor_tarjeta_guardar','$foliocpagos', '$respuesta_guardar' ,NULL);";

// echo $sql_guardar;
$res_guardar = query_bd($sql_guardar);

$message_paciente_denied = "";
$message_admin_denied = "";

$message_paciente_error = "";
$message_admin_error = "";

$to = $correo;
$to_admin = "contacto@laboratoriocorregidora.com.mx, eddy@inkwonders.com";
//$to_admin = "oswaldoferral@gmail.com, alain@inkwonders.com";
//$to_admin_all = "oswaldoferral@gmail.com, alain@inkwonders.com";
$to_admin_all = "contacto@laboratoriocorregidora.com.mx, facturas@laboratoriocorregidora.com.mx, eddy@inkwonders.com";

// $to_admin = "alainttlm@gmail.com";
// $to_admin_all = "alainttlm@gmail.com";

//Evaluar response
if ($response == 'approved') {   //=======================  PAGO APROVADO

    if ($tipo_pago != '2') {

        $sql = "UPDATE citas SET pagado = 1, response = '$response', cadena = '$originalString', tipo_tarjeta = '$tipo_tarjeta', digitos = '$cuatro_digitos_tarjeta', updated_at = CURRENT_TIMESTAMP WHERE no_confirmacion = '$idPago'";
        $res_pago = query_bd($sql);

        $info_cita = ControladorCita::ctrBuscarCita($idPago);

        echo "fecha registro: " . $info_cita['created_at'];
        echo "fecha pago: " . $info_cita['updated_at'];

        $fecha_hora_registro = date('d/m/Y G:ia', strtotime($info_cita['created_at']));
        $fecha_hora_pago = date('d/m/Y G:ia', strtotime($info_cita['updated_at']));

        $lista_fk_pacientes = ControladorPaciente::ConsultaPacientesNoConfirmacion($idPago);

        var_dump($lista_fk_pacientes);

        //     ///////////////////////////////////////////////////////////////////////////// fin armado

        $no_confirmacion = $idPago;

        $subject_reservacion_paciente = "Reservación Laboratorio Corregidora";

        $message_paciente .= '<html>
            <head>
                <meta charset="utf-8">
                <title>Se agendó la cita</title>
                <style media="screen">
                .contenedor * {
                    box-sizing: border-box;
                    font-family: arial;
                }
                .contenedor {
                    box-sizing: border-box;
                    width: 600px;
                    max-width: 100%;
                    background-color: #e6e6e6;
                    margin-top: 50px;
                    padding: 17px;
                }
                .mensaje {
                    width: 100%;
                    background-color: #f2f2f2;
                    display: inline-flex;
                    flex-direction: column;
                    align-items: center;
                    text-align: center;
                    font-size: 18px;
                    margin: 0;
                    padding: 5%;
                    color: #194271;
                }
                .logo_head {
                    width: 90%;
                    height: auto;
                }
                .texto_msg, .manifiesto_txt {
                    margin-top: 10px;
                }
                .texto_confirmacion {
                    font-size: 20px;
                }
                .texto_confirmacion, .texto_numero {
                    margin-top: 40px;
                }
                .link_aviso {
                    text-decoration: none;
                    margin-top: 30px;
                    font-size: 16px;
                    color: #194271;
                }
                .numero_confirmacion {
                    color: #007af6;
                    font-weight: bold;
                    font-size: 25px;
                }
                .manifiesto_txt {
                    color: #848484;
                    font-size: 13px;
                }
                .numero_tel {
                    font-weight: bold;
                }
                .numero_tel a {
                    text-decoration: none;
                    color: #194271;
                }
                span {
                    overflow-wrap: anywhere;
                }
                .izq{
                    width: 100%;
                    text-align: left;
                }
                .colorgris{
                    background-color: #f2f2f2;
                    padding: 5%;
                    color: #194271;
                }
                .txt_fecha_hora{
                    font-size: 18px;
                    color: #194271;
                }
                .btn_imprimir {
                    background-color: #194271;
                    border: solid 1px #194271;
                    border-radius: 6px;
                    color: #fff;
                    cursor: pointer;
                    padding: 5px 15px;
                    transition: all 1s;
                    text-decoration: none;
                }
                .btn_imprimir:hover {
                    background-color: #fff;
                    color: #194271;
                }
            </style>
            </head>
            <body>
                <div class="contenedor">
                    <table class="colorgris">
                        <tr class="mensaje">
                            <td>
                                <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                <br><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_msg izq txt_fecha_hora">Usted agendó una cita para el día ' . $fecha_cita . '</span>
                                <br>
                                <span class="texto_msg izq txt_fecha_hora">Horario: ' . $hora_inicial . " - " . $hora_final . '</span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Número de Confirmación: <span class="numero_confirmacion">' . $no_confirmacion . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">' . $num_pacientes . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Fecha y hora de registro: <span class="numero_confirmacion">' . $fecha_hora_registro . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Fecha y hora de pago: <span class="numero_confirmacion">' . $fecha_hora_pago . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Pagado: <span class="numero_confirmacion">$' . $total . '</span></span>
                                <br><br>
                            </td>
                        </tr>
                        ';

        $bandera = 0;

        foreach ($lista_fk_pacientes as $key => $dato) {

            $fk = $dato['paciente_id'];

            var_dump($datos_paciente);

            $datos_paciente = ControladorPaciente::ConsultaPaciente($fk);

            /** sintomas paciente **/

            $consulta_sintomas_paciente = ControladorSintomas::ctrConsultaSintomaPaciente($fk);

            $array_sintomas = [];
            $array_nombre_sintoma = []; //sintomas

            if (!empty($consulta_sintomas_paciente)) {

                $total_sintomas = sizeof($consulta_sintomas_paciente);

                foreach ($consulta_sintomas_paciente as $key => $value) {
                    array_push($array_sintomas, $value);
                }

                $contador_sintomas = 0;

                $sintomas_paciente = "";

                foreach ($array_sintomas as $value) {

                    $contador_sintomas++;

                    $consulta_nombre_sintoma = ControladorSintomas::ctrConsultaNombreSintoma($value[0]);
                    array_push($array_nombre_sintoma, $consulta_nombre_sintoma["nombre"]);

                    if ($total_sintomas == $contador_sintomas) {

                        if (!empty($otro_sintoma)) {
                            $sintomas_paciente .= $consulta_nombre_sintoma["nombre"] . ",";
                            $sintomas_paciente .= $otro_sintoma . ".";
                        } else {
                            $sintomas_paciente .= $consulta_nombre_sintoma["nombre"] . ".";
                        }
                    } else {
                        $sintomas_paciente .= $consulta_nombre_sintoma["nombre"] . ",";
                    }
                }
            } else {
                $sintomas_paciente = "Sin síntomas.";
            }

            /** enfermedades paciente **/

            $consulta_enfermedades_paciente = ControladorEnfermedades::ctrConsultaEmfermedadesPaciente($fk);

            $array_enfermedades = [];
            $array_nombre_emfermedad = []; //enfermedades

            if (!empty($consulta_enfermedades_paciente)) {

                echo "entro if";

                $total_emfermedades = sizeof($consulta_enfermedades_paciente);

                foreach ($consulta_enfermedades_paciente as $key => $value) {
                    array_push($array_enfermedades, $value["enfermedad_id"]);
                    echo $value["enfermedad_id"];
                }

                $contador_emfermedades = 0;
                $emfermedades_paciente = "";

                foreach ($array_enfermedades as $value) {

                    $contador_emfermedades++;

                    $consulta_nombre_emfermedad = ControladorEnfermedades::ctrConsultaNombreEmfermedad($value);
                    array_push($array_nombre_emfermedad, $consulta_nombre_emfermedad["nombre"]);

                    echo "nombre = " . $consulta_nombre_emfermedad["nombre"];

                    if ($total_emfermedades == $contador_emfermedades) {

                        if (!empty($otra_enfermedad)) {
                            $emfermedades_paciente .= $consulta_nombre_emfermedad["nombre"] . ",";
                            $emfermedades_paciente .= $otra_enfermedad . ".";
                        } else {
                            $emfermedades_paciente .= $consulta_nombre_emfermedad["nombre"] . ".";
                        }
                    } else {
                        $emfermedades_paciente .= $consulta_nombre_emfermedad["nombre"] . ",";
                    }
                }
            } else {
                $emfermedades_paciente = "Sin enfermedades.";
            }

            /** pruebas_paciente **/

            $consulta_pruebas_paciente = ControladorPruebas::ctrConsultaPruebasPaciente($fk);

            $array_pruebas = [];
            $array_nombre_prueba = []; //Pruebas

            if (!empty($consulta_pruebas_paciente)) {

                $total_pruebas = sizeof($consulta_pruebas_paciente);

                foreach ($consulta_pruebas_paciente as $key => $value) {
                    array_push($array_pruebas, $value["prueba_id"]);
                    echo $value["prueba_id"];
                }

                $contador_pruebas = 0;

                $pruebas_paciente = "";

                foreach ($array_pruebas as $value) {

                    $contador_pruebas++;

                    $consulta_nombre_prueba = ControladorPruebas::ctrConsultaNombrePruebas($value);
                    array_push($array_nombre_prueba, $consulta_nombre_prueba["nombre"]);

                    if ($total_pruebas == $contador_pruebas) {

                        if (!empty($otro_sintoma)) {
                            $pruebas_paciente .= $consulta_nombre_prueba["nombre"] . ",";
                            $pruebas_paciente .= $otro_prueba . ".";
                        } else {
                            $pruebas_paciente .= $consulta_nombre_prueba["nombre"] . ".";
                        }
                    } else {
                        $pruebas_paciente .= $consulta_nombre_prueba["nombre"] . ",";
                    }
                }
            } else {
                $pruebas_paciente = "Sin Pruebas.";
            }

            ////////////////////////////////////////////////////

            if ($bandera == 0) {
                $message_paciente .= '<hr>';
            }

            $message_paciente .= '<tr>
                                            <td>
                                                <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">' . $datos_paciente['nombre'] . ' ' . $datos_paciente['apellido_paterno'] . ' ' . $datos_paciente['apellido_materno'] . '</span></span></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">' . $datos_paciente['tel_celular'] . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">' . $datos_paciente['paciente_email'] . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Sintomas: </span><span class="numero_tel">' . $sintomas_paciente . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Enfermedades crónicas: </span><span class="numero_tel">' . $emfermedades_paciente . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?:  </span><span class="numero_tel">' . ($datos_paciente['contacto_persona_positivo'] == 1 ? 'Si' : 'No') . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">¿Está tomando algún antiviral?:  </span><span class="numero_tel">' . ($datos_paciente['antiviral'] == 1 ? 'Si' : 'No') . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><span class="numero_tel">' . ($datos_paciente['influenza'] == 1 ? 'Si' : 'No') . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Porque se realiza esta prueba: </span><span class="numero_tel">' . $pruebas_paciente . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;">
                                                <br>
                                                <a class="btn_imprimir" href="https://laboratoriocorregidora.com.mx/recibo.php?no_confirmacion=' . $no_confirmacion . '" target="_blank">
                                                    Imprimir Recibo
                                                </a>
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                        <br>
                                        <br>
                                        <hr>';

            $bandera++;
        }

        $message_paciente .= '
                        <tr>
                            <td>
                                <span class="texto_numero"><br>Dirección y link de ubicación de la sucursal a la que deben acudir para realizar la prueba: <span class="numero_tel">Avenida Prolongación Constituyentes No. 32 Oriente Calle Avenida Marqués de la Villa del Villar del Aguila, Altos del Marques, 76140 Santiago de Querétaro, Qro.</span> <br><br> <a href="https://goo.gl/maps/ibT9atD4xPkXeAxy8" target="_blank">https://goo.gl/maps/ibT9atD4xPkXeAxy8</a> </span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td><br>
                                <span class="texto_numero">Indicaciones de como deben presentarse para realizar la prueba: </span> <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ul> 

                                    <li style="text-align: left;">DEBERÁ ACUDIR A SU TOMA DE MUESTRA EN EL LABORATORIO MATRIZ (MARQUÉS).</li>

                                    <br> 

                                    <li style="text-align: left;">PCR: Acudir con ayuno mínimo de 4 horas de alimentos sólidos. No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra, esta prueba se debe de hacer a partir del día 3 en adelante en que el paciente presenta los síntomas o que estuvo en contacto directo con una persona COVID- 19 Positivo.</li>

                                    <br> 

                                    <li style="text-align: left;">ANTIGENOS: No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra. esta prueba debe de hacerse durante los primeros 7 días de haberse presentado por lo menos dos síntomas sugestivos a COVID-19.</li> <br>
                                </ul> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">442 2121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                                <br><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                                <br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
        </html>';

        echo  $message_paciente;

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora <noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to, $subject_reservacion_paciente, $message_paciente, $headers)) {
            echo 'llega correo paciente';
        } else {
            echo 'no llega correo paciente';
        }

        $subject_reservacion_admin = "Reservación Laboratorio Corregidora";
        $message_admin .= '<html>
            <head>
                <meta charset="utf-8">
                <title>Se agendó la cita</title>
                <style media="screen">
                .contenedor * {
                    box-sizing: border-box;
                    font-family: arial;
                }
                .contenedor {
                    box-sizing: border-box;
                    width: 600px;
                    max-width: 100%;
                    background-color: #e6e6e6;
                    margin-top: 50px;
                    padding: 17px;
                }
                .mensaje {
                    width: 100%;
                    background-color: #f2f2f2;
                    display: inline-flex;
                    flex-direction: column;
                    align-items: center;
                    text-align: center;
                    font-size: 18px;
                    margin: 0;
                    padding: 5%;
                    color: #194271;
                }
                .logo_head {
                    width: 90%;
                    height: auto;
                }
                .texto_msg, .manifiesto_txt {
                    margin-top: 10px;
                }
                .texto_confirmacion {
                    font-size: 20px;
                }
                .texto_confirmacion, .texto_numero {
                    margin-top: 40px;
                }
                .link_aviso {
                    text-decoration: none;
                    margin-top: 30px;
                    font-size: 16px;
                    color: #194271;
                }
                .numero_confirmacion {
                    color: #007af6;
                    font-weight: bold;
                    font-size: 25px;
                }
                .manifiesto_txt {
                    color: #848484;
                    font-size: 13px;
                }
                .numero_tel {
                    font-weight: bold;
                }
                .numero_tel a {
                    text-decoration: none;
                    color: #194271;
                }
                span {
                    overflow-wrap: anywhere;
                }
                .izq{
                    width: 100%;
                    text-align: left;
                }
                .colorgris{
                    background-color: #f2f2f2;
                    padding: 5%;
                    color: #194271;
                }
                .txt_fecha_hora{
                    font-size: 18px;
                    color: #194271;
                }
                .btn_imprimir {
                    background-color: #194271;
                    border: solid 1px #194271;
                    border-radius: 6px;
                    color: #fff;
                    cursor: pointer;
                    padding: 5px 15px;
                    transition: all 1s;
                    text-decoration: none;
                }
                .btn_imprimir:hover {
                    background-color: #fff;
                    color: #194271;
                }
            </style>
            </head>
            <body>
                <div class="contenedor">
                    <table class="colorgris">
                        <tr class="mensaje">
                            <td>
                                <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                <br><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_msg izq txt_fecha_hora">Se agendó una cita para el día ' . $fecha_cita . '</span>
                                <br>
                                <span class="texto_msg izq txt_fecha_hora">Horario: ' . $hora_inicial . " - " . $hora_final . '</span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Número de Confirmación: <span class="numero_confirmacion">' . $no_confirmacion . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">' . $num_pacientes . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Fecha y hora de registro: <span class="numero_confirmacion">' . $fecha_hora_registro . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Fecha y hora de pago: <span class="numero_confirmacion">' . $fecha_hora_pago . '</span></span>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="texto_confirmacion izq">Pagado: <span class="numero_confirmacion">$'.$total.'</span></span>
                                <br><br>
                            </td>
                        </tr>
                        <br>
                       ';

        $bandera = 0;

        foreach ($lista_fk_pacientes as $key => $dato) {

            $fk = $dato['paciente_id'];

            $datos_paciente = ControladorPaciente::ConsultaPaciente($fk);

            var_dump($datos_paciente);

            /** sintomas paciente **/

            $consulta_sintomas_paciente = ControladorSintomas::ctrConsultaSintomaPaciente($fk);

            $array_sintomas = [];
            $array_nombre_sintoma = []; //sintomas

            if (!empty($consulta_sintomas_paciente)) {

                $total_sintomas = sizeof($consulta_sintomas_paciente);

                foreach ($consulta_sintomas_paciente as $key => $value) {
                    array_push($array_sintomas, $value);
                }

                $contador_sintomas = 0;

                $sintomas_paciente = "";

                foreach ($array_sintomas as $value) {

                    $contador_sintomas++;

                    $consulta_nombre_sintoma = ControladorSintomas::ctrConsultaNombreSintoma($value[0]);
                    array_push($array_nombre_sintoma, $consulta_nombre_sintoma["nombre"]);

                    if ($total_sintomas == $contador_sintomas) {

                        if (!empty($otro_sintoma)) {
                            $sintomas_paciente .= $consulta_nombre_sintoma["nombre"] . ",";
                            $sintomas_paciente .= $otro_sintoma . ".";
                        } else {
                            $sintomas_paciente .= $consulta_nombre_sintoma["nombre"] . ".";
                        }
                    } else {
                        $sintomas_paciente .= $consulta_nombre_sintoma["nombre"] . ",";
                    }
                }
            } else {
                $sintomas_paciente = "Sin síntomas.";
            }

            /** enfermedades paciente **/

            $consulta_enfermedades_paciente = ControladorEnfermedades::ctrConsultaEmfermedadesPaciente($fk);

            $array_enfermedades = [];
            $array_nombre_emfermedad = []; //enfermedades

            if (!empty($consulta_enfermedades_paciente)) {

                $total_emfermedades = sizeof($consulta_enfermedades_paciente);

                foreach ($consulta_enfermedades_paciente as $key => $value) {
                    array_push($array_enfermedades, $value["enfermedad_id"]);
                    //echo $value["fk_enferemdad"];
                }

                $contador_emfermedades = 0;
                $emfermedades_paciente = "";

                foreach ($array_enfermedades as $value) {

                    $contador_emfermedades++;

                    $consulta_nombre_emfermedad = ControladorEnfermedades::ctrConsultaNombreEmfermedad($value);
                    array_push($array_nombre_emfermedad, $consulta_nombre_emfermedad["nombre"]);

                    //echo "nombre = ".$consulta_nombre_emfermedad["nombre"];

                    if ($total_emfermedades == $contador_emfermedades) {

                        if (!empty($otra_enfermedad)) {
                            $emfermedades_paciente .= $consulta_nombre_emfermedad["nombre"] . ",";
                            $emfermedades_paciente .= $otra_enfermedad . ".";
                        } else {
                            $emfermedades_paciente .= $consulta_nombre_emfermedad["nombre"] . ".";
                        }
                    } else {
                        $emfermedades_paciente .= $consulta_nombre_emfermedad["nombre"] . ",";
                    }
                }
            } else {
                $emfermedades_paciente = "Sin enfermedades.";
            }

            /** pruebas_paciente **/

            $consulta_pruebas_paciente = ControladorPruebas::ctrConsultaPruebasPaciente($fk);

            $array_pruebas = [];
            $array_nombre_prueba = []; //Pruebas

            if (!empty($consulta_pruebas_paciente)) {

                $total_pruebas = sizeof($consulta_pruebas_paciente);

                foreach ($consulta_pruebas_paciente as $key => $value) {
                    array_push($array_pruebas, $value["prueba_id"]);
                    // echo $value["fk_prueba"];
                }

                $contador_pruebas = 0;

                $pruebas_paciente = "";

                foreach ($array_pruebas as $value) {

                    $contador_pruebas++;

                    $consulta_nombre_prueba = ControladorPruebas::ctrConsultaNombrePruebas($value);
                    array_push($array_nombre_prueba, $consulta_nombre_prueba["nombre"]);

                    if ($total_pruebas == $contador_pruebas) {

                        if (!empty($otro_sintoma)) {
                            $pruebas_paciente .= $consulta_nombre_prueba["nombre"] . ",";
                            $pruebas_paciente .= $otro_prueba . ".";
                        } else {
                            $pruebas_paciente .= $consulta_nombre_prueba["nombre"] . ".";
                        }
                    } else {
                        $pruebas_paciente .= $consulta_nombre_prueba["nombre"] . ",";
                    }
                }
            } else {
                $pruebas_paciente = "Sin Pruebas.";
            }

            ////////////////////////////////////////////////////

            if ($bandera == 0) {
                $message_admin .= '<hr>';
            }

            $message_admin .= '<tr>
                                            <td>
                                                <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">' . $datos_paciente["nombre"] . ' ' . $datos_paciente["apellido_paterno"] . ' ' . $datos_paciente["apellido_materno"] . '</span></span></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">' . $datos_paciente["tel_celular"] . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">' . $datos_paciente["paciente_email"] . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Sintomas: </span><span class="numero_tel">' . $sintomas_paciente . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Enfermedades crónicas: </span><span class="numero_tel">' . $emfermedades_paciente . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?:  </span><span class="numero_tel">' . ($datos_paciente["contacto_persona_positivo"] == 1 ? 'Si' : 'No') . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">¿Está tomando algún antiviral?:  </span><span class="numero_tel">' . ($datos_paciente["antiviral"] == 1 ? 'Si' : 'No') . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><span class="numero_tel">' . ($datos_paciente["influenza"] == 1 ? 'Si' : 'No') . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="pad_top"><span class="azul_sel">Porque se realiza esta prueba: </span><span class="numero_tel">' . $pruebas_paciente . '</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;">
                                                <br>
                                                <a class="btn_imprimir" href="https://laboratoriocorregidora.com.mx/recibo.php?no_confirmacion=' . $no_confirmacion . '" target="_blank">
                                                    Imprimir Recibo
                                                </a>
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                        <br>
                                        <br>
                                        <hr>';
            $bandera++;
        }

        $message_admin .= '<tr>
                            <td>
                                <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">442 2121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                                <br><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                                <br><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
        </html>';
        echo $message_admin;
        if (mail($to_admin_all, $subject_reservacion_admin, $message_admin, $headers)) {
        } else {
        }
    } else {
        $no_confirmacion = $xml->datos_adicionales->data[0]->value;
        $nombre = $xml->datos_adicionales->data[1]->value;
        $telefono = $xml->datos_adicionales->data[2]->value;
        $estudio = $xml->datos_adicionales->data[3]->value;
        $total_pre = (int)$xml->datos_adicionales->data[4]->value;
        $total = number_format($total_pre, 2);
        $tipo_pago = $xml->datos_adicionales->data[5]->value;

        $estudio = explode('@', $estudio);





        $sql_orden = "UPDATE orden SET estatus = 1, response = '$response', cadena = '$originalString', tipo_tarjeta = '$tipo_tarjeta', digitos = '$cuatro_digitos_tarjeta', updated_at = CURRENT_TIMESTAMP WHERE no_confirmacion = '$no_confirmacion'";
        echo $sql_orden;
        if (mysqli_query($dbCon, $sql_orden)) {
        }

        $to = $correo;

        $subject = "Pago Análisis Laboratorio Corregidora";

        $message .= '
            <html>
                <head>
                    <meta charset="utf-8">
                    <title>Se registro el pago</title>
                    <style media="screen">
                
                    .contenedor * {
                    box-sizing: border-box;
                    font-family: arial;
                    }
                
                    .contenedor {
                    box-sizing: border-box;
                    width: 600px;
                    max-width: 100%;
                    background-color: #e6e6e6;
                    margin-top: 50px;
                    padding: 17px;
                    }
                
                    .mensaje {
                    width: 100%;
                    background-color: #f2f2f2;
                    display: inline-flex;
                    flex-direction: column;
                    align-items: center;
                    text-align: center;
                    font-size: 18px;
                    margin: 0;
                    padding: 5%;
                    color: #194271;
                    }
                
                    .logo_head {
                    width: 90%;
                    height: auto;
                    }
                    .texto_msg, .manifiesto_txt {
                    margin-top: 80px;
                    }
                    .texto_confirmacion {
                    font-size: 20px;
                    }
                    .texto_confirmacion, .texto_numero {
                    margin-top: 40px;
                    }
                    .link_aviso {
                    text-decoration: none;
                    margin-top: 30px;
                    font-size: 16px;
                    color: #194271;
                    }
                    .numero_confirmacion {
                    color: #007af6;
                    font-weight: bold;
                    font-size: 25px;
                    }
                    .manifiesto_txt {
                    color: #848484;
                    font-size: 13px;
                    }
                    .numero_tel {
                    font-weight: bold;
                    }
                    .numero_tel a {
                    text-decoration: none;
                    color: #194271;
                    }
                    span {
                    overflow-wrap: anywhere;
                    }
                    .border{
                        border: solid 1px ;
                        text-align:left;
                        padding:10px;
                    }
                    </style>
                </head>
                <body>
                    <div class="contenedor">
                        <table class="mensaje">
                            <tr>
                                <td>
                                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png"><br><br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_msg">Se realizo el cobro de los siguientes estudios:</span>
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td class="texto_confirmacion"> Cantidad y Nombre:
                                </td>
                            </tr>';
        for ($i = 0; $i < count(array_filter($estudio)); $i++) {

            $message .= '
                                <tr>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border">
                                    ' . $estudio[$i] . '
                                    </td>
                                </tr>';
        }

        $message .= '
                                <tr>
                                    <td>
                                    <span class="texto_confirmacion"><br>Número de Confirmación: <span class="numero_confirmacion">' . $no_confirmacion . '</span></span>
                                    <br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <span class="texto_confirmacion">Pagado: <span class="numero_confirmacion">$' . $total . '</span></span>
                                    <br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">4422121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                                    <br><br><br>
                                    </td>
                                </tr>
                                    <tr>
                                    <td>
                                        <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                                        <br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                                    </td>
                                </tr>
                        </table>
                    </div>
                </body>
            </html>';


        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to, $subject, $message, $headers)) {
        } else {
        }

        $message .=  '
                <html>
                    <head>
                        <meta charset="utf-8">
                        <title>Se compraron los siguientes estudios</title>
                        <style media="screen">
                            .contenedor * {
                                box-sizing: border-box;
                                font-family: arial;
                            }
                            .contenedor {
                                box-sizing: border-box;
                                width: 600px;
                                max-width: 100%;
                                background-color: #e6e6e6;
                                margin-top: 50px;
                                padding: 17px;
                            }
                            .mensaje {
                                width: 100%;
                                background-color: #f2f2f2;
                                display: inline-flex;
                                flex-direction: column;
                                align-items: center;
                                text-align: center;
                                font-size: 18px;
                                margin: 0;
                                padding: 5%;
                                color: #194271;
                            }
                            .logo_head {
                                width: 90%;
                                height: auto;
                            }
                            .texto_msg, .manifiesto_txt {
                                margin-top: 80px;
                            }
                            .texto_confirmacion {
                                font-size: 20px;
                            }
                            .texto_confirmacion, .texto_numero {
                                margin-top: 40px;
                            }
                            .link_aviso {
                                text-decoration: none;
                                margin-top: 30px;
                                font-size: 16px;
                                color: #194271;
                            }
                            .numero_confirmacion {
                                color: #007af6;
                                font-weight: bold;
                                font-size: 25px;
                            }
                            .manifiesto_txt {
                                color: #848484;
                                font-size: 13px;
                            }
                            .numero_tel {
                                font-weight: bold;
                            }
                            .numero_tel a {
                                text-decoration: none;
                                color: #194271;
                            }
                            span {
                                overflow-wrap: anywhere;
                            }
                            .border{
                                border: solid 1px ;
                                text-align:left;
                                padding:10px;
                            }
                        </style>
                    </head>
                    <body>
                        <div class="contenedor">
                        <table class="mensaje">
                            <tr>
                                <td>
                                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                    <br><br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_msg">Se realizo el pago de los siguientes estudios:
                                    <br></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                            <td class="texto_confirmacion"> Cantidad y Nombre:
                            </td>
                            </tr><br>';
        for ($i = 0; $i < count(array_filter($estudio)); $i++) {

            $message .= '
                                <tr>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border">
                                    ' . $estudio[$i] . '
                                    </td>
                                </tr>
                                ';
        }

        $message .= '
                            <tr>
                                <td>
                                    <span class="texto_confirmacion"><br>Número de Confirmación: <span class="numero_confirmacion">' . $no_confirmacion . '</span></span>
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion">Total Pagado: <span class="numero_confirmacion">$' . $total . '</span></span>
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                            <td>
                                <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">' . $nombre . '</span></span></label>
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <label><span class="azul_sel bold_c" style="text-transform: uppercase;">correo: <span class="numero_tel">' . $correo . '</span></span></label>
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Telefono: <span class="numero_tel">' . $telefono . '</span></span></label>
                            </td>
                            </tr>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_numero"><br>Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">442 2121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                                    <br><br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    </body>
                </html>';
        echo $message;

        if (mail($to_admin, $subject, $message, $headers)) {
        }
        echo "respuesta aprovado";
    }
} else if ($response == 'denied') { //=======================  PAGO DENEGADO
    if ($tipo_pago != '2') {

        $sql_paciente = "SELECT paciente_id FROM citas WHERE no_confirmacion = '$idPago'";
        $res_paciente = query_bd($sql_paciente);
        $fk_paciente_fetch = mysqli_fetch_array($res_paciente);

        $no_confirmacion = $idPago;

        /** nuevo */
        $no_confirmacion = $idPago;
        $info_cita = ControladorCita::ctrBuscarCita($idPago);
        $fecha_hora_registro = date('d/m/Y G:ia', strtotime($info_cita['created_at']));
        $fecha_hora_pago = date('d/m/Y G:ia', strtotime($info_cita['updated_at']));
        $lista_fk_pacientes = ControladorPaciente::ConsultaPacientesNoConfirmacion($idPago);
        var_dump($lista_fk_pacientes);
        $datos_paciente = ControladorPaciente::ConsultaPaciente((int)$lista_fk_pacientes[0]);

        $monto = number_format($info_cita['monto'],2);
        $tipo_prueba = $info_cita['tipo_prueba'] == 1 ? 'PCR': 'ANTGENOS';

        /** fin nuevo */

        $subject_pago_denegado_paciente = "SU PAGO DENEGADO - LABORATORIO CORREGIDORA";

        $message_paciente_denied = '<html>
                <head>
                    <meta charset="utf-8">
                    <title>Pago denegado</title>
                    <style media="screen">
                    .contenedor * {
                        box-sizing: border-box;
                        font-family: arial;
                    }
                    .contenedor {
                        box-sizing: border-box;
                        width: 600px;
                        max-width: 100%;
                        background-color: #e6e6e6;
                        margin-top: 50px;
                        padding: 17px;
                    }
                    .mensaje {
                        width: 100%;
                        background-color: #f2f2f2;
                        display: inline-flex;
                        flex-direction: column;
                        align-items: center;
                        text-align: center;
                        font-size: 18px;
                        margin: 0;
                        padding: 5%;
                        color: #194271;
                    }
                    .logo_head {
                        width: 90%;
                        height: auto;
                    }
                    .texto_msg, .manifiesto_txt {
                        margin-top: 80px;
                    }
                    .link_aviso {
                        text-decoration: none;
                        margin-top: 30px;
                        font-size: 16px;
                        color: #194271;
                    }
                    .manifiesto_txt {
                        color: #848484;
                        font-size: 13px;
                    }
                    span {
                        overflow-wrap: anywhere;
                    }
                </style>
                </head>
                <body>
                    <div class="contenedor">
                        <table class="mensaje">
                            <tr>
                                <td>
                                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                    <br><br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_msg">SU PAGO HA SIDO DENEGADO POR SU BANCO.<br>
                                    <br></span>
                                </td>
                            </tr>

                           
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">' . $num_pacientes . '</span></span>
                                    <br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Tipó prueba: </span> <span class="numero_tel">' . $tipo_prueba . '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">' . $datos_paciente['nombre_completo'] . '</span></span></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">' . $datos_paciente['tel_celular'] . '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">' . $datos_paciente['paciente_email'] . '</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de registro: <span class="numero_confirmacion">' . $fecha_hora_registro . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de pago: <span class="numero_confirmacion">' . $fecha_hora_pago . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Monto: <span class="numero_confirmacion">$' . $monto . '</span></span>
                                    <br><br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </body>
            </html>
            ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        echo $message_paciente_denied;

        // if (mail($to, $subject_pago_denegado_paciente, $message_paciente_denied, $headers)) {
        // } else {
        // }

        /////////////

        
        /** nuevo */
        $no_confirmacion = $idPago;
        $info_cita = ControladorCita::ctrBuscarCita($idPago);
        $fecha_hora_registro = date('d/m/Y G:ia', strtotime($info_cita['created_at']));
        $fecha_hora_pago = date('d/m/Y G:ia', strtotime($info_cita['updated_at']));
        $lista_fk_pacientes = ControladorPaciente::ConsultaPacientesNoConfirmacion($idPago);
        var_dump($lista_fk_pacientes);
        $datos_paciente = ControladorPaciente::ConsultaPaciente((int)$lista_fk_pacientes[0]);

        $monto = number_format($info_cita['monto'],2);
        $tipo_prueba = $info_cita['tipo_prueba'] == 1 ? 'PCR': 'ANTGENOS';

        /** fin nuevo */

        $subject_pago_denegado_admin = "PAGO DENEGADO - LABORATORIO CORREGIDORA";

        $message_admin_denied_admin_denied = '<html>
                <head>
                    <meta charset="utf-8">
                    <title>Un pago ha sido denegado.</title>
                    <style media="screen">
                    .contenedor * {
                        box-sizing: border-box;
                        font-family: arial;
                    }
                    .contenedor {
                        box-sizing: border-box;
                        width: 600px;
                        max-width: 100%;
                        background-color: #e6e6e6;
                        margin-top: 50px;
                        padding: 17px;
                    }
                    .mensaje {
                        width: 100%;
                        background-color: #f2f2f2;
                        display: inline-flex;
                        flex-direction: column;
                        align-items: center;
                        text-align: center;
                        font-size: 18px;
                        margin: 0;
                        padding: 5%;
                        color: #194271;
                    }
                    .logo_head {
                        width: 90%;
                        height: auto;
                    }
                    .texto_msg, .manifiesto_txt {
                        margin-top: 80px;
                    }
                    .link_aviso {
                        text-decoration: none;
                        margin-top: 30px;
                        font-size: 16px;
                        color: #194271;
                    }
                    .manifiesto_txt {
                        color: #848484;
                        font-size: 13px;
                    }
                    span {
                        overflow-wrap: anywhere;
                    }
                </style>
                </head>
                <body>
                    <div class="contenedor">
                        <table class="mensaje">
                            <tr>
                                <td>
                                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                    <br><br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_msg">UN PAGO HA SIDO DENEGADO.<br>
                                    <br></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">' . $num_pacientes . '</span></span>
                                    <br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Tipó prueba: </span> <span class="numero_tel">' . $tipo_prueba . '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">' . $datos_paciente['nombre_completo'] . '</span></span></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">' . $datos_paciente['tel_celular'] . '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">' . $datos_paciente['paciente_email'] . '</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de registro: <span class="numero_confirmacion">' . $fecha_hora_registro . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de pago: <span class="numero_confirmacion">' . $fecha_hora_pago . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Monto: <span class="numero_confirmacion">$' . $monto . '</span></span>
                                    <br><br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </body>
            </html>
            ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        echo $message_admin_denied_admin_denied;

        echo "llego";

        // if (mail($to_admin, $subject_pago_denegado_admin, $message_admin_denied_admin_denied, $headers)) {
        // } else {
        // }

    } else {
        $no_confirmacion = $xml->datos_adicionales->data[0]->value;
        $nombre = $xml->datos_adicionales->data[1]->value;
        $telefono = $xml->datos_adicionales->data[2]->value;
        $estudio = $xml->datos_adicionales->data[3]->value;
        $total = $xml->datos_adicionales->data[4]->value;
        $tipo_pago = $xml->datos_adicionales->data[5]->value;

        $sql_orden = "UPDATE orden SET estatus = 0, response = '$response', cadena = '$originalString', tipo_tarjeta = '$tipo_tarjeta', digitos = '$cuatro_digitos_tarjeta',  updated_at = CURRENT_TIMESTAMP WHERE no_confirmacion = '$no_confirmacion'";
        if (mysqli_query($dbCon, $sql_orden)) {
        }


        $to = $correo;


        $subject = "PAGO DE ANALISIS DENEGADO - LABORATORIO CORREGIDORA";

        $message = '<html>
                    <head>
                        <meta charset="utf-8">
                        <title>Pago denegado</title>
                        <style media="screen">
                        .contenedor * {
                            box-sizing: border-box;
                            font-family: arial;
                        }
                        .contenedor {
                            box-sizing: border-box;
                            width: 600px;
                            max-width: 100%;
                            background-color: #e6e6e6;
                            margin-top: 50px;
                            padding: 17px;
                        }
                        .mensaje {
                            width: 100%;
                            background-color: #f2f2f2;
                            display: inline-flex;
                            flex-direction: column;
                            align-items: center;
                            text-align: center;
                            font-size: 18px;
                            margin: 0;
                            padding: 5%;
                            color: #194271;
                        }
                        .logo_head {
                            width: 90%;
                            height: auto;
                        }
                        .texto_msg, .manifiesto_txt {
                            margin-top: 80px;
                        }
                        .link_aviso {
                            text-decoration: none;
                            margin-top: 30px;
                            font-size: 16px;
                            color: #194271;
                        }
                        .manifiesto_txt {
                            color: #848484;
                            font-size: 13px;
                        }
                        span {
                            overflow-wrap: anywhere;
                        }
                    </style>
                    </head>
                    <body>
                        <div class="contenedor">
                            <table class="mensaje">
                                <tr>
                                    <td>
                                        <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                        <br><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="texto_msg">SU PAGO HA SIDO DENEGADO POR SU BANCO.<br>
                                        <br></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </body>
                </html>
                ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to, $subject, $message, $headers)) {
        }
        /////////////

        $subject = "PAGO ANALISIS DENEGADO - LABORATORIO CORREGIDORA";

        $message = '<html>
                    <head>
                        <meta charset="utf-8">
                        <title>Un pago ha sido denegado.</title>
                        <style media="screen">
                        .contenedor * {
                            box-sizing: border-box;
                            font-family: arial;
                        }
                        .contenedor {
                            box-sizing: border-box;
                            width: 600px;
                            max-width: 100%;
                            background-color: #e6e6e6;
                            margin-top: 50px;
                            padding: 17px;
                        }
                        .mensaje {
                            width: 100%;
                            background-color: #f2f2f2;
                            display: inline-flex;
                            flex-direction: column;
                            align-items: center;
                            text-align: center;
                            font-size: 18px;
                            margin: 0;
                            padding: 5%;
                            color: #194271;
                        }
                        .logo_head {
                            width: 90%;
                            height: auto;
                        }
                        .texto_msg, .manifiesto_txt {
                            margin-top: 80px;
                        }
                        .link_aviso {
                            text-decoration: none;
                            margin-top: 30px;
                            font-size: 16px;
                            color: #194271;
                        }
                        .manifiesto_txt {
                            color: #848484;
                            font-size: 13px;
                        }
                        span {
                            overflow-wrap: anywhere;
                        }
                    </style>
                    </head>
                    <body>
                        <div class="contenedor">
                            <table class="mensaje">
                                <tr>
                                    <td>
                                        <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                        <br><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="texto_msg">UN PAGO DE ANALISIS HA SIDO DENEGADO POR SU BANCO.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </body>
                </html>
                ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to_admin, $subject, $message, $headers)) {
        }
    }
} else {
    if ($tipo_pago != '2') {
        echo "pago rechazado error";

        $sql_paciente = "SELECT paciente_id FROM citas WHERE no_confirmacion = '$idPago'";
        $res_paciente = query_bd($sql_paciente);
        $fk_paciente_fetch = mysqli_fetch_array($res_paciente);


        $no_confirmacion = $idPago;

        /** nuevo */
        $no_confirmacion = $idPago;
        $info_cita = ControladorCita::ctrBuscarCita($idPago);
        $fecha_hora_registro = date('d/m/Y G:ia', strtotime($info_cita['created_at']));
        $fecha_hora_pago = date('d/m/Y G:ia', strtotime($info_cita['updated_at']));
        $lista_fk_pacientes = ControladorPaciente::ConsultaPacientesNoConfirmacion($idPago);
        var_dump($lista_fk_pacientes);
        $datos_paciente = ControladorPaciente::ConsultaPaciente((int)$lista_fk_pacientes[0]);

        $monto = number_format($info_cita['monto'],2);
        $tipo_prueba = $info_cita['tipo_prueba'] == 1 ? 'PCR': 'ANTGENOS';

        /** fin nuevo */

        $subject_error_pago_paciente = "SU PAGO RECHAZADO - LABORATORIO CORREGIDORA";

        $message_paciente_error = '<html>
                <head>
                    <meta charset="utf-8">
                    <title>Pago rechazado - error</title>
                    <style media="screen">
                    .contenedor * {
                        box-sizing: border-box;
                        font-family: arial;
                    }
                    .contenedor {
                        box-sizing: border-box;
                        width: 600px;
                        max-width: 100%;
                        background-color: #e6e6e6;
                        margin-top: 50px;
                        padding: 17px;
                    }
                    .mensaje {
                        width: 100%;
                        background-color: #f2f2f2;
                        display: inline-flex;
                        flex-direction: column;
                        align-items: center;
                        text-align: center;
                        font-size: 18px;
                        margin: 0;
                        padding: 5%;
                        color: #194271;
                    }
                    .logo_head {
                        width: 90%;
                        height: auto;
                    }
                    .texto_msg, .manifiesto_txt {
                        margin-top: 80px;
                    }
                    .link_aviso {
                        text-decoration: none;
                        margin-top: 30px;
                        font-size: 16px;
                        color: #194271;
                    }
                    .manifiesto_txt {
                        color: #848484;
                        font-size: 13px;
                    }
                    span {
                        overflow-wrap: anywhere;
                    }
                </style>
                </head>
                <body>
                    <div class="contenedor">
                        <table class="mensaje">
                            <tr>
                                <td>
                                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                    <br><br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_msg">SU PAGO HA SIDO RECHAZADO POR SU BANCO.<br>
                                    <br></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">' . $num_pacientes . '</span></span>
                                    <br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Tipó prueba: </span> <span class="numero_tel">' . $tipo_prueba . '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">' . $datos_paciente['nombre_completo'] . '</span></span></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">' . $datos_paciente['tel_celular'] . '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">' . $datos_paciente['paciente_email'] . '</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de registro: <span class="numero_confirmacion">' . $fecha_hora_registro . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de pago: <span class="numero_confirmacion">' . $fecha_hora_pago . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Monto: <span class="numero_confirmacion">$' . $monto . '</span></span>
                                    <br><br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </body>
            </html>
            ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to, $subject_error_pago_paciente, $message_paciente_error, $headers)) {
        }

        ////////////////////

         /** nuevo */
         $no_confirmacion = $info_cita;
         $info_cita = ControladorCita::ctrBuscarCita($idPago);
         $fecha_hora_registro = date('d/m/Y G:ia', strtotime($info_cita['created_at']));
         $fecha_hora_pago = date('d/m/Y G:ia', strtotime($info_cita['updated_at']));
         $lista_fk_pacientes = ControladorPaciente::ConsultaPacientesNoConfirmacion($idPago);
         var_dump($lista_fk_pacientes);
 
         /** fin nuevo */

        $subject_error_pago_admin = "PAGO RECHAZADO - LABORATORIO CORREGIDORA";

        $message_admin_error = '<html>
                <head>
                    <meta charset="utf-8">
                    <title>Un pago ha sido rechazado</title>
                    <style media="screen">
                    .contenedor * {
                        box-sizing: border-box;
                        font-family: arial;
                    }
                    .contenedor {
                        box-sizing: border-box;
                        width: 600px;
                        max-width: 100%;
                        background-color: #e6e6e6;
                        margin-top: 50px;
                        padding: 17px;
                    }
                    .mensaje {
                        width: 100%;
                        background-color: #f2f2f2;
                        display: inline-flex;
                        flex-direction: column;
                        align-items: center;
                        text-align: center;
                        font-size: 18px;
                        margin: 0;
                        padding: 5%;
                        color: #194271;
                    }
                    .logo_head {
                        width: 90%;
                        height: auto;
                    }
                    .texto_msg, .manifiesto_txt {
                        margin-top: 80px;
                    }
                    .link_aviso {
                        text-decoration: none;
                        margin-top: 30px;
                        font-size: 16px;
                        color: #194271;
                    }
                    .manifiesto_txt {
                        color: #848484;
                        font-size: 13px;
                    }
                    span {
                        overflow-wrap: anywhere;
                    }
                </style>
                </head>
                <body>
                    <div class="contenedor">
                        <table class="mensaje">
                            <tr>
                                <td>
                                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                    <br><br><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_msg">UN PAGO HA SIDO RECHAZADO.<br>
                                    <br></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion">' . $num_pacientes . '</span></span>
                                    <br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Tipó prueba: </span> <span class="numero_tel">' . $info_cita['tipo_prueba'] == 1 ? 'PCR': 'ANTIGENOS'. '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel">' . $datos_paciente['nombre'] . ' ' . $datos_paciente['apellido_paterno'] . ' ' . $datos_paciente['apellido_materno'] . '</span></span></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel">' . $datos_paciente['tel_celular'] . '</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel">' . $datos_paciente['paciente_email'] . '</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de registro: <span class="numero_confirmacion">' . $fecha_hora_registro . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Fecha y hora de pago: <span class="numero_confirmacion">' . $fecha_hora_pago . '</span></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="texto_confirmacion izq">Monto: <span class="numero_confirmacion">$' . $total . '</span></span>
                                    <br><br>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="manifiesto_txt"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </body>
            </html>
            ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to_admin, $subject_error_pago_admin, $message_admin_error, $headers)) {
        }
    } else {
        $no_confirmacion = $xml->datos_adicionales->data[0]->value;
        $nombre = $xml->datos_adicionales->data[1]->value;
        $telefono = $xml->datos_adicionales->data[2]->value;
        $estudio = $xml->datos_adicionales->data[3]->value;
        $total = $xml->datos_adicionales->data[4]->value;
        $tipo_pago = $xml->datos_adicionales->data[5]->value;

        $sql_orden = "UPDATE orden SET estatus = 0, response = '$response', cadena = '$originalString', tipo_tarjeta = '$tipo_tarjeta', digitos = '$cuatro_digitos_tarjeta', update_at = CURRENT_TIMESTAMP WHERE no_confirmacion = '$no_confirmacion'";

        $to = $correo;


        $subject = "PAGO DE ANALISIS DENEGADO - LABORATORIO CORREGIDORA";

        $message = '<html>
                    <head>
                        <meta charset="utf-8">
                        <title>Pago denegado</title>
                        <style media="screen">
                        .contenedor * {
                            box-sizing: border-box;
                            font-family: arial;
                        }
                        .contenedor {
                            box-sizing: border-box;
                            width: 600px;
                            max-width: 100%;
                            background-color: #e6e6e6;
                            margin-top: 50px;
                            padding: 17px;
                        }
                        .mensaje {
                            width: 100%;
                            background-color: #f2f2f2;
                            display: inline-flex;
                            flex-direction: column;
                            align-items: center;
                            text-align: center;
                            font-size: 18px;
                            margin: 0;
                            padding: 5%;
                            color: #194271;
                        }
                        .logo_head {
                            width: 90%;
                            height: auto;
                        }
                        .texto_msg, .manifiesto_txt {
                            margin-top: 80px;
                        }
                        .link_aviso {
                            text-decoration: none;
                            margin-top: 30px;
                            font-size: 16px;
                            color: #194271;
                        }
                        .manifiesto_txt {
                            color: #848484;
                            font-size: 13px;
                        }
                        span {
                            overflow-wrap: anywhere;
                        }
                    </style>
                    </head>
                    <body>
                        <div class="contenedor">
                            <table class="mensaje">
                                <tr>
                                    <td>
                                        <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                        <br><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="texto_msg">SU PAGO HA SIDO DENEGADO POR SU BANCO.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </body>
                </html>
                ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to, $subject, $message, $headers)) {
        } else {
        }

        /////////////

        $subject = "PAGO ANALISIS DENEGADO - LABORATORIO CORREGIDORA";

        $message = '<html>
                    <head>
                        <meta charset="utf-8">
                        <title>Un pago ha sido denegado.</title>
                        <style media="screen">
                        .contenedor * {
                            box-sizing: border-box;
                            font-family: arial;
                        }
                        .contenedor {
                            box-sizing: border-box;
                            width: 600px;
                            max-width: 100%;
                            background-color: #e6e6e6;
                            margin-top: 50px;
                            padding: 17px;
                        }
                        .mensaje {
                            width: 100%;
                            background-color: #f2f2f2;
                            display: inline-flex;
                            flex-direction: column;
                            align-items: center;
                            text-align: center;
                            font-size: 18px;
                            margin: 0;
                            padding: 5%;
                            color: #194271;
                        }
                        .logo_head {
                            width: 90%;
                            height: auto;
                        }
                        .texto_msg, .manifiesto_txt {
                            margin-top: 80px;
                        }
                        .link_aviso {
                            text-decoration: none;
                            margin-top: 30px;
                            font-size: 16px;
                            color: #194271;
                        }
                        .manifiesto_txt {
                            color: #848484;
                            font-size: 13px;
                        }
                        span {
                            overflow-wrap: anywhere;
                        }
                    </style>
                    </head>
                    <body>
                        <div class="contenedor">
                            <table class="mensaje">
                                <tr>
                                    <td>
                                        <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                                        <br><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="texto_msg">UN PAGO DE ANALISIS HA SIDO DENEGADO POR SU BANCO.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="manifiesto_txt"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </body>
                </html>
                ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Laboratorio Corregidora<noreply@laboratoriocorregidora.com.mx>' . "\r\n";

        if (mail($to_admin, $subject, $message, $headers)) {
        } else {
        }

        echo 'error';
    }
}
