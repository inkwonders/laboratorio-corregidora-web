<?php

error_reporting(0);
// ini_set("display_errors", 1);

header("Content-type: application/json; charset=utf-8");

date_default_timezone_set('America/Mexico_city');

require_once "../controladores/cita.controlador.php";
require_once "../controladores/paciente.controlador.php";
require_once "../controladores/user.controlador.php";

require_once "../modelos/cita.modelo.php";
require_once "../modelos/paciente.modelo.php";
require_once "../modelos/user.modelo.php";

class DatosPaciente{

  public $respuesta = null;

  
  public function InsertaPaciente(){

    $logConfirmación = $this->logConfirmación;

    $nombre             = $this->nombre;
    $paterno            = $this->paterno;
    $materno            = $this->materno;
    $nombre_completo    = $this->nombre." ".$this->paterno." ".$this->materno;
    $fecha_nacimiento   = $this->fecha_nacimiento;
    $calle              = $this->calle;
    $colonia            = $this->colonia;
    $no_exterior        = $this->no_exterior;
    $no_interior        = $this->no_interior;
    $ubicacion          = $this->ubicacion;
    $codigo_postal      = $this->codigo_postal;
    $celular            = $this->celular;
    $telefono           = $this->telefono;
    $email              = $this->email;
    $pasaporte          = $this->pasaporte;
    $nombre_medico      = $this->nombre_medico;
    $email_medico       = $this->email_medico;
    $rs                 = $this->rs;
    $ref                = $this->ref;
    $domicilio_fiscal   = $this->domicilio_fiscal;
    $sintoma            = $this->sintoma;
    $enfermedad_cronica = $this->enfermedad_cronica;
    $prueba             = $this->prueba;
    $genero             = $this->genero;
    $contacto           = $this->contacto;
    $antiviral          = $this->antiviral;
    $influenza          = $this->influenza;
    $hora_formato       = $this->hora_formato;
    $no_conf            = $this->no_confirmacion;
    // $precio             = $this->precio;
    $tipo_prueba        = $this->tipo_prueba;
    $tipo_numero        = $this->tipo_numero;
    $es                 = $this->es;    
    $en                 = $this->en;    
    $regimen_fiscal     = $this->regimen_fiscal;
    $cfdi_paciente      = $this->cfdi_paciente;

    if($tipo_prueba == 1){
      $clave_estudios = 8436; 
    }else{
      $clave_estudios = 1029; 
    }

    $precio_bdd = ControladorCita::consultaPrecio($clave_estudios);
    $precio = $precio_bdd[0];  

    $id = ModeloCita::uuid();
    $uuidLog = $id;
    $clave_paciente = explode('-',$id[0]);

    $esLog =  $es;
    $enLog = $en;


    $arreglo_datos = array(
      "clave"                     => $clave_paciente[0], 
      "nombre"                    => $nombre,
      "apellido_paterno"          => $paterno,
      "apellido_materno"          => $materno,
      "nombre_completo"           => $nombre_completo,
      "genero"                    => $genero,
      "fecha_nacimiento"          => $fecha_nacimiento,
      "calle"                     => $calle,
      "colonia"                   => $colonia,
      "no_exterior"               => $no_exterior,
      "no_interior"               => $no_interior,
      "fk_ubicacion"              => $ubicacion,
      "cp"                        => $codigo_postal,
      "tel_celular"               => $celular,
      "tel_casa"                  => $telefono,
      "paciente_email"            => $email,
      "pasaporte"                 => $pasaporte,
      "medico_nombre"             => $nombre_medico,
      "medico_email"              => $email_medico,
      "rs"                        => $rs,
      "rfc"                       => $ref,
      "regimen_fiscal"            => $regimen_fiscal,
      "cfdi_paciente"             => $cfdi_paciente,
      "domicilio_fiscal"          => $domicilio_fiscal,
      "otro_sintoma"              => $sintoma,
      "otra_enfermedad"           => $enfermedad_cronica,
      "contacto_persona_positivo" => $contacto,
      "antiviral"                 => $antiviral,
      "influenza"                 => $influenza,
      "otro_prueba"               => $prueba,
      "fecha_alta"                => date("Y-m-d h:i:s"),
      "es"                        => $es,
      "en"                        => $en
    );

    $array_cronicas=$this->array_cronicas;
    $array_sintomas=$this->array_sintomas;
    $array_pruebas=$this->array_pruebas;

    $fecha=$this->dia;
    $hora=$this->hora;

    $fechats = strtotime($fecha);

    switch (date('w', $fechats)){
        case 0: $nombre_dia = "Domingo"; break;
        case 1: $nombre_dia = "Lunes"; break;
        case 2: $nombre_dia = "Martes"; break;
        case 3: $nombre_dia = "Miercoles"; break;
        case 4: $nombre_dia = "Jueves"; break;
        case 5: $nombre_dia = "Viernes"; break;
        case 6: $nombre_dia = "Sabado"; break;
    }

    if($nombre_dia == "Sabado"){
      $dia_sem = 2;
    }else if($nombre_dia == "Domingo"){
      $status= "domingo";
    }else{
      $dia_sem = 1;
    }

    $id_paciente = null;

    $consulta_sk_horario = ControladorCita::ConsultaSkHorario($dia_sem,$hora);

    if(!empty($consulta_sk_horario['id'])){

      $disponibilidad_cita = ControladorCita::VerificarCitasHorario($consulta_sk_horario['id'],$fecha);

      if($disponibilidad_cita['num_citas'] < 1000){

        $id_user = null;
        $no_confirmacion = null;

        $existe_user = ControladorUser::ctrVerficaUser($email);

        if($existe_user == "error"){

          // ControladorUser::ctrInsertarUser($nombre_completo, $email);

        ControladorUser::ctrInsertarUser($nombre_completo, $email);

        }else{

          $id_user = $existe_user;
        }

        $inserta_paciente = ControladorPaciente::insertarPaciente($arreglo_datos, $id_user);

        if($inserta_paciente == "ok"){

          $pacienteLog = $clave_paciente[0];
          $id_paciente = ControladorPaciente::consultaIdPaciente($clave_paciente[0]);
      
          $datos_cita = array(
            "fk_horario" => $consulta_sk_horario['id'],
            "fk_paciente" => $id_paciente['id'],
            "fecha_seleccionada" => $fecha,
            "no_confirmacion" => $no_conf, 
            "email" => $email,
            "precio" => $precio,
            "tipo" => $tipo_numero
          );
          
          $pacienteIdLog = $id_paciente['id'];

          $inserta_cita = ControladorPaciente::insertarCita($datos_cita);

          $inserta_sintomas = null;
          $inserta_cronicas = null;
          $inserta_pruebas = null;

          if($inserta_cita == "ok"){

            foreach($array_sintomas as $valor){
              
              if($valor->id!=""){
               
                $inserta_sintomas .= ControladorPaciente::InsertDatosGeneral('sintomas_pacientes', $id_paciente['id'], (int) $valor->id, 'sintoma_id');
              }

            }

            // var_dump($array_cronicas);

            foreach($array_cronicas as $valor){
             
              if($valor!=""){ 
               
                $inserta_cronicas .= ControladorPaciente::InsertDatosGeneral('enfermedades_pacientes', $id_paciente['id'], (int) $valor, 'enfermedad_id');
              }
            }

            foreach($array_pruebas as $valor){
             
              if($valor!=""){
                
                $inserta_pruebas .= ControladorPaciente::InsertDatosGeneral('pruebas_pacientes', $id_paciente['id'], (int) $valor, 'prueba_id');
              }
            }

            // echo "cronicas res ".$inserta_cronicas;
            // echo "sintomas res ".$inserta_sintomas;
            // echo "pruebas res ".$inserta_pruebas;

            $pos_cronicas = strpos($inserta_cronicas,'2');
            $pos_sintomas = strpos($inserta_sintomas,'2');
            $pos_pruebas = strpos($inserta_pruebas,'2');

            // echo "res cro".$pos_cronicas." ";
            // echo "res sin".$pos_sintomas." ";
            // echo "res pru".$pos_pruebas." ";

            if($pos_cronicas < -1 && $pos_sintomas < -1 && $pos_pruebas < -1){
              
              $no_confirmacion = $no_conf;

              $status="ok";
            }else {
              $status= "error aqui !";
            }

          }else {
            $status= $inserta_cita;
          }

        }else {
          $status= $inserta_paciente;
        }

      }else {
        $status= "error_cita";
      }

    }else {
      $status= "sk_horario";
    }

    $id_cita = null;
    $id_cita = ControladorPaciente::consultaIdCita($id_paciente['id']);

    $this->respuesta = [
      "status" => $status,
      "sk_cita" => $id_cita,
      "horario" => $hora_formato,
      "fk_paciente" => $id_paciente,
      "fecha_seleccionada" => $fecha,
      "no_confirmacion"=> $no_confirmacion,
      "email" => $email,
      "precio" => $precio,
      "logConfirmación" => $logConfirmación,
      "esLog" => $esLog,
      "enLog" => $enLog
    ];

 }

}

if(isset($_REQUEST['obj_paciente']) && isset($_POST['dia_seleccionado']) && isset($_POST['hora_seleccionada'])){

  $obj_paciente = json_decode($_REQUEST['obj_paciente']);

  $respuestas = [];

  $id_cita = ModeloCita::uuid();
  $no_confirmacion = explode('-',$id_cita[0]);

  $logConfirmacion = $id_cita[0]."  / ".$no_confirmacion;

  foreach ($obj_paciente as $paciente) {

    $datosPaciente = new DatosPaciente();

    $datosPaciente -> nombre = $paciente->nombre;
    $datosPaciente -> paterno = $paciente->paterno;
    $datosPaciente -> materno = $paciente->materno;
    $datosPaciente -> fecha_nacimiento = $paciente->fecha_nacimiento;
    $datosPaciente -> calle = $paciente->calle;
    $datosPaciente -> colonia = $paciente->colonia;
    $datosPaciente -> no_exterior = $paciente->no_exterior;
    $datosPaciente -> no_interior = $paciente->no_interior;
    $datosPaciente -> ubicacion = $paciente->ubicacion;
    $datosPaciente -> codigo_postal = $paciente->codigo_postal;
    $datosPaciente -> celular = $paciente->celular;
    $datosPaciente -> telefono = $paciente->telefono;
    $datosPaciente -> email = $paciente->email;
    $datosPaciente -> pasaporte = $paciente->pasaporte;
    $datosPaciente -> nombre_medico = $paciente->nombre_medico;
    $datosPaciente -> email_medico = $paciente->email_medico;
    $datosPaciente -> rs = $paciente->rs;
    $datosPaciente -> ref = $paciente->rfc;
    $datosPaciente -> domicilio_fiscal = $paciente->domicilio_fiscal;
    
    if($paciente->regimen_fiscal != ''){

      $datosPaciente -> regimen_fiscal = $paciente->regimen_fiscal;
      
    }else{

      $datosPaciente -> regimen_fiscal = 0;

    }

    if($paciente->cfdi_paciente != ''){
      
      $datosPaciente -> cfdi_paciente = $paciente->cfdi_paciente;
      
    }else{

      $datosPaciente -> cfdi_paciente = 0;

    }

    $datosPaciente -> sintoma = $paciente->sintoma;
    $datosPaciente -> enfermedad_cronica = $paciente->enfermedad;
    $datosPaciente -> prueba = $paciente->prueba;
    $datosPaciente -> genero = $paciente->genero;
    $datosPaciente -> contacto = $paciente->contacto;
    $datosPaciente -> antiviral = $paciente->antiviral;
    $datosPaciente -> influenza = $paciente->influenza;
    $datosPaciente -> dia = $_POST['dia_seleccionado'];
    $datosPaciente -> hora = $_POST['hora_seleccionada'];
    $datosPaciente -> hora_formato = $_POST['hora_formato'];
    $datosPaciente -> precio = $_POST['precio'];
    $datosPaciente -> tipo_prueba = $_POST['tipo'];
    $datosPaciente -> tipo_numero = $_POST['tipo_numero'];
    $datosPaciente -> es = $paciente->es;
    $datosPaciente -> en = $paciente->en;
    $datosPaciente -> no_confirmacion =  $no_confirmacion[0];
    $datosPaciente -> logConfirmación = $logConfirmación;


    $datosPaciente -> array_cronicas = $paciente->array_cronicas;
    $datosPaciente -> array_sintomas = $paciente->array_sintomas;
    $datosPaciente -> array_pruebas = $paciente->array_pruebas;

    $datosPaciente-> InsertaPaciente();

    $respuestas[] = $datosPaciente->respuesta;

  }

  echo json_encode($respuestas);
  // echo json_encode($obj_paciente);
  // var_dump($obj_paciente);


}