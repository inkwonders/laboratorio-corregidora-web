<?php

require_once "../modelos/pagos.modelo.php";
include "AESCrypto.php";

//FUNCION GENERAR LIGA
function generarLiga($pago_monto,$pago_id,$cliente_correo,$datosAdicionaless){

  $tabla = "encriptacion14f4gv7m";
  $res = ModeloPpagos::mdlConsultaPagos($tabla);

	$id_company = trim($res['id_company']);
	$id_branch =  trim($res['id_branch']);
	$user = trim($res['user']);
	$pwd =  trim($res['pwd']);
	$moneda = trim($res['moneda']);
	$canal =  trim($res['canal']);
	$omitir_notif_default = trim($res['notif']);
	$st_correo = trim($res['correo']);

  $key = trim($res['semilla_aes']);
  $url_encriptacion = trim($res['url_encriptacion']);
  $data_0 = trim($res['data_0']);

  //ENCRIPTACION DE LA CADENA XML
	// $key = '5DCC67393750523CD165F17E1EFADD21'; //Llave de 128 bits
  // $url_encriptacion = 'https://wppsandbox.mit.com.mx/gen';


  // echo "<script> alert('algo = ".$pago_monto." - ".$pago_id." - ".$cliente_correo." - ".$id_company." - ".$id_branch." - ".$user." - ".$pwd." - ".$moneda." - ".$canal." - ".$omitir_notif_default." - ".$st_correo." - ".$key." - ".$url_encriptacion." - ".$data_0."'); </script>";

  $fecha_actual = date("d/m/Y");
  $pago_vigencia = date("d/m/Y",strtotime($fecha_actual." + 3 days"));

  $datosAdicionales = '<datos_adicionales>
                            <data id="1" display="true">
                                <label>Concepto:</label>
                                <value>prueba covid</value>
                            </data>
                            <data id="2" display="true">
                                <label>Tipo:</label>
                                <value>Solicitud</value>
                            </data>
                        </datos_adicionales>';


	//CADENA XML CON LOS DATOS DEL PAGO
	/*$originalString = '<?xml version="1.0" encoding="UTF-8"?>
										<P>
											<business>
												<id_company>'.$id_company.'</id_company>
												<id_branch>'.$id_branch.'</id_branch>
												<user>'.$user.'</user>
												<pwd>'.$pwd.'</pwd>
											</business>
											<url>
												<reference>'.$pago_id.'</reference>
												<amount>120.00</amount>
												<moneda>'.$moneda.'</moneda>
												<canal>'.$canal.'</canal>
												<omitir_notif_default>'.$omitir_notif_default.'</omitir_notif_default>
												<st_correo>'.$st_correo.'</st_correo>
												<fh_vigencia>'.$pago_vigencia.'</fh_vigencia>
												<mail_cliente>'.$cliente_correo.'</mail_cliente>
												<st_cr>A</st_cr>
												<datos_adicionales>
										      <data id="1" display="false">
										        <label>PRUEBA</label>
										        <value>CHINOGRAPHICS</value>
										      </data>
										  </datos_adicionales>
											</url>
										</P>';*/

  /*$originalString = '<?xml version="1.0" encoding="UTF-8"?>
                      <P>
                        <business>
                          <id_company>SNBX</id_company>
                          <id_branch>01SNBXBRNCH</id_branch>
                          <user>SNBXUSR01</user>
                          <pwd>SECRETO</pwd>
                        </business>
                        <url>
                          <reference>'.$pago_id.'</reference>
                          <amount>51.00</amount>
                          <moneda>MXN</moneda>
                          <canal>W</canal>
                          <omitir_notif_default>1</omitir_notif_default>
                          <promociones>C</promociones>
                          <st_correo>1</st_correo>
                          <fh_vigencia>22/02/2021</fh_vigencia>
                          <mail_cliente>mail@dominio.com</mail_cliente>
                          <datos_adicionales>
                            <data id="1" display="true">
                              <label>Talla</label>
                              <value>Grande</value>
                            </data>
                            <data id="2" display="false">
                              <label>Color</label>
                              <value>Azul</value>
                            </data>
                          </datos_adicionales>
                        </url>
                      </P>';*/

    $originalString = '<?xml version="1.0" encoding="UTF-8"?>
                                           <P>
                                               <business>
                                                   <id_company>'.$id_company.'</id_company>
                                                   <id_branch>'.$id_branch.'</id_branch>
                                                   <user>'.$user.'</user>
                                                   <pwd>'.$pwd.'</pwd>
                                               </business>
                                               <url>
                                                   <reference>'.$pago_id.'</reference>
                                                   <amount>'.$pago_monto.'</amount>
                                                   <moneda>'.$moneda.'</moneda>
                                                   <canal>'.$canal.'</canal>
                                                   <omitir_notif_default>'.$omitir_notif_default.'</omitir_notif_default>
                                                   <promociones>C</promociones>
                                                   <st_correo>'.$st_correo.'</st_correo>
                                                   <fh_vigencia>'.$pago_vigencia.'</fh_vigencia>
                                                   <mail_cliente>'.$cliente_correo.'</mail_cliente>
                                                   <st_cr>A</st_cr>
                                                   '.$datosAdicionales.'
                                               </url>
                                           </P>';

	//ENCRIPTACION DE LA CADENA XML

	$encryptedString = AESCrypto::encriptar($originalString, $key);

  // var_dump($encryptedString);

	//CODIFICACION DE LA CADENA XML ENCRIPTADA
	$post_string = "xml=".urlencode("<pgs><data0>".$data_0."</data0><data>".$encryptedString."</data></pgs>");

	$post = "<pgs><data0>".$data_0."</data0><data>".$encryptedString."</data></pgs>";

  // $post_string = "xml=".urlencode("<pgs><data0>SNDBX123</data0><data>".$encryptedString."</data></pgs>");
  //   $post = "<pgs><data0>SNDBX123</data0><data>".$encryptedString."</data></pgs>";

	//ENVIO DE DATOS A URL DE GENERACION POR CURL METODO POST
	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url_encriptacion,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS => $post_string,
	  CURLOPT_HTTPHEADER => array(
		"Content-Type: application/x-www-form-urlencoded"
	  ),
	));

	//RESPUESTA DE LA PAGINA DE GENERACION
	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if($err){
	  echo "cURL Error #:" . $err;
	}else{
		$result = $response;
	}

	//DESENCRIPTADO DE LA CADENA DE RESPUESTA
	$decryptedString = AESCrypto::desencriptar($response, $key);

  echo "<script> alert('decrypt = ".$decryptedString."'); </script>";

//EXTRACION DE LA LIGA DESENCRIPTADA
	$newurl = substr($decryptedString,81,35);

	//DEVUELVE LA LIGA DE PAGO
	return $newurl;
}
