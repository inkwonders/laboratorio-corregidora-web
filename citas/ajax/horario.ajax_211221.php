<?php

date_default_timezone_set('America/Mexico_city');

require_once "../controladores/cita.controlador.php";
require_once "../modelos/cita.modelo.php";

class AjaxHorario{

    public function ConsultaHorario(){

      $res = null;

      $fecha = $this->fecha1;

      $fechats = strtotime($fecha);

      switch (date('w', $fechats)){
          case 0: $nombre_dia = "Domingo"; break;
          case 1: $nombre_dia = "Lunes"; break;
          case 2: $nombre_dia = "Martes"; break;
          case 3: $nombre_dia = "Miercoles"; break;
          case 4: $nombre_dia = "Jueves"; break;
          case 5: $nombre_dia = "Viernes"; break;
          case 6: $nombre_dia = "Sabado"; break;
      }

      if($nombre_dia == "Sabado" ){

        $resultado_horarios = ControladorCita::ctrConsultaHorarioDisponible($nombre_dia);
        $res .= "<div class='parte_reservacion'>
                    <div class='cont_head'>
                        <span class='titulo_testcovid t_horario'>Mañana</span>
                        <span class='subtitulo_testcovid margen_tiempo'>07:30 am - 01:00 pm</span>
                    </div>
                    <div class='seleccion_horas manana'>";
        foreach ($resultado_horarios as $key => $value) {

          $hora = strtotime($value["hora_inicial"]);
            $con_hora_inicial = date("h:i a", $hora);
            $con_hora_final = date("h:i a", strtotime($value["hora_final"]));
            $resultado_disponibles = ControladorCita::ctrConsultaValidacionHorario($value["id"],$nombre_dia,$fecha);
            if($resultado_disponibles[0]==15){
              $res .=  "<span name='".($key + 1)."' class='seleccion_hora' context=seleccion_hora' style='color:#AAD1FE;cursor: default;' disabled value='".$value['hora_inicial']."' hora_final='".$value['hora_final']."'>
                        ".$con_hora_inicial." - ".$con_hora_final."
                        <div></div>
                      </span>";
            }else {
              // print_r($value);
              $res .=  "<span name='".($key + 1)."' class='seleccion_hora' context=seleccion_hora' value='".$value['hora_inicial']."'  hora_final='".$value['hora_final']."'>
                        ".$con_hora_inicial." - ".$con_hora_final."
                        <div></div>
                      </span>";
            }
            // foreach ($resultado_disponibles as $key2 => $value_horario) {
            //
            //   $hora = strtotime($value_horario["hora_inicial"]);
            //   $con_hora_inicial." - ".$con_hora_final = date("h:i a", $hora);
            //
            //   $res .=  "<span name='".($key2 + 1)."' class='seleccion_hora' context=seleccion_hora' value='".$value_horario[1]."'>
            //               ".$con_hora_inicial." - ".$con_hora_final."
            //               <div></div>
            //             </span>";
            // }

        }

        $res .= "   </div>
                 </div>";

      }
    
      

      else if($nombre_dia == "Domingo"){

        $res .= "<div class='parte_reservacion' style='position: relative; width: 100%;'>
                    <div class='cont_head' style='position: relative; width: 100%; display: flex; justify-content: center; align-items:center;'>
                        <span class='titulo_testcovid t_horario' style='text-align:center; width: 100%;'>No hay citas en domingo</span>
                    </div>
                </div>";

      }else{

        $resultado = ControladorCita::ctrConsultaHorarioDisponible($nombre_dia);

        $res .= "<div class='parte_reservacion'>
                    <div class='cont_head'>
                        <span class='titulo_testcovid t_horario'>Mañana</span>
                        <span class='subtitulo_testcovid margen_tiempo'>07:30 am - 03:00 pm</span>
                    </div>
                    <div class='seleccion_horas manana'>";

        foreach ($resultado as $key => $value) {

          $hora = strtotime($value["hora_inicial"]);
          $con_hora_inicial= date("h:i a", $hora);
          $con_hora_final = date("h:i a", strtotime($value["hora_final"]));
          $resultado_disponibles = ControladorCita::ctrConsultaValidacionHorario($value["id"],$nombre_dia,$fecha);

          // if($hora== strtotime("07:30:00")){
          if($hora == strtotime("16:00:00")){
            $res .= "   </div>
                 </div>";

            $res .= "<div class='parte_reservacion'>
                      <div class='cont_head'>
                          <span class='titulo_testcovid t_horario'>Tarde</span>
                          <span class='subtitulo_testcovid margen_tiempo'>04:00 pm - 06:00 pm</span>
                      </div>
                      <div class='seleccion_horas manana'>";
                      if(strtotime($fecha)==strtotime(date("Y-m-d"))){
                        if(strtotime($value["hora_inicial"])<strtotime(date("H:i:s"))){
                          $res .=  "<span name='".($key + 1)."' class='' context=seleccion_hora' style='color:#AAD1FE;cursor: default;' disabled value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                                    ".$con_hora_inicial." - ".$con_hora_final."
                                    <div></div>
                                  </span>";
                        }else if($resultado_disponibles[0]==15){
                          $res .=  "<span name='".($key + 1)."' class='' context=seleccion_hora' style='color:#AAD1FE;cursor: default;' disabled value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                                    ".$con_hora_inicial." - ".$con_hora_final."
                                    <div></div>
                                  </span>";
                        }else {
                          $res .=  "<span name='".($key + 1)."' class='seleccion_hora' context=seleccion_hora' value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                                    ".$con_hora_inicial." - ".$con_hora_final."
                                    <div></div>
                                  </span>";
                        }
                      }else {
                         if($resultado_disponibles[0]==15){
                          $res .=  "<span name='".($key + 1)."' class='' context=seleccion_hora' style='color:#AAD1FE;cursor: default;' disabled value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                                    ".$con_hora_inicial." - ".$con_hora_final."
                                    <div></div>
                                  </span>";
                        }else {
                          $res .=  "<span name='".($key + 1)."' class='seleccion_hora' context=seleccion_hora' value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                                    ".$con_hora_inicial." - ".$con_hora_final."
                                    <div></div>
                                  </span>";
                        }
                      }


          }else{
            if(strtotime($fecha)==strtotime(date("Y-m-d"))){
            
              if(strtotime($value["hora_inicial"])<strtotime(date("H:i:s"))){
                $res .=  "<span name='".($key + 1)."' class='' context=seleccion_hora' style='color:#AAD1FE;cursor: default;' disabled value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                          ".$con_hora_inicial." - ".$con_hora_final."
                          <div></div>
                        </span>";
              }else if($resultado_disponibles[0]==15){
                $res .=  "<span name='".($key + 1)."' class='' context=seleccion_hora' style='color:#AAD1FE;cursor: default;' disabled value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                          ".$con_hora_inicial." - ".$con_hora_final."
                          <div></div>
                        </span>";
              }else {
                $res .=  "<span name='".($key + 1)."' class='seleccion_hora' context=seleccion_hora' value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                          ".$con_hora_inicial." - ".$con_hora_final."
                          <div></div>
                        </span>";
              }
            }else {
               if($resultado_disponibles[0]==15){
                $res .=  "<span name='".($key + 1)."' class='' context=seleccion_hora' style='color:#AAD1FE;cursor: default;' disabled value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                          ".$con_hora_inicial." - ".$con_hora_final."
                          <div></div>
                        </span>";
              }else {
                $res .=  "<span name='".($key + 1)."' class='seleccion_hora' context=seleccion_hora' value='".$value["hora_inicial"]."'  hora_final='".$value['hora_final']."'>
                          ".$con_hora_inicial." - ".$con_hora_final."
                          <div></div>
                        </span>";
              }
            }


          }



        }

        $res .= "   </div>
                 </div>";

      }

      echo $res;

    }

}

$objetHorario = new AjaxHorario();

if(isset($_POST['fecha']) && $_POST['fecha'] != NULL){

  $fecha_actual = strtotime(date("Y-m-d"));
  $fecha_entrada = strtotime(($_POST['fecha']));

  if($fecha_entrada < $fecha_actual){

    echo "error_fecha_menor".$fecha_entrada."-".$fecha_actual."-".$_POST['fecha']."-".date("Y-m-d");

  }else{

    $objetHorario->fecha1 = $_POST['fecha'];
    $objetHorario->ConsultaHorario();

  }

}

