<?php
header("Content-Type: application/json");
require "../modelos/conexion.php";

$request = json_decode(file_get_contents('php://input'));

$stmt = Conexion::conectar()->prepare("SELECT id, nombreMunicipio as code, nombreMunicipio as name, idEstado as state_id FROM ubicaciones WHERE idEstado = :estado");
$stmt->bindParam(":estado", $request->state, PDO::PARAM_INT);

$stmt->execute();

echo json_encode($stmt->fetchAll());

$stmt = null;
