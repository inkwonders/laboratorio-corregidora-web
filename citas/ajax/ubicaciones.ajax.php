<?php

require_once "../modelos/ubicaciones.modelo.php";
require_once "../controladores/ubicaciones.controlador.php";

class AjaxUbicacion
{
    public function ConsultaMunicipio() {
        $estado = $this->idEstado;
        $tabla = "ubicaciones";

        $resultado = ControladorUbicacion::ctrConsultaMunicipio($tabla,$estado);
        $options="<option value='0' selected disabled>Seleccione Municipio</option>";

        foreach($resultado as $key => $value){
            $options.= " <option value=".$value['idMunicipio']." >".$value['nombreMunicipio']."</option>";
        }
        echo $options;
    }
}

$objetMunicipio=new AjaxUbicacion();

if(isset($_POST['idEstado']) && $_POST['idEstado'] != NULL){
    $objetMunicipio-> idEstado = $_POST['idEstado'];
    $objetMunicipio-> ConsultaMunicipio();
}
