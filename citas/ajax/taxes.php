<?php
header("Content-Type: application/json");
require "../modelos/conexion.php";

$stmt = Conexion::conectar()->prepare("SELECT * FROM taxes");
$stmt->execute();

echo json_encode($stmt->fetchAll());

$stmt = null;
