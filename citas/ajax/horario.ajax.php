<?php
header("Content-Type: application/json");
date_default_timezone_set('America/Mexico_city');

$request = json_decode(file_get_contents('php://input'));

require_once $_SERVER['DOCUMENT_ROOT']."/citas/modelos/conexion.php";
require_once $_SERVER['DOCUMENT_ROOT']."/citas/controladores/cita.controlador.php";
require_once $_SERVER['DOCUMENT_ROOT']."/citas/modelos/cita.modelo.php";

class AjaxHorario
{
    public function ConsultaHorario() {
        $res = [];
        $fecha = $this->fecha;
        $baned_dates = [
            "2023-12-25" => "No hay citas el 25 de diciembre",
            "2024-01-01" => "No hay citas el 1 de enero",
        ];

        if (array_key_exists($fecha, $baned_dates)) {
            return ([
                "success" => false,
                "msg" => $baned_dates[$fecha],
            ]);
        }

        $fechats = strtotime($fecha);
        $dias = [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ];
        $nombre_dia = $dias[date('w', $fechats)];

        if($nombre_dia == "Domingo"){
            return ([
                "success" => false,
                "msg" => "No hay citas en domingo",
            ]);
        }

        $resultado_horarios = ControladorCita::ctrConsultaHorarioDisponible($nombre_dia);

        $res = [
            "success" => true,
            "early_schedules" => [],
            "late_schedules" => [],
        ];

        foreach ($resultado_horarios as $key => $value) {
            $hora = strtotime($fecha." ".$value["hora_inicial"]);
            $con_hora_inicial = date("h:i a", $hora);
            $con_hora_final = date("h:i a", strtotime($value["hora_final"]));
            $resultado_disponibles = ControladorCita::ctrConsultaValidacionHorario($value["id"],$nombre_dia,$fecha);

            $type = ($hora < strtotime($fecha." 16:00:00"))? "early_schedules" : "late_schedules";

            $res[$type][] = [
                "start_time" => $fecha." ".$value["hora_inicial"],
                "end_time" => $fecha." ".$value["hora_final"],
                "available" => time() < $hora && !($resultado_disponibles[0]==15),
            ];
        }

        return $res;
    }
}

$objetHorario = new AjaxHorario();

if(isset($request->fecha) && $request->fecha != NULL) {
    $fecha_actual = strtotime(date("Y-m-d"));
    $fecha_entrada = strtotime($request->fecha);

    if($fecha_entrada < $fecha_actual) {
        echo json_encode([
            "success" => false,
            "msg" => "error_fecha_menor".$fecha_entrada."-".$fecha_actual."-".$request->fecha."-".date("Y-m-d"),
        ]);
    } else {
        $objetHorario->fecha = $request->fecha;
        echo json_encode($objetHorario->ConsultaHorario());
    }
}
