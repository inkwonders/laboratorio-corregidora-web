<?php
class ControladorPruebas{

  static public function ctrConsultaPruebas(){

    $tabla = "pruebas";

    $resultado = ModeloPruebas::mdlConsultaPruebas($tabla);

    return $resultado;

  }

  static public function ctrConsultaSkPruebas($prueba){

    $tabla = "pruebas";

    $resultado = ModeloPruebas::mdlConsultaSkPruebas($tabla,$prueba);

    return $resultado[0];

  }
  static public function ctrConsultaNombrePruebas($prueba){

    $tabla = "pruebas";

    $resultado = ModeloPruebas::mdlConsultaNombrePruebas($tabla,$prueba);

    return $resultado;

  }
  static public function ctrConsultaPruebasPaciente($sk_paciente){

    $tabla = "pruebas_pacientes";

    $resultado = ModeloPruebas::mdlConsultaPruebasPaciente($tabla,$sk_paciente);

    return $resultado;

  }

}
 ?>
