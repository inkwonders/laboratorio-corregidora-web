<?php
class ControladorValidacion{

  static public function ctrValidarCorreo($dato,$valido){
      if (filter_var($dato, FILTER_VALIDATE_EMAIL)) {
        return true;
      }else{
        if($dato==""){
          if($valido==1){
            return false;
          }else {
            return true;
          }
        }else{
          return false;
        }
      }
  }

  static public function ctrValidaLongitud($texto,$valor){
    $cont_descripcion=trim(preg_replace('/\s+/', ' ', $texto));
    $cont_descripcion=trim(preg_replace('([À-ÿ])', '', $cont_descripcion));
    if($cont_descripcion!=""){
      if(strlen($cont_descripcion)<=$valor){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }

  static public function ctrValidarTelefono($dato,$valido){
    if(preg_match('/^[0-9]{10}+$/', $dato)){
      return true;
    }else if($dato==""){
        if($valido==1){
          return false;
        }else {
          return true;
        }

    }else {
      return false;
    }
  }

  //permite caracteres a-z A-Z "." "-" "espacios" y acentuaciones
  static public function ctrValidarNombres($dato,$valido){
    if(preg_match('/^[a-zA-ZÀ-ÿ .-]+$/', $dato)){
      if(preg_match('/([\.]{2,})|([\-]$)|([\-]{2,})|([\.]+[\-])|([\-]+[\.])|([ ]+[\.])|([ ]+[\-])|([\.]+[\w])|(^[\-])|(^[ ])|(^[\.])|([\-]+[ ]) /',$dato)){
        return false;
      }else{
        if(strlen($dato)<=50 && strlen($dato)>=2){
          return true;
        }else {
          return false;
        }
      }
    }else if($dato==''){
        if($valido==1){
          return false;
        }else{
          return true;

          }
      }else {
        return false;
      }
  }

  //valida que una fecha pueda o no estar vacia si es obligatoria, y sea menor  a la fecha actual
  static public function ctrValidarFechaNacimiento($dato,$valido){
  if($dato=='' && $valido==1){
    return false;
  }else if($dato=='' && $valido==0){
    return true;
  }else{
    $resultado=ctrGuardarFecha($dato);
    if($resultado=="error"){
      return false;
    }else if(strtotime(date("Y-m-d",time())) >= strtotime($resultado)){
      return true;
    }else{
      return false;
    }
  }
  }

  //valida que una fecha pueda o no estar vacia si es obligatoria, y sea mayor igual a la fecha actual
  static public function ctrValidarFecha($dato,$valido){
    if($dato=='' && $valido==1){
      return false;
    }else if($dato=='' && $valido==0){
      return true;
    }else{
      $resultado=ctrGuardarFecha($dato);
      if($resultado=="error"){
        return false;
      }else if(strtotime($resultado) >=strtotime(date("Y-m-d",time())) ){
        return true;
      }else{
        return false;

      }
    }
  }

  //valida que un radio button este seleccionado
  static public function ctrvalidarRadioButton($dato){
    if($dato==0){
      return false;
    }else{
      return true;
    }
  }

  static public function ctrValidarCampoObligatorio($dato,$valido){
    if($dato=='' && $valido==1){
      return false;
    }else if($dato=='' && $valido==0){
      return true;
    }else {
      return true;
    }
  }

  static public function ctrValidarSelect($dato,$valido){
   if($dato=="0"){
     if($valido==1){
       return false;
     }else{
       return true;
     }
   }else {
     return true;
   }
  }
  static public function ctrValidarRFC($valor){
        $valor = str_replace("-", "", $valor);
        $cuartoValor = substr($valor, 3, 1);
        //RFC sin homoclave
        if(strlen($valor)==10){
            $letras = substr($valor, 0, 4);
            $numeros = substr($valor, 4, 6);
            if (ctype_alpha($letras) && ctype_digit($numeros)) {
                return true;
            }
            return false;
        }
        // Sólo la homoclave
        else if (strlen($valor) == 3) {
            $homoclave = $valor;
            if(ctype_alnum($homoclave)){
                return true;
            }
            return false;
        }
        //RFC Persona Moral.
        else if (ctype_digit($cuartoValor) && strlen($valor) == 12) {
            $letras = substr($valor, 0, 3);
            $numeros = substr($valor, 3, 6);
            $homoclave = substr($valor, 9, 3);
            if (ctype_alpha($letras) && ctype_digit($numeros) && ctype_alnum($homoclave)) {
                return true;
            }
            return false;
        //RFC Persona Física.
        } else if (ctype_alpha($cuartoValor) && strlen($valor) == 13) {
            $letras = substr($valor, 0, 4);
            $numeros = substr($valor, 4, 6);
            $homoclave = substr($valor, 10, 3);
            if (ctype_alpha($letras) && ctype_digit($numeros) && ctype_alnum($homoclave)) {
                return true;
            }
            return false;
        }else {
            return false;
        }
 }//fin validaRFC
}
 function ctrGuardarFecha($fecha){
  if($fecha==''){
    return "0000-00-00";
  }else{

    // $newDate = date("Y-m-d", strtotime($fecha));

    $date = date_create($fecha);
    error_reporting(0);
    $dato=date_format($date, 'Y-m-d');
    if($dato==""){
      return "error";
    }else{
      return $dato;
    }
  }
 }
 ?>
