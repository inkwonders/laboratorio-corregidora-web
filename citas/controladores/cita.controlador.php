<?php

class ControladorCita
{

  static public function ctrUpdateTipoPago($tipo_pago, $no_confirmacion, $precio)
  {

    $tabla = "citas";

    $respuesta = ModeloCita::mdlUpdateTipoPago($tabla, $tipo_pago, $no_confirmacion, $precio);

    return $respuesta;
  }

  static public function ctrConsultaCita()
  {

    $tabla = "citas";

    $respuesta = ModeloCita::mdlConsultaCita($tabla);

    return $respuesta;
  }

  static public function ctrConsultaHorario($fk_horario)
  {

    $tabla = "horarios_citas";

    $respuesta = ModeloCita::mdlConsultaHorario($tabla, $fk_horario);

    return $respuesta;
  }

  static public function ctrConsultaHorarioDisponible($nombre_dia)
  {

    $tabla = "horarios_citas";
    $respuesta = ModeloCita::mdlConsultaHorarioDisponible($tabla, $nombre_dia);
    // echo "ok";

    return $respuesta;
  }

  static public function ctrConsultaHorarioDisponibleLugares($sk_horario, $nombre_dia, $fecha)
  {

    $tabla1 = "horarios_citas";
    $tabla2 = "citas";

    $respuesta = ModeloCita::mdlConsultaHorarioDisponibleLugares($tabla1, $tabla2, $sk_horario, $nombre_dia, $fecha);

    return $respuesta;
  }
  static public function ctrConsultaValidacionHorario($sk_horario, $nombre_dia, $fecha)
  {
    // echo "[$sk_horario, $nombre_dia, $fecha]";

    $tabla1 = "horarios_citas";
    $tabla2 = "citas";

    $respuesta = ModeloCita::mdlConsultaValidacionHorario($tabla1, $tabla2, $sk_horario, $nombre_dia, $fecha);

    return $respuesta;
  }
  static public function ConsultaSkHorario($dia_sem, $hora)
  {

    $tabla = "horarios_citas";

    $respuesta = ModeloCita::ConsultaSkHorario($tabla, $dia_sem, $hora);

    return $respuesta;
  }

  static public function VerificarCitasHorario($sk_horario, $fecha)
  {

    $tabla = "citas";

    $respuesta = ModeloCita::VerificarCitasHorario($tabla, $sk_horario, $fecha);

    return $respuesta;
  }

  static public function ctrBuscarCita($no_confirmacion)
  {

    $tabla = "citas";

    $respuesta = ModeloCita::mdlBuscarCita($tabla, $no_confirmacion);

    return $respuesta;
  }

  // nuevo 
  static public function consultaPrecio($tipo_estudio)
  {

    $tabla = "analisis"; 

    $respuesta = ModeloCita::ConsultaPrecio($tabla, $tipo_estudio);

    return $respuesta;
  }
}
