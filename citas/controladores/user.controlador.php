<?php

  class ControladorUser{

    public static function ctrVerficaUser($email){

      $tabla = "users";

      $respuesta = ModeloUser::mdlVerficaUser($tabla, $email);

      return $respuesta;

    }

    public static function ctrInsertarUser($nombre_completo, $email){

        $tabla = "users";
  
        $respuesta = ModeloUser::mdlInsertaUser($tabla, $nombre_completo, $email);
  
        return $respuesta;
  
      }

  }