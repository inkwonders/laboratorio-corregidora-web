<?php

  class ControladorSintomas{

    static public function ctrConsultaSintomas(){

      $tabla = "sintomas";

      $resultado = ModeloSintomas::mdlConsultaSintomas($tabla);

      return $resultado;

    }

    static public function ctrConsultaSkSintoma($sintoma){

      $tabla = "sintomas";

      $resultado = ModeloSintomas::mdlConsultaSkSintomas($tabla,$sintoma);

      return $resultado;

    }

    static public function ctrConsultaNombreSintoma($sintoma){

      $tabla = "sintomas";
      $resultado = ModeloSintomas::mdlConsultaNombreSintomas($tabla,$sintoma);

      return $resultado;

    }

    static public function ctrConsultaSintomaPaciente($sk_paciente){

      $tabla = "sintomas_pacientes";
      $resultado = ModeloSintomas::mdlConsultaSintomaPaciente($tabla,$sk_paciente);

      return $resultado;

    }

  }
