<?php

class ControladorPaciente{

  static public function insertarPaciente($arreglo_datos, $id_user){

    $tabla = "pacientes";

    $resultado = ModeloPaciente::insertarPaciente($tabla, $arreglo_datos, $id_user);

    return $resultado;

  }

  static public function insertarCita($datos){

    $tabla = "citas";

    $resultado = ModeloPaciente::insertarCita($tabla, $datos);

    return $resultado;

  }

  // static public function InsertDatosGeneral($tabla, $item1, $item2, $item3, $valor2, $valor3){

  //   $resultado = ModeloPaciente::InsertDatosGeneral($tabla, $item1, $item2, $item3, $valor2, $valor3);

  //   return $resultado;

  // }

  static public function InsertDatosGeneral($tabla, $id_paciente, $valor, $item3){

    $resultado = ModeloPaciente::InsertDatosGeneral($tabla, $id_paciente, $valor, $item3);

    return $resultado;

  }

  static public function ConsultaPaciente($sk_paciente){

     $tabla = "pacientes";

    $resultado = ModeloPaciente::ConsultaPaciente($tabla,$sk_paciente);

    return $resultado;

  }

  static public function ConsultaPacientesNoConfirmacion($no_confirmacion){

    $tabla = "citas";

   $resultado = ModeloCita::ConsultaPacienteEnCita($tabla,$no_confirmacion);

   return $resultado;

 }

  static public function consultaIdPaciente($clave_paciente){

    $tabla = "pacientes";

    $resultado = ModeloPaciente::consultaIdPaciente($tabla,$clave_paciente);

    return $resultado;

  }

  static public function consultaIdCita($id_paciente){

    $tabla = "citas";

    $resultado = ModeloPaciente::consultaIdCita($tabla,$id_paciente);

    return $resultado;

  }

}

