<?php

class ControladorUbicacion{

  static public function ctrConsultaEstado(){

    $tabla = "ubicaciones";

    $resultado = ModeloUbicaciones::mdlConsultaEstado($tabla);

    return $resultado;

  }

  static public function ctrConsultaMunicipio($tabla,$estado){

    $resultado = ModeloUbicaciones::mdlConsultaMunicipio($tabla,$estado);

    return $resultado;

  }

  static public function ctrConsultaSkUbicacion($estado,$municipio){

    $tabla = "ubicaciones";

    $resultado = ModeloUbicaciones::mdlConsultaSkUbicacion($tabla,$estado,$municipio);

    return $resultado[0];

  }

  static public function ctrCfdi(){

    $tabla = "cfdis";

    $resultado = ModeloUbicaciones::mdlConsultaCfdi($tabla);

    return $resultado;

  }

  static public function ctrRegimenFiscal(){

    $tabla = "regimen_fiscal";

    $resultado = ModeloUbicaciones::mdlRegimenFiscal($tabla);

    return $resultado;

  }


}

