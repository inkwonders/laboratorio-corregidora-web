<?php

  class ControladorEnfermedades{

    static public function ctrConsultaEnfermedades(){

      $tabla = "enfermedades";

      $resultado = ModeloEnfermedades::mdlConsultaEnfermedades($tabla);

      return $resultado;

    }
    static public function ctrConsultaSkEnfermedades($enfermedad){

      $tabla = "enfermedades";

      $resultado = ModeloEnfermedades::mdlConsultaSkEnfermedades($tabla,$enfermedad);

      return $resultado[0];

    }

    static public function ctrConsultaNombreEnfermedades($enfermedad){

      $tabla = "enfermedades";

      $resultado = ModeloEnfermedades::mdlConsultaNombreEnfermedades($tabla,$enfermedad);

      return $resultado;

    }

    static public function ctrConsultaEmfermedadesPaciente($sk_paciente){

      $tabla = "enfermedades_pacientes";

      $resultado = ModeloEnfermedades::mdlConsultaEmfermedadesPaciente($tabla,$sk_paciente);

      return $resultado;

    }

    static public function ctrConsultaNombreEmfermedad($sk_emfermedad){

      $tabla = "enfermedades";

      $resultado = ModeloEnfermedades::ctrConsultaNombreEmfermedad($tabla,$sk_emfermedad);

      return $resultado;

    }

  }
