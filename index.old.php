<?php
session_start();
error_reporting(0);

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1

header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado

?>

<!doctype html>
<html lang="es">

<head>
<!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>

  <!-- <meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" /> -->

  <!-- <meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache"> -->

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, maximum-scale=5" />
  <meta http-equiv="X-UA-Compatible" content="IE=11" />
  <title>LABORATORIO CORREGIDORA</title>

  <meta name="description" content="Laboratorio de Análisis clínicos en Querétaro">
  <meta name="robots" content="all" />
  <meta name="laboratorio corregidora" content="laboratoriocorregidora.com.mx" />
  <meta name="distribution" content="global" />
  <meta name="keywords" content="Mejor laboratorio en Querétaro
Laboratorios en Querétaro
Laboratorios Corregidora
Corregidora
Juriquilla Laboratorio
Milenio Juriquilla
Pueblo Nuevo Laboratorio
Jurica Laboratorio
Tecnológico Laboratorio
Mejor laboratorio de Querétaro
Laboratorio de análisis clínicos Querétaro
Resultados laboratorio corregidora
Laboratorio acreditado Querétaro
Mejor laboratorio de Querétaro
Análisis Laboratorio Corregidora
Costos Laboratorio Corregidora
Laboratorios Corregidora
Laboratorios Querétaro certificado
Laboratorios Querétaro acreditado
Laboratorio especialista en toma de muestras difíciles
Laboratorio Querétaro alta tecnología
Laboratorio con amplio menú de pruebas
Laboratorio con más años de experiencia
Laboratorio confiable
Resultados exactos y precisos laboratorio
Laboratorios Vázquez Mellado
Toma de muestras a domicilio laboratorio
Laboratorio con servicio fines de semana
Laboratorio con personal competente
Laboratorio con servicio los sábados y domingos
Sucursales laboratorio corregidora
Costos análisis laboratorio corregidora
Laboratorios en Querétaro ISO
Laboratorio corregidora sucursales
Laboratorio con costos accesibles en Querétaro
Laboratorio con costos económicos en Querétaro
Laboratorio con costos bajos en Querétaro
Laboratorio entrega rápido resultados
Laboratorio resultados confiables
Laboratorio entrega resultados el mismo día
Laboratorio con mayor prestigio en Querétaro
Laboratorio más recomendado en Querétaro
Laboratorio con el mejor control de calidad
Laboratorio de análisis clínicos con mayor calidad
Laboratorio de calidad
Laboratorio con excelencia en los resultados
Laboratorio con resultados confiables
Laboratorio personal especialista en toma de muestra pediátrica
Análisis Clínicos
Análisis de sangre
Análisis de orina
Análisis generales
Análisis para infección
Análisis de laboratorio
Análisis  de VIH
Análisis de prueba de embarazo
Menú de análisis de laboratorio
Menú de análisis clínicos
Análisis clínicos
Análisis check up" />

  <link rel="stylesheet" href="estilo.css" />
  <!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->

  <link rel="stylesheet" type="text/css" href="slick/slick.css" />
  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />

  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
  <link rel="manifest" href="img/favicon/site.webmanifest">
  <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="theme-color" content="#ffffff">
  <!-- favicon -->

  <!-- fuentes -->
  <link rel="preload" href="/fonts/Montserrat-Black.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-Bold.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-ExtraBold.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-Medium.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-Regular.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-SemiBold.ttf" as="font" type="font/ttf" crossorigin>
  <!-- end fuentes -->

    <!-- Meta Pixel Code -->
  <!-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '224775150126235');
  fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=224775150126235&ev=PageView&noscript=1"
  /></noscript> -->
  <!-- End Meta Pixel Code -->
  <!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '2067231483629662');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=2067231483629662&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

  <!-- Start of labcorregidorachat Zendesk Widget script -->
  <!-- <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=a4e70b89-6ec3-4ab7-b3d9-447e3d21424a"> </script> -->
  <!-- End of labcorregidorachat Zendesk Widget script -->

  <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
  <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->

  <!-- <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'AW-691396176');
  </script> -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script> -->
  <!-- <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-152013601-2');
  </script> -->
  <!-- end Global site tag (gtag.js) - Google Analytics -->

</head>

<body>
  <?php include('header.php'); ?>
  <section id="home">

    <div class="slider-contender" id="home-slider">
      <!-- <div class="sl-11 sl"></div> -->
      <!-- <div class="sl-15 sl"></div> aviso de septiembre-->
      <div class="sl-16 sl"></div>
      <div class="sl-10 sl"></div>
      <div class="sl-9 sl"></div>
      <div class="sl-4 sl" onclick="abrir_citas_banner()"></div>
      <div class="sl-5 sl"></div>
      <div class="sl-0 sl"></div>
      <div class="sl-1 sl">
        <div class="left1 left_sl"></div>
        <div class="right1 right_sl"></div>
      </div>
      <div class="sl-2 sl">
        <div class="left2 left_sl"></div>
        <div class="right2 right_sl"></div>
      </div>
      <div class="sl-3 sl" onclick="abrir_citas_imagenologia()">
        <div class=""></div>
      </div>
    </div>

    <div class="tabla-datos-contender">
      <table class="tabla-datos">
        <tr>
          <td class="nosotros" colspan="2">
            <span class="titulo" style="color:#194271">NOSOTROS</span><br /><br />
            <p class="texto" style="color:#4D4D4D">
              Somos un laboratorio de más de 70 años de experiencia. En Laboratorio Corregidora nuestro propósito es cuidar la salud de nuestros pacientes ofreciendo un servicio cálido y de excelencia.
            </p>
          </td>
          <td class="nuevas" rowspan="2">
            <span class="titulo" style="color:#FFF">NUEVAS<br />INSTALACIONES</span><br /><br />
            <p class="texto" style="color:#FFF">
              <!-- Atención en nuevas instalaciones en el laboratorio central en El Marqués con amplio estacionamiento. -->
              Nuevas instalaciones de toma de muestra en El Refugio
            </p><br /><br />
            <a rel="noopener" class="boton-ver" href="https://goo.gl/maps/aKKPSdGSAPKGxmow6" target="_blank">
              VER MÁS
            </a>
          </td>
        </tr>
        <tr>
          <td class="contacto" style="color:#FFF">
            <span class="titulo subrayado">CONTACTO</span><br /><br />
            <p class="texto-light">

            </p>
            <div class="cont_tel_cuadro_azul">
              <div class="cont_tel_cuadro_azul_int">
                <a rel="noopener" class="no_link" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank" style="justify-content: space-around;display: flex;align-items: center;">
                  <img loading="lazy" class="img_menu p_r" src="img/svg/wa.svg" alt="whatsapp" width="auto" height="auto">
                  <span style="padding-left: 5px;">442 120 6215 <br>(solo mensajes)</span>
                </a>
              </div>
              <div class="cont_tel_cuadro_azul_int">
                <img loading="lazy" class="img_menu p_r" src="img/svg/tel.svg" alt="teléfono" width="auto" height="auto">
                <div class="cont_tel_cuadro_azul_int_num">
                  <a class="no_link" href="tel:4422121052">442 212 1052</a>
                  <!-- <a class="no_link" href="tel:4422121901">442 212 1901</a> -->
                </div>
              </div>
            </div>
            <a class="no_padding no_link" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank">
              <p class="texto-light correo_txt">contacto@laboratoriocorregidora.com.mx</p>
            </a>
          </td>
          <td class="resultados btn-res" style="color:#FFF">
            <span class="titulo">QUIERO VER MIS RESULTADOS</span>
          </td>
        </tr>
      </table>
    </div>
  </section>
  <section id="servicios">
    <span class="titulo" style="color:#194271">SERVICIOS</span><br /><br />
    <div class="div-servicio">
      <img loading="lazy" src="img/icon-clinic.svg" class="img-servicio" alt="Laboratorio Corregidora" width="auto" height="auto" /><br /><br />
      <span class="sub-servicio">Análisis clínicos</span><br /><br />
      <p class="texto-servicio">
        Estudios de sangre, orina, heces, etc. No requiere programar cita.
      </p><br /><br />
      <a href="/analisis.php" rel="noopener">
        <div class="boton-serv">VER MÁS</div>
      </a>
    </div>
    <div class="div-servicio">
      <img loading="lazy" src="img/icon-dom.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
      <span class="sub-servicio">Toma de muestras a domicilio</span><br /><br />
      <p class="texto-servicio">
        Llame para programar su cita.
      </p><br /><br />
      <div class="boton-serv oculto">VER MÁS</div>
    </div>
    <div class="div-servicio">
      <img loading="lazy" src="img/svg/imagenologia.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
      <span class="sub-servicio">Imagenología</span><br /><br />
      <p class="texto-servicio">
      Ultrasonidos, radiografías, estudios contrastados y biopsias.
      </p><br /><br />
      <div class="boton-serv"><a href="https://imagenologia.laboratoriocorregidora.com.mx/">VER MÁS</a></div>
    </div>
    <div class="div-servicio">
      <img loading="lazy" src="img/svg/icono-covid-info.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
      <span class="sub-servicio">Información Covid-19</span><br /><br />
      <p class="texto-servicio">
      Algoritmo informativo de las diferentes pruebas para Covid-19.
      </p><br /><br />
      <a class="boton-serv" href="covid.php">VER MÁS</a>
    </div>
    <div class="div-servicio">
      <img loading="lazy" src="img/icon-insti.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
      <span class="sub-servicio">Atención a empresas e instituciones</span><br /><br />
      <p class="texto-servicio">
        Ofrecemos análisis con un costo especial para las empresas.
      </p><br /><br />
      <a class="boton-serv" href="mailto:lcn75@laboratoriocorregidora.com.mx">Contáctanos</a>
    </div>
    <div class="div-servicio">
      <img loading="lazy" src="img/icon-hema.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
      <span class="sub-servicio">Clínica Hematológica</span><br /><br />
      <p class="texto-servicio">
        Consulta médica con un especialista Hematólogo, llamé para agendar su cita.
      </p><br /><br />
      <a rel="noopener" class="boton-serv" href="http://clinicahematologica.com/" target="_blank">VER MÁS</a>
    </div>
  </section>
  <section id="preguntas">
    <div class="noventa">
      <div class="left-preg">PREGUNTAS FRECUENTES</div>
      <div class="right-preg oculto">VER+</div>
      <br /><br />
      <div class="vid-preg">
        <img loading="lazy" src="img/vid-sangre.svg" class="img-preg" />
        <img loading="lazy" src="img/icon-vid.svg" class="vid-icon" op="1" alt="Laboratorio Corregidora" />
        <div class="w-full div-sub-preg">
          ¿Cómo debo de venir a la toma de muestra en sangre?
        </div>
      </div>
      <div class="vid-preg">
        <img loading="lazy" src="img/vid-24.svg" class="img-preg" />
        <img loading="lazy" src="img/icon-vid.svg" class="vid-icon" op="2" />
        <div class="w-full div-sub-preg">
          ¿Cómo recolecto una muestra para el exámen general de orina?
        </div>
      </div>
      <!--<div class="vid-preg">
        <img src="img/vid-examen.svg" class="img-preg" />
        <img src="img/icon-vid.svg" class="vid-icon" />
        <div class="div-sub-preg">
          ¿Cómo debo de venir a toma de sangre?
        </div>
      </div>
      <div class="vid-preg">
        <img src="img/vid-urocultivo.svg" class="img-preg" />
        <img src="img/icon-vid.svg" class="vid-icon" />
        <div class="div-sub-preg">
          ¿Cómo debo de venir a toma de sangre?
        </div>
      </div>-->
    </div>
  </section>
  <section id="video">
    <div class="slider-contender">
      <!--<img class="img-video" src="img/home_slide/micro.png" />-->
    </div>
    <div class="mapa-contender" id="sucursales">
      <div class="mapa">
        <!--<iframe class="imapa" src="mapa.html" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
        <!-- <iframe src="https://www.google.com/maps/d/embed?mid=1sCTZ5RO71qjH9feW9PXnBM4V1kBGjlff&hl=es-419" style="width: 100%; height: 100%;"></iframe> -->
        <div id="map"></div>
      </div>
      <div class="datos-mapa" id="">
        <div style="text-align:center">
          <span class="titulo sucursales-boton" style="color:#FFF">SUCURSALES</span>
        </div><br /><br />

        <div class="cont_cuadro" style="display: flex;align-items: center;">
                <!-- <a rel="noopener" class="no_link" href="https://www.facebook.com/Laboratorios-Corregidora-108767437596443" target="_blank" style="justify-content: space-around;display: flex;padding: 3px;"><img style="width: 27px;" class="img_menu" src="img/svg/ico_fb.svg" alt="facebook"></a>
                <a rel="noopener" class="no_link" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank" style="justify-content: space-around;display: flex;padding: 3px;"><img style="width: 27px;" class="img_menu" src="img/svg/ico_mail3.svg" alt="email"></a> -->

                <!-- <a rel="noopener" class="no_link" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank" style="justify-content: space-around;display: flex;padding: 3px;">
                  <img style="width: 25px;"  class="img_menu " src="img/svg/ico_ws2.svg" alt="whatsapp" width="auto" height="auto">
                  <span style="padding-left: 5px;font-size: 14px;"><b>442 120 6215</b> <br><p style="font-size: 13px;">(solo mensajes)</p></span>
                </a> -->
                <!-- <div style="display: flex;align-items: center;padding-left: 18px;">
                <img style="width: 38px;" loading="lazy" class="img_menu p_r" src="img/svg/ico_fon.svg" alt="teléfono" width="auto" height="auto">
                <div class="cont_tel_cuadro_azul_int_num">
                  <a class="no_link" style="font-size: 15px;" href="tel:4422121052"><b>442 212 1052</b></a>
                </div>
                </div> -->
        </div>
        <br /><br />
        <!-- <div class="cont_cuadro" style="display: flex;justify-content: center;align-items: center;">
                
        </div>
         -->
        <span class="menu-map opc-menu-map-0" op="proceso"><b>Laboratorio Central (El Marqués), <label style="color: #22b573;">Sucursal para pruebas COVID</label></b></span><br /><br />
        <span class="menu-map opc-menu-map-1" op="centro">Centro histórico</span><br /><br />
        <span class="menu-map opc-menu-map-2" op="jurica">Jurica</span><br /><br />
        <span class="menu-map opc-menu-map-3" op="juriquilla">Juriquilla</span><br /><br />
        <span class="menu-map opc-menu-map-4" op="tecnologico">Tecnológico</span><br /><br />
        <span class="menu-map opc-menu-map-5" op="pueblo">Pueblo nuevo</span><br /><br />
        <span class="menu-map opc-menu-map-6" op="milenio">Milenio III</span><br /><br />
        <span class="menu-map opc-menu-map opc-menu-map-7" op="refugio">El Refugio </span><br /><br /><br />

        <div class="datos-suc">
          Av. 5 de Febrero No. 2125 Local 9-10 Planta Baja<br />
          Condominio Plaza Norte<br />
          Zona Industrial Benito Juárez<br /><br />
          Horario de atención: <br>
          Lunes a sábado de
          7:00 hrs. a 11:00 hrs. <br /><br /><br />

          <!-- <a class="a-white" href='tel:442 212 1052'>442 212 10 52 </a> / -->
          <!-- <a rel="noopener" class="a-white" href='tel:442 212 1901'> 442 212 19 01</a> -->
        </div>
      </div>
    </div>
  </section>
  <div class="div-footer">
    <div class="footer-cell" style="width: 48%;">
      <a rel="noopener" href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de Privacidad</a> &nbsp; / &nbsp; <a href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a> &nbsp; / &nbsp; <a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a> &nbsp; / &nbsp; <span class="menu-el" op="sucursales" style="cursor: pointer; ">Contáctanos</span> &nbsp; / &nbsp; <a class="menu-el" href="/contacto.php"> Felicitaciones, Quejas y Sugerencias </a>
    </div>
    <div class="footer-cell">
      <p>Permiso de Publicidad: 203301201A0872</p>
    </div>
    <div class="footer-cell" style="text-align:right">
      <a rel="noopener" class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a>
    </div>
  </div>
  <section id="home-mov">

    <div class="slider-contender" id="slide-mov">
      <!-- <div class="sl-11-mov sl-mov"></div> -->
      <!-- <div class="sl-15-mov sl-mov"></div> aviso de septiembre -->
      <div class="sl-16-mov sl-mov"></div> 
      <div class="sl-10-mov sl-mov"></div>
      <div class="sl-9-mov sl-mov"></div>
      <div class="sl-7-mov sl-mov" onclick="abrir_citas_banner()"></div>
      <div class="sl-8-mov sl-mov" ></div>
      <div class="sl-1-mov sl-mov"></div>
      <div class="sl-2-mov sl-mov pdf_boton1"></div>
      <div class="sl-3-mov sl-mov pdf_boton2"></div>
      <div class="sl-4-mov sl-mov pdf_boton3"></div>
      <div class="sl-5-mov sl-mov pdf_boton4"></div>
      <div class="sl-6-mov sl-mov"></div>

      <!--<div class="sl-2 sl-mov">
        <section class="guardias-mov">
          <span class="title">ESTIMADO CLIENTE: </span><br><br>
          <span class="texto">Le informamos que el Laboratorio Corregidora ha detectado personas ajenas a la institución que ofrecen servicios a nuestro nombre, es por ello que para su seguridad solicitamos que para contactarnos utilice nuestros medios de comunicación oficiales.</span>
        </section>
      </div>


      <div class="sl-3 sl-mov">
        <section class="cont_azul">
          <p class="min">SERVICIO DE</p><br>
          <p class="large">GUARDIAS</p><br><br>
          <p class="semi">SÁBADOS / DOMINGOS</p><br>
          <p class="nom">Sábados de 5 a 7 PM y Domingos de 9 AM a 12 PM <br>
            Comunicarse a los teléfonos: <br>
            <a href="tel:4427925970">442 792 5970</a> o al <a href="tel:4427905722">442 790 5722</a><br>
          Costo adicional del 50% más sobre el precio del estudio(s).</p>
        </section>
      </div>-->

    </div>
    <div class="nosotros">
      <span class="titulo" style="color:#194271">NOSOTROS</span><br /><br />
      <p class="texto" style="color:#4D4D4D">
        Somos un laboratorio de más de 70 años de experiencia. En laboratorio corregidora nuestro propósito es cuidar la salud de nuestros pacientes ofreciendo un servicio cálido y de excelencia.
      </p>
    </div>
    <div class="contacto" style="color:#FFF">
      <span class="titulo">CONTACTO</span><br /><br />
      <p class="texto-light">

      </p>
      <div class="cont_tel_cuadro_azul">
        <div class="cont_tel_cuadro_azul_int">
          <a rel="noopener" class="no_link" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank" style="justify-content: space-around;display: flex;align-items: center;">
            <img loading="lazy" class="img_menu p_r" src="img/svg/wa.svg" alt="whatsapp" width="auto" height="auto">
            <span style="padding-left: 5px;">442 120 6215 <br> (solo mensajes)</span>
          </a>
        </div>
        <div class="cont_tel_cuadro_azul_int">
          <img loading="lazy" class="img_menu p_r" src="img/svg/tel.svg" alt="teléfono" width="auto" height="auto">
          <div class="cont_tel_cuadro_azul_int_num">
            <!-- <a class="no_link" href="tel:4422121052">442 212 1052</a> -->
            <a rel="noopener" class="no_link" href="tel:4422121901">442 212 1901</a>
          </div>
        </div>
      </div>
      <a rel="noopener" class="no_padding no_link" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank">
        <p class="texto-light correo_txt">contacto@laboratoriocorregidora.com.mx</p>
      </a>
    </div>
    <div class="resultados btn-res" style="color:#FFF;text-align:center">
      <span class="titulo" style="font-size:16px">QUIERO VER MIS RESULTADOS</span>
    </div>
    <div class="nuevas">
      <span class="titulo" style="color:#FFF">NUEVAS<br />INSTALACIONES</span><br /><br />
      <p class="texto" style="color:#FFF">
        A partir del 4 de Marzo del presente año estaremos atendiéndoles en nuestras nuevas instalaciones en El Marqués.<br /><br />
        Contamos con amplio estacionamiento.
      </p><br /><br />
      <a rel="noopener" class="boton-ver" href="https://goo.gl/maps/DGbuYA5VX7YMsqEJ7" target="_blank">
        VER MÁS
      </a>
    </div>
  </section>
  <section id="serv-mov">
    <br /><br />
    <span class="titulo" style="color:#194271;font-size:32px">SERVICIOS</span><br /><br /><br /><br />
    <div class="" id="slide-serv">
      <div class="slide slide-serv">
        <div class="noventa">
          <img loading="lazy" src="img/icon-clinic.svg" class="img-servicio" alt="Laboratorio Corregidora" width="auto" height="auto" /><br /><br />
          <span class="sub-servicio">Análisis clínico</span><br /><br />
          <p class="texto-servicio">
            Estudios de sangre, orina, heces, etc. No requiere programar cita.
          </p><br /><br />
          <div class="boton-serv">VER MÁS</div>
        </div>
      </div>
      <div class="slide slide-serv">
        <div class="noventa">
          <img loading="lazy" src="img/icon-dom.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
          <span class="sub-servicio">Análisis a domicilio</span><br /><br />
          <p class="texto-servicio">
            Acudimos a su domicilio, llamé para programar su cita.
          </p><br /><br />
          <div class="boton-serv no-visible">VER MÁS</div>
        </div>
      </div>
      <div class="slide slide-serv">
        <div class="noventa">
          <img loading="lazy" src="img/svg/imagenologia.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
          <span class="sub-servicio">Imagenología</span><br /><br />
          <p class="texto-servicio">
          Ultrasonidos, radiografías, estudios contrastados y biopsias.
          </p><br /><br />
          <div class="boton-serv"><a href="https://imagenologia.laboratoriocorregidora.com.mx/">VER MÁS</a></div>
        </div>
      </div>
      <div class="slide slide-serv">
        <div class="noventa">
          <img loading="lazy" src="img/svg/icono-covid-info.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
          <span class="sub-servicio">Información Covid-19</span><br /><br />
          <p class="texto-servicio">
          Algoritmo informativo de las diferentes pruebas para Covid-19.
          </p><br /><br />
          <div class="boton-serv"><a href="covid.php">VER MÁS</a></div>
        </div>
      </div>
      <div class="slide slide-serv">
        <div class="noventa">
          <img loading="lazy" src="img/icon-insti.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
          <span class="sub-servicio">Atención a empresas e instituciones</span><br /><br />
          <p class="texto-servicio">
            Ofrecemos análisis con un costo especial para las empresas e instituciones.
          </p><br /><br />
          <a rel="noopener" class="boton-serv" href="mailto:lcn75@laboratoriocorregidora.com.mx">Contáctanos</a>
        </div>
      </div>
      <div class="slide slide-serv">
        <div class="noventa">
          <img loading="lazy" src="img/icon-hema.svg" class="img-servicio" width="auto" height="auto" /><br /><br />
          <span class="sub-servicio">Clínica Hematológica</span><br /><br />
          <p class="texto-servicio">
            Consulta médica con un especialista Hematólogo, llamé para agendar su cita.
          </p><br /><br />
          <a rel="noopener" class="boton-serv" href="http://clinicahematologica.com/" target="_blank">VER MÁS</a>
        </div>
      </div>
    </div>
  </section>
  <section id="preg-mov">
    <br /><br />
    <span class="titulo" style="color:#FFF;">PREGUNTAS FRECUENTES</span><br /><br /><br /><br />
    <div class="slider-contender" id="slide-preg-mov" style="height:70vh">
      <div class="slide slide-preg">
        <div class="noventa">
          <div class="vid-preg-mov" style="background:url(img/vid-sangre.svg);"><img loading="lazy" src="img/icon-vid.svg" class="vid-icon" op="1" alt="Laboratorio Corregidora" /></div><br /><br />
          <span class="text-preg">¿Cómo debo de venir a toma de sangre?</span>
        </div>
      </div>
      <div class="slide slide-preg">
        <div class="noventa">
          <div class="vid-preg-mov" style="background:url(img/vid-24.svg);"><img loading="lazy" src="img/icon-vid.svg" class="vid-icon" op="2" alt="Laboratorio Corregidora" /></div><br /><br />
          <span class="text-preg">¿Cómo recolecto una muestra para el exámen general de orina?</span>
        </div>
      </div>
      <!--<div class="slide slide-preg">
        <div class="noventa">
          <div class="vid-preg-mov" style="background:url(img/vid-examen.svg);"><img src="img/icon-vid.svg" class="vid-icon" /></div><br /><br />
          <span class="text-preg">¿Cómo recolectar muestras para examen general de orina?</span>
        </div>
      </div>
      <div class="slide slide-preg">
        <div class="noventa">
          <div class="vid-preg-mov" style="background:url(img/vid-urocultivo.svg);"><img src="img/icon-vid.svg" class="vid-icon" /></div><br /><br />
          <span class="text-preg">¿Cómo recolectar muestra para urocultivo?</span>
        </div>
      </div>-->
    </div>
    <div class="next"></div>
    <div class="prev"></div>
  </section>
  <section id="vid-mov">
    <div class="mapa">
      <iframe title="mapa" class="imapa" src="mapa.html" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <!-- <iframe src="https://www.google.com/maps/d/embed?mid=1sCTZ5RO71qjH9feW9PXnBM4V1kBGjlff&hl=es-419" width="640" height="480"></iframe> -->
    </div>
    <div class="datos-mapa">
      <h3 class="titulo">SUCURSALES</h3><br /><br />
      <div class="cont_cuadro" style="display: flex;align-items: center;padding-left: 30px;">
                <!-- <a rel="noopener" class="no_link" href="https://www.facebook.com/Laboratorios-Corregidora-108767437596443" target="_blank" style="justify-content: space-around;display: flex;padding: 3px;"><img style="width: 36px;" class="img_menu" src="img/svg/fa.svg" alt="facebook"></a>
                <a rel="noopener" class="no_link" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank" style="justify-content: space-around;display: flex;padding: 3px;"><img style="width: 36px;" class="img_menu" src="img/svg/em.svg" alt="email"></a>

                <a rel="noopener" class="no_link" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank" style="justify-content: space-around;display: flex;padding: 3px;">
                  <img style="width: 36px;"  class="img_menu " src="img/svg/wa.svg" alt="whatsapp" width="auto" height="auto">
                </a>
                <div style="display: flex;align-items: center;padding-left: 50px;">
                 <img style="width: 45px;" loading="lazy" class="img_menu p_r" src="img/svg/ico_phone_azul.svg" alt="teléfono" width="auto" height="auto">
                </div> -->
        </div>
        <br>
        <div class="cont_cuadro" style="display: flex;align-items: center;padding-left: 35px;">
                
                  <!-- <span style="padding-left: 5px; color: black;"><b>442 120 6215</b> <br><p style="font-size: 13px;">(solo mensajes)</p></span> -->
                <div style="display: flex;align-items: center;padding-left: 40px;">
                
                <div class="cont_tel_cuadro_azul_int_num">
                  <!-- <a style="color: black;" class="no_link" href="tel:4422121052"><b>442 212 1052</b></a> -->
                </div>
                </div>
        </div>
        <br><br>
      <!--<span class="opc-menu-map">Centro histórico</span> | <span class="opc-menu-map">Jurica</span> | <span class="opc-menu-map">Juriquilla</span><br /><br />
      <span class="opc-menu-map">Tecnológico</span> | <span class="opc-menu-map">Pueblo nuevo</span> | <span class="opc-menu-map">Milenio III</span><br /><br /><br />-->
      <div class="cont_sucursales_mov">
        <p class="menu-map opc-menu-map opc-menu-map-0" op="proceso"><b>Laboratorio Central (El Marqués), <label style="color: #22b573;">Sucursal para pruebas COVID</label></b></p>
        <p class="menu-map opc-menu-map opc-menu-map-1" op="centro">Centro histórico</p>
        <p class="menu-map opc-menu-map opc-menu-map-2" op="jurica">Jurica</p>
        <p class="menu-map opc-menu-map opc-menu-map-3" op="juriquilla">Juriquilla</p>
        <p class="menu-map opc-menu-map opc-menu-map-4" op="tecnologico">Tecnológico </p>
        <p class="menu-map opc-menu-map opc-menu-map-5" op="pueblo">Pueblo nuevo</p>
        <p class="menu-map opc-menu-map opc-menu-map-6" op="milenio">Milenio III</p>
        <p class="menu-map opc-menu-map opc-menu-map-7" op="refugio">El Refugio</p>
        <p class="lh-2">
          <br><br>
          <span class="datos-suc a-blue">
            Av. 5 de Febrero No. 2125 Local 9-10 Planta Baja<br />
            Condominio Plaza Norte, Zona Industrial Benito Juárez<br />
            Horario de atención: Lunes a sábado de 7:00 a.m. a 11:00 a.m.<br /><br /><br />

            <!--<a class="a-blue bold-font" href='tel:442 212 1052'> 442 212 10 52</a> / --><a rel="noopener" class="a-blue bold-font" href='tel:442 212 1901'> 442 212 19 01</a>
          </span>

        </p>
      </div>
      <!--<div class="datos-suc">
        Av. 5 de Febrero No. 2125 Local 9-10 Planta Baja<br />
        Condominio Plaza Norte, Zona Industrial Benito Juárez<br /><br /><br />

       <a class="a-white" href='tel:442 212 1052'> 442 212 10 52</a> / <a class="a-white" href='tel:442 212 1901'> 442 212 19 01</a>
      </div>-->
    </div>
  </section>
  <div class="footer-mov">
    <div class="cont_footer_mov">
      <p><a rel="noopener" href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de privacidad</a></p>
      <p><a rel="noopener" href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a></p>
      <p><a rel="noopener" href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a></p>
      <p><span class="menu-el link-a" op="vid-mov" style="font-family: m-regular!important; font-size: 16px!important; cursor: pointer!important;">Contáctanos</span></p>
      <p><a href="contacto.php" class="link-a">Felicitaciones, Quejas y Sugerencias</a></p>
      <p style="text-align: center;">Permiso de Publicidad: 203301201A0872</p>
      <p><a rel="noopener" class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a></p>
    </div>
  </div>

  <!-- CHAT WHATSAPP -->
  <a rel="noreferrer" class="wa-contenedor" href="https://wa.link/b0x4ly" target="_blank">
        <div class="wa-contenedor__icon"></div>
        <div class="wa-contenedor__banner">
            <span class="wa-contenedor__texto">¿Ayuda?</span>
            <div class="wa-contenedor__barra"></div>
        </div>
    </a>
    <!-- FIN CHAT -->

  <!-- modal -->
  <div class="modal no-visible">
    <div class="close-modal">
      <div class="izq"></div>
      <div class="der"></div>
    </div>
    <!--<iframe id='video-youtube' src='' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>-->

    <iframe title="video 2" id='video-youtube' src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <!-- end modal -->

  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtmAZBLkqZEWcMd6tZPElNrxIhT4k4tMI&libraries=places&callback=initMap"></script>
  <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQB4qG_dwBhjFw377qcK8QX26OhEW4xYA&callback=initMap"></script> -->
  


  <script type="text/javascript">
    $(document).ready(function() {
      cargar_carrito_header();
      $('#home-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        autoplay: true,
        autoplaySpeed: 5000,
        slide: 'div',
        mobileFirst: true,
      });
      $('#slide-mov').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        autoplay: false,
        autoplaySpeed: 2000,
      });
      $('#slide-serv').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true,
      });
      $('#slide-preg-mov').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
      });

      let ruta = localStorage.getItem("opcion_animate");

      console.log(ruta);

      if (ruta != null) {


        if (screen.width < 980) {
          $('html, body').animate({
            scrollTop: ($("#" + ruta).offset().top - 100)
          }, 1500);
        } else {
          $('html, body').animate({
            scrollTop: ($("#" + ruta).offset().top - 100)
          }, 1500);
        }

        localStorage.setItem("opcion_animate", null);

      }

    });
  </script>
  <script src="js/script.js"></script>
  <script type="text/javascript">
    function abrir_citas_banner() {
      window.open("https://www.laboratoriocorregidora.com.mx/citas/", "_blank");

    }
    function abrir_citas_imagenologia() {
      window.open("https://imagenologia.laboratoriocorregidora.com.mx/");

    }
    $(".pdf_boton").click(function() {
      window.open("pdf/rt-pcr_para_la_deteccion_de_covid-19.pdf", "_blank");
    });

    $(".pdf_boton1, .left1").click(function() {
      window.open("pdf/MAYORES INFORMES PRUEBA PCR PARA CORONAVIRUS 12 de marzo 2021.pdf", "_blank");
      console.log("click 1");
    });

    $(".pdf_boton2, .right1").click(function() {
      window.open("pdf/MAYORES INFORMES PRUEBA RAPIDA DE ANTIGENO CORONAVIRUS 12 de marzo 2021.pdf", "_blank");
      console.log("click 2");
    });

    $(document).on("click", ".pdf_boton3, .left2", function() {
      window.open("pdf/MAYORES INFORMES PRUEBA RAPIDA DE ANTicuerpos CORONAVIRUS 12 de marzo 2021.pdf", "_blank");
      console.log("click 3");
    });

    $(document).on("click", ".pdf_boton4, .right2", function() {
      window.open("pdf/MAYORES INFORMES MUESTRA DE SANGRE PACIENTE POSITIVO DE COVID-19 12 de marzo 2021.pdf", "_blank");
      console.log("click 4");
    });

    // $(".notificacion").click(function() {
    //   console.log("medidas de ancho = " + screen.width);
    //   if (screen.width < 600) {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid_mov";
    //   } else {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid";
    //   }

    // });

    // $(".citas_covid").click(function() {
    //   window.location = "https://www.laboratoriocorregidora.com.mx/citas/";
    // });
  </script>
</body>

</html>