<?php error_reporting(0) ?>
<!doctype html>
<html lang="es">
<head>
  <!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<title>LABORATORIO CORREGIDORA</title>
<!-- <link rel="stylesheet" href="estilo.css" /> -->
<!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->

<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
<link rel="manifest" href="img/favicon/site.webmanifest">
<link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
<meta name="msapplication-TileColor" content="#000000">
<meta name="theme-color" content="#ffffff">
<!-- favicon -->

<!-- <meta name="Description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/>
<meta property="og:url" content="https://almecmexico.com"/>
<meta property="og:title" content="ALMEC"/>
<meta property="og:image" content="https://almecmexico.com/img/almec-share.jpg"/>
<meta property="og:description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/> -->

<style media="screen">
.footer-resultados {
  position: fixed;
  bottom: -70vh;
}
</style>

<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-691396176');
</script> -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-152013601-2');
</script> -->
<!-- end Global site tag (gtag.js) - Google Analytics -->

</head>
<body>
  <?php include('header.php'); ?>
  <section class="w-full">
    <div class="relative w-full cont_imagen_fondo_covid h-max">
      <div class="relative flex justify-center w-full p-8 md:p-16 h-max">
        <?php include('./dist/modules/links.html'); ?>
      </div>
      <div class="flex items-center justify-center w-full h-20 bg-opacity-80 bg-blue-lab-oscuro">
        <h2 class="text-xl text-white lg:text-2xl xl:text-3xl font-rob-medium">Información COVID 19</h2>
      </div>
    </div>

    <div class="w-full cont-opciones-resultados-covid">
      <div class="py-4 cont-menu-interno-covid w-full max-w-[94rem] lg:py-8">
          <img class="w-full desk img_cov" id="dia_img_covid" src="img/diagrama_desk.jpg" alt="covid">
          <img class="w-full mov img_cov" id="dia_img_covid_mov" src="img/diagrama_mov.jpg" alt="covid">
      </div>
    </div>
  </section>




  <?php include('footer.php'); ?>




  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>

  <script type="text/javascript">
  cargar_carrito_header();
  var URLactual = window.location.pathname ;

  // if(URLactual == "/covid.php" || URLactual == "/covid.php#dia_img_covid_mov" || URLactual == "/covid.php#dia_img_covid"){
  //   $(".citas_covid").css("top","100px");
  // }

  // if(screen.width>600){}else{ $(".citas_covid").css("top","80px"); }



  // $(".citas_covid").click(function(){
	//   window.location = "https://www.laboratoriocorregidora.com.mx/citas/";
	// });

    // $(".notificacion").css("display","none");

    $(".op_covid").addClass("menu_activo_desktop");

    $(document).on("click", ".menu-el", function(){
      let opcion = $(this).attr("op");
      switch(opcion){
        case "servicios":
          localStorage.setItem("opcion_animate", "servicios");
          window.location = "/";
          break;
        case "preguntas":
          localStorage.setItem("opcion_animate", "preguntas");
          window.location = "/";
          break;
        case "sucursales":
          localStorage.setItem("opcion_animate", "sucursales");
          window.location = "/";
          break;
        case "serv-mov":
          localStorage.setItem("opcion_animate", "serv-mov");
          window.location = "/";
          break;
        case "preg-mov":
          localStorage.setItem("opcion_animate", "vid-mov");
          window.location = "/";
          break;
        case "vid-mov":
          localStorage.setItem("opcion_animate", "sucursales");
          window.location = "/";
          break;
        default:
          $('html, body').animate({
            scrollTop: ($("#" + opcion).offset().top - 100)
          }, 600);
          break;
      }
    });

    $(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function(){
      window.location = "resultados.php";
    });

    function myFunction(x) {
      x.classList.toggle("change");
      if(document.getElementById("contenidoMenu").style.display == "none"){
        document.getElementById("contenidoMenu").style.display = "";
        $("#contenidoMenu").animate({
          height: '100vh'
        }, "slow");
      }else{
        $("#contenidoMenu").animate({
          height: '0'
        }, "slow", function(){ document.getElementById("contenidoMenu").style.display = "none"; });
      }
    }

  </script>
</body>
</html>
