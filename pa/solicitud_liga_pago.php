<?php

	include('AESCrypto.php');

	 include "../citas/controladores/motorpago.controlador.php";
	 include "../citas/modelos/motorpago.modelo.php";

	function creaxml($pago_monto,$pago_id,$cliente_correo,$datosAdicionales){

		$originalString = '<centerofpayments>
			<reference>'.$pago_id.'</reference>
			<response>approved</response>
			<foliocpagos>300591622</foliocpagos>
			<auth>437486</auth>
			<cd_response>0C</cd_response>
			<cd_error></cd_error>
			<nb_error></nb_error>
			<time>13:07:00</time>
			<date>15/08/2020</date>
			<nb_company>PRUEBAS INTEGRACIONES</nb_company>
			<nb_merchant>2016995123 AMEX MOTO</nb_merchant>
			<cc_type>CREDITO/BANCO EXTRANJERO/AMEX</cc_type>
			<tp_operation>VENTA</tp_operation>
			<cc_name></cc_name>
			<cc_number>1016</cc_number>
			<cc_expmonth>10</cc_expmonth>
			<cc_expyear>22</cc_expyear>
			<amount>'.$pago_monto.'</amount>
			<emv_key_date></emv_key_date>
			<id_url>RXVKCWCN</id_url>
			<email>'.$cliente_correo.'</email>
			<name></name>
			<ap></ap>
			<am></am>
			<cc_mask>371774XXXXX1016</cc_mask>
			'.$datosAdicionales.'
		</centerofpayments>';

		$key = "DD33CEF1A4A0DDF130996A2BD052486D";
		$url = "https://bc.mitec.com.mx/p/gen";

		$encryptedString = AESCrypto::encriptar($originalString, $key);
		//ENCRIPTACION DE LA CADENA XML
		return $encryptedString;

	}

	//FUNCION GENERAR LIGA
	function generarLiga($pago_monto,$pago_id,$cliente_correo,$pago_vigencia,$datosAdicionales){

		$conf = MotorPago::ctrConsultaConfiguracion();

		// CADENA XML CON LOS DATOS DEL PAGO

							$originalString = '<?xml version="1.0" encoding="UTF-8"?>
												<P>
													<business>
														<id_company>'.$conf["id_company"].'</id_company>
														<id_branch>'.$conf["id_branch"].'</id_branch>
														<user>'.$conf["user"].'</user>
														<pwd>'.$conf["pwd"].'</pwd>
													</business>
													<url> 
														<reference>'.$pago_id.'</reference>
														<amount>'.$pago_monto.'</amount>
														<moneda>MXN</moneda>
														<canal>W</canal>
														<omitir_notif_default>1</omitir_notif_default>
														<promociones>C</promociones>
														<st_correo>1</st_correo>
														<fh_vigencia>'.$pago_vigencia.'</fh_vigencia>
														<mail_cliente>'.$cliente_correo.'</mail_cliente>
														<st_cr>A</st_cr>
														'.$datosAdicionales.'
													</url>
												</P>';

						    

		//$key = $conf["semilla_aes"];
		$key = "DD33CEF1A4A0DDF130996A2BD052486D";

		$url = "https://bc.mitec.com.mx/p/gen";

		//ENCRIPTACION DE LA CADENA XML
		$encryptedString = AESCrypto::encriptar($originalString, $key);

		//CODIFICACION DE LA CADENA XML ENCRIPTADA
		// $post_string = "xml=".urlencode("<pgs><data0>".$conf["data_0"]."</data0><data>".$encryptedString."</data></pgs>");
		//
		// $post = "<pgs><data0>".$conf["data_0"]."</data0><data>".$encryptedString."</data></pgs>";

		$post_string = "xml=".urlencode("<pgs><data0>9265655295</data0><data>".$encryptedString."</data></pgs>");

	  $post = "<pgs><data0>9265655295</data0><data>".$encryptedString."</data></pgs>";

		//ENVIO DE DATOS A URL DE GENERACION POR CURL METODO POST

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  	CURLOPT_URL => $url,
		  	CURLOPT_RETURNTRANSFER => true,
		  	CURLOPT_ENCODING => "",
		  	CURLOPT_MAXREDIRS => 10,
		  	CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
		  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  	CURLOPT_CUSTOMREQUEST => 'POST',
		  	CURLOPT_POSTFIELDS => $post_string,
		  	CURLOPT_HTTPHEADER => array(
				"Content-Type: application/x-www-form-urlencoded"
		  	),
		));

		//RESPUESTA DE LA PAGINA DE GENERACION

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if($err){
		  echo "cURL Error #:" . $err;
		}else{
			$result = $response;
		}

		//DESENCRIPTADO DE LA CADENA DE RESPUESTA
		$decryptedString = AESCrypto::desencriptar($response, $key);

		//EXTRACION DE LA LIGA DESENCRIPTADA
		$nueva_liga = simplexml_load_string($decryptedString);
		$newurl = $nueva_liga->nb_url;
		// $newurl = substr($decryptedString,81,35); //nburl

		//DEVUELVE LA LIGA DE PAGO
		return $newurl;
	}
