/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./modules/**/*.html",
        "./modules/**/*.vue",
        "./**/*.php"
    ],
    theme: {
        extend: {
            colors: {
                'blueBT':       '#81b84e',
                'greenBT':      '#ff1d25',
                'grayBT':       '#50b9e7',
                'grayDarkBT':   '#808080',
                'whiteBT':      '#FFFFFF',
                'redBT':        '#666666',
                'redDark':      '#c03a2d',
                'gris-tabla':   '#f2f2f2',
                'naranja':      '#e2492d',
                'blue': {
                    'lab-condensed': "#011a63",
                    'lab': "#003870",
                    'lab-oscuro': "#003870",
                    'lab-claro': "#007AF6",
                },
            },
            fontFamily: {
                'rob-light': ['roboto-light'],
                'rob-bold': ['roboto-bold'],
                'rob-bold-italic': ['roboto-bold-italic'],
                'rob-italic': ['roboto-italic'],
                'rob-medium': ['roboto-medium'],
                'rob-medium-italic': ['roboto-medium-italic'],
                'rob-regular': ['roboto-regular'],
                'mitr': ['Mitr'],
                'roboto-slab': ['Roboto Slab'],
                'source-sans-pro': ['Source Sans Pro']
            },
        },
    },
    plugins: [],
}
