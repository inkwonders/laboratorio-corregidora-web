#!/bin/bash

read -p "Ingrese una version: " version
read -p "Ingrese el puerto apache: " port




echo ""
echo "generando imagenes con la versión $version"

docker build -t labc-builder:$version docker-images/labc-builder/
# docker build -t labc-apache:$version docker-images/labc-apache/




echo ""
echo "generando script npm"

# Al parecer es necesario guardar los parámetros en una variable ya que sólo de esta manera pasa los
# comandos COMMAND=$@ al ENV del contenedor
# ----------------------------------
echo '#!/bin/bash
COMMAND=$@
docker run \
    -v ./:/src \
    -e "COMMAND=npm $COMMAND" \
    labc-builder:'$version > npm
# ----------------------------------

# En producción quiza no sea necesario mantener la imagen labc-builder una vez que se generen los
# archivos de distribución pero eso es a criterio del SysAdmin/DevOps




echo ""
echo "generando docker-compose.yml"
# ----------------------------------
echo 'version: "3"

services:
    localhost:
        image: mariadb:11.2
        restart: always
        ports:
            - 3306:3306
        environment:
            - ALLOW_EMPTY_PASSWORD=no
            - MARIADB_USER=admin_root
            - MARIADB_PASSWORD=B8U6vhIv4xnWrUjiAmqc
            - MARIADB_ROOT_PASSWORD=B8U6vhIv4xnWrUjiAmqc
            - MARIADB_DATABASE=admin_desarrollo_laboratorio
            - APP_DB_HOST=localhost
        volumes:
            - mariadb-data:/var/lib/mysql
        networks:
            - localhost
    web:
        image: labc-apache:'$version'
        build:
            dockerfile: ./docker-images/labc-apache/Dockerfile
        volumes:
            - ./:/var/www/html/
        ports:
            - '$port':80
        networks:
            - localhost
        depends_on:
            - db
    phpmyadmin:
        image: phpmyadmin
        restart: always
        ports:
            - 8001:80
        environment:
            - PMA_ARBITRARY=1
        networks:
            - localhost
        depends_on:
            - localhost

volumes:
    mariadb-data:

networks:
    localhost: {}' > docker-compose.yml
# ----------------------------------




RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

echo ""
echo -e "${GREEN}Listo:${ENDCOLOR}"
echo "Use \"./npm run\" para ver más opciones."
echo "Para levantar el servicio apache use \"docker compose up\", pero una vez presione ctrl + c se finalizará el servicio."
echo -e "Vea ${RED}https://docs.docker.com/engine/reference/commandline/docker/${ENDCOLOR} para más información."
