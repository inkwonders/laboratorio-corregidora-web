import { defineConfig } from 'vite'
import { resolve } from 'path'

import vue from '@vitejs/plugin-vue'

const src = [
    './modules/app.css',
    './modules/coments.js',

    './modules/app.js',

    './modules/links.html',
    './modules/information.html',
    './modules/sucursales.html',
    './modules/content.html',
    './modules/citas.html',
]

export default defineConfig({
    plugins: [vue()],
    base: '/dist/',
    build: {
        manifest: true,
        rollupOptions: {
            input: src.map(file => {
                return resolve(__dirname, file)
            }),
        },
    },
})
