<?php

error_reporting(1);
ini_set("display_errors", 1);

$host = 'localhost';
$username = 'admin_root';
$password = 'B8U6vhIv4xnWrUjiAmqc';
$database = 'admin_desarrollo_laboratorio';

// $username = 'root';
// $password = '';
// $database = 'lab_corregidora';

$dbCon = mysqli_connect($host, $username, $password, $database);
// $dbCon->set_charset("utf8");

if(!$dbCon) {
    die('Connection failed: ' . mysqli_connect_error());
}

// include 'ajax/dbcon.php';

$no_conf = $_GET['no_confirmacion'];

date_default_timezone_set('America/Mexico_city');

if (!$_GET['no_confirmacion']) {
    header('Location:https://laboratoriocorregidora.com.mx');
}

if ($no_conf != '') {

    $sql = "SELECT hora_inicial,hora_final,fecha_seleccionada,tipo_prueba,costo,citas.created_at,citas.updated_at FROM citas INNER JOIN horarios_citas ON horarios_citas.id = citas.horario_id WHERE no_confirmacion = '$no_conf'";

    $sql_pacientes = "SELECT nombre,apellido_paterno,apellido_materno,genero,fecha_nacimiento,calle,colonia,no_exterior,no_interior,ubicacion_id,cp,tel_celular,tel_casa,paciente_email,medico_nombre,medico_email,razon_social,rfc,domicilio_fiscal,otro_sintoma,otra_enfermedad,contacto_persona_positivo,antiviral,influenza,otro_prueba,pasaporte,es,en FROM citas INNER JOIN pacientes ON pacientes.id = citas.paciente_id WHERE no_confirmacion = '$no_conf'";

    $sql_costo_total = "SELECT SUM(costo) AS total FROM citas WHERE no_confirmacion = '$no_conf'";

    $result_cita = mysqli_query($dbCon, $sql);

    if ($fila_cita = mysqli_fetch_array($result_cita)) {

        $otra = mysqli_query($dbCon, $sql_costo_total);

        if ($fila_total = mysqli_fetch_array($otra)) {
            $num = $fila_total['total'];
        }

?>
        <!DOCTYPE html>

        <html lang="es">

        <head>
            <title>Imprimir Recibo</title>
<!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>
            <!-- favicon -->
            <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
            <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
            <link rel="manifest" href="img/favicon/site.webmanifest">
            <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
            <meta name="msapplication-TileColor" content="#000000">
            <meta name="theme-color" content="#ffffff">
            <!-- favicon -->

            <!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->
            <style>
                .azul_textos {
                    color: #00559b;
                }

                @media print {
                    .no-print {
                        display: none;
                    }
                }
            </style>
        </head>

        <body>

            <div id="div_imprimir">
                <div class="container flex flex-col mx-auto ">
                    <!-- logo -->
                    <div class="flex justify-center w-full p-2 ">
                        <img class="h-12" src="img/svg/logo_imprimir.svg" alt="">
                    </div>
                    <!-- nombre -->
                    <div class="flex justify-center w-full no-print">
                        <button class="p-3 px-5 m-1 font-bold text-center text-gray-200 bg-blue-500 rounded md:flex " onclick="imprimir()">IMPRIMIR <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 17h2a2 2 0 002-2v-4a2 2 0 00-2-2H5a2 2 0 00-2 2v4a2 2 0 002 2h2m2 4h6a2 2 0 002-2v-4a2 2 0 00-2-2H9a2 2 0 00-2 2v4a2 2 0 002 2zm8-12V5a2 2 0 00-2-2H9a2 2 0 00-2 2v4h10z" />
                            </svg></button>
                    </div>
                    <div class="w-full p-2 text-xl font-bold text-center border-b-2 border-gray-300 azul_textos">
                        <h2>RESUMEN Y PAGO</h2>
                    </div>
                    <div class="w-full border-b-2 border-gray-300 ">
                        <h2 class="text-lg font-bold azul_textos">INFORMACIÓN DE LA CITA</h2>
                        <div class="grid grid-cols-2 ">
                            <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Numero Confirmación:
                                </p>
                                <p>
                                    <?= $no_conf; ?>
                                </p>
                            </div>
                            <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Monto pagado:
                                </p>
                                <p>
                                $<?= number_format($num,2); ?>
                                </p>
                            </div>
                        </div>
                        <div class="grid grid-cols-2 ">
                        <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Fecha de pago de la cita:
                                </p>
                                <p>
                                    <?= date('d/m/Y', strtotime($fila_cita['updated_at'])); ?>
                                </p>
                            </div>
                            <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Hora de pago de la cita:
                                </p>
                                <p>
                                    <?= date('h:m:s', strtotime($fila_cita['updated_at']));  ?>
                                </p>
                            </div>
                            <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Hora de inicio de la cita:
                                </p>
                                <p>
                                    <?= $fila_cita['hora_inicial']; ?>
                                </p>
                            </div>
                            <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Hora de termino de la cita:
                                </p>
                                <p>
                                    <?= $fila_cita['hora_final']; ?>
                                </p>
                            </div>
                            <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Fecha seleccionada:
                                </p>
                                <p>
                                    <?= date('d/m/Y', strtotime($fila_cita['fecha_seleccionada'])); ?>
                                </p>
                            </div>
                            <div class="flex text-lg">
                                <p class="mr-2 azul_textos ">
                                    Tipo de prueba:
                                </p>
                                <p>
                                    <?php
                                    if ($fila_cita['tipo_prueba'] == '1') {
                                        echo 'PCR';
                                    } else {
                                        echo 'Antigenos';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- info paciente -->
                    <?php $result = mysqli_query($dbCon, $sql_pacientes);
                    while ($fila = mysqli_fetch_array($result)) { ?>
                        <div class="w-full border-b-2 border-gray-300 ">
                            <h2 class="text-lg font-bold azul_textos">INFORMACIÓN DEL PACIENTE</h2>
                            <div class="grid grid-cols-2">
                                <div class="flex text-lg">
                                    <p class="mr-2 azul_textos ">
                                        NOMBRE:
                                    </p>
                                    <p>
                                        <?= $fila['nombre'] . ' ' . $fila['apellido_paterno'] . ' ' . $fila['apellido_materno'] ?>
                                    </p>
                                </div>
                                <div class="flex text-lg">
                                    <p class="mr-2 azul_textos ">
                                        CORREO:
                                    </p>
                                    <p>
                                        <?= $fila['paciente_email'] ?>
                                    </p>
                                </div>
                                <div class="flex text-lg">
                                    <p class="mr-2 azul_textos ">
                                        TELÉFONO:
                                    </p>
                                    <p>
                                        <?= $fila['tel_celular'] ?>
                                    </p>
                                </div>
                                <div class="flex text-lg">
                                    <p class="mr-2 azul_textos ">
                                        GENERO:
                                    </p>
                                    <p>
                                        <?php

                                        if ($fila['genero'] == '1') {
                                            echo "Masculino";
                                        } else {
                                            echo "Femenino";
                                        }

                                        ?>
                                    </p>
                                </div>
                                <div class="flex text-lg">
                                    <p class="mr-2 azul_textos ">
                                        FECHA NACIMIENTO:
                                    </p>
                                    <p>
                                        <?= date('d/m/Y', strtotime($fila['fecha_nacimiento'])); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- logo_grande -->
                    <!-- <div class="flex justify-center w-full p-6 border-b-2 border-gray-300">
                        <img class="h-36" src="img/svg/logo_imprimir_dos.svg" alt="">
                    </div> -->
                    <!-- pagina -->
                    <div class="flex justify-center w-full p-2">
                        <span>www.laboratoriocorregidora.com.mx</span>
                    </div>

                </div>


            </div>

            <script>
                function imprimir() {
                    window.print()
                }
            </script>
        </body>

        </html>

<?php
    }
}
