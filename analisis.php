<?php
  session_start();
  
  error_reporting(0);
  $_SESSION['datos_carrito'];

?>
<!doctype html>
<html lang="es">
<head>
  <!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<title>LABORATORIO CORREGIDORA</title>
<!-- <link rel="stylesheet" href="estilo.css" /> -->
<!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
<link rel="manifest" href="img/favicon/site.webmanifest">
<link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
<meta name="msapplication-TileColor" content="#000000">
<meta name="theme-color" content="#ffffff">
<!-- favicon -->

<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
<!-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-691396176');
</script> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-152013601-2');
</script> -->
<!-- end Global site tag (gtag.js) - Google Analytics -->


</head>
<body style="background-color:#f2f2f2">
  <?php include('header.php'); ?>

  <!-- home_tipos_analisis  -->
  <section id="home_tipos_analisis" class="flex flex-col items-center justify-start w-full">
    <div class="relative flex w-full h-max min-h-[calc(100vh-24rem)]">
      <img src="/modules/img/nuevas_imagenes/DSCF0166.JPG" class="absolute top-0 bottom-0 left-0 right-0 block object-cover w-full h-full" />
      <div class="relative flex justify-center w-full p-8 md:p-16 h-max">
        <?php
          include('./dist/modules/links.html');
        ?>
      </div>
    </div>

    <div id="search-container" class="flex flex-col items-center w-full px-6 py-12 gap-y-10 bg-blue-lab-oscuro">
      <h2 class="text-4xl text-center text-white font-rob-medium xl:text-5xl">Análisis Médicos</h2>
      <div class="flex flex-wrap items-center justify-center w-full gap-y-8 gap-x-3">
        <input id="buscador_analisis" class="w-full h-12 max-w-2xl px-5 text-2xl rounded-md text-blue-lab-oscuro placeholder:font-rob-regular placeholder-blue-lab font-rob-medium" required type="text" placeholder="Buscador...">
        <div class="flex items-center w-full md:w-auto gap-x-3">
          <button class="w-1/2 h-12 px-5 text-2xl text-white rounded-md md:w-32 font-rob-regular bg-blue-lab-claro"  onclick="buscar_estudio()">Buscar</button>
          <select onchange="categoria_acomodar()" class="w-1/2 h-12 px-4 text-2xl rounded-md md:w-32 font-rob-regular text-blue-lab-oscuro" required="required" name="" id="tipo_estudio">
          </select>
        </div>
      </div>
    </div>

    <div class="w-full px-4 py-8 lg:py-20 max-w-[94rem] lg:px-10">
      <div class="flex justify-center pb-12">
        <h2 id="estudio_titulo" class="text-2xl font-bold md:text-3xl text-blue-lab-oscuro font-rob-bold"><b>PRUEBAS FRECUENTES</b></h2>
      </div>
      <!-- titulo -->
      <div class="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3" id="contenedor_estudios"></div>
      <!-- grid -->
    </div>
    <!-- contenedor estudios -->
  </section>

  <?php include('footer.php'); ?>

  <!-- end home_tipos_analisis -->
  <!-- <div class="footer-mov">
    <div class="cont_footer_mov">
      <p><a rel="noopener" href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de privacidad</a></p>
      <p><a rel="noopener" href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a></p>
      <p><a rel="noopener" href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a></p>
      <p><span class="menu-el link-a" op="vid-mov" style="font-family: m-regular!important; font-size: 16px!important; cursor: pointer!important;">Contáctanos</span></p>
      <p><a href="contacto.php" class="link-a">Felicitaciones, Quejas y Sugerencias</a></p>

      <p style="text-align: center;">Permiso de Publicidad: 203301201A0872</p>
      <p><a rel="noopener" class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a></p>
    </div>
  </div>

  <div class="div-footer">
    <div class="footer-cell" style="width: 48%;">
      <a rel="noopener" href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de Privacidad</a> &nbsp; / &nbsp; <a href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a> &nbsp; / &nbsp; <a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a> &nbsp; / &nbsp; <span class="menu-el" op="sucursales" style="cursor: pointer; ">Contáctanos</span> &nbsp; / &nbsp; <a class="menu-el" href="/contacto.php"> Felicitaciones, Quejas y Sugerencias </a>
    </div>
    <div class="footer-cell">
      <p>Permiso de Publicidad: 203301201A0872</p>
    </div>
    <div class="footer-cell" style="text-align:right">
      <a rel="noopener" class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a>
    </div>
  </div> -->

  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="module" src="js/script.js"></script>

  <script type="text/javascript">
    $( document ).ready(function() {
      cargar_estudios('1');
      cargar_select_categoria();
      cargar_carrito_header();

  });

  $(document).on("click", ".menu-el", function(){
      let opcion = $(this).attr("op");
      switch(opcion){
        case "servicios":
          localStorage.setItem("opcion_animate", "servicios");
          window.location = "/";
          break;
        case "preguntas":
          localStorage.setItem("opcion_animate", "preguntas");
          window.location = "/";
          break;
        case "nosotros":
          localStorage.setItem("opcion_animate", "nosotros");
          window.location = "/";
          break;
        case "sucursales":
          localStorage.setItem("opcion_animate", "sucursales");
          window.location = "/";
          break;
        case "serv-mov":
          localStorage.setItem("opcion_animate", "serv-mov");
          window.location = "/";
          break;
        case "preg-mov":
          localStorage.setItem("opcion_animate", "vid-mov");
          window.location = "/";
          break;
        case "vid-mov":
          localStorage.setItem("opcion_animate", "sucursales");
          window.location = "/";
          break;
        default:
          break;
      }
    });

  // funcion para menu movil
  function myFunction(x) {
      x.classList.toggle("change");
      if(document.getElementById("contenidoMenu").style.display == "none"){
        document.getElementById("contenidoMenu").style.display = "";
        $("#contenidoMenu").animate({
          height: '100vh'
        }, "slow");
      }else{
        $("#contenidoMenu").animate({
          height: '0'
        }, "slow", function(){ document.getElementById("contenidoMenu").style.display = "none"; });
      }
    }
    // termina funcion menu movil


    // funcion ver mis resultados
    $(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function(){ window.location = "resultados.php"; });
    // termina funcion ver mis resultados

    // ajax para cargar los estudios
    function cargar_estudios(categoria){
      // frmData
      var frmData = new FormData();
       frmData.append("categoria",categoria);

      $.ajax({
          type: "POST",
          data:frmData,
          processData: false,
          contentType: false,
          cache: false,
          url: "ajax/cargar_estudios.php",
          beforeSend: function () {
            $('#contenedor_estudios').addClass('loading');
            // $('.buscador_analisis').val('');
          },
          success: function(datos) {
            $("#contenedor_estudios").html(datos).show();
          },complete: function() {
            $('#contenedor_estudios').removeClass('loading');
          }
        });
        // ajax
    }


    function rellenar_modal(id_estudio){
      // frmData
      var frmData = new FormData();
       frmData.append("id_estudio",id_estudio);
       frmData.append("modal_num",'1');

      $.ajax({
          type: "POST",
          data:frmData,
          processData: false,
          contentType: false,
          cache: false,
          url: "ajax/modal.php",
          beforeSend: function () {
            $(".modal-content").empty();

          },
          success: function(datos) {
            $(".modal-content").html(datos).show();
          },complete: function() {
            $('.modal-content').removeClass('loading');
          }
        });
        // ajax
    }

    document.getElementById("buscador_analisis").addEventListener("keyup", ({ code }) => {
      if (code == "Enter")
        buscar_estudio();
    });

    function buscar_estudio() {
      $('html, body').animate({
          scrollTop: ($("#search-container").offset().top - 80)
      }, 300);
  
      if ($('#buscador_analisis').val().length > 0) {
        $categoria_seleccionada = $('#tipo_estudio').val();
        var frmData = new FormData();
        frmData.append("buscar", $('#buscador_analisis').val() );
        frmData.append("categoria", $('#tipo_estudio').val());
        $.ajax({
            type: "POST",
            data:frmData,
            processData: false,
            contentType: false,
            cache: false,
            url: "ajax/buscador_estudios.php",
            beforeSend: function () {
              $('#contenedor_estudios').addClass('loading');
              // $('#buscador_analisis').val('');
            },
            success: function(datos) {
              $("#contenedor_estudios").html(datos).show();
              switch ($categoria_seleccionada) {
                case '1':   $texto = 'PRUEBAS FRECUENTES';    break;
                case '2':   $texto = 'HEMATOLOGÍA';           break;
                case '3':   $texto = 'COAGULACIÓN';           break;
                case '4':   $texto = 'COPROPARASITOSCOPIA';   break;
                case '5':   $texto = 'UROANÁLISIS';           break;
                case '6':   $texto = 'QUÍMICA CLÍNICA';       break;
                case '7':   $texto = 'QUÍMICA URINARIA';      break;
                case '8':   $texto = 'HORMONAS';              break;
                case '9':   $texto = 'ALERGIA';               break;
                case '10':  $texto = 'ANTICUERPOS';           break;
                case '11':  $texto = 'CULTIVOS';              break;
                case '12':  $texto = 'BIOLOGIA MOLECULAR';    break;
                case '13':  $texto = 'PERFILES';              break;
                case '14':  $texto = 'MARCADORES TUMORALES';  break;
                case '15':  $texto = 'GENERAL';               break;
              }
              $('#estudio_titulo').html($texto);
            },complete: function() {
              $('#contenedor_estudios').removeClass('loading');
            }
        });
      }
    }

    function cargar_select_categoria(){
      $.ajax({
          type: "POST",
          processData: false,
          contentType: false,
          cache: false,
          url: "ajax/cargar_select_categoria.php",
          beforeSend: function () {
            // $('.buscador_analisis').val('');
          },
          success: function(datos) {
            $("#tipo_estudio").html(datos).show();
          },complete: function() {
            $('#tipo_estudio').removeClass('loading');
          }
        });
        // ajax

    }

    function categoria_acomodar(){
      $categoria_seleccionada = $('#tipo_estudio').val();
      cargar_estudios($categoria_seleccionada);

      switch ($categoria_seleccionada) {
        case '1':   $texto = 'PRUEBAS FRECUENTES';    break;
        case '2':   $texto = 'HEMATOLOGÍA';           break;
        case '3':   $texto = 'COAGULACIÓN';           break;
        case '4':   $texto = 'COPROPARASITOSCOPIA';   break;
        case '5':   $texto = 'UROANÁLISIS';           break;
        case '6':   $texto = 'QUÍMICA CLÍNICA';       break;
        case '7':   $texto = 'QUÍMICA URINARIA';      break;
        case '8':   $texto = 'HORMONAS';              break;
        case '9':   $texto = 'ALERGIA';               break;
        case '10':  $texto = 'ANTICUERPOS';           break;
        case '11':  $texto = 'CULTIVOS';              break;
        case '12':  $texto = 'BIOLOGIA MOLECULAR';    break;
        case '13':  $texto = 'PERFILES';              break;
        case '14':  $texto = 'MARCADORES TUMORALES';  break;
        case '15':  $texto = 'GENERAL';               break;
      }
      $('#estudio_titulo').html($texto);
    }

    function agregar_carrito(id,precio){
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Agregado con éxito',
        showConfirmButton: false,
        timer: 1500
      })

            // frmData
      var frmData = new FormData();
       frmData.append('id',id);

      $.ajax({
          type: "POST",
          data:frmData,
          processData: false,
          contentType: false,
          cache: false,
          url: "ajax/agregar_carrito.php",
          beforeSend: function () {},
          success: function(datos) {
            $(".pruebas").html(datos).show();
          },
          complete: function() {
            cargar_carrito_header();
          }
        });
        // ajax

    }

    // $(".citas_covid").click(function(){
    //   window.location = "https://www.laboratoriocorregidora.com.mx/citas/";
    // });

    // $(".notificacion").css("z-index","100");

    // $(".notificacion").click(function(){
    //   if (screen.width < 600) {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid_mov";
    //   }else{
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid";
    //   }

    // });

  </script>
</body>
</html>
 