<?php
session_start();
error_reporting(0);


?>

<!doctype html>
<html lang="es">

<head>
<!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
  <meta http-equiv="X-UA-Compatible" content="IE=11" />
  <title>LABORATORIO CORREGIDORA</title>
  <!-- <link rel="stylesheet" href="estilo.css" /> -->
  <!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->


  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
  <link rel="manifest" href="img/favicon/site.webmanifest">
  <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="theme-color" content="#ffffff">
  <!-- favicon -->

  <style>
        .clasificacion {
          direction: rtl;
          unicode-bidi: bidi-override;
          font-size: 16px;
        }

        .label_estrella:hover {
          color: orange;
          cursor: pointer;
        }

        .label_estrella:hover~.label_estrella {
          color: orange;
        }

        input[type="radio"]:checked~.label_estrella {
          color: orange;
        }
      </style>


  <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
  <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
  <!-- <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'AW-691396176');
  </script> -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-152013601-2');
  </script> -->
  <!-- end Global site tag (gtag.js) - Google Analytics -->

</head>

<body>

  <section id="Encuestas">
    <div class="container mx-auto ">

      <div class="flex justify-center w-full p-16 ">
        <img class="w-1/2" src="img/logo-mov.svg" alt="Laboratorios Corregidora">
      </div>
      <article class="flex flex-col items-center content-center w-full mb-16 azul_textos">
        <h2 class="mt-5 mb-5 text-4xl font-bold" style="color:#194271">- ENCUESTA DE SATISFACCIÓN -</h2>
        <span class="w-7/12 text-3xl">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore</span>
      </article>


      <form type="post" class="flex flex-col items-center content-center ">
        <?php for ($i = 1; $i < 6; $i++) {

          $random = rand(1, 5000);
          $random2 = rand(1, 5000);
          $random3 = rand(1, 5000);
          $random4 = rand(1, 5000);
          $random5 = rand(1, 5000);

        ?>
          <div class="flex flex-col items-center content-center mb-6">
            <p class="text-3xl font-semibold text-gray-600">
              <?= $i ?>.-¿Lorem ipsum dolor sit amet, consectetuer adiectetuer?
            </p>
            <p class="clasificacion">
              <input class="hidden" id="radio<?= $random ?>" type="radio" name="estrellas<?= $i ?>" value="5">
              <label for="radio<?= $random ?>" class="text-5xl label_estrella">★</label>
              <input class="hidden" id="radio<?= $random2 ?>" type="radio" name="estrellas<?= $i ?>" value="4">
              <label for="radio<?= $random2 ?>" class="text-5xl label_estrella">★</label>
              <input class="hidden" id="radio<?= $random3 ?>" type="radio" name="estrellas<?= $i ?>" value="3">
              <label for="radio<?= $random3 ?>" class="text-5xl label_estrella">★</label>
              <input class="hidden" id="radio<?= $random4 ?>" type="radio" name="estrellas<?= $i ?>" value="2">
              <label for="radio<?= $random4 ?>" class="text-5xl label_estrella">★</label>
              <input class="hidden" id="radio<?= $random5 ?>" type="radio" name="estrellas<?= $i ?>" value="1">
              <label for="radio<?= $random5 ?>" class="text-5xl label_estrella">★</label>
            </p>
          </div>
        <?php } ?>
        <div>
          <button class="px-8 py-4 mt-8 mb-8 text-sm text-3xl font-medium text-center text-white transition-colors duration-150 bg-blue-600 border border-blue-600 hover:bg-white hover:text-blue-600 focus:outline-none focus:shadow-outline-blue">RESPONDER</button>
        </div>
      </form>

    </div>

  </section>


  <script type="text/javascript" src="js/jquery.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</body>

</html>