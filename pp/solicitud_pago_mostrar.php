<?php

  require_once "../citas/modelos/conexion.php";
  include('solicitud_liga_pago.php');

  date_default_timezone_set("America/Mexico_City");

  // $obj_pacientes = $_GET["obj"];

  $no = $_GET['confirmacion'];
  $tipo = $_GET['tipo'];
  $total_precio = $_GET['total'];
  $num_pacientes = $_GET['numpacientes'];
  $hora_inicial = $_GET['horainicial'];
  $hora_final = $_GET['horafinal'];
  $sql_verificar = "SELECT count(no_confirmacion) AS total FROM citas WHERE no_confirmacion = '$no'";
  $res_verificar = query_bd($sql_verificar);

  $existe = mysqli_fetch_array($res_verificar);
  $total = $existe["total"];

  if($total == 0){
    header("Location:https://www.laboratoriocorregidora.com.mx/citas");
  }

  $sql_verificar_existe = "SELECT pagado FROM citas WHERE no_confirmacion = '$no'";
  $res_verificar_existe = query_bd($sql_verificar_existe);

  $existe_pagado = mysqli_fetch_array($res_verificar_existe);
  $res_existe_pagado = $existe_pagado["pagado"];

  if($res_existe_pagado == 1){
    header("Location:https://www.laboratoriocorregidora.com.mx/citas");
  }

  $email = $_GET['email'];
  $fec = date("d-m-Y",strtotime($_GET['fecha']));

  $solicitud_monto = $total_precio;
  $solicitud_id = $no;
  $solicitud_correo_cliente =  $email;

  $fecha_actual = date("d-m-Y");
  $pago_vigencia = date("d-m-Y",strtotime($fecha_actual." + 3 days"));

  $nuevo_vigencia = $pago_vigencia;
  $datos_adicionales = '<datos_adicionales>
    <data id="1" display="true">
      <label>Número de Confirmación de cita:</label>
      <value>'.$solicitud_id.'</value>
    </data>
    <data id="2" display="true">
      <label>Concepto:</label>
      <value>'.$tipo.'</value>
    </data>
    <data id="3" display="true">
      <label>Fecha cita:</label>
      <value>'.$fec.'</value>
    </data>
    <data id="4" display="true">
      <label>Horario:</label>
      <value>'.$hora_inicial.' / '.$hora_final.'</value>
    </data>
    <data id="5" display="true">
      <label>Número de pacientes:</label>
      <value>'.$num_pacientes.'</value>
    </data>
    <data id="6" display="true">
    <label>Tipo pago:</label>
    <value>1</value>
  </data>
  </datos_adicionales>';

//   $df = ' <data id="6" display="true">
//   <label>Lista pacientes:</label>
//   <value>'.$obj_pacientes.'</value>
// </data>';

  $liga = generarLiga($solicitud_monto, $solicitud_id, $solicitud_correo_cliente, $nuevo_vigencia, $datos_adicionales);

  header("Location:$liga");

// <!doctype html>
// <html>
// <head>
// <meta charset="utf-8">
// <title>LABOATORIO CORREGIDORA | PAGOS</title>

// <!-- favicon -->
// <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
// <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
// <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
// <link rel="manifest" href="favicon/site.webmanifest">
// <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#cbf202">
// <meta name="msapplication-TileColor" content="#000000">
// <meta name="theme-color" content="#ffffff">
// <!-- favicon -->

// <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
// <link rel="stylesheet" href="estilopanel.css">
// <link rel="stylesheet" href="estilocursos.css">
// <link href="css/datetimepicker.css" rel="stylesheet">

// <script src="//code.jquery.com/jquery-1.10.2.js"></script>
// <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

// <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
// <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
// <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
// <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">


// <link rel="stylesheet" href="../citas/vistas/assets/css/estilo.css" />


// <!-- Global site tag (gtag.js) - Google Analytics -->
// <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137998144-1"></script>
// <script>
//   window.dataLayer = window.dataLayer || [];
//   function gtag(){dataLayer.push(arguments);}
//   gtag('js', new Date());
//   gtag('config', 'UA-137998144-1');
// </script>
// <style>
//   #infoPanel{
//     padding: 50px 0px;
//   }
// </style>
// </head>
// <body style="background-color: #194271;">

//   <?php include("../citas/vistas/header.php"); ? >

//   < ?php

//     // $consulta_enfermedades_paciente = ControladorEnfermedades::ctrConsultaEmfermedadesPaciente("6859c270-8345-11eb-ab88-5254009623cd");
//     // var_dump($consulta_enfermedades_paciente);

//   ? >

//   <div id="contenidoPanel">
//     <section id="infoPanel" style="text-align:center; padding-top: 120px; font-family: m-semibold;">

// 				<span xml="<?php echo creaxml($solicitud_monto, $solicitud_id, $solicitud_correo_cliente, $datos_adicionales); ? >" class="texto-pago" style="color: white;">Captura tus datos de pago y haz clic en el botón PAGAR.<br />NO salgas de esta página hasta no ver la confirmación de pago, puede tardar algunos segundos.</span><br />
//         <iframe src="< ?php echo $liga ? >" width="640px" height="1000px" frameborder="0" scrolling="no" seamless style="margin-top:20px"></iframe>


//     </section>
//   </div>

//   <?php include("../citas/vistas/footer.php"); ? >

// </body>
// </html>
