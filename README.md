Para instalar el entorno es necesario tener <a href="https://docs.docker.com/get-docker/" target="_blank" rel="noreferrer">Docker</a> instalado.

Ejecute el script Bash:

<sup>Importante: Este script fue pensado para ejecutarse en un entorno compatible con linux, si no cuenta con eso deberá crear las imagenes y contenedores de docker manualmente, así como el archivo **docker-compose.yml** y el script **npm** (compatible con su sistema o manualmente usar <a href="https://docs.docker.com/engine/reference/commandline/container_run/" target="_blank" rel="noreferrer">docker run</a>).</sup>
```console
./install.sh
```


Si no funciona añada los permisos necesarios:
```console
chmod +x install.sh
```

Le pedirá un número de versión y un puerto para el apache, ej:
<pre>Ingrese una version: 1
Ingrese el puerto apache: 8080</pre>

Cuando finalize use:
```console
./npm i && ./npm run build && docker compose up
```

Y así podrá acceder al sitio con su localhost y el puerto seleccionado, ej: `localhost:8080`
