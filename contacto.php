<?php error_reporting(0); ?>
<!doctype html>
<html lang="es">
<head>
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
  <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
  <meta http-equiv="X-UA-Compatible" content="IE=11" />
  <title>LABORATORIO CORREGIDORA</title>
  <!-- <link rel="stylesheet" href="estilo.css" /> -->
  <link rel="stylesheet" type="text/css" href="slick/slick.css" />
  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />
  <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
  <link rel="manifest" href="img/favicon/site.webmanifest">
  <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="theme-color" content="#ffffff">

  <style>
    label {
      color: #194271;
      font-family: m-semibold;
    }
    .texto_landing_felicitaciones {
      color: #194271;
      font-family: m-bold;
    }
    /* The container */
    .container {
      display: block;
      position: relative;
      padding-left: 35px;
      /* margin-bottom: 12px; */
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }
    /* Hide the browser's default radio button */
    .container input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }
    /* Create a custom radio button */
    .checkmark {
      position: absolute;
      top: 0;
      left: 0;
      height: 25px;
      width: 25px;
      background-color: #fff;
      border-radius: 50%;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input~.checkmark {
      background-color: #ccc;
    }
    /* When the radio button is checked, add a blue background */
    .container input:checked~.checkmark {
      /* background-color: #2196F3; */
    }
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }
    /* Show the indicator (dot/circle) when checked */
    .container input:checked~.checkmark:after {
      display: block;
    }
    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
      top: 9px;
      left: 9px;
      width: 8px;
      height: 8px;
      border-radius: 50%;
      background: #2196F3;
    }
  </style>
</head>
<body class="bg-[#e5e5e5] flex flex-col justify-between min-h-screen">
  <?php include('header.php'); ?>

  <div class="flex justify-center w-full p-4 lg:p-8 xl:p-16">
    <div class="w-full grid grid-cols-1 py-8 lg:grid-cols-2 gap-16 max-w-[1400px]" id="ContainerForm">

      <div class="flex items-center w-full gap-4 text-left lg:gap-5 lg:items-start">
          <div class="w-[5px] min-w-[5px] h-20 bg-[#007af6]"></div>
          <span class="text-lg text-blue-lab-oscuro lg:text-xl xl:text-2xl font-rob-regular lg:font-rob-light">
              <p class="text-xl lg:text-2xl xl:text-3xl font-rob-bold"><b>Queremos saber de ti.</b></p>
              <p>
                  Escríbenos, para darnos una sugerencia,
                  <br class="hidden lg:inline">
                  hacer una queja o felicitarnos.
              </p>
              <p>
                  Nos dará mucho gusto
                  <br class="hidden lg:inline">
                  comunicarnos contigo.
              </p>
          </span>
      </div>

      <div class="flex flex-col items-center w-full gap-8 lg:gap-12 xl:gap-16">
        <div class="flex flex-col items-center w-full gap-8 lg:gap-12 xl:gap-16" id="formularioMensaje">
          <div class="flex items-center justify-center gap-4 md:gap-8">
            <label class="flex flex-col items-center justify-center sm:flex-row gap-x-3" for="felicitaciones">
              <input type="radio" checked="checked" name="txt_radio" id="felicitaciones" value="felicitaciones" class="w-6 h-6 bg-white border-[8px] border-white rounded-full shadow outline-none appearance-none checked:bg-blue-lab-claro sm:h-7 md:h-8 lg:h-9 xl:h-10 sm:w-7 md:w-8 lg:w-9 xl:w-10 sm:w-border-[9px] md:w-border-[10px] lg:w-border-[11px] xl:border-[12px] ring-0" />
              <span>Felicitación</span>
            </label>
            <label class="flex flex-col items-center justify-center sm:flex-row gap-x-3" for="queja">
              <input type="radio" name="txt_radio" id="queja" value="queja" class="w-6 h-6 bg-white border-[8px] border-white rounded-full shadow outline-none appearance-none checked:bg-blue-lab-claro sm:h-7 md:h-8 lg:h-9 xl:h-10 sm:w-7 md:w-8 lg:w-9 xl:w-10 sm:w-border-[9px] md:w-border-[10px] lg:w-border-[11px] xl:border-[12px] ring-0" />
              <span>Queja</span>
            </label>
            <label class="flex flex-col items-center justify-center sm:flex-row gap-x-3" for="sugerencia">
              <input type="radio" name="txt_radio" id="sugerencia" value="sugerencia" class="w-6 h-6 bg-white border-[8px] border-white rounded-full shadow outline-none appearance-none checked:bg-blue-lab-claro sm:h-7 md:h-8 lg:h-9 xl:h-10 sm:w-7 md:w-8 lg:w-9 xl:w-10 sm:w-border-[9px] md:w-border-[10px] lg:w-border-[11px] xl:border-[12px] ring-0" />
              <span>Sugerencia</span>
            </label>
          </div>
          <div class="flex flex-col items-center w-full gap-8 text-blue-lab-oscuro lg:gap-12 xl:gap-16">
            <div class="flex flex-col w-full">
              <div class="w-full custom-input">
                <input required="" name="txt_nombre" placeholder="NOMBRE COMPLETO" type="text" id="txt_nombre" oninput="validar(); validar_nombre();" class="w-full text-base bg-transparent outline-none lg:text-lg xl:text-xl focus-visible:outline-none h-11">
                <label for="txt_nombre">
                  <b class="text-sm font-rob-bold lg:text-base">NOMBRE COMPLETO</b>
                </label>
              </div>
              <span id="error_nombre"></span>
            </div>
            <div class="flex flex-col w-full">
              <div class="w-full custom-input">
                <input required="" name="txt_correo" placeholder="CORREO" type="email" id="txt_correo" oninput="validar(); validar_email();" class="w-full text-base bg-transparent outline-none lg:text-lg xl:text-xl focus-visible:outline-none h-11">
                <label for="txt_correo">
                  <b class="text-sm font-rob-bold lg:text-base">CORREO</b>
                </label>
              </div>
              <span id="error_correo"></span>
            </div>
            <div class="flex flex-col w-full">
              <div class="w-full custom-input">
                <textarea required="" name="txt_mensaje" placeholder="MENSAJE" id="txt_mensaje" rows="2" oninput="validar(); validar_mensaje();" class="w-full text-base bg-transparent outline-none lg:text-lg xl:text-xl focus-visible:outline-none"></textarea>
                <label for="txt_mensaje">
                  <b class="text-sm font-rob-bold lg:text-base">MENSAJE</b>
                </label>
              </div>
              <span id="error_mensaje"></span>
            </div>

            <div class="flex justify-center">
              <input onclick="opiniones()" type="submit" value="ENVIAR" class="px-6 py-3 text-base text-white rounded-lg cursor-pointer btn_submit lg:text-lg xl:text-xl font-rob-bold bg-blue-lab-claro" style="display: none;">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- </div> -->

  <?php include('footer.php'); ?>

  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>

  <script type="text/javascript">
    cargar_carrito_header();
    $(".op_guardias").addClass("menu_activo_desktop");

    $(document).on("click", ".menu-el", function() {
      let opcion = $(this).attr("op");
      switch (opcion) {
        case "servicios":
          localStorage.setItem("opcion_animate", "servicios");
          window.location = "/";
          break;
        case "preguntas":
          localStorage.setItem("opcion_animate", "preguntas");
          window.location = "/";
          break;
        case "sucursales":
          localStorage.setItem("opcion_animate", "sucursales");
          window.location = "/";
          break;
        case "nosotros":
          localStorage.setItem("opcion_animate", "nosotros");
          window.location = "/";
          break;
        case "serv-mov":
          localStorage.setItem("opcion_animate", "serv-mov");
          window.location = "/";
          break;
        case "preg-mov":
          localStorage.setItem("opcion_animate", "preg-mov");
          window.location = "/";
          break;
        case "vid-mov":
          localStorage.setItem("opcion_animate", "vid-mov");
          window.location = "/";
          break;
        default:
          break;
      }
    });

    $(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function() {
      window.location = "resultados.php";
    });

    function myFunction(x) {
      x.classList.toggle("change");
      if (document.getElementById("contenidoMenu").style.display == "none") {
        document.getElementById("contenidoMenu").style.display = "";
        $("#contenidoMenu").animate({
          height: '100vh'
        }, "slow");
      } else {
        $("#contenidoMenu").animate({
          height: '0'
        }, "slow", function() {
          document.getElementById("contenidoMenu").style.display = "none";
        });
      }
    }

    var correo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;
    var nombre = 1;
    var email = 1;
    var mensaje = 1;

    function validar_nombre() {
      var nombre_val = $('#txt_nombre').val();
      if (nombre_val.length == 0 || !nombre_val.trim()) {
        document.getElementById('error_nombre').innerHTML = "El campo esta vacío.";
        nombre = 1;
      } else if (nombre_val.length < 3) {
        document.getElementById('error_nombre').innerHTML = "El campo debe tener más de 3 caracteres.";
        nombre = 1;
      } else if (!letras.test(nombre_val)) {
        document.getElementById('error_nombre').innerHTML = "Caracteres no validos";
        nombre = 1;
      } else {
        document.getElementById('error_nombre').innerHTML = "";
        nombre = 0;
      }
      return nombre;
    }

    function validar_email() {
      var correo_val = $('#txt_correo').val();
      if (correo_val.length == 0 || !correo_val.trim()) {
        document.getElementById('error_correo').innerHTML = "El campo esta vacío.";
        email = 1;
      } else if (correo_val.length < 3) {
        document.getElementById('error_correo').innerHTML = "El campo debe tener más de 3 caracteres.";
        email = 1;
      } else if (!correo.test(correo_val)) {
        document.getElementById('error_correo').innerHTML = "El campo debe ser un correo.";
        email = 1;
      } else {
        document.getElementById('error_correo').innerHTML = "";
        email = 0;
      }
      return email;
    }

    function validar_mensaje() {
      var mensaje_val = $('#txt_mensaje').val();
      if (mensaje_val.length == 0 || !mensaje_val.trim()) {
        document.getElementById('error_mensaje').innerHTML = "El campo esta vacío.";
        mensaje = 1;
      } else if (mensaje_val.length < 3) {
        document.getElementById('error_mensaje').innerHTML = "El campo debe tener más de 3 caracteres.";
        mensaje = 1;
      } else {
        document.getElementById('error_mensaje').innerHTML = "";
        mensaje = 0;
      }
      return mensaje;
    }

    function validar() {
      if (nombre == 0 && email == 0 && mensaje == 0) {
        $('.btn_submit').css('display', 'inherit');
      } else {
        $('.btn_submit').css('display', 'none');
      }
    }

    function opiniones() {
      txt_radio = $('input[name=txt_radio]:checked').val();
      txt_nombre = $('#txt_nombre').val();
      txt_correo = $('#txt_correo').val();
      txt_mensaje = $('#txt_mensaje').val();
      var frmData = new FormData();
      frmData.append("txt_radio", txt_radio);
      frmData.append("txt_nombre", txt_nombre);
      frmData.append("txt_correo", txt_correo);
      frmData.append("txt_mensaje", txt_mensaje);

      $.ajax({
        type: "POST",
        data: frmData,
        processData: false,
        contentType: false,
        cache: false,
        url: "/send-contact-form.php",
        beforeSend: function() {
          $("#ContainerForm").empty();
        },
        success: function(datos) {
          $("#ContainerForm").html(datos).show();
          console.log(datos)
        },
      });
    }
  </script>
</body>
</html>