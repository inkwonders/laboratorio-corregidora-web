<?php
session_start();
// $_SESSION['datos_carrito'];

function asset($filename) {
    $manifest_path = './dist/.vite/manifest.json';

    if (file_exists($manifest_path)) {
        $manifest = json_decode(file_get_contents($manifest_path), TRUE);
    } else {
        $manifest = [];
    }

    $route = "";
    if (array_key_exists($filename, $manifest)) {
        $route = "dist/".($manifest[$filename]["file"]);
    } else {
        $route = "dist/".$filename;
    }

    return $route;
}
?>




<link rel="stylesheet" type="text/css" href="<?=asset("modules/app.css"); ?>" />




<header class="sticky top-0 left-0 right-0 w-full shadow no-print">
    <div id="menu">
        <div class="flex items-center gap-4">
            <div class="px-4 cont_logo">
                <a href="/">
                    <img src="/modules/img/logos/logo-nuevo-labc.svg" alt="Logo" class="hidden h-12 xl:block">
                    <img src="/modules/img/logos/logo.svg" alt="Logo" class="block h-12 xl:hidden">
                </a>
            </div>

            <ul class="menu-nav" id="menu-slide">
                <li class="menu-el menu-suc" op='servicios'><span>SERVICIOS</span></li>
                <li id="analisis" class="menu-el menu-ana" op='analisis'> <a href="/analisis.php"><span>ANÁLISIS</span></a></li>
                <li id="imagenologia" class="menu-el menu-ana" op='imagenologia'> <a href="https://imagenologia.laboratoriocorregidora.com.mx/"><span>IMAGENOLOGÍA</span></a></li>
                <li id="bSucursal" class="menu-el menu-suc" op='sucursales'><span>SUCURSALES</span></li>
                <li id="qr" class="menu-el menu-qr" op='qr'><a target="_blank" href="https://www.youtube.com/watch?v=_mlnDgrFvEw"><span>CÓDIGO QR</span></a></li>
            </ul>
        </div>

        <div class="flex items-center h-full gap-8">
            <div class="flex items-center gap-2">
                <a rel="noopener" class="no_padding" href="https://www.facebook.com/Laboratorios-Corregidora-108767437596443" target="_blank"><img class="img_menu" src="img/svg/fa.svg" alt="facebook"></a>
                <a rel="noopener" class="no_padding" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank"><img class="img_menu" src="img/svg/em.svg" alt="email"></a>
                <a rel="noopener" class="no_padding no_link_menu" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank" style="justify-content: space-around;display: flex;align-items: center;"><img class="img_menu" src="img/svg/wa.svg" alt="whatsapp">
                    <span style="padding-left: 5px;" class="text-xs font-rob-bold">442 120 62 15<br> (solo mensajes)</span>
                </a>
            </div>

            <div class="opcion_ext">
                <img loading="lazy" class="img_menu img_tel" src="img/svg/tel.svg" alt="teléfono">
                <div class="text-xs font-rob-bold">
                    <p>Call Center:</p>
                    <p><a href="tel:4421201052" class="no_link_menu">442 212 10 52</a></p>
                    <p><a href="tel:4422121901" class="no_link_menu">442 212 19 01</a></p>
                </div>
            </div>

            <a href="/resultados.php" id="boton--res" class="flex items-center h-full gap-4 p-4 bg-blue-lab-claro">
                <img loading="lazy" class="h-16 img__pdf" src="img/svg/pdf.svg" alt="">
                <div class="text-sm text-left text-white font-rob-bold inter--res">
                    QUIERO VER<br />
                    MIS RESULTADOS
                </div>
            </a>
        </div>
    </div>
</header>

<div class="sticky justify-between px-4 shadow menu-bar no-print">
    <a class="block" href="/">
        <img loading="lazy" src="img/svg/logo-nuevo.svg" id="logo-movil" class="block h-10 logo-mov-g" alt="Laboratorio Corregidora">
    </a>
    <div class="container" onclick="myFunction(this)" id="container">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
    </div>
    <div class="absolute right-20 top-5 " onclick="abrir_modal(); abrir_carrito();"></div>
</div>
<div id="contenidoMenu" style="display: none;">
    <div class="menu-conten">
        <span class="menu-el" id="btn-proy-mov" op="servicios">SERVICIOS</span>
        <span class="menu-el" id="btn-cons-mov" op="analisis-mov"><a style="color: #194271;" href="/analisis.php"> ANÁLISIS </a></span>
        <span class="menu-el" id="btn-cons-mov" op="analisis-mov"><a style="color: #194271;" href="https://imagenologia.laboratoriocorregidora.com.mx/"> IMAGENOLOGÍA </a></span>
        <span class="menu-el" id="btn-cons-mov" op="preg-mov">FAQ’s</span>
        <span class="menu-el" id="btn-acab-mov" op="vid-mov">SUCURSALES</span>
        <span class="menu-el" id="btn-cons-mov" op="qr"><a style="color: #194271;" target="_blank" href="https://www.youtube.com/watch?v=_mlnDgrFvEw"> CÓDIGO QR </a></span>
        <a href="contacto.php" class="no_link_menu"><span class="menu-gua opc_menu_desktop op_covid" id="bGuardias" op="covid">Felicitaciones, Quejas y Sugerencias</span></a>
        <a href="guardias.php" class="no_link_menu"><span class="menu-gua opc_menu_desktop op_guardias" id="bGuardias" op="guardias">GUARDIAS</span></a>
        <span class="menu-res" id="btn-resl-mov">QUIERO VER MIS RESULTADOS</span><br>
        <span class="menu-el" id="btn-tel-mov"><a href="tel:4422121052" class="menu-tel" style="margin-right:5px; color: #194271;">442 212 1052</a> <a href="tel:4422121901" class="menu-tel" style="margin-right:5px; color: #194271;">442 212 19 01</a> </span>
        <div class="opcion_ext_movil">
            <a rel="noopener" class="no_padding" href="https://www.facebook.com/Laboratorios-Corregidora-108767437596443" target="_blank"><img loading="lazy" class="img_menu_movil" src="img/svg/fa.svg" alt="facebook"></a>
            <a rel="noopener" class="no_padding" href="mailto:contacto@laboratoriocorregidora.com.mx" target="_blank"><img loading="lazy" class="img_menu_movil" src="img/svg/em.svg" alt="email"></a>
            <a rel="noopener" class="no_padding" href="https://api.whatsapp.com/send?phone=524421206215" target="_blank"><img loading="lazy" class="img_menu_movil" src="img/svg/wa.svg" alt="whatsapp"></a><br>(solo mensajes)
        </div>
    </div>
</div>

<style media="screen">
    .opcion_ext_movil {
        width: 100%;
        height: auto;
        padding-top: 0;
    }

    .menu-conten {
        display: inline-flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
        height: 76vh;
    }
</style>


<!-- modal -->
<div class="fixed inset-0 top-0 bottom-0 left-0 right-0 z-50 flex flex-col items-center justify-center p-4 pt-32 transition-all duration-300 ease-in-out scale-125 opacity-0 pointer-events-none lg:pt-32 lg:p-8 main-modal" style="background: rgba(0,0,0,.7);">
    <div class="relative block w-full max-w-6xl max-h-full mx-auto h-max">
        <button type="button" onclick="cerrar_modal()" class="absolute block w-10 h-10 ml-auto overflow-hidden rounded-full cursor-pointer modal-close hover:bg-white hover:shadow right-4 top-3">
            <div class="absolute top-0 bottom-0 left-2 right-2 h-[2px] m-auto rotate-45 bg-blue-lab-claro"></div>
            <div class="absolute top-2 bottom-2 left-0 right-0 w-[2px] m-auto rotate-45 bg-blue-lab-claro"></div>
        </button>
    </div>
    <div style="background-color: #f2f2f2;" class="flex items-start justify-center w-full h-auto max-w-6xl max-h-full overflow-y-auto bg-white border border-teal-500 rounded shadow-lg modal-container">
        <div id="modal-content" class="w-full px-4 py-16 space-y-4 text-left md:px-16 modal-content md:space-y-0" style="background-color: #f2f2f2!important;"></div>
    </div>
</div>

<script>
    // modal
    let modal = document.querySelector('.main-modal');
    const closeButton = document.querySelectorAll('.modal-close');


    const openModal = () => {
        modal.classList.remove('opacity-0');
        modal.classList.remove('pointer-events-none');
        modal.classList.remove('scale-125');
        modal.classList.add('opacity-1');
        modal.classList.add('scale-100');
        modal.style.display = 'flex';
    }

    for (let i = 0; i < closeButton.length; i++) {

        const elements = closeButton[i];

        elements.onclick = () => {
            modal.classList.remove('opacity-1');
            modal.classList.remove('scale-100');
            modal.classList.add('scale-125');
            modal.classList.add('opacity-0');
            modal.classList.add('pointer-events-none');
            // setTimeout(() => {
            //     modal.style.display = 'none';
            // }, 500);
        };
    }

    function abrir_modal() {
        $('body').css('overflow', 'hidden');
        openModal();
    }

    function cerrar_modal() {
        $('body').css('overflow', 'auto');
        // modalClose();
        modal.classList.remove('opacity-1');
        modal.classList.remove('scale-100');
        modal.classList.add('opacity-0');
        modal.classList.add('scale-125');
        modal.classList.add('pointer-events-none');
        // setTimeout(() => {
        //     modal.style.display = 'none';
        // }, 500);
    }
    // termina modal

    function abrir_carrito() {
        var frmData = new FormData();
        frmData.append("modal_num", '2');

        $.ajax({
            type: "POST",
            data: frmData,
            processData: false,
            contentType: false,
            cache: false,
            url: "ajax/modal.php",
            beforeSend: function() {
                $(".modal-content").empty();
            },
            success: function(datos) {
                $(".modal-content").html(datos).show();
            },
            complete: function() {
                $('.modal-content').removeClass('loading');
                cargar_carrito_header();
            }
        });
        // ajax
    }

    function vaciar_carrito() {
      sessionStorage.clear();

      $.ajax({
        type: "POST",
        processData: false,
        contentType: false,
        cache: false,
        url: "ajax/vaciar_carrito.php",
        beforeSend: function() {},
        success: function(datos) {
          $(".contenedor_carrito").empty();
          $(".contenedor_carrito").html(datos).show();
        },
        complete: function() {
          $('.contenedor_carrito').removeClass('loading');
          cargar_carrito_header();
          abrir_carrito();

        }
      });
    }

    function eliminar_item(id, precio) {
      var frmData = new FormData();
      frmData.append("id", id);
      frmData.append("precio", precio);

      $.ajax({
        type: "POST",
        data: frmData,
        processData: false,
        contentType: false,
        cache: false,
        url: "ajax/eliminar_item.php",
        beforeSend: function() {},
        success: function(datos) {
          $(".contenedor_carrito").empty();
          $(".contenedor_carrito").html(datos).show();
        },
        complete: function() {
          cargar_carrito_header();
          abrir_carrito();

          // $('.contenedor_carrito').removeClass('loading');
        }
      });
    }

    function anadir_quitar(id, accion) {
      var frmData = new FormData();
      frmData.append("id", id);
      frmData.append("accion", accion);

      $.ajax({
        type: "POST",
        data: frmData,
        processData: false,
        contentType: false,
        cache: false,
        url: "ajax/anadir_quitar.php",
        beforeSend: function() {
          $(".contenedor_carrito").empty();
        },
        success: function(datos) {
          // $(".pruebas_modal").html(datos).show();
          $(".contenedor_carrito").html(datos).show();
        },
        complete: function() {
          cargar_carrito_header();
          abrir_carrito();

          $('.contenedor_carrito').removeClass('loading');
        }
      });
    }


    function formulario() {
        var frmData = new FormData();
        frmData.append("modal_num", '3');
        $.ajax({
            type: "POST",
            data: frmData,
            processData: false,
            contentType: false,
            cache: false,
            url: "ajax/formulario_registro.php",
            beforeSend: function() {
                $(".modal-content").empty();
            },
            success: function(datos) {
                $(".modal-content").html(datos).show();
            },
            complete: function() {
                $('.modal-content').removeClass('loading');
            }
        });
    }

    function enviar_formulario() {
        var nombre = $('#txt_nombre').val();
        var correo = $('#txt_correo').val();
        var telefono = $('#txt_telefono').val();
        sessionStorage.setItem('nombre', nombre);
        sessionStorage.setItem('correo', correo);
        sessionStorage.setItem('telefono', telefono);
        var frmData = new FormData();
        frmData.append("modal_num", '4');
        frmData.append("nombre", nombre);
        frmData.append("correo", correo);
        frmData.append("telefono", telefono);
        $.ajax({
            type: "POST",
            data: frmData,
            processData: false,
            contentType: false,
            cache: false,
            url: "ajax/listado_pedido.php",
            beforeSend: function() {
                $(".modal-content").empty();

            },
            success: function(datos) {
                $(".modal-content").html(datos).show();
            },
            complete: function() {
                $('.modal-content').removeClass('loading');
            }
        });
    }

    function cargar_carrito_header() {
        $.ajax({
            type: "POST",
            url: "ajax/recargar_carrito.php",
            beforeSend: function() {
                $("#btn_carrito").empty();
            },
            success: function(datos) {
                $("#btn_carrito").html(datos).show();
            }
        });
    }

    function imprimir() {
        var printContents = document.getElementById('div_imprimir').innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    function guardar_orden(nom, tel, corr, no) {
        var frmData = new FormData();
        frmData.append("nombre", nom);
        frmData.append("correo", corr);
        frmData.append("telefono", tel);
        frmData.append("no_conf", no);
        $.ajax({
            type: "POST",
            data: frmData,
            processData: false,
            contentType: false,
            cache: false,
            url: "ajax/guardar_orden.php",
            beforeSend: function() { },
            success: function(datos) {
                window.location.href = datos;
                localStorage.clear();
                sessionStorage.clear();
            },
            complete: function() { }
        });
    }
</script>
