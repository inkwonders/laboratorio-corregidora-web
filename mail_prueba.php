<?php
$no_confirmacion = '08980756';
?>
<!-- <html>

<head>
    <meta charset="utf-8">
    <title>Se agendó la cita</title>
    <style media="screen">
        .contenedor * {
            box-sizing: border-box;
            font-family: arial;
        }

        .contenedor {
            box-sizing: border-box;
            width: 600px;
            max-width: 100%;
            background-color: #e6e6e6;
            margin-top: 50px;
            padding: 17px;
        }

        .mensaje {
            width: 100%;
            background-color: #f2f2f2;
            display: inline-flex;
            flex-direction: column;
            align-items: center;
            text-align: center;
            font-size: 24px;
            margin: 0;
            padding: 40px;
            color: #194271;
        }

        .logo_head {
            width: 90%;
            height: auto;
        }

        .texto_msg,
        .manifiesto_txt {
            margin-top: 80px;
        }

        .texto_confirmacion {
            font-size: 30px;
        }

        .texto_confirmacion {
            margin-top: 40px;
        }

        .link_aviso {
            text-decoration: none;
            margin-top: 30px;
            font-size: 17px;
            color: #194271;
        }

        .manifiesto_txt {
            color: #848484;
            font-size: 16px;
        }

        .btn_aviso {
            background-color: #ccc;
            color: white;
            text-decoration: none;
            margin-top: 20px;
            padding: 10px 20px;
            border-radius: 10px;
            border: 2px #ccc solid;
        }

        .btn_aviso:hover{
            color: #848484;
            background-color: transparent;
        }

    </style>
</head>

<body>
    <div class="contenedor">
        <div class="mensaje">
            <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
            <span class="texto_msg">Gracias por confiar en LABORATORIO CORREGIDORA. Te agradeceríamos que nos dieras 5 minutos o menos para responder una breve encuesta y compartirnos cómo fue tu experiencia con nosotros.</span>
            <a class="btn_aviso" href="encuesta.php">Clic aqui </a>
            <span class="manifiesto_txt">Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
            <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf">AVISO DE PRIVACIDAD</a>
        </div>
    </div>
</body>

</html> -->


<!-- <html>

    <head>
        <meta charset="utf-8">
        <title>Se agendó la cita</title>
        <style media="screen">
            .contenedor * {
                box-sizing: border-box;
                font-family: arial;
            }

            .contenedor {
                box-sizing: border-box;
                width: 600px;
                max-width: 100%;
                background-color: #e6e6e6;
                margin-top: 50px;
                padding: 17px;
            }

            .mensaje {
                width: 100%;
                background-color: #f2f2f2;
                display: inline-flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
                font-size: 18px;
                margin: 0;
                padding: 5%;
                color: #194271;
            }

            .logo_head {
                width: 90%;
                height: auto;
            }

            .texto_msg,
            .manifiesto_txt {
                margin-top: 10px;
            }

            .texto_confirmacion {
                font-size: 20px;
            }

            .texto_confirmacion,
            .texto_numero {
                margin-top: 40px;
            }

            .link_aviso {
                text-decoration: none;
                margin-top: 30px;
                font-size: 16px;
                color: #194271;
            }

            .numero_confirmacion {
                color: #007af6;
                font-weight: bold;
                font-size: 25px;
            }

            .manifiesto_txt {
                color: #848484;
                font-size: 13px;
            }

            .numero_tel {
                font-weight: bold;
            }

            .numero_tel a {
                text-decoration: none;
                color: #194271;
            }

            span {
                overflow-wrap: anywhere;
            }

            .izq {
                width: 100%;
                text-align: left;
            }

            .colorgris {
                background-color: #f2f2f2;
                padding: 5%;
                color: #194271;
            }

            .txt_fecha_hora {
                font-size: 18px;
                color: #194271;
            }

            .btn_imprimir {
                background-color: #194271;
                border: solid 1px #194271;
                border-radius: 6px;
                color: #fff;
                cursor: pointer;
                padding: 5px 15px;
                transition: all 1s;
                text-decoration: none;
            }

            .btn_imprimir:hover {
                background-color: #fff;
                color: #194271;
            }

            
        </style>
    </head>

    <body>
        <div class="contenedor">
            <table class="colorgris">
                <tr class="mensaje">
                    <td>
                        <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_msg izq txt_fecha_hora">Usted agendó una cita para el día </span>
                        <br>
                        <span class="texto_msg izq txt_fecha_hora">Horario: </span>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_confirmacion izq">Número de Confirmación: <span class="numero_confirmacion"></span></span>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_confirmacion izq">Número de Pacientes: <span class="numero_confirmacion"></span></span>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_confirmacion izq">Pagado: <span class="numero_confirmacion"></span></span>
                        <br><br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label><span class="azul_sel bold_c" style="text-transform: uppercase;">Nombre Paciente: <span class="numero_tel"></span></span></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">Teléfono: </span> <span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">Email: </span> <span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">Sintomas: </span><span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">Enfermedades crónicas: </span><span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">¿Tuvo usted contacto directo con una persona con COVID-19 positivo las últimas 2 semanas?: </span><span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">¿Está tomando algún antiviral?: </span><span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">¿Se vacunó contra la Influenza el último año?: </span><span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pad_top"><span class="azul_sel">Porque se realiza esta prueba: </span><span class="numero_tel"></span></span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <br>
                        <a class="btn_imprimir" href="https://laboratoriocorregidora.com.mx/recibo.php?no_confirmacion=<?= $no_confirmacion ?>" target="_blank">
                            Imprimir Recibo
                        </a>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_numero"><br>Dirección y link de ubicación de la sucursal a la que deben acudir para realizar la prueba: <span class="numero_tel">Avenida Prolongación Constituyentes No. 32 Oriente Calle Avenida Marqués de la Villa del Villar del Aguila, Altos del Marques, 76140 Santiago de Querétaro, Qro.</span> <br><br> <a href="https://goo.gl/maps/ibT9atD4xPkXeAxy8" target="_blank">https://goo.gl/maps/ibT9atD4xPkXeAxy8</a> </span>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td><br>
                        <span class="texto_numero">Indicaciones de como deben presentarse para realizar la prueba: </span> <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul>
                            <li style="text-align: left;">PCR: Acudir con ayuno mínimo de 4 horas de alimentos sólidos. No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra, esta prueba se debe de hacer a partir del día 3 en adelante en que el paciente presenta los síntomas o que estuvo en contacto directo con una persona COVID- 19 Positivo.</li>

                            <br>

                            <li style="text-align: left;">ANTIGENOS: No haberse aplicado atomizaciones, nebulizaciones, soluciones, gotas, geles o cremas nasales mínimo 6 horas antes de la toma de muestra. Evitar tratamientos antivirales al menos 48 horas antes de la toma de muestra. esta prueba debe de hacerse durante los primeros 7 días de haberse presentado por lo menos dos síntomas sugestivos a COVID-19.</li> <br>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">442 2121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                    </td>
                </tr>
            </table>
        </div>
    </body>

    </html>  -->



<html>

<head>
    <meta charset="utf-8">
    <title>Se registro el pago</title>
    <style media="screen">
        .contenedor * {
            box-sizing: border-box;
            font-family: arial;
        }

        .contenedor {
            box-sizing: border-box;
            width: 600px;
            max-width: 100%;
            background-color: #e6e6e6;
            margin-top: 50px;
            padding: 17px;
        }

        .mensaje {
            width: 100%;
            background-color: #f2f2f2;
            display: inline-flex;
            flex-direction: column;
            align-items: center;
            text-align: center;
            font-size: 18px;
            margin: 0;
            padding: 5%;
            color: #194271;
        }

        .logo_head {
            width: 90%;
            height: auto;
        }

        .texto_msg,
        .manifiesto_txt {
            margin-top: 80px;
        }

        .texto_confirmacion {
            font-size: 20px;
        }

        .texto_confirmacion,
        .texto_numero {
            margin-top: 40px;
        }

        .link_aviso {
            text-decoration: none;
            margin-top: 30px;
            font-size: 16px;
            color: #194271;
        }

        .numero_confirmacion {
            color: #007af6;
            font-weight: bold;
            font-size: 25px;
        }

        .manifiesto_txt {
            color: #848484;
            font-size: 13px;
        }

        .numero_tel {
            font-weight: bold;
        }

        .numero_tel a {
            text-decoration: none;
            color: #194271;
        }

        span {
            overflow-wrap: anywhere;
        }

        .border {
            border: solid 1px;
            text-align: left;
            padding: 10px;
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <table class="mensaje">
            <tr>
                <td>
                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png"><br><br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="texto_msg">Se realizo el cobro de los siguientes estudios:</span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td class="texto_confirmacion"> Cantidad y Nombre:
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td class="border">
                    ' asd'
                </td>
            </tr>

            <tr>
                <td>
                    <span class="texto_confirmacion"><br>Número de Confirmación: <span class="numero_confirmacion"></span></span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="texto_confirmacion">Pagado: <span class="numero_confirmacion"></span></span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a href="tel:4422121052">4422121052</a></span> / WhatsApp: <span class="numero_tel"><a href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442 1206215</a></span></span>
                    <br><br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la información haga caso omiso. Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf" target="_blank">AVISO DE PRIVACIDAD</a>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>