<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $txt_radio = $_POST['txt_radio'];
    $txt_nombre = $_POST['txt_nombre'];
    $txt_correo = $_POST['txt_correo'];    
    $txt_mensaje = $_POST['txt_mensaje'];
    
    $mail = new PHPMailer(true);

    try {
        // Configuración del servidor
        // $mail->SMTPDebug = 2; // Habilita el modo de depuración detallado
        $mail->isSMTP();
        $mail->Host       = 'smtp.gmail.com'; // Configura el servidor SMTP
        $mail->SMTPAuth   = true;
        $mail->Username   = 'contacto@laboratoriocorregidora.com.mx'; // Tu dirección de correo
        $mail->Password   = 'itvkiunqhhmgrolw'; // Tu contraseña
        $mail->SMTPSecure = 'tls'; // Habilita cifrado TLS; 'ssl' también es aceptado
        $mail->Port       = 587;

        // Recipientes
        $mail->setFrom('contacto@laboratoriocorregidora.com.mx', 'Contacto Lab Corregidora');
        $mail->addAddress('ryetzin@gmail.com', 'Receptor'); // Añade un destinatario
        $mail->addCC('ryetzin@gmail.com');

        // Contenido del correo
        $mail->isHTML(true); // Establece el formato del correo como HTML
        $mail->Subject = 'Nuevo contacto desde el sitio web';
        // $mail->Body    = "Nombre: $nombre<br>Apellido: $apellido<br>Teléfono: $telefono<br>Correo: $correo";
        $mail->Body    = '<html>
        <head>
            <meta charset="utf-8">
            <title>Se agendó la cita</title>
            <style media="screen">
            .contenedor * {
                box-sizing: border-box;
                font-family: arial;
            }
            .contenedor {
                box-sizing: border-box;
                width: 600px;
                max-width: 100%;
                background-color: #e6e6e6;
                margin-top: 50px;
                padding: 17px;
            }
            .mensaje {
                width: 100%;
                background-color: #f2f2f2;
                /*display: inline-flex;
                flex-direction: column;
                align-items: center;*/
                position: relative;
                text-align: center;
                font-size: 24px;
                margin: 0;
                padding: 40px;
                color: #194271;
            }
            .logo_head {
                width: 90%;
                height: auto;
            }
            .texto_msg, .manifiesto_txt {
                margin-top: 80px;
            }
            .texto_confirmacion {
                font-size: 30px;
            }
            .texto_confirmacion, .texto_numero {
                margin-top: 40px;
            }
            .link_aviso {
                text-decoration: none;
                margin-top: 30px;
                font-size: 17px;
                color: #194271;
            }
            .numero_confirmacion {
                color: #007af6;
                font-weight: bold;
            }
            .manifiesto_txt {
                color: #848484;
                font-size: 16px;
            }
            .numero_tel {
                font-weight: bold;
            }
        </style>
        </head>
        <body>
            <div class="contenedor">
                <div class="mensaje">
                    <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
                    <span class="texto_msg">Correo de '.$txt_radio.' recibido</span>
                    <span class="texto_msg">Nombre:'.$txt_nombre.' </span>
                    <span class="texto_msg">Correo:'.$txt_correo.' </span>
                    <span class="texto_msg">Mensaje:'.$txt_mensaje.' </span>
                </div>
            </div>
        </body>
    </html>
    ';

        $mail->send();
        // echo 'El mensaje ha sido enviado';
        // header('Location: /index.php');
        // exit();

    } catch (Exception $e) {
        exit("El mensaje no pudo ser enviado. Error de envío: {$mail->ErrorInfo}");
    }
}
?>

<div class="flex flex-col items-center justify-center w-full col-span-2 gap-4">
    <?php
        switch ($txt_radio) {
            case 'felicitaciones':
                $img = 'thanks_1.svg';
                $titulo = '¡Gracias por tu felicitación!';
                $texto = 'Nos alegra saber que tuviste una buena experiecia. Seguiremos trabajando para mantener a nuestros clientes contentos y superar nuestros estándares de calidad.';
            break;

            case 'queja':
                $img = 'thanks_2.svg';
                $titulo = '¡Lamentamos saber que tuviste una mala experiencia!';
                $texto = 'Estamos trabajando para ponernos en contacto contigo y buscar una solución satisfactoria para ti.';
            break;

            case 'sugerencia':
                $img = 'thanks_3.svg';
                $titulo = '¡Gracias por tu sugerencia!';
                $texto = 'Para nosostros tu opinión es muy importante. Tus comentarios nos ayudan a seguir mejorando nuestro servicio.';
            break;
        }
    ?>

    <img src="img/svg/<?=$img?>" alt="" class="w-3/5 pt-32 lg:pt-16 lg:w-1/5">
    <div class="w-full px-4 pb-8 text-center lg:w-2/3">
        <h2  class="py-4 text-xl lg:text-3xl texto_landing_felicitaciones"><?=$titulo?></h2>
        <span class="text-sm lg:text-xl" style="color: #194271; font-family: m-regular;"><?=$texto?></span>
    </div>
    <a rel="noopener" class="boton-serv" href="index.php" >Regresar</a>        
</div>
