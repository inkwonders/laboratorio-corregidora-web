
<div class="flex flex-col items-center w-full p-3 mt-4 bg-white rounded-lg md:flex-row">
<div class="flex mb-4 md:hidden">
  <h2 class="text-xl font-bold azul_textos">Cantidad</h2>
</div>
<div class="flex justify-center w-full md:w-1/12">
      <div class="text-white cursor-pointer" onclick="anadir_quitar('<?= $value['itemcarrito_id']; ?>','2')">
        <svg xmlns="http://www.w3.org/2000/svg" class="bg-blue-600 rounded h-7 w-7" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 12H4" /></svg>
      </div>
      <span class="ml-4 mr-4 text-xl font-bold azul_textos">
      <?=  $value['cantidad'];  ?>
      </span>
       <div class="text-white cursor-pointer"  onclick="anadir_quitar('<?= $value['itemcarrito_id']; ?>','1')">
       <svg xmlns="http://www.w3.org/2000/svg" class="bg-blue-600 rounded h-7 w-7" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" /></svg>
       </div>
  </div>
  <div class="flex mt-4 mb-4 md:hidden">
      <h2 class="text-xl font-bold azul_textos">Nombre</h2>
    </div>
  <div class="w-full text-2xl text-center md:text-left md:w-9/12 azul_textos">
    <?=  $value['itemcarrito_nombre'];  ?>
  </div>
  <div class="flex mt-4 mb-4 md:hidden">
      <h2 class="text-xl font-bold azul_textos">Precio</h2>
    </div>
  <div class="w-full text-2xl font-semibold text-center text-green-500 md:w-1/12">
    $  <?= number_format($value['itemcarrito_precio'] ,2)  ?>
  </div>
  
  <div class="flex justify-center w-full text-center md:w-1/12">
    <a href="#" onclick="eliminar_item('<?= $value['itemcarrito_id'] ?>','<?= $value['itemcarrito_precio'] ?>')">  
     <img src="img/svg/papelera_roja.svg" alt="" class="h-7 m-7 " >
    </a> 
  </div>
</div>