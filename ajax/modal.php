<?php
session_start();
error_reporting(0);
$modal_num = $_POST['modal_num'];
$datos_carrito = $_SESSION['datos_carrito'];
if ($modal_num == '1') {
  include 'dbcon.php';
  $id_estudio = $_POST['id_estudio'];
  $consulta_estudio = "SELECT `clave`, `nombre`, `estudios`, `ayuno`, `entrega`, `indicaciones`, `precio` FROM `analisis` WHERE `id` = '$id_estudio'  ";
  $result = mysqli_query($dbCon, $consulta_estudio);

?>
  <?php if ($fila = mysqli_fetch_array($result)) { ?>
    <div class="flex flex-col justify-between w-full space-y-4 md:flex-row md:space-y-0 md:mb-8 md:space-x-8 ">
      <div class="flex flex-col w-full p-3 bg-white rounded-lg md:w-8/12 md:p-7">
        <h2 class="mb-2 text-xl font-semibold text-gray-400 md:text-2xl"> CLAVE <?= $fila['clave']; ?> </h2>
        <h2 class="w-full text-xl font-bold md:w-3/6 md:text-4xl azul_textos"> <?= $fila['nombre']; ?> </h2>
      </div>
      <div class="flex flex-col justify-center w-full bg-white rounded-lg md:w-4/12 p-7">
        <p class="text-base font-bold md:text-lg azul_textos">ESTUDIOS CONTENIDOS</p>
        <pre class="mb-2 text-base whitespace-pre-line md:text-lg" style="font-family:m-regular"><?= $fila['estudios']; ?></pre>
        <p class="text-base font-bold md:text-lg azul_textos">AYUNO</p>
        <p class="mb-2 text-base md:text-lg"><?= $fila['ayuno']; ?></p>
        <p class="text-base font-bold md:text-lg azul_textos">TIEMPOS DE ENTREGA</p>
        <p class="mb-2 text-base md:text-lg"><?= $fila['entrega']; ?></p>
      </div>
    </div>
    <div class="flex flex-col justify-between w-full space-y-4 md:flex-row md:space-y-0 md:mb-8 md:space-x-8">
      <div class="flex flex-col justify-center w-full bg-white rounded-lg md:w-8/12 p-7">
        <h2 class="text-lg font-semibold azul_textos">INDICACIONES PARA EL PACIENTE</h2>
        <pre class="text-lg whitespace-pre-line" style="font-family:m-regular"> <?= $fila['indicaciones'] ?></pre>
      </div>
      <div class="flex flex-col justify-center w-full overflow-hidden text-center bg-white rounded-lg md:w-4/12">
        <p class="text-2xl font-semibold azul_textos">Precio</p>
        <?php if ($fila['precio'] != '0.00') { ?>
          <p class="pb-2 text-3xl font-bold text-green-500">$<?= number_format($fila['precio'], 2); ?> <br></p>
          <!-- <a class="mt-6 bg-blue-500 text-2xl tracking-wider uppercase font-bold text-gray-100 hover:bg-blue-700  p-2.5 cursor-pointer" onclick="agregar_carrito('<?= $id_estudio ?>', <?= $fila['precio'] ?>);">+ AGREGAR</a> -->
        <?php } else { ?>
          <p class="pb-4 text-xl font-bold text-green-500">Favor de llamar al teléfono 442 212 10 52 para solicitar el precio de este estudio. <br></p>
        <?php } ?>
      </div>
    </div>
    <!-- <div class="flex justify-center w-full pb-8">
      <br><br> -->
      <!-- <button class="p-3 px-4 text-white bg-gray-500 rounded-lg md:mt-8 focus:outline-none modal-close hover:bg-gray-400">Regresar</button> -->
      <!-- <button type="button" onclick="cerrar_modal()" class="absolute block w-10 h-10 ml-auto overflow-hidden rounded-full cursor-pointer modal-close hover:bg-white hover:shadow right-1 top-1">
        <div class="absolute top-0 bottom-0 left-2 right-2 h-[2px] m-auto rotate-45 bg-blue-lab-claro"></div>
        <div class="absolute top-2 bottom-2 left-0 right-0 w-[2px] m-auto rotate-45 bg-blue-lab-claro"></div>
      </button> -->
    <!-- </div> -->
  <?php } ?>

<?php } else { ?>
  <?php if ($datos_carrito == null) { ?>
    <div class="flex w-full mt-8">
      <div class="flex flex-col justify-center w-full pb-20 md:flex-row">
        <div class="flex w-full mb-2 md:m-2 md:w-2/12">
          <button onclick="cerrar_modal()" class="w-full p-2 text-2xl font-bold text-white bg-gray-400 rounded-lg hover:bg-gray-500">
            SEGUIR <br> COMPRANDO
          </button>
        </div>
      </div>
    </div>
  <?php } else { ?>
    <div class="flex flex-col w-full mt-4 md:flex-row">
      <div class="flex w-full md:w-6/12">
        <div class="flex items-center text-xl font-bold azul_textos">
          <img src="img/svg/carrito_azul.svg" alt="" class="h-16 m-6 "> CARRITO
        </div>
      </div>
      <div class="flex items-center md:justify-end justify-center w.full md:w-6/12">
        <button class="h-12 p-3 px-5 m-1 font-bold text-center text-white bg-blue-600 rounded-lg hover:bg-blue-500 md:w-3/12" onclick="vaciar_carrito()">VACIAR CARRITO</button>
      </div>
    </div>
    <div class="hidden w-full p-2 mb-4 font-bold border-b-2 border-blue-600 md:flex azul_textos">
      <div class="w-1/12">CANTIDAD <div class="pruebas_modal"></div>
      </div>
      <div class="w-9/12">NOMBRE</div>
      <div class="w-1/12">PRECIO</div>
      <div class="w-1/12">ELIMINAR</div>
    </div>

    <div class="flex flex-col space-y-4 contenedor_carrito">

      <?php if (count(array_filter($datos_carrito)) != '0') {
        foreach ($datos_carrito as $key => $value) {
          if ($value['itemcarrito_id']) {
            include 'contenido_modal.php';
          }
      ?>


        <?php }
      }

      if (count(array_filter($_SESSION['datos_carrito'])) != '0') {  ?>
        <div class="flex justify-end">
          <div class="flex p-6 text-2xl font-bold bg-white rounded-lg">
            <span class="mr-2 azul_textos">TOTAL </span> <span class="text-green-500"> $ <?= number_format($_SESSION['total_carrito'], 2)  ?> </span>
          </div>
        </div>
      <?php } ?>
    </div>
    <div class="flex w-full ">
      <div class="flex flex-col justify-center w-full pb-20 md:flex-row">
        <div class="flex w-full mb-2 md:m-2 md:w-2/12">
          <button onclick="cerrar_modal()" class="w-full p-2 text-2xl font-bold text-white bg-gray-400 rounded-lg hover:bg-gray-500">
            SEGUIR <br> COMPRANDO
          </button>
        </div>
        <?php if (count(array_filter($_SESSION['datos_carrito'])) != '0') { ?>
          <div id="btn_pagar" class="flex w-full mb-2 md:m-2 md:w-2/12">
            <button onclick="formulario()" class="w-full p-2 text-2xl font-bold text-white bg-blue-500 rounded-lg hover:bg-blue-600">
              PAGAR
            </button>
          </div>
        <?php } ?>

      </div>
    </div>
  <?php } ?>

<?php }  ?>

<script>
  // const modalClose = () => {
  //   modal.classList.remove('fadeIn');
  //   modal.classList.add('fadeOut');
  //   setTimeout(() => {
  //     modal.style.display = 'none';
  //   }, 500);
  // }
</script>