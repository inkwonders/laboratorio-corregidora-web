<?php
session_start();
error_reporting(0);
if($_POST["nombre"]!=''){
    $_SESSION['nombre'] = $_POST["nombre"];
    $_SESSION['telefono'] = $_POST["telefono"]; 
    $_SESSION['correo'] = $_POST["correo"];
}

$nombre = $_SESSION['nombre'];
$telefono = $_SESSION['telefono'];
$correo = $_SESSION['correo'];

$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
$no_confirmacion =  substr(str_shuffle($permitted_chars), 0, 12);

foreach ($_SESSION['datos_carrito'] as $value) {
   $estudios .= ' '.$value['cantidad'].' '.$value['itemcarrito_nombre'].'& ';
}

$estudio_explode = explode("&", $estudios);


?>

<style>

@media print {
   .no-print{
       display: none;
   } 
    /* body { visibility: hidden; } */
  /* .modal-content { visibility: visible; } */
  #div_imprimir{
      display: block

  }
  
}

</style>

<div class="flex flex-row items-center mb-2 no-print">
    <img class="m-3 h-9" src="img/svg/tarjeta_credito_azul.svg" alt=""> <span class="text-2xl font-bold azul_textos">PROCESO DE PAGO</span>
</div>
<div class="flex flex-row justify-center mb-2 space-x-4 no-print md:justify-end">
    <div class="w-3 h-3 mb-4 border border-blue-600 rounded-full" ></div>
    <div class="w-3 h-3 mb-4 bg-blue-600 rounded-full" ></div>
    <div class="w-3 h-3 mb-4 border border-blue-600 rounded-full" ></div>
</div>
<div class="flex flex-col w-full p-8 bg-white rounded no-print azul_textos">
    <h2 class="mb-4 text-2xl font-bold text-center ">RESUMEN Y PAGO</h2>
    <?php
     for ($i=0; $i < count($_SESSION['datos_carrito']); $i++) {
         if($_SESSION['datos_carrito'][$i]['cantidad']){
    ?>
    <div class="flex flex-col w-full p-4 text-xl text-gray-900 md:flex-row ">
        <div class="flex justify-center w-full font-bold md:justify-start md:w-1/12">
            <?= $_SESSION['datos_carrito'][$i]['cantidad'] ?>
        </div>
        <div class="flex justify-center w-full md:justify-start md:w-10/12">
           <?= $_SESSION['datos_carrito'][$i]['itemcarrito_nombre'] ?>
        </div>
        <div class="flex justify-center w-full text-green-500 md:justify-end md:w-1/12">
           $ <?= number_format( $_SESSION['datos_carrito'][$i]['itemcarrito_precio'],2 ); ?>
        </div>
    </div>
    <?php  }} ?>
    <div class="flex justify-start w-full p-4 font-bold border-t-2 border-b-2 md:justify-end">
        <span class="ml-4 text-xl textos_azules">
            TOTAL  &nbsp 
        </span>
        <span class="text-xl text-green-500">
            $<?= number_format($_SESSION['total_carrito'],2); ?>
        </span>
    </div class="flex w-full ">
        <div class="flex flex-wrap p-4 mr-2 text-xl ">
            <p>NOMBRE:&nbsp&nbsp&nbsp&nbsp&nbsp</p> <p class="text-gray-900"> <?= $nombre ?> </p>
        </div>
        <div class="flex flex-wrap p-4 mr-2 text-xl ">
            <p>CORREO:&nbsp&nbsp&nbsp&nbsp &nbsp</p> <p class="text-gray-900"> <?= $correo ?> </p>
        </div>
        <div class="flex flex-wrap p-4 mr-2 text-xl ">
            <p>TELÉFONO:&nbsp&nbsp&nbsp </p> <p class="text-gray-900"> <?= $telefono ?> </p>
        </div>
    <div>
    </div>
</div>
<div class="flex justify-center pb-20 space-x-6 no-print">
    <button id="regresar_btn" onclick="formulario();" class="p-6 mt-6 text-xl text-white bg-gray-400 rounded-lg text- hover:bg-gray-500"> REGRESAR </button>

    <button  onclick="imprimir()" class="p-6 mt-6 text-xl text-white bg-blue-500 rounded-lg no-print text- hover:bg-blue-400">IMPRIMIR</button>
    <!-- <a href="../pa/solicitud_pago_mostrar.php?no_confirmacion=<?= $no_confirmacion; ?>&nombre=<?= $nombre; ?>&telefono=<?= $telefono; ?>&estudios=<?= $estudios; ?>&total=<?= $_SESSION['total_carrito']; ?>&correo=<?= $correo; ?>"> -->
    <button onclick="guardar_orden('<?= $nombre ?>', '<?= $telefono ?>', '<?= $correo ?>', '<?= $no_confirmacion ?>')" class="p-6 mt-6 text-xl text-white bg-blue-500 rounded-lg no-print text- hover:bg-blue-400">PAGAR</button>
    <!-- </a> -->

</div>

<div id="div_imprimir" class="hidden">
<div class="container flex flex-col mx-auto ">
        <!-- logo -->
        <div class="flex justify-center w-full p-8 ">
            <img class="h-20" src="img/svg/logo_imprimir.svg" alt="">
        </div>
        <!-- nombre -->
        <div class="w-full p-4 text-2xl font-bold text-center border-b-2 border-gray-300 azul_textos">
            <h2>RESUMEN Y PAGO</h2>
        </div>
        <!-- info paciente -->
        <div class="w-full border-b-2 border-gray-300 ">
        <h2 class="p-2 text-xl font-bold azul_textos">INFORMACIÓN DEL PACIENTE</h2>
        <div class="flex p-2 text-xl">
         <p class="w-3/12 azul_textos ">
            NOMBRE:
         </p>
         <p class="w-9/12">
            <?= $nombre ?>
         </p>
        </div>
        <div class="flex p-2 text-xl">
        <p class="w-3/12 azul_textos ">
            CORREO:
        </p>
        <p class="w-9/12">
            <?= $correo ?>
        </p>
        </div>
        <div class="flex p-2 text-xl">
        <p class="w-3/12 azul_textos ">
            TELÉFONO:
        </p>
        <p class="w-9/12">
        <?= $telefono ?>

        </p>
        </div>
        </div>
        <!-- listado -->
        <div class="w-full border-b-2 border-gray-300 ">
           <h2 class="p-2 text-xl font-bold azul_textos">LISTADO DE ANALISÍS</h2>

            <?php
                for ($i=0; $i < count($_SESSION['datos_carrito']); $i++) {
                if($_SESSION['datos_carrito'][$i]['cantidad']){
            ?>        
                    <div class="flex items-center w-full p-2 text-xl">
                <span class="w-1/12 font-bold"><?= $_SESSION['datos_carrito'][$i]['cantidad'] ?></span>
                <span class="w-8/12"><?= $_SESSION['datos_carrito'][$i]['itemcarrito_nombre'] ?> </span>
                <span class="w-2/12 text-right text-green-500">$ <?= number_format( $_SESSION['datos_carrito'][$i]['itemcarrito_precio'],2 ); ?></span>
            </div>

                <?php } ?>
            <?php } ?>    
            <div class="flex justify-end p-2 mt-4 text-xl font-bold border-t-2 border-gray-300 w-12/12">
               <div class="mr-4">
                <span class="mr-4 azul_textos">TOTAL</span> <span class="text-green-500">$ <?= number_format($_SESSION['total_carrito'],2); ?> </span>
               </div>
            </div>
        
        </div>
        <!-- logo_grande -->
        <div class="flex justify-center w-full p-6 border-b-2 border-gray-300">
            <img class="h-36" src="img/svg/logo_imprimir_dos.svg" alt="">
        </div>
        <!-- pagina -->
        <div class="flex justify-center w-full p-2">
            <span>www.laboratoriocorregidora.com.mx</span>
        </div>

    </div>


</div>

 

 