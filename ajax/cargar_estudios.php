<?php
include $_SERVER['DOCUMENT_ROOT'].'/ajax/dbcon.php';

$categoria_estudio = $_POST['categoria'];
$consulta_analisis = "SELECT `id`,`clave`, `nombre`, `estudios`, `ayuno`, `entrega`, `indicaciones`, `precio`, `categoria_id`,`estatus` FROM `analisis` WHERE `categoria_id` = '$categoria_estudio' AND `estatus` = 1 ";
$result = mysqli_query($dbCon, $consulta_analisis);
?>

<?php while ($fila = mysqli_fetch_array($result)) { ?>
  <div class="relative flex flex-wrap content-between overflow-hidden bg-white">
    <div class="flex flex-col justify-center w-full px-6 pt-4">
      <div class="absolute left-0 w-1 h-14 bg-blue-lab-oscuro top-4"></div>
      <div class="text-lg tracking-wide font-rob-medium azul_textos">
        <?= $fila['nombre']; ?>
      </div>
    </div>

    <div class="w-full">
      <!-- <div class="px-6 text-gray-500">
        <hr />
      </div> -->
      <div class="block p-4 text-lg text-left">
        <?php if ($fila['precio'] == '0.00') { ?>
          <p class="text-green-500 font-rob-regular">Favor de llamar al teléfono 442 212 10 52 para solicitar el precio de este estudio. </p>
        <?php } else { ?>
          <p>
            <span class="text-lg text-center font-rob-medium azul_textos">PRECIO</span>
            <span class="text-lg font-bold text-green-500"><b>$ <?= number_format($fila['precio'], 2); ?></b></span>
          </p>
        <?php } ?>
      </div>
      <div class="grid text-center md:grid-cols-1">
        <a class="px-4 py-1 font-bold tracking-wider text-white uppercase cursor-pointer bg-blue-lab-claro hover:bg-blue-lab-oscuro" onclick="openModal(); rellenar_modal('<?= $fila['id']; ?>')">VER ESTUDIO</a>
      </div>
    </div>

  </div>
<?php } ?>