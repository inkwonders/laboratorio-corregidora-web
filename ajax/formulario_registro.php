<?php 
?>

<div class="flex flex-row items-center mb-2 pt-12">
    <img class="m-3 h-9" src="img/svg/tarjeta_credito_azul.svg" alt=""> <span class="text-2xl font-bold azul_textos">PROCESO DE PAGO</span>
</div>
<div class="flex flex-row justify-center mb-2 space-x-4 md:justify-end">
    <div class="w-3 h-3 mb-4 bg-blue-600 rounded-full" ></div>
    <div class="w-3 h-3 mb-4 border border-blue-600 rounded-full" ></div>
    <div class="w-3 h-3 mb-4 border border-blue-600 rounded-full" ></div>
</div>
<div class="flex flex-col w-full p-8 bg-white rounded azul_textos">
    <h2 class="mb-4 text-2xl font-bold text-center ">DATOS DEL PACIENTE</h2>
    <label class="text-xl " for="txt_nombre">NOMBRE COMPLETO*</label>
    <input autocomplete="off" maxlength="120" oninput="valida_nombre();validar();" class="p-2 mb-4 border rounded-lg border-grey-200 focus:ring-2 focus:outline-none focus:ring-blue-400 focus:border-transparent" type="text" id="txt_nombre">
    <p class="mb-2 text-red-600 error_nombre"></p>
    <label class="mb-2 text-xl" for="txt_correo">CORREO*</label>
    <input autocomplete="off" maxlength="120" oninput="valida_correo();validar();" class="p-2 mb-4 border rounded-lg border-grey-200 focus:ring-2 focus:outline-none focus:ring-blue-400 focus:border-transparent" type="text" id="txt_correo">
    <p class="mb-2 text-red-600 error_correo"></p>
    <label class="mb-2 text-xl" for="txt_telefono">TELÉFONO*</label>
    <input autocomplete="off" maxlength="10" oninput="valida_telefono();validar();" class="p-2 mb-4 border rounded-lg border-grey-200 focus:ring-2 focus:outline-none focus:ring-blue-400 focus:border-transparent" type="tel" id="txt_telefono">
    <p class="mb-2 text-red-600 error_telefono"></p>
    <span class="text-center text-gray-400" > Los campos con * son obligatorios </span>
</div>
<div class="flex justify-center space-x-4">
    <!-- <button id="siguiente_btn" onclick="enviar_formulario()" class="hidden p-6 mt-6 text-xl text-white bg-blue-500 rounded-lg text- hover:bg-blue-400">SIGUIENTE</button> -->
    <button id="regresar_btn" onclick="abrir_modal(); abrir_carrito();" class="p-6 mt-6 text-xl text-white bg-gray-400 rounded-lg text- hover:bg-gray-500"> REGRESAR </button>
    <button id="siguiente_btn" onclick="enviar_formulario()" class="hidden p-6 mt-6 text-xl text-white bg-blue-500 rounded-lg text- hover:bg-blue-400"> SIGUIENTE </button>
</div>

<script>

$(document).ready(function(){

  $('#txt_nombre').val(sessionStorage.getItem('nombre'));
  if(sessionStorage.getItem('nombre')){
    valida_nombre();
    validar();
  }
  
  $('#txt_correo').val(sessionStorage.getItem('correo'));
  if(sessionStorage.getItem('correo')){
    valida_correo();
    validar();
  }
  
  $('#txt_telefono').val(sessionStorage.getItem('telefono'));
  if(sessionStorage.getItem('telefono')){
    valida_telefono();
    validar();
  }

}); 


var nombre = 0;
var correo = 0;
var telefono = 0;


function valida_nombre() {
  var txt_nombre = $('#txt_nombre').val();
  var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;
  
  if(txt_nombre.length == 0 ||  !txt_nombre.trim()){
   $('.error_nombre').html('Campo requerido');
   nombre= 0;
  }else if(txt_nombre.length < 2){
   $('.error_nombre').html('Ingresar más de un caracter');
   nombre= 0;
  }else if(!letras.test(txt_nombre)){
   $('.error_nombre').html('Caracteres no validos  ');
   nombre= 0;
  }else{
   $('.error_nombre').html('');
   nombre= 1;
  }
  return nombre;
}

function valida_telefono() {
  var telefono_numeros = /^[0-9\- ]+$/;
  var formatotelefono = /^(\d{3}(\-|\s)\d{3}(\-|\s)\d{4})|(\d{10})$/;
  var txt_telefono = $('#txt_telefono').val();

  if(txt_telefono.length == 0 ||  !txt_telefono.trim()){
    $('.error_telefono').html('Campo requerido');
    telefono = 0;
  }else if (txt_telefono.length < 2){
    $('.error_telefono').html('Ingresar más de un caracter');
    telefono = 0;
  }else if(!telefono_numeros.test(txt_telefono)){
    $('.error_telefono').html('Caracteres no validos  ');
    telefono = 0;
  }else{
    $('.error_telefono').html('');
    telefono = 1;
  }
  return telefono;
}

function valida_correo() {
  var txt_correo = $('#txt_correo').val();
  var correo_formato = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  
  if(txt_correo.length == 0 ||  !txt_correo.trim()){
    $('.error_correo').html('Campo requerido');
    correo = 0;
  }else if (txt_correo.length < 2){
    $('.error_correo').html('Ingresar más de un caracter');
    correo = 0;
  }else if(!correo_formato.test(txt_correo)){
    $('.error_correo').html('Correo electronico invalido');
    correo = 0;
  }else{
    $('.error_correo').html('');
    correo = 1;
  }
  return correo;
}

function validar(){
  if(nombre == 1 && telefono == 1 && correo == 1 ){
    $('#siguiente_btn').removeClass('hidden');
  }else{
    $('#siguiente_btn').addClass('hidden');

  }
}

</script>