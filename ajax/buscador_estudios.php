<?php
include 'dbcon.php';
$buscar_estudio = $_POST['buscar'];
$categoria = $_POST['categoria'];

if($categoria != 1){
  $consulta_analisis = "SELECT `id`, `clave`, `nombre`, `estudios`, `ayuno`, `entrega`, `indicaciones`, `precio`, `categoria_id`,`estatus` FROM `analisis` WHERE (`clave` LIKE '%$buscar_estudio%' OR `nombre` LIKE '%$buscar_estudio%' OR `estudios` LIKE '%$buscar_estudio%') AND `categoria_id` LIKE '$categoria' AND `estatus` = 1 ";
}else{
  $consulta_analisis = "SELECT `id`, `clave`, `nombre`, `estudios`, `ayuno`, `entrega`, `indicaciones`, `precio`, `categoria_id`,`estatus` FROM `analisis` WHERE (`clave` LIKE '%$buscar_estudio%' OR `nombre` LIKE '%$buscar_estudio%' OR `estudios` LIKE '%$buscar_estudio%') AND `estatus` = 1 ";
}

// $consulta_analisis = "SELECT `id`, `clave`, `nombre`, `estudios`, `ayuno`, `entrega`, `indicaciones`, `precio`, `categoria_id`,`estatus` FROM `analisis` WHERE (`clave` LIKE '%$buscar_estudio%' OR `nombre` LIKE '%$buscar_estudio%' OR `estudios` LIKE '%$buscar_estudio%') AND `categoria_id` LIKE '$categoria' AND `estatus` = 1 ";

$result = mysqli_query($dbCon, $consulta_analisis);
?>
<?php $contador_registro = 0; //if ( $fila = mysqli_fetch_assoc($result)) {
?>
<?php while ($fila = mysqli_fetch_assoc($result)) {
  $contador_registro++; ?>
  <div class="relative flex flex-wrap content-between overflow-hidden bg-white">
    <div class="flex flex-col justify-center w-full px-6 pt-4">
      <div class="absolute left-0 w-1 h-14 bg-blue-lab-oscuro top-4"></div>
      <div class="text-lg tracking-wide font-rob-medium azul_textos">
        <?= $fila['nombre']; ?>
      </div>
    </div>

    <div class="w-full">
      <!-- <div class="px-6 text-gray-500">
        <hr />
      </div> -->
      <div class="block p-4 text-lg text-left">
        <?php if ($fila['precio'] == '0.00') { ?>
          <p class="text-green-500 font-rob-regular">Favor de llamar al teléfono 442 212 10 52 para solicitar el precio de este estudio. </p>
        <?php } else { ?>
          <p class="text-lg text-center font-rob-medium azul_textos">PRECIO</p>
          <p class="text-lg text-green-500 font-rob-bold"><b>$ <?= number_format($fila['precio'], 2); ?></b></p>
        <?php } ?>
      </div>

      <div class="grid text-center md:grid-cols-1">
        <a class="px-4 py-1 font-bold tracking-wider text-gray-100 uppercase cursor-pointer bg-blue-lab-claro hover:bg-blue-lab-oscuro" onclick="openModal(); rellenar_modal('<?= $fila['id']; ?>')">VER ESTUDIO</a>
      </div>
    </div>
  </div>

<?php }
if ($contador_registro == 0) { //}else{   
?>
  <div></div>
  <div class="w-full sin_resultados">
    <p class="">No se han encontrado resultados</p>
  </div>
<?php }  ?>
</div>