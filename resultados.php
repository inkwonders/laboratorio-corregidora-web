<?php
session_start();
error_reporting(0);
?>

<!doctype html>
<html lang="es">

<head>

<!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>

  <!-- <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" /> -->

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
  <meta http-equiv="X-UA-Compatible" content="IE=11" />
  <title>LABORATORIO CORREGIDORA</title>
  <!-- <link rel="stylesheet" href="estilo.css" /> -->
  <!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->

  <link rel="stylesheet" type="text/css" href="slick/slick.css" />
  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />
  <link rel="stylesheet" type="text/css" href="main.css" />

  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
  <link rel="manifest" href="img/favicon/site.webmanifest">
  <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="theme-color" content="#ffffff">
  <!-- favicon -->
  <style>
    .div-footer {
      width: 100%;
    }

    .footer-mov {
      width: 100% !important;
    }

    .fc-button,
    .fc-button-group {
      float: right !important;

    }

    .footer-mov-resultados {
      margin-top: 150vh;
    }

    /* modal */

    #modal_video {
      display: none;
      width: 100%;
      height: 100vh;
      position: absolute;
      top: 0;
      left: 0;
      background-color: rgba(0, 0, 0, 0.7);
      z-index: 101;
    }

    .posicion_modal_video {
      width: 75%;
      height: 75%;
      background-color: #fff;
      position: fixed;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    .contenido_modal_video {
      width: 100%;
      height: 100%;
      padding: 16px;
      position: relative;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
    }

    .contenedor_video {
      width: 95%;
      height: 85%;
    }

    .btn_cerrar {
      margin-top: 20px;
      border: solid #00559b;
      padding: 10px 20px;
      border-radius: 36px;
      background-color: #00559b;
      color: white;
      font-size: 18px;
      transition: all 1s;
      cursor: pointer;
    }

    .btn_cerrar:hover {
      background-color: white;
      color: #00559b;
    }

    .video {
      width: 100%;
      height: 100%;
    }

    .instrucciones {
      background-color: #00559b;
      position: absolute;
      top: 10px;
      right: 15px;
      padding: 10px 15px;
      cursor: pointer;
      transition: all 1s;
      border-radius: 5px;
    }

    .instrucciones:hover {
      background-color: #fff;
      color: #00559b;
    }

    @media (max-width:980px) {

      .posicion_modal_video {
        width: 90%;
        height: 50%;
      }

      .btn_cerrar {
        font-size: 14px;
      }

      .instrucciones {
        right: 8px;
        font-size: 14px;
        bottom: 9px;
        top: unset;
        padding: 5px 10px;
      }

    }
  </style>
  <!-- <meta name="Description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/>
<meta property="og:url" content="https://almecmexico.com"/>
<meta property="og:title" content="ALMEC"/>
<meta property="og:image" content="https://almecmexico.com/img/almec-share.jpg"/>
<meta property="og:description" content="SOMOS una empresa mexicana dedicada a ofrecer soluciones integrales en servicios e instalaciones metalmecánicas."/> -->

  <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
  <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'AW-691396176');
  </script> -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-152013601-2');
  </script> -->
  <!-- end Global site tag (gtag.js) - Google Analytics -->

</head>

<body>
  <?php include('header.php'); ?>


  <section id="resultados" class="bg-no-repeat bg-cover" style="background-image: url(img/resultados_back.jpg);">


    <div class="relative flex flex-col items-center justify-center mt-16">
      <div class="flex flex-col w-11/12 md:w-6/12 md:mt-8">
        <div class="h-20 md:h-28" style="background:transparent;"></div>
        <div class="relative p-4 text-white bg-blue-400">
          <span class="text-2xl">Por favor ingrese la fecha en la que realizó sus estudios</span>
        </div>
        <div id="datepicker" class="p-3 bg-white"></div>
        <input class="hidden" type="text" id="fecha">
      </div>
      <div class="m-5 md:mb-5">
        <button id="consultar" class="hidden p-4 text-xl text-white bg-blue-600" onclick="enviar_fecha();">Consultar</button>
      </div>



      <div class=" md:h-20" style="background:transparent;"></div>


    </div>



    <!-- footer movil -->

    <div class="footer-mov ">
      <div class="cont_footer_mov">
        <p><a href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de privacidad</a></p>
        <p><a href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a></p>
        <p><a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a></p>
        <p><span class="menu-el link-a" op="vid-mov" style="font-family: m-regular!important; font-size: 16px!important; cursor: pointer!important;">Contáctanos</span></p>
        <p><a href="contacto.php" class="link-a">Felicitaciones, Quejas y Sugerencias</a></p>
        <p style="text-align: center;">Permiso de Publicidad: 203301201A0872</p>
        <p><a class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a></p>
      </div>
    </div>

    <!-- end footer movil -->

  </section>
  <div class="div-footer footer-resultados">
    <div class="footer-cell" style="width: 48%;">
      <a rel="noopener" href="pdf/AVISODEPRIVACIDAD.pdf" target="_blank" class="link-a">Aviso de Privacidad</a> &nbsp; / &nbsp; <a href="pdf/DICOTOMIA.pdf" target="_blank" class="link-a">Dicotomía</a> &nbsp; / &nbsp; <a href="pdf/ACREDITACION.pdf" target="_blank" class="link-a">Acreditación</a> &nbsp; / &nbsp; <span class="menu-el" op="sucursales" style="cursor: pointer; ">Contáctanos</span> &nbsp; / &nbsp; <a class="menu-el" href="/contacto.php"> Felicitaciones, Quejas y Sugerencias </a>
    </div>
    <div class="footer-cell">
      <p>Permiso de Publicidad: 203301201A0872</p>
    </div>
    <div class="footer-cell" style="text-align:right">
      <a rel="noopener" class="link-a" href="https://inkwonders.com/" target="_blank">DESARROLLADO POR INKWONDERS</a>
    </div>
  
  </div>


  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    // if (info.dateStr > '2021-07-02') {
    //   seleccion = 0;
    // } else {
    //   seleccion = 1;
    // }

    seleccion = 0;
    var frmData = new FormData();
    frmData.append("seleccion", seleccion);
    $.ajax({
      type: "POST",
      data: frmData,
      processData: false,
      contentType: false,
      cache: false,
      url: "ajax/resultados_fecha.php",
      beforeSend: function() {
        $("#resultados").empty();

      },
      success: function(datos) {
        $("#resultados").html(datos).show();
      },
    });

    $(document).ready(function() {
      cargar_carrito_header();
    });

    // $(".notificacion").click(function() {
    //   console.log("medidas de ancho = " + screen.width);
    //   if (screen.width < 600) {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid_mov";
    //   } else {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid";
    //   }

    // });

    // $(".citas_covid").click(function() {
    //   window.location = "https://www.laboratoriocorregidora.com.mx/citas/";
    // });

    $(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function() {

      window.location = "resultados.php";

    });

    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('datepicker');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        contentHeight: 430,
        selectable: true,
        longPressDelay: 1

      });
      calendar.setOption('locale', 'es');
      calendar.on('dateClick', function(info) {
        //calendar.fullCalendar('refresh');


        // $('#fecha').val(info.dateStr);

        if (info.dateStr > '2021-07-02') {
          seleccion = 0;
        } else {
          seleccion = 1;
        }

        var frmData = new FormData();
        frmData.append("seleccion", seleccion);

        $.ajax({
          type: "POST",
          data: frmData,
          processData: false,
          contentType: false,
          cache: false,
          url: "ajax/resultados_fecha.php",
          beforeSend: function() {
            $("#resultados").empty();

          },
          success: function(datos) {
            $("#resultados").html(datos).show();
          },
        });

      });
      calendar.render();
    });

    function enviar_fecha() {


    }

    // $(".notificacion").css("z-index", "100");

    //$(".op_guardias").addClass("menu_activo_desktop");

    $(document).on("click", ".menu-el", function() {
      let opcion = $(this).attr("op");
      switch (opcion) {
        case "servicios":
          localStorage.setItem("opcion_animate", "servicios");
          window.location = "/";
          break;
        case "preguntas":
          localStorage.setItem("opcion_animate", "preguntas");
          window.location = "/";
          break;
        case "sucursales":
          localStorage.setItem("opcion_animate", "sucursales");
          window.location = "/";
          break;
        case "serv-mov":
          localStorage.setItem("opcion_animate", "serv-mov");
          window.location = "/";
          break;
        case "preg-mov":
          localStorage.setItem("opcion_animate", "preg-mov");
          window.location = "/";
          break;
        case "vid-mov":
          localStorage.setItem("opcion_animate", "vid-mov");
          window.location = "/";
          break;
        default:
          break;
      }
    });

    function myFunction(x) {
      x.classList.toggle("change");
      if (document.getElementById("contenidoMenu").style.display == "none") {
        document.getElementById("contenidoMenu").style.display = "";
        $("#contenidoMenu").animate({
          height: '100vh'
        }, "slow");
      } else {
        $("#contenidoMenu").animate({
          height: '0'
        }, "slow", function() {
          document.getElementById("contenidoMenu").style.display = "none";
        });
      }
    }


    function funcion_modal_video(f, m) {
      var frmData = new FormData();
      frmData.append("link", m);

      $.ajax({
        type: "POST",
        data: frmData,
        processData: false,
        contentType: false,
        cache: false,
        url: "ajax/modal_video.php",
        beforeSend: function() {},

        success: function(datos) {
          $("#modal").html(datos).show();
          if (f == '1') {
            $('#modal_video').css('display', 'none');
          } else {
            $('#modal_video').css('display', 'block');
          }
        },
      });
    }

    function redirigir(l){
      window.open(l)
    }
  </script>
</body>

</html>