<?php
session_start();
error_reporting(0);

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1

header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado

?>

<!doctype html>
<html lang="es">

<head>
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
  <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>
  <script type="text/javascript" src="js/jquery.js"></script>
  <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtmAZBLkqZEWcMd6tZPElNrxIhT4k4tMI&libraries=places"></script> -->

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, maximum-scale=5" />
  <meta http-equiv="X-UA-Compatible" content="IE=11" />
  <title>LABORATORIO CORREGIDORA</title>

  <meta name="description" content="Laboratorio de Análisis clínicos en Querétaro">
  <meta name="robots" content="all" />
  <meta name="laboratorio corregidora" content="laboratoriocorregidora.com.mx" />
  <meta name="distribution" content="global" />
  <meta name="keywords" content="Mejor laboratorio en Querétaro
Laboratorios en Querétaro
Laboratorios Corregidora
Corregidora
Juriquilla Laboratorio
Milenio Juriquilla
Pueblo Nuevo Laboratorio
Jurica Laboratorio
Tecnológico Laboratorio
Mejor laboratorio de Querétaro
Laboratorio de análisis clínicos Querétaro
Resultados laboratorio corregidora
Laboratorio acreditado Querétaro
Mejor laboratorio de Querétaro
Análisis Laboratorio Corregidora
Costos Laboratorio Corregidora
Laboratorios Corregidora
Laboratorios Querétaro certificado
Laboratorios Querétaro acreditado
Laboratorio especialista en toma de muestras difíciles
Laboratorio Querétaro alta tecnología
Laboratorio con amplio menú de pruebas
Laboratorio con más años de experiencia
Laboratorio confiable
Resultados exactos y precisos laboratorio
Laboratorios Vázquez Mellado
Toma de muestras a domicilio laboratorio
Laboratorio con servicio fines de semana
Laboratorio con personal competente
Laboratorio con servicio los sábados y domingos
Sucursales laboratorio corregidora
Costos análisis laboratorio corregidora
Laboratorios en Querétaro ISO
Laboratorio corregidora sucursales
Laboratorio con costos accesibles en Querétaro
Laboratorio con costos económicos en Querétaro
Laboratorio con costos bajos en Querétaro
Laboratorio entrega rápido resultados
Laboratorio resultados confiables
Laboratorio entrega resultados el mismo día
Laboratorio con mayor prestigio en Querétaro
Laboratorio más recomendado en Querétaro
Laboratorio con el mejor control de calidad
Laboratorio de análisis clínicos con mayor calidad
Laboratorio de calidad
Laboratorio con excelencia en los resultados
Laboratorio con resultados confiables
Laboratorio personal especialista en toma de muestra pediátrica
Análisis Clínicos
Análisis de sangre
Análisis de orina
Análisis generales
Análisis para infección
Análisis de laboratorio
Análisis  de VIH
Análisis de prueba de embarazo
Menú de análisis de laboratorio
Menú de análisis clínicos
Análisis clínicos
Análisis check up" />

  <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
  <link rel="manifest" href="img/favicon/site.webmanifest">
  <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="theme-color" content="#ffffff">

  <!-- <link rel="preload" href="/fonts/Montserrat-Black.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-Bold.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-ExtraBold.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-Medium.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-Regular.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="/fonts/Montserrat-SemiBold.ttf" as="font" type="font/ttf" crossorigin> -->

  <!-- <link rel="stylesheet" href="estilo.css" /> -->
  <!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->

  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2067231483629662');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2067231483629662&ev=PageView&noscript=1" />
  </noscript>
</head>

<body>
  <?php include('header.php'); ?>

  <div class="relative">
    <section id="home">
      <div class="slider-contender" id="home-slider">
        <!-- <div class="sl-11 sl"></div> -->
        <!-- aviso 1 -->
        <!-- <div class="sl-15 sl"></div> -->

        <!-- <div class="sl-16 sl"></div> -->
        <div class="sl-10 sl"></div>
        <div class="sl-9 sl"></div>
        <div class="sl-4 sl" onclick="abrir_citas_banner()"></div>
        <!-- <div class="sl-5 sl"></div> -->
        <div class="sl-0 sl"></div>
        <div class="sl-1 sl">
          <div class="left1 left_sl"></div>
          <div class="right1 right_sl"></div>
        </div>
        <div class="sl-2 sl">
          <div class="left2 left_sl"></div>
          <div class="right2 right_sl"></div>
        </div>
        <div class="sl-3 sl" onclick="abrir_citas_imagenologia()">
          <div class=""></div>
        </div>
      </div>
    </section>
    <section id="home-mov">
      <div class="slider-contender" id="slide-mov">
        <!--aviso 1 -->
        <!-- <div class="sl-15-mov sl-mov"></div> -->

        <!-- <div class="sl-16-mov sl-mov"></div>  -->
        <div class="sl-10-mov sl-mov"></div>
        <div class="sl-9-mov sl-mov"></div>
        <div class="sl-7-mov sl-mov" onclick="abrir_citas_banner()"></div>
        <!-- <div class="sl-8-mov sl-mov" ></div> -->
        <div class="sl-1-mov sl-mov"></div>
        <div class="sl-2-mov sl-mov pdf_boton1"></div>
        <div class="sl-3-mov sl-mov pdf_boton2"></div>
        <div class="sl-4-mov sl-mov pdf_boton3"></div>
        <div class="sl-5-mov sl-mov pdf_boton4"></div>
        <div class="sl-6-mov sl-mov"></div>
      </div>
    </section>
    <div class="md:absolute md:px-0 px-8 py-8 md:bg-transparent bg-blue-lab-claro bg-opacity-60 top-0 z-10 block m-auto left-16 md:max-w-[calc(100%-8rem)] md:items-start gap-y-4">
      <?php include('./dist/modules/links.html'); ?>
    </div>
  </div>

  <?php include('./dist/modules/information.html'); ?>

  <?php include('./dist/modules/content.html'); ?>

  <?php include('./dist/modules/video.html'); ?>

  <?php include('./dist/modules/sucursales.html'); ?>

  <?php include('footer.php'); ?>

  <a rel="noreferrer" class="block wa-contenedor" href="https://wa.link/b0x4ly" target="_blank">
    <div class="wa-contenedor__icon"></div>
    <div class="wa-contenedor__banner">
      <span class="wa-contenedor__texto">¿Ayuda?</span>
      <div class="wa-contenedor__barra"></div>
    </div>
  </a>

  <div class="modal no-visible">
    <div class="close-modal">
      <div class="izq"></div>
      <div class="der"></div>
    </div>

    <iframe title="video 2" id='video-youtubecitas/log-pagos.txt' src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>




  <link rel="stylesheet" type="text/css" href="slick/slick.css" />
  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />

  <script type="text/javascript" src="slick/slick.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      cargar_carrito_header();
      $('#home-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        autoplay: true,
        autoplaySpeed: 5000,
        slide: 'div',
        mobileFirst: true,
      });
      $('#slide-mov').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        autoplay: false,
        autoplaySpeed: 2000,
      });
      $('#slide-serv').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true,
      });
      $('#slide-preg-mov').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
      });

      let ruta = localStorage.getItem("opcion_animate");

      // console.log(ruta);

      if (ruta != null && ruta != 'null') {
        if (screen.width < 980) {
          $('html, body').animate({
            scrollTop: ($("#" + ruta).offset().top - 100)
          }, 1500);
        } else {
          $('html, body').animate({
            scrollTop: ($("#" + ruta).offset().top - 100)
          }, 1500);
        }
        localStorage.setItem("opcion_animate", null);
      }
    });
  </script>
  <script type="module" src="js/script.js"></script>
  <script type="text/javascript">
    function abrir_citas_banner() {
      window.open("https://www.laboratoriocorregidora.com.mx/citas/", "_blank");
    }
    function abrir_citas_imagenologia() {
      window.open("https://imagenologia.laboratoriocorregidora.com.mx/");
    }
    $(".pdf_boton").click(function() {
      window.open("pdf/rt-pcr_para_la_deteccion_de_covid-19.pdf", "_blank");
    });

    $(".pdf_boton1, .left1").click(function() {
      window.open("pdf/MAYORES INFORMES PRUEBA PCR PARA CORONAVIRUS 12 de marzo 2021.pdf", "_blank");
      console.log("click 1");
    });

    $(".pdf_boton2, .right1").click(function() {
      window.open("pdf/MAYORES INFORMES PRUEBA RAPIDA DE ANTIGENO CORONAVIRUS 12 de marzo 2021.pdf", "_blank");
      console.log("click 2");
    });

    $(document).on("click", ".pdf_boton3, .left2", function() {
      window.open("pdf/MAYORES INFORMES PRUEBA RAPIDA DE ANTicuerpos CORONAVIRUS 12 de marzo 2021.pdf", "_blank");
      console.log("click 3");
    });

    $(document).on("click", ".pdf_boton4, .right2", function() {
      window.open("pdf/MAYORES INFORMES MUESTRA DE SANGRE PACIENTE POSITIVO DE COVID-19 12 de marzo 2021.pdf", "_blank");
      console.log("click 4");
    });

    // $(".notificacion").click(function() {
    //   console.log("medidas de ancho = " + screen.width);
    //   if (screen.width < 600) {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid_mov";
    //   } else {
    //     window.location = "https://laboratoriocorregidora.com.mx/covid.php#dia_img_covid";
    //   }
    // });

    // $(".citas_covid").click(function() {
    //   window.location = "https://www.laboratoriocorregidora.com.mx/citas/";
    // });
  </script>
</body>

</html>