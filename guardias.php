<!doctype html>
<html lang="es">
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
    <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <title>LABORATORIO CORREGIDORA</title>
    <!-- <link rel="stylesheet" href="estilo.css" /> -->

    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/site.webmanifest">
    <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
    <?php include('header.php'); ?>

    <section class="w-full">
        <div class="relative w-full cont_imagen_fondo_guardias h-max">
            <div class="relative flex justify-center w-full p-8 md:p-16 h-max">
                <?php include('./dist/modules/links.html'); ?>
            </div>
            <!-- <div class="block w-full h-32"></div> -->
        </div>

        <div class="w-full bg-[#e8e8e8]">
            <div class="w-full px-6 py-12 lg:py-16 flex flex-col gap-8 lg:gap-16 items-center mx-auto max-w-[94rem]">
                <b class="text-lg text-blue-lab font-rob-bold md:text-xl lg:text-2xl xl:text-3xl">LABORATORIO MATRIZ</b>

                <div class="flex flex-col w-full text-base gap-x-16 gap-y-12 md:text-lg lg:text-xl xl:text-2xl lg:flex-row font-rob-regular text-blue-lab">
                    <div class="flex items-start w-full gap-4 text-left lg:gap-5 lg:w-1/2">
                        <div class="w-[6px] min-w-[6px] h-16 bg-[#007af6]"></div>
                        <span>
                            <p><b class="text-lg font-rob-bold md:text-xl lg:text-2xl xl:text-3xl">EL MARQUÉS</b></p>
                            <p>
                                Av. Prolongación Constituyentes Ote. No. 32 C.P.76240,
                                Col. Villas del Marqués del Águila, El Marqués, Querétaro.
                                (A un costado del Super Q gasolineria de PEMEX. Frente
                                al Fraccionamiento El Mirador)
                            </p>
                        </span>
                    </div>

                    <div class="hidden border-r lg:block border-blue-lab-condensed h-80"></div>

                    <div class="flex items-start w-full gap-4 text-left lg:gap-5 lg:w-1/2">
                        <div class="w-[6px] min-w-[6px] h-16 bg-[#007af6]"></div>
                        <span>
                            <span class="text-lg md:text-xl lg:text-2xl xl:text-3xl">
                                <p><b class="font-rob-bold">SERVICIO DE GUARDIAS</b></p>
                                <br>
                                <p>SÁBADOS DE 16:00 A 18:00 Hrs.</p>
                                <p>DOMINGOS DE 08:00 A 12:00 Hrs.</p>
                            </span>
                            <br>
                            <!-- <p>
                                Para programar su cita comuníquese al teléfono
                            </p>
                            <a href="tel:4427905722"><b class="font-rob-bold">442 790 5722</b></a> -->
                            <p>Costo adicional por servicio de guardia en matriz $250.00</p>
                            <p>** Solo se procesan los estudios de la lista</p>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="w-full px-6 py-12 bg-white lg:py-16">
            <p class="pb-12 text-center lg:pb-16">
                <b class="text-lg md:text-xl lg:text-2xl xl:text-3xl text-blue-lab font-rob-bold">
                    LISTADO DE ESTUDIOS QUE SE PROCESAN
                    <br>
                    EN EL SERVICIO DE GUARDIAS
                </b>
            </p>

            <div class="mx-auto max-w-[94rem] text-blue-lab">
                <div class="w-full">
                    <ul style="grid-template-rows: repeat(31, minmax(auto, auto));" class="inline-grid w-full pl-0 text-base list-decimal list-inside md:grid-cols-2 lg:grid-flow-col lg:grid-cols-3 lg:text-lg xl:text-xl gap-x-8">
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Ácido láctico en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Ácido úrico en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Adenovirus en heces fecales</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Ag. Streptococcus del grupo A</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Amilasa en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Amonio en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Análisis toxicológico en drogas de abuso en orina al azar</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Antígeno de Clostridium diffcile en heces</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Antígeno para COVID-19</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Bicarbonato en orina</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Bílirrubinas en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Biometría hemática con reticulocitos en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Búsqueda de amiba en fresco y citología de moco fecal</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Búsqueda de criptosporidium en heces</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Búsqueda de sangre oculta en heces</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Calcio en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Citoquímico de líquido cefalorraquídeo</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Citoquímico de líquido de ascitis</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>CItoquímico de líquido de diálisis</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Citoquímico de líquido de lavado o aspirado bronquial</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Citoquímico de líquido pleural</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Citoquímico*</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Cloro en LCR</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>CO2 total en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Coombs directo en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Coprológico</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Coproparasitoscópico</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Creatinina en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Cuantificación de fibrinógeno en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Cuenta celular en líquidos</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Cuenta de plaquetas con citrato en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Cuenta de plaquetas con edta en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Cuenta de plaquetas con heparina en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Cuenta de reticulocitos en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Deshidrogenasa láctica en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Detección de ag del virus de influenza A y B</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Dímeros D-D en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Electrolitos de 6 elementos en sangre (Na, K, Cl, Ca, P, Mg)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Electrolitos séricos de 3 elementos en sangre (Na, K, CI)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Electrolitos séricos de 4 elementos en sangre (Na, K, CI, Ca)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Examen general de orina</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Ferritina en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Fórmula blanca en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Fórmula roja en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Fosfatasa alcalina en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Fosfocinasa de la creatina (CPK) en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Fosfocinasa de la creatina, fracción MB (CPK-MB) en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Fósforo en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Fracción beta de la HGC en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Gama Glutamil Transpeptidasa en sangre (GGT)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Gasometría en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Glucosa basal en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Glucosa en LCR</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Grupo sanguíneo y factor RH en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Isoenzimas de la fosfocinasa de la creatina (CPK. CPK-MB. CPK-MM) en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Lipasa en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Magnesio en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Panel de meningitis/encefalitis en [LCR (Film Array)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Panel de sepsis BCID (24 microorganismos FIlm Array)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Panel gastrointestinal (21 microorganismos Film Array)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Panel respiratorlo (Film Array)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Panel viral (23 microorganismos FIlm Array)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Péptido Natriurético Cerebral en sangre  (BNP)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Perfil básico de enzimas cardíacas en sangre (CPK, CPK-MB, CPK-MM, TGO y DHL, Troponina)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Perfil preoperatorlo (EGO, GL, TP, TPT, Gpo sanguíneo, BH con reticulocitos)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>PH y azúcares reductores en heces</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Potasio en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Ppd (Derivado proteico purificado)**</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Procalcitonina en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Proteína C reactiva de alta sensibilidad en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Proteínas de LCR</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Prueba Inmunológica para HGC humana cualitativa</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Química sanguinea completa (GLU URE BUN CREA AU COL) en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Química sanguínea de 3 elementos (GLU URE BUN CRE) en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Química sanguínea de 4 elementos (GLU URE BUN CRE AU)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Química sanguínea de 6 elementos (GLU, URE, BUN, CRE, AU, COL, TRIG)</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Reacciones de coaglutinación en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Rotavirus en heces</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Sodio en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Tiempo de protrombina en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Tiempo de trombina en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Tiempo de tromboplastina parcial en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Tinción de BAAR</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Tinción de GRAM</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Tinción de tinta china</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Transaminasa glutámica oxalacética en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Transaminasa glutámico piruvica en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Triglicéridos en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Troponina cardíaca cuantitativa en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Urea en sangre</span>
                        </li>
                        <li class="w-full px-6 py-1 list-decimal bg-[#e8e8e8]">
                            <span>Velocidad de sedimentación globular en sangre</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>




    <?php include('footer.php'); ?>




    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script type="text/javascript">
        cargar_carrito_header();

        $(".op_guardias").addClass("menu_activo_desktop");

        $(document).on("click", ".menu-el", function(){
            let opcion = $(this).attr("op");
            switch(opcion){
                case "servicios":
                    localStorage.setItem("opcion_animate", "servicios");
                    window.location = "/";
                    break;
                case "preguntas":
                    localStorage.setItem("opcion_animate", "preguntas");
                    window.location = "/";
                    break;
                case "sucursales":
                    localStorage.setItem("opcion_animate", "sucursales");
                    window.location = "/";
                    break;
                case "serv-mov":
                    localStorage.setItem("opcion_animate", "serv-mov");
                    window.location = "/";
                    break;
                case "preg-mov":
                    localStorage.setItem("opcion_animate", "preg-mov");
                    window.location = "/";
                    break;
                case "vid-mov":
                    localStorage.setItem("opcion_animate", "vid-mov");
                    window.location = "/";
                    break;
                default:
                    break;
            }
        });

        $(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function(){
            window.location = "resultados.php";
        });

        function myFunction(x) {
            x.classList.toggle("change");
            if(document.getElementById("contenidoMenu").style.display == "none"){
                document.getElementById("contenidoMenu").style.display = "";
                $("#contenidoMenu").animate({
                    height: '100vh'
                }, "slow");
            }else{
                $("#contenidoMenu").animate({
                    height: '0'
                }, "slow", function(){ document.getElementById("contenidoMenu").style.display = "none"; });
            }
        }
    </script>
</body>
</html>
