<?php

  error_reporting(E_ALL);
  ini_set('display_errors', '1');

  require_once("../controladores/usuario.controlador.php");

  class Ingreso{

    public function login(){

      $clave = $this->clave;

      $respuesta = Usuarios::validar_clave($clave);

      echo $respuesta;

    }

  }

  $ingreso = new Ingreso();

  if(isset($_POST['clave_acceso']) && $_POST['clave_acceso'] != ""){

      $ingreso -> clave = $_POST['clave_acceso'];

      $ingreso -> login();

  }
