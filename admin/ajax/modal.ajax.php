<?php

  error_reporting(E_ALL);
  ini_set('display_errors', '1');

  require_once("../controladores/citas.controlador.php");
  require_once("../modelos/citas.modelo.php");

  class Modal{

    public function consulta_paciente(){

      $clave = $this->clave;

      $respuesta = ControladorCitas::ctrConsultaPacienteCita($clave);
      array_push($respuesta,ControladorCitas::ctrConsultaPacienteSintomas($clave));
      array_push($respuesta,ControladorCitas::ctrConsultaPacienteEnfermedades($clave));
      array_push($respuesta,ControladorCitas::ctrConsultaPacientePruebas($clave));
      echo json_encode($respuesta,JSON_FORCE_OBJECT);

    }

  }

  $modal = new Modal();

  if(isset($_POST['fk_usuario']) && $_POST['fk_usuario'] != ""){

      $modal -> clave = $_POST['fk_usuario'];

      $modal -> consulta_paciente();

  }
