<?php

date_default_timezone_set('America/Mexico_city');

require_once "../controladores/usuario.controlador.php";
require_once "../modelos/citas.modelo.php";

class Status{

  public function cambioStatus(){

    $sk = $this->sk_cita;
    $quitar = $this->quitar;
    $tipo_pago = $this->tipo_pago;

    $respuesta = Usuarios::statusCita($sk, $quitar, $tipo_pago);

    echo $respuesta;

  }

}

$status = new Status();

if(isset($_POST['sk']) && $_POST['sk'] != ""){

  $status -> quitar = $_REQUEST['quitar'];
  $status -> sk_cita = $_REQUEST['sk'];
  $status -> tipo_pago = $_REQUEST['tipo_pago'];
  $status-> cambioStatus();

}


