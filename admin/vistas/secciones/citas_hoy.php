<?php

  require_once("../../controladores/citas.controlador.php");
  require_once("../../modelos/citas.modelo.php");

  date_default_timezone_set('America/Mexico_city');

?>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <dl class="dl-horizontal">
                <dt>NO. CONFIRMACIÓN</dt>
                <dd id="no_confirmacion"></dd>
                <dt>NOMBRE PACIENTE</dt>
                <dd id="nombre_paciente"></dd>
                <dt>GENERO</dt>
                <dd id="genero"></dd>
                <dt>FECHA DE NACIMIENTO</dt>
                <dd id="fecha_nac"></dd>
                <dt>TELÉFONO CASA</dt>
                <dd id="tel_casa"></dd>
                <dt>TELÉFONO CELULAR</dt>
                <dd id="tel_cel"></dd>
                <dt>EMAIL</dt>
                <dd id="email_paciente"></dd>
                <dt>CALLE</dt>
                <dd id="calle"></dd>
                <dt>COLONIA</dt>
                <dd id="colonia"></dd>
                <dt>NO. EXTERIOR</dt>
                <dd id="no_ext"></dd>
                <dt>NO. INTERIOR</dt>
                <dd id="no_int"></dd>
                <dt>C.P.</dt>
                <dd id="cp"></dd>
                <dt>NOMBRE MEDICO</dt>
                <dd id="nombre_medico"></dd>
                <dt>EMAIL MEDICO</dt>
                <dd id="email_medico"></dd>
                <dt>RAZÓN SOCIAL</dt>
                <dd id="rs"></dd>
                <dt>RFC</dt>
                <dd id="rfc"></dd>
                <dt>DOMICILIO FISCAL</dt>
                <dd id="dom_fiscal"></dd>
                <dt>SINTOMAS</dt>
                <dd id="sintomas"></dd>
                <!-- <dt>OTRO SINTOMA</dt>
                <dd id="otro_sintoma"></dd> -->
                <dt>ENFERMEDADES</dt>
                <dd id="enfermedades"></dd>
                <dt>CONTACTO PERSONA POSITIVO</dt>
                <dd id="contacto_positivo"></dd>
                <dt>ANTIVIRAL</dt>
                <dd id="antiviral"></dd>
                <dt>INFLUENZA</dt>
                <dd id="influenza"></dd>
                <dt>MOTIVO DE LA PRUEBA</dt>
                <dd id="pruebas"></dd>
                <dt>TIPO PRUEBA</dt>
                <dd id="tipo_prueba"></dd>
                <dt>TIPO PAGO</dt>
                <dd id="tipo_pago"></dd>
                <dt>IDIOMAS DE ESTUDIO</dt>
                <dd id="idiomas"></dd>
                <dt>PASAPORTE</dt>
                <dd id="pasaporte"></dd>

              </dl>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">CITAS DEL DÍA</h1>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="container-fluid">
        <div class="row ">

          <table id="table" class="display" style="width:100%!important;">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>NOMBRE PACIENTE</th>
                      <th>FECHA DE CITA</th>
                      <th>HORARIO</th>
                      <!--<th>TÉL. (celular)</th> -->
                      <!--<th>EMAIL </th> -->
                      <th>TIPO PRUEBA</th>
                      <th>NO. CONFIRMACIÓN</th>
                      <th>PAGADO</th>
                      <th>TIPO PAGO</th>
                      <th>COSTO</th>
                      <th>TIPO TARJETA</th>
                      <th>ULTIMOS 4 DIGITOS</th>
                      <th>ACCIONES</th>
                  </tr>
              </thead>

              <?php

                $fecha = date("Y-m-d");
                $consulta_citas = ControladorCitas::ctrConsultaCitasHoy($fecha);

                $i = 1;

                foreach($consulta_citas as $key => $value){
                  $flag_asistencia=true;
                  $consulta_horarios = ControladorCitas::ctrConsultaHorario($value["fk_horario"]);

                  $consulta_horario = ControladorCitas::ctrConsultaPaciente($value["fk_paciente"]);

                  echo "<tr>";


                  if($value["status_asistencia"]==1){
                    // echo "<tr class='bg-success text-white'>";
                    echo "<td class='bg-success text-white' style='color: white !important;'>".$i."</td>";

                  }else if(strtotime($value["fecha_seleccionada"]) < strtotime(date("Y-m-d"))){
                    //  echo "<tr class='bg-danger text-white'>";
                    echo "<td  class='bg-danger text-white' style='color: white !important;'>".$i."</td>";

                      $flag_asistencia=false;
                  }else if(strtotime($value["fecha_seleccionada"]) == strtotime(date("Y-m-d"))){
                      if(strtotime($consulta_horarios[2])<strtotime(date("H:i:s"))){
                        // echo "<tr class='bg-danger text-white'>";
                        echo "<td class='bg-danger text-white' style='color: white !important;'>".$i."</td>";

                        $flag_asistencia=false;
                      }else {
                          echo "<td>".$i."</td>";
                          
                      }

                  }else {
                      echo "<td>".$i."</td>";
                  }

                  // if(strtotime($value["fecha_seleccionada"]) < strtotime(date("Y-m-d"))){
                  //     echo "<tr class='bg-danger text-white'>";
                  // }else {
                  //     echo "<tr>";
                  // }


                    $nombre = strtoupper($consulta_horario["nombre"]." ".$consulta_horario["apellido_paterno"]."  ".$consulta_horario["apellido_materno"]);

                    echo "<td>".$nombre."</td>";

                    $hora_inicio = date("H:i",strtotime($consulta_horarios[1]));
                    $hora_fin = date("H:i",strtotime($consulta_horarios[2]));



                    $fecha = date("d/m/Y",strtotime($value["fecha_seleccionada"]));
                    echo "<td>".$fecha."</td>";
                    echo "<td>".$hora_inicio."hrs - ".$hora_fin."hrs</td>";
                    //echo "<td>".$consulta_horario["tel_celular"]."</td>";
                    //echo "<td>".$consulta_horario["paciente_email"]."</td>";
                    // agrege este switch para poner el valor de el tipo de prueba 
                    // Guillo 
                    switch ($value["tipo_prueba"]) {
                      case '1':
                        echo "<td>PCR</td>";
                      break;
                      case '2':
                        echo "<td>ANTÍGENOS</td>";
                      break;
                    }

                    echo "<td>".$value["no_confirmacion"]."</td>";
                    if($value["pagado"] == 0){
                      echo "<td>No</td>";
                    }else{
                      echo "<td>Si</td>";
                    }

                    switch ($value["tipo_pago"]) {
                      case '0':
                        echo "<td>Sucursal</td>";
                      break;
                      case '1':
                      echo "<td>Online</td>";
                      break;
                      default:
                      echo "<td>----------</td>";
                      # code...
                        break;
                    }

                    echo "<td>$".number_format($value['costo'],2)."</td>";

                    if($value["tipo_tarjeta"] != ""){
                        echo "<td>".$value["tipo_tarjeta"]."</td>";
                    }else{
                        echo "<td>----------</td>";
                    }

                    if($value["digitos"] != ""){
                        echo "<td>".$value["digitos"]."</td>";
                    }else{
                        echo "<td>----------</td>";
                    }

                    echo "<td>";
                    echo "<button type='button' class='btn_ver_cita btn btn-primary btn-xs' data-toggle='modal' data-target='#myModal' cita='".$value["fk_paciente"]."'><i class='fas fa-eye'></i></button>";
                    if($value["status_asistencia"] == 1){
                        echo "<button type='button' class='btn_status_false btn btn-danger btn-xs' style='margin-left: 10px;' cita='".$value["sk_cita"]."' tipo_pago='".$value["tipo_pago"]."'><i class='far fa-times-circle'></i></button>";
                    }else{
                      // if($flag_asistencia){
                        echo "<button type='button' class='btn_status btn btn-success btn-xs' style='margin-left: 10px;' cita='".$value["sk_cita"]."' tipo_pago='".$value["tipo_pago"]."'><i class='fas fa-check'></i></button>";
                      // }
                    }
                    echo "</td>";


                  echo "</tr>";

                  $i++;

                }

              ?>

          </table>

        </div>

    </div>
</div>

<script type="text/javascript">

  $(document).ready(function() {

    $('#table').DataTable({
        "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "No existen coincidencias",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No existen registros",
          "infoFiltered": "(filtrado por _MAX_ total de registros)",
          "search": "BUSCAR",
          "paginate": {
            "previous": "Anterior",
            "next": "Siguiente",
          }
        },
          "bSort": true,
          "scrollY": "500px",
          "scrollCollapse": true
    });

  });

</script>
