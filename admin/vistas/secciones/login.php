<div class="login-box">

    <div class="login-logo">
        <a href="https://laboratoriocorregidora.com.mx/">
            <img src="../img/svg/logo-nuevo.svg" alt="">
        </a>
    </div>

    <div class="card">
        <div class="card-body login-card-body">
            <br>

            <form id="form_ingreso" method="post">
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="clave_acceso" id="clave_acceso" placeholder="Ingrese su clave de acceso">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fas fa-key"></i>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <div class="cont_ver_clave">
                        <input type="checkbox" class="ver_clave" name="ver_clave" id="ver_clave" value="0">
                        <label class="ver_clave label_clave" for="ver_clave">Ver clave</label>
                    </div>
                </div>
                <div class="respuesta_clave"></div>
                <br>
                <div class="row btn_aceptar">
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Ingresar</button>

                        <?php
                            // $ingreso = new Usuarios();
                            // $ingreso->validar_clave();
                        ?>

                    </div>
                </div>
            </form>

        </div>
    </div>

</div>
