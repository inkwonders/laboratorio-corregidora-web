<!-- Content Header (Page header) -->
<?php

  require_once("../../controladores/citas.controlador.php");
  require_once("../../modelos/citas.modelo.php");

  date_default_timezone_set('America/Mexico_city');

?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">PANEL</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <!-- <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Starter Page</li>
                </ol> -->
            </div>
        </div>
    </div>
</div>
<?php
$consulta_citas = ControladorCitas::ctrConsultaCitas();
$cont_asistidos=0;
$cont_no_asistidos=0;
$cont_restantes=0;
foreach($consulta_citas as $key => $value){
  $consulta_horario = ControladorCitas::ctrConsultaHorario($value["fk_horario"]);
  if($value["status_asistencia"]==1){
    $cont_asistidos++;
  }else if(strtotime($value["fecha_seleccionada"]) < strtotime(date("Y-m-d"))){
      $cont_no_asistidos++;
  }else if(strtotime($value["fecha_seleccionada"]) == strtotime(date("Y-m-d"))){
      if(strtotime($consulta_horario[2])<strtotime(date("H:i:s"))){
        $cont_no_asistidos++;
      }else {
          $cont_restantes++;
      }

  }else {
      $cont_restantes++;
  }

}
$fecha = date("Y-m-d");
$consulta_citas_hoy = ControladorCitas::ctrConsultaCitasHoy($fecha);
$cont_asistidos_hoy=0;
$cont_no_asistidos_hoy=0;
$cont_restantes_hoy=0;
foreach($consulta_citas_hoy as $key => $value){

  $consulta_horarios = ControladorCitas::ctrConsultaHorario($value["fk_horario"]);

  $consulta_horario = ControladorCitas::ctrConsultaPaciente($value["fk_paciente"]);

  if($value["status_asistencia"]==1){
    $cont_asistidos_hoy++;
  }else if(strtotime($value["fecha_seleccionada"]) < strtotime(date("Y-m-d"))){
      $cont_no_asistidos_hoy++;
  }else if(strtotime($value["fecha_seleccionada"]) == strtotime(date("Y-m-d"))){
      if(strtotime($consulta_horarios[2])<strtotime(date("H:i:s"))){
        $cont_no_asistidos_hoy++;
      }else {
          $cont_restantes_hoy++;
      }

  }else {
      $cont_restantes_hoy++;
  }

}
 ?>
 <div class="content">
     <div class="container-fluid">
         <div class="row">

           <div class="row">
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-blue"><i class="fas fa-chart-bar"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Total de citas</span>
                         <span class="info-box-number"><?php echo ControladorCitas::ctrConsultaTotalCitas() ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-green"><i class="far fa-check-square"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Citas asistidas</span>
                         <span class="info-box-number"><?php echo $cont_asistidos ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-red"><i class="far fa-window-close"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Citas no asistidas</span>
                         <span class="info-box-number"><?php echo $cont_no_asistidos ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-yellow"><i class="far fa-plus-square text-white"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Citas restantes</span>
                         <span class="info-box-number"><?php echo $cont_restantes ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-purple"><i class="fas fa-calendar-day"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Total de citas de hoy</span>
                         <span class="info-box-number"><?php echo ControladorCitas::ctrConsultaTotalCitasHoy(date("Y-m-d")) ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-green"><i class="fas fa-calendar-check"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Citas de hoy asistidas</span>
                         <span class="info-box-number"><?php echo $cont_asistidos_hoy ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-red"><i class="fas fa-calendar-times"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Citas de hoy no asistidas</span>
                         <span class="info-box-number"><?php echo $cont_no_asistidos_hoy ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class="info-box">
                       <span class="info-box-icon bg-yellow"><i class="fas fa-calendar-plus text-white"></i></span>

                       <div class="info-box-content">
                         <span class="info-box-text">Citas de hoy restantes</span>
                         <span class="info-box-number"><?php echo $cont_restantes_hoy ?></span>
                       </div>
                       <!-- /.info-box-content -->
                     </div>
                     <!-- /.info-box -->
                   </div>
                   <!-- /.col -->
                 </div>

               </div>
             </div>



     <div class="row" style="height: 150px;">

     </div>
     <div class="row" >
     <div class="col-lg-4 col-xs-4">
           <!-- small box -->
           <div class="small-box bg-blue">
             <div class="inner">
               <h2>Historico</h2>

               <p>Todas las citas registradas</p>
             </div>
             <div class="icon">
               <i class="far fa-chart-bar"></i>
             </div>
             <div  class="small-box-footer menu_admin" op="citas">
               ver <i class="fa fa-arrow-circle-right"></i>
             </div>
           </div>
         </div>
         <div class="col-lg-4 col-xs-4">
               <!-- small box -->
               <div class="small-box bg-purple">
                 <div class="inner">
                   <h2>Citas del dia</h2>

                   <p>Citas registradas el día de hoy</p>
                 </div>
                 <div class="icon">
                   <i class="fas fa-calendar-day"></i>
                 </div>
                 <div  class="small-box-footer menu_admin" op="citas_hoy">
                   ver <i class="fa fa-arrow-circle-right"></i>
                 </div>
               </div>
             </div>

             <div class="col-lg-4 col-xs-4">
                   <!-- small box -->
                   <div class="small-box bg-pink">
                     <div class="inner">
                       <h2>Citas del día de mañana</h2>

                       <p>Citas registradas para el dia de mañana</p>
                     </div>
                     <div class="icon">
                       <i class="fas fa-forward"></i>
                     </div>
                     <div  class="small-box-footer menu_admin" op="citas_manana">
                       ver <i class="fa fa-arrow-circle-right"></i>
                     </div>
                   </div>
                 </div>
             </div>



 </div>
