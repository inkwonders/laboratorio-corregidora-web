<?php

  header("Cache-Control: no-cache, must-revalidate");
  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  

  session_start();

  $ruta_datos = "https://www.laboratoriocorregidora.com.mx/admin/";

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <title>Laboratorio Corregidora | Panel citas</title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $ruta_datos; ?>vistas/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $ruta_datos; ?>vistas/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $ruta_datos; ?>vistas/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $ruta_datos; ?>vistas/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $ruta_datos; ?>vistas/assets/favicon/safari-pinned-tab.svg" color="#cbf202">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="vistas/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo $ruta_datos; ?>vistas/assets/adminlte/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo $ruta_datos; ?>vistas/assets/adminlte/dist/css/adminlte.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo $ruta_datos; ?>vistas/assets/css/estilo.css">

</head>


  <?php

    if(isset($_GET["pagina"])){

      $rutas = explode("/", $_GET["pagina"]);

      $item = "ruta";
      $valor =  $rutas[0];

      if($rutas[0] == "home"){

        if(isset($_SESSION["sesion_iniciada"]) && $_SESSION["sesion_iniciada"] == "ok"){

          echo "<body class='hold-transition sidebar-mini principal'>";

          include "vistas/secciones/home.php";

        }else{

          echo "<body class='hold-transition sidebar-mini login-page principal'>";

          include "vistas/secciones/login.php";

        }

      }else if($rutas[0] == "salir"){

        include "vistas/secciones/salir.php";

      }else{
        echo "<body class='hold-transition sidebar-mini login-page principal'>";

        include "vistas/secciones/login.php";
      }

      // if(!empty($rutas[1] || !empty($rutas[2] || !empty($rutas[3]){
      //   echo "<sript> window.location = 'https://www.laboratoriocorregidora.com.mx/admin/home';</script>";
      // }

    }else{

      echo "<body class='hold-transition sidebar-mini login-page principal'>";

      include "vistas/secciones/login.php";

    }

  ?>

  <script src="vistas/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <script src="vistas/assets/adminlte/plugins/sweetalert2/sweetalert2.all.min.js"></script>
  <script src="vistas/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vistas/assets/adminlte/dist/js/adminlte.min.js"></script>
  <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <script src="vistas/assets/js/script.js" charset="utf-8"></script>

</body>
</html>
