$("input[name='ver_clave']").click(function(){
    if (document.getElementById("ver_clave").checked) {
        $("#clave_acceso").attr("type", "text");
    } else {
        $("#clave_acceso").attr("type", "password");
    }
});

$("#form_ingreso").submit(function(e){
    e.preventDefault();

    console.log("mando formulario");

    let datos = $(this).serialize();

    $.ajax({
        url: "ajax/validacion.ajax.php",
        data: datos,
        type: 'POST',
        beforeSend: function(response){
        },
        success: function(response){

           $(".respuesta_clave").html(response);
        }
    });
});

// $(".menu_admin").click(function(){
$(document).on("click", ".menu_admin", function(){
  let opcion = $(this).attr("op");
  switch(opcion){
    case "panel":
      console.log("panel");
      peticion_vista(opcion);
      break;
    case "citas":
      console.log("citas");
      peticion_vista(opcion);
      break;
      case "citas_hoy":
        console.log("citas_hoy");
        peticion_vista(opcion);
        break;
    case "citas_manana":
	console.log("citas_mñana");
        peticion_vista(opcion);
	break;
    default:
      console.log("default");
  }
});

$(document).on("click", ".btn_status", function(){
  console.log("ejecuto btn_status");
  let sk=$(this).attr("cita");
  let quitar=0;
  let type_pago=$(this).attr("tipo_pago");
  let datos= {
    "sk":sk,
    "quitar": quitar,
    "tipo_pago": type_pago
  }
  $.ajax({
    url:"ajax/status.ajax.php",
    type:"POST",
    data:datos,
    beforeSend: function(){},
    success:function(response){
      console.log("res= "+response);
      if(response=="ok"){
        Swal.fire({
          title: 'EXITO!',
          text: "Cita completada.",
          icon: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          // cancelButtonColor: '#d33',
          confirmButtonText: 'OK'
        }).then((result) => {
          if (!result.isConfirmed) {
            peticion_vista("citas_hoy");
          }
        })
      }else {

      }
    }
  });
});

$(document).on("click", ".btn_status_false", function(){
  console.log("ejecuto btn_status_false");
  let sk=$(this).attr("cita");
  let type_pago=$(this).attr("tipo_pago");
  let quitar=1;
  let datos= {
    "sk":sk,
    "quitar": quitar,
    "tipo_pago": type_pago
  }

  Swal.fire({
    title: '¿Seguro que quieres quitar la asistencia?',
    text: "El status sera de no asistio",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ok'
  }).then((result) => {
    if (!result.isConfirmed) {
      $.ajax({
        url:"ajax/status.ajax.php",
        type:"POST",
        data:datos,
        beforeSend: function(){
        },
        success:function(response){
          console.log("res= "+response);
          if(response=="ok"){
            peticion_vista("citas_hoy");
          }else {
          }
        }
      });
    }else{
      console.log("cancelado");
    }
  })
});

function peticion_vista(ruta){
  let ruta_final = "vistas/secciones/"+ruta+".php";
  $.ajax({
     url: ruta_final,
     type: 'POST',
     beforeSend: function(response){
       $(".contenido_principal").html("<i class='fas fa-atom fa-3x text-blue spinner'></i>");
     },
     success: function(response){
       $(".contenido_principal").html(response);
    }
   });
}

$(document).on("click",".btn_ver_cita",function(){

  let key = $(this).attr("cita");

  let datos = {
    "fk_usuario": key
  }

  $.ajax({
     url: "ajax/modal.ajax.php",
     type: 'POST',
     data: datos,
     beforeSend: function(){
     },
     success: function(response){

       var content = JSON.parse(response);
      //  console.log(response);
       console.log(content);

	     // console.log("sintomas",Object.values(content["40"]));
      //  console.log("enfermedades",Object.values(content["41"]));
      //  console.log("pruebas",Object.values(content["42"]));

       if(Object.values(content["48"])){
          var sintomas=Object.values(content["48"]);
           console.log(sintomas);

          // console.log("entro sintomas");
       }
       
       if(Object.values(content["49"])){
          var enfermedades=Object.values(content["49"]);
           console.log(enfermedades);
       }

       if(Object.values(content["50"])){
          var pruebas=Object.values(content["50"]);
           console.log(pruebas);
       }
      
       let nombre = content["nombre"]+" "+content["apellido_paterno"]+" "+content["apellido_materno"];
       let genero = content["genero"];
       let fecha_nacimiento = content["fecha_nacimiento"];
       let email_paciente = content["paciente_email"];
       let calle = content["calle"];
       let colonia = content["colonia"];
       let no_exterior = content["no_exterior"];
       let no_interior = content["no_interior"];
       let cp = content["cp"];
       let tel_celular = content["tel_celular"];
       let tel_casa = content["tel_casa"];
       let paciente_email = content["paciente_email"];
       let medico_nombre = content["medico_nombre"];
       let medico_email = content["medico_email"];
       let rs = content["razon_social"];
      //  console.log("razon social: "+rs);
       let rfc = content["rfc"];
       let domicilio_fiscal = content["domicilio_fiscal"];
       let otro_sintoma = content["otro_sintoma"];
       let otra_enfermedad = content["otra_enfermedad"];
       let contacto_persona_positivo = content["contacto_persona_positivo"];
       let antiviral = content["antiviral"];
       let influenza = content["influenza"];
       let otro_prueba = content["otro_prueba"];
       let fecha_alta = content["fecha_alta"];
       let no_confirmacion = content["no_confirmacion"];
       let tipo_prueba = content["tipo_prueba"];
       let tipo_pago = content["tipo_pago"];
       console.log(tipo_pago);
        if(tipo_prueba == 1){
           $("#tipo_prueba").text("PCR");
        }else{
           $("#tipo_prueba").text("ANTÍAGENOS");
        }

       $("#no_confirmacion").text(no_confirmacion);
       $("#nombre_paciente").text(nombre);

       if(genero == 1){
         $("#genero").text("HOMBRE");
       }else{
         $("#genero").text("MUJER");
       }

       switch (tipo_pago) {
         case '0':
           $("#tipo_pago").text("SUCURSAL");
         break;
         case '1':
          $("#tipo_pago").text("ONLINE");
          break;
         default:
            $("#tipo_pago").text("NO PAGADO");
         break;
       }

       $("#fecha_nac").text(fecha_nacimiento);
       $("#tel_casa").text(tel_casa);
       $("#tel_cel").text(tel_celular);
       $("#email_paciente").text(email_paciente);
       $("#calle").text(calle);
       $("#colonia").text(colonia);
       $("#cp").text(cp);
       $("#no_ext").text(no_exterior);
       $("#no_int").text(no_interior);
       $("#nombre_medico").text(medico_nombre);
       $("#email_medico").text(medico_email);
       $("#rs").text(rs);
       $("#rfc").text(rfc);
       $("#dom_fiscal").text(domicilio_fiscal);

      let idiomas;
      if(content["es"] == 1){
        idiomas = "Español";
      }else{
        idiomas = "";
      }

      if(content["en"] == 1){
        idiomas = idiomas+" Ingles";
      }else{
        idiomas = idiomas+"";
      }
      console.log("es = "+content["es"]);
      console.log("en = "+content["en"]);
      if(content["es"] === null && content["en"] === null){
        idiomas = "No hubo seleccion, por default es español";
      }
      
       $("#idiomas").text(idiomas);
       let pasaporte = content["pasaporte"];
       $("#pasaporte").text(pasaporte);

       var aux_txt="";
       var cont_aux=0;

       // console.log("sintomas", sintomas);
       // console.log("llego algo", sintomas.length);

       for(var i=0;i<sintomas.length;i++){
         if(cont_aux!=0){
           aux_txt+=", ";
         }
         aux_txt += sintomas[i][0];
         cont_aux++;
         // console.log("for sintomas paso", aux_txt);
       }

       if(cont_aux!=0 && otro_sintoma!=""){
         aux_txt += ", "+otro_sintoma;
       }else {
         aux_txt += otro_sintoma;
       }

       $("#sintomas").text(aux_txt+".");

       /****/

       aux_txt="";
       cont_aux=0;

       for(var i=0;i<enfermedades.length;i++){
         if(cont_aux!=0){
           aux_txt+=", ";
         }
         aux_txt += enfermedades[i][0];
         cont_aux++;
       }

       if(cont_aux!=0 && otra_enfermedad!=""){
         aux_txt += ", "+otra_enfermedad;
       }else {
         aux_txt += otra_enfermedad;
       }

       $("#enfermedades").text(aux_txt+".");

       /****/

       aux_txt="";
       cont_aux=0;

       for(var i=0;i<pruebas.length;i++){
         if(cont_aux!=0){
           aux_txt+=", ";
         }
         aux_txt += pruebas[i][0];
         cont_aux++;
       }

       if(cont_aux!=0 && otro_prueba!=""){
         aux_txt += ", "+otro_prueba;
       }else {
         aux_txt += otro_prueba;
       }

       $("#pruebas").text(aux_txt+".");

       /****/

       
       (antiviral==1)?$("#antiviral").text("SI"):$("#antiviral").text("NO");
       (influenza==1)?$("#influenza").text("SI"):$("#influenza").text("NO");
       (contacto_persona_positivo==1)?$("#contacto_positivo").text("SI"):$("#contacto_positivo").text("NO");
       

    }
   });

});

// function cerrar_c(){
//     $.ajax({
//         url: "cerrar_c.php",
//         type: 'POST',
//         beforeSend: function(response){
//         },
//         success: function(response){
//             // if (response == "ok") {
//             // } else {
//             //     alert(response);
//             // }
//             location.reload();
//         }
//     });
// }
