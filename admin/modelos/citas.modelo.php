<?php

  require_once "conexion.php";

	class MoldeloCitas{

		static public function mdlConsultaCitas($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE NOT (tipo_pago = 1 AND pagado = 0)");
      // $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlStatusCita($tabla,$sk, $quitar, $tipo_pago){

      if($quitar == 0){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET status_asistencia=1, pagado = 1 WHERE sk_cita=:sk");
      }else{
        if($tipo_pago == 1){
          $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET status_asistencia=0 WHERE sk_cita=:sk");
        }else{
          $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET status_asistencia=0, pagado = 0 WHERE sk_cita=:sk");
        }
      }
      
      $stmt->bindParam(":sk", $sk, PDO::PARAM_STR);
      
      if($stmt -> execute()){
        return "ok";
      }else {
        return "error";
      }

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlConsultaCitasHoy($tabla,$fecha){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha_seleccionada=:fecha AND NOT (tipo_pago = 1 AND pagado = 0)");
      $stmt->bindParam(":fecha", $fecha, PDO::PARAM_STR);
      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

		}

    static public function mdlConsultaTotalCitas($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(sk_cita) FROM $tabla ");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlConsultaTotalCitasHoy($tabla,$fecha){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(sk_cita) FROM $tabla  WHERE fecha_seleccionada='".$fecha."'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

		}

    static public function mdlConsultaTotalCitasAsistidas($tabla,$status){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(sk_cita) FROM $tabla WHERE status_asistencia=$status");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlConsultaTotalCitasHoyAsistidas($tabla,$fecha,$status){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(sk_cita) FROM $tabla WHERE fecha_seleccionada='".$fecha."' AND status_asistencia=$status");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

		}

    static public function mdlConsultaHorario($tabla, $sk){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE sk_horario = :sk_horario");

      $stmt->bindParam(":sk_horario", $sk, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

		}

    static public function mdlConsultaPaciente($tabla, $sk){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE sk_paciente = :sk_paciente");

      $stmt->bindParam(":sk_paciente", $sk, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlConsultaPacienteCita($tabla1,$tabla2, $sk){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla1,$tabla2 WHERE sk_paciente = :sk_paciente and fk_paciente=sk_paciente");

      $stmt->bindParam(":sk_paciente", $sk, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlConsultaPacienteSintomas($tabla1,$tabla2,$tabla3, $sk){

      $stmt = Conexion::conectar()->prepare("SELECT s.nombre FROM $tabla1 as p,$tabla2 as sp,$tabla3 as s WHERE sk_paciente = :sk_paciente and fk_paciente=sk_paciente and fk_sintoma=sk_sintoma");

      $stmt->bindParam(":sk_paciente", $sk, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlConsultaPacienteEnfermedades($tabla1,$tabla2,$tabla3, $sk){

      $stmt = Conexion::conectar()->prepare("SELECT e.nombre FROM $tabla1 as p,$tabla2 as ep,$tabla3 as e WHERE sk_paciente = :sk_paciente and sk_paciente=fk_paciente and fk_enferemdad=sk_enfermedades");

      $stmt->bindParam(":sk_paciente", $sk, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

		}
    static public function mdlConsultaPacientePruebas($tabla1,$tabla2,$tabla3, $sk){

      $stmt = Conexion::conectar()->prepare("SELECT pl.nombre FROM $tabla1 as p,$tabla2 as pp,$tabla3 as pl WHERE sk_paciente = :sk_paciente and sk_paciente=fk_paciente and fk_prueba=sk_prueba");

      $stmt->bindParam(":sk_paciente", $sk, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

		}
	}
