<?php

	class ControladorCitas{

		static public function ctrConsultaCitas(){

      $tabla = "citas_f731d7ac";

      $resultado = MoldeloCitas::mdlConsultaCitas($tabla);

      return $resultado;

		}
		static public function ctrConsultaCitasHoy($fecha){

      $tabla = "citas_f731d7ac";

      $resultado = MoldeloCitas::mdlConsultaCitasHoy($tabla,$fecha);

      return $resultado;

		}


		static public function ctrConsultaCitasManana($fecha){

      $tabla = "citas_f731d7ac";

      $resultado = MoldeloCitas::mdlConsultaCitasHoy($tabla,$fecha);

      return $resultado;

		}

		static public function ctrConsultaTotalCitas(){

      $tabla = "citas_f731d7ac";

      $resultado = MoldeloCitas::mdlConsultaTotalCitas($tabla);

      return $resultado[0];

		}
		static public function ctrConsultaTotalCitasHoy($fecha){

      $tabla = "citas_f731d7ac";

      $resultado = MoldeloCitas::mdlConsultaTotalCitasHoy($tabla,$fecha);

      return $resultado[0];

		}
		static public function ctrConsultaTotalCitasHoyAsistidas($fecha,$status){

      $tabla = "citas_f731d7ac";

      $resultado = MoldeloCitas::mdlConsultaTotalCitasHoyAsistidas($tabla,$fecha,$status);

      return $resultado[0];

		}

		static public function ctrConsultaTotalCitasAsistidas($status){

      $tabla = "citas_f731d7ac";

      $resultado = MoldeloCitas::mdlConsultaTotalCitasAsistidas($tabla,$status);

      return $resultado[0];

		}

    static public function ctrConsultaHorario($sk){

      $tabla = "horarios_citas_90e5a804";

      $resultado = MoldeloCitas::mdlConsultaHorario($tabla, $sk);

      return $resultado;

		}

    static public function ctrConsultaPaciente($sk){

      $tabla = "pacientes_b7db6bae";

      $resultado = MoldeloCitas::mdlConsultaPaciente($tabla, $sk);

      return $resultado;

		}
		static public function ctrConsultaPacienteCita($sk){

      $tabla1 = "pacientes_b7db6bae";
			$tabla2 = "citas_f731d7ac";
      $resultado = MoldeloCitas::mdlConsultaPacienteCita($tabla1,$tabla2, $sk);

      return $resultado;

		}
		static public function ctrConsultaPacienteSintomas($sk){

      $tabla1 = "pacientes_b7db6bae";
			$tabla2 = "sintomas_paciente_f30a2728";
			$tabla3 = "sintomas_listado_974ac312";
      $resultado = MoldeloCitas::mdlConsultaPacienteSintomas($tabla1,$tabla2,$tabla3, $sk);

      return $resultado;

		}
		static public function ctrConsultaPacienteEnfermedades($sk){

      $tabla1 = "pacientes_b7db6bae";
			$tabla2 = "enfermedades_paciente_0276a999";
			$tabla3 = "enfermedades_listado_8490db68";
      $resultado = MoldeloCitas::mdlConsultaPacienteEnfermedades($tabla1,$tabla2,$tabla3, $sk);

      return $resultado;

		}
		static public function ctrConsultaPacientePruebas($sk){

      $tabla1 = "pacientes_b7db6bae";
			$tabla2 = "pruebas_paciente_75d90786";
			$tabla3 = "pruebas_listado_48feb2bb";
      $resultado = MoldeloCitas::mdlConsultaPacientePruebas($tabla1,$tabla2,$tabla3, $sk);

      return $resultado;

		}

	}
