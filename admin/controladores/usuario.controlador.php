<?php

    class Usuarios{

        static public function validar_clave($clave) {

            if(crypt($clave, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$') === '$2a$07$asxx54ahjppf45sd87a5auI8BP8uQjtg/mKKcxdQWfuI7i7dABuj.' || crypt($clave, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$') === '$2a$07$asxx54ahjppf45sd87a5auPRGVYd8qJRVwRtx07Ln4hi1f2Qk.PM.' || crypt($clave, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$') === '$2a$07$asxx54ahjppf45sd87a5auOqYugqEH3YxXft7R5zLU2Wvy0ZW7CZq') {

                session_start();

                $_SESSION["sesion_iniciada"] = "ok";

                return "<script> window.location = 'home'; </script>";

            }else{

                return "Contraseña incorrecta";

            }

        }

        static public function statusCita($sk,$quitar,$tipo_pago) {

          $tabla = "citas_f731d7ac";

          $resultado = MoldeloCitas::mdlStatusCita($tabla, $sk, $quitar, $tipo_pago);

          return $resultado;

        }

    }
