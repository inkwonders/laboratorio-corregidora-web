<?php

  date_default_timezone_set('America/Mexico_city');

  // error_reporting(E_ALL);
  // ini_set('display_errors', '1');

  require_once "controladores/plantilla.controlador.php";
  require_once "controladores/citas.controlador.php";
  require_once "controladores/usuario.controlador.php";

  require_once "modelos/citas.modelo.php";

	$plantilla = new ControladorPlantilla();
	$plantilla -> plantilla();
