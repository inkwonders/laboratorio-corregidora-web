import "https://maps.googleapis.com/maps/api/js?key=AIzaSyDtmAZBLkqZEWcMd6tZPElNrxIhT4k4tMI&libraries=places";

const f = new Date();
const fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();

const styles = [
	{ "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }] },
	{ "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] },
	{ "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
	{ "elementType": "labels.text.stroke", "stylers": [{ "color": "#f5f5f5" }] },
	{ "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#bdbdbd" }] },
	{ "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
	{ "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
	{ "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
	{ "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
	{ "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }] },
	{ "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
	{ "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#dadada" }] },
	{ "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
	{ "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
	{ "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
	{ "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
	{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#c9c9c9" }] },
	{ "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] }
];

const markers = [
    { id: 1, title: 'Laboratorio Corregidora El Marqués Matriz', name: 'Laboratorio Corregidora El Marqués Matriz', lat: 20.590621, lng: -100.330477, icon: "icon_covid", address: "Avenida Prolongación Constituyentes No. 32, Oriente, Colonia Villas del Marqués del Águila C.P. 76240, Altos del Marques, El Marqués, Querétaro.", schedule: "Lunes a Viernes de 7:00 hrs. a 19:30 hrs.<br/>Sábados de 7:00 hrs. a 13:30 hrs." },
    { id: 2, title: 'Laboratorio Corregidora Sucursal Centro Histórico', name: 'Laboratorio Corregidora Sucursal Centro Histórico', lat: 20.595638, lng: -100.392729, icon: "icon", address: "Corregidora 75-101 Col. Centro, Querétaro, Qro.", schedule: "Lunes a Viernes de 7:00 hrs. a 20:00 hrs.<br/>Sábados de 7:00 hrs. a 13:30 hrs." },
    { id: 3, title: 'Laboratorio Corregidora Sucursal Jurica', name: 'Laboratorio Corregidora Sucursal Jurica', lat: 20.645469, lng: -100.431589, icon: "icon", address: "Av. 5 de Febrero No. 2125 Local 9-10 Planta Baja, Condominio Plaza Norte, Zona Industrial Benito Juárez", schedule: "Lunes a sábado de 7:00 hrs. a 11:00 hrs." },
    { id: 4, title: 'Laboratorio Corregidora Sucursal Juriquilla', name: 'Laboratorio Corregidora Sucursal Juriquilla', lat: 20.707342, lng: -100.447255, icon: "icon", address: "Boulevard Universitario 560, Centro Comercial Juriquilla, Col. Jurica Acueducto", schedule: "Lunes a sábado de 7:00 hrs. a 11:30 hrs." },
    { id: 5, title: 'Laboratorio Corregidora Sucursal Tecnológico', name: 'Laboratorio Corregidora Sucursal Tecnológico', lat: 20.588998, lng: -100.404089, icon: "icon", address: "Av. Tecnológico Sur No. 2, local 103 planta baja, Col. Niños Héroes.", schedule: "Lunes a Sábado de 7:00 hrs. a 10:30 hrs." },
    { id: 6, title: 'Laboratorio Corregidora Sucursal Pueblo Nuevo', name: 'Laboratorio Corregidora Sucursal Pueblo Nuevo', lat: 20.546885, lng: -100.428507, icon: "icon", address: "Av. Rufino Tamayo No. 37 Planta, baja interior 1 Col. Pueblo Nuevo, Corregidora, Qro.", schedule: "Lunes a sábado de 7:00 hrs. a 11:00 hrs." },
    { id: 7, title: 'Laboratorio Corregidora Sucursal Milenio', name: 'Laboratorio Corregidora Sucursal Milenio', lat: 20.594070, lng: -100.349487, icon: "icon", address: "Camino Real de Carretas 416, local 105 Plaza Ubika, Fracc. Milenio III", schedule: "Lunes a Sábado de 7:00 hrs. a 10:30 hrs." },
    { id: 8, title: 'Laboratorio Corregidora Sucursal El Refugio', name: 'Laboratorio Corregidora Sucursal Milenio', lat: 20.6421003, lng: -100.3461815, icon: "icon", address: "Anillo Fray Junípero Serra, Plaza Ubika El Refugio, Local. 124, El Refugio, C.P. 76148, Querétaro, Qro.", schedule: "Lunes a Sábado de 7:00 hrs. a 11:00 hrs." },
];

const center = {
	lat: 20.609427580021816, lng: -100.39599799324692
};

var map = null;
var pre_zoom = false;
var selected_location = {
	lat: 20.609427580021816, lng: -100.39599799324692
};

const mapNavItem = ({ id, title, address, schedule }) => (`
	<div>
		<button onclick="mapItem(${id})" class="flex items-start gap-2">
			<div id="button_item_opened_${id}" class="opened_button_item mt-2 w-3 min-w-[12px] h-3" style="display: none;">
				<img src="/modules/img/svg/opened.svg" class="object-contain object-center w-full h-full">
			</div>
			<div id="button_item_closed_${id}" class="closed_button_item mt-2 w-3 min-w-[12px] h-3">
				<img src="/modules/img/svg/closed.svg" class="object-contain object-center w-full h-full">
			</div>
			<span class="text-base text-left lg:text-xl font-rob-bold">${title}</span>
		</button>
		<div id="content_item_${id}" class="flex content_item flex-col w-full gap-4 pt-1.5 pb-6 pl-5" style="display: none;">
			<p>${address}</p>
			<span class="flex flex-col">
				<span class="inline-flex items-center gap-1">
					<img src="/modules/img/svg/clock.svg" alt="clock" class="object-contain object-center w-4 h-4">
					<span class="font-rob-bold">Horario:</span>
				</span>
				<span>${schedule}</span>
			</span>
		</div>
	</div>
`)

function mapAllItem() {
	const opened_button_items = document.getElementsByClassName("opened_button_item")
	const closed_button_items = document.getElementsByClassName("closed_button_item")
	const content_items = document.getElementsByClassName("content_item")


	for (const element of opened_button_items) {
		element.style.display = "none"
	}

	for (const element of closed_button_items) {
		element.style.display = ""
	}

	for (const element of content_items) {
		element.style.display = "none"
	}

	map.setOptions({
        zoom: 11,
        center: center
    })

	selected_location = center

	pre_zoom = false

	$('html, body').animate({
		scrollTop: ($("#map").offset().top - 100)
	}, 300);
}

var mapItem = (selected) => {
	const [ location ] = markers.filter(({ id }) => {
		return ( id == selected )
	})

	const opened_button_items = document.getElementsByClassName("opened_button_item")
	const closed_button_items = document.getElementsByClassName("closed_button_item")
	const content_items = document.getElementsByClassName("content_item")


	for (const element of opened_button_items) {
		element.style.display = "none"
	}

	for (const element of closed_button_items) {
		element.style.display = ""
	}

	for (const element of content_items) {
		element.style.display = "none"
	}

	document.getElementById(`button_item_opened_${selected}`).style.display = ""
	document.getElementById(`button_item_closed_${selected}`).style.display = "none"
	document.getElementById(`content_item_${selected}`).style.display = ""


	if (selected_location.lat != location.lat && selected_location.lng != location.lng) {
		if (pre_zoom) {
			map.setOptions({
				zoom: 13,
				center: center
			})
	
			setTimeout(() => {
				map.setOptions({
					zoom: 15,
					center: location
				})
			}, 600)
		} else {
			map.setOptions({
				zoom: 15,
				center: location
			})
	
			pre_zoom = true
		}

		selected_location = location

		$('html, body').animate({
			scrollTop: ($("#map").offset().top - 100)
		}, 300);
	} else {
		mapAllItem()
	}
}

$(document).ready(function () {
	window.myFunction = myFunction
	window.mapItem = mapItem
	window.mapAllItem = mapAllItem

	$(document).on("click", ".menu-el", function () {
		let opcion = $(this).attr("op");
		let ruta = filename();

		if (screen.width < 980) {
			if (ruta == "resultados.php" || ruta == "guardias.php") {
				switch (opcion) {
					case "serv-mov":
						window.location = "/#serv-mov";
						break;
					case "preg-mov":
						window.location = "/#preg-mov";
						break;
					case "vid-mov":
						window.location = "/#vid-mov";
						break;
					default:
						break;
				}
			} else {
				$('html, body').animate({
					scrollTop: $("#" + opcion).offset().top
				}, 600);
			}
		} else {
			console.log("opcion:", $("#" + opcion));

			$('html, body').animate({
				scrollTop: ($("#" + opcion).offset().top - 100)
			}, 600);
		}
	});

	$(document).on("click", ".inter-res, .btn-res, #btn-resl-mov", function () {
		window.location = "resultados.php";
	});

	$(document).on("click", ".video-button", function () {
		let opcion = parseInt($(this).attr("op"));

		if (opcion == 0) {
			return;
		}

		$(".modal").removeClass("no-visible");

		switch (opcion) {
			case 1:
				$(".modal iframe").attr("src", "https://www.youtube.com/embed/12xzMAJT-to");
				break;
			case 2:
				$(".modal iframe").attr("src", "https://www.youtube.com/embed/kj8ZlqsA7Tw");
				break;
			default:
				break;
		}
	});

	$(document).on("click", ".close-modal", function () {
		$(".modal").addClass("no-visible");
		$(".modal iframe").attr("src", "");
	});

	initMap();
});

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: center,
        styles: styles
    });

    markers.map(location => {
		document.getElementById("dropdowns-list").innerHTML += mapNavItem(location)
        new google.maps.Marker({
            position: location,
            map: map,
            icon: {
                url: `/img/${location.icon}.svg`,
                scaledSize: new google.maps.Size(40, 40),
            },
            title: location.title,
        });
    });
}

function filename() {
	var rutaAbsoluta = self.location.href;
	var posicionUltimaBarra = rutaAbsoluta.lastIndexOf("/");
	var rutaRelativa = rutaAbsoluta.substring(posicionUltimaBarra + "/".length, rutaAbsoluta.length);
	return rutaRelativa;
}

function myFunction(x) {
	x.classList.toggle("change");
	if (document.getElementById("contenidoMenu").style.display == "none") {
		document.getElementById("contenidoMenu").style.display = "";
		$("#contenidoMenu").animate({
			height: '100vh',
			top: '0px'
		}, "slow");
	} else {
		$("#contenidoMenu").animate({
			height: '0'
		}, "slow", function () {
			document.getElementById("contenidoMenu").style.display = "none";
		});
	}
}

function closemenumov() {
	let menu = document.getElementById("container");
	menu.classList.toggle("change");

	if (document.getElementById("contenidoMenu").style.display == "none") {
		document.getElementById("contenidoMenu").style.display = "";
		$("#contenidoMenu").animate({
			height: '100vh'
		}, "slow");
	} else {
		$("#contenidoMenu").animate({
			height: '0'
		}, "slow", function () {
			document.getElementById("contenidoMenu").style.display = "none";
		});
	}
}

let preguntas = $("#preguntas").offset().top;
let sucursales = $("#sucursales").offset().top;

window.onscroll = function () {
	let scroll_pos = $(window).scrollTop();

	if (screen.width > 980) {
		if (scroll_pos >= (preguntas - 100)) {
			$('.menu-el').removeClass("menu_active");
			$('.menu-preg').addClass("menu_active");
		}

		if (scroll_pos >= (sucursales - 100)) {
			$('.menu-el').removeClass("menu_active");
			$('.menu-suc').addClass("menu_active");
		}
	} else { }
}
