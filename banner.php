<?php
session_start();
error_reporting(0);

?>

<!doctype html>
<html lang="es">

<head>
<!-- Google tag (gtag.js) --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8R2HZPPJFM"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-8R2HZPPJFM'); </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, maximum-scale=5" />
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <title>LABORATORIO CORREGIDORA</title>

    <meta name="description" content="Laboratorio de Análisis clínicos en Querétaro">
    <meta name="robots" content="all" />
    <meta name="laboratorio corregidora" content="laboratoriocorregidora.com.mx" />
    <meta name="distribution" content="global" />
    <meta name="keywords" content="Mejor laboratorio en Querétaro
Laboratorios en Querétaro
Laboratorios Corregidora
Corregidora
Juriquilla Laboratorio
Milenio Juriquilla
Pueblo Nuevo Laboratorio
Jurica Laboratorio
Tecnológico Laboratorio
Mejor laboratorio de Querétaro
Laboratorio de análisis clínicos Querétaro
Resultados laboratorio corregidora
Laboratorio acreditado Querétaro
Mejor laboratorio de Querétaro
Análisis Laboratorio Corregidora
Costos Laboratorio Corregidora
Laboratorios Corregidora
Laboratorios Querétaro certificado
Laboratorios Querétaro acreditado
Laboratorio especialista en toma de muestras difíciles
Laboratorio Querétaro alta tecnología
Laboratorio con amplio menú de pruebas
Laboratorio con más años de experiencia
Laboratorio confiable
Resultados exactos y precisos laboratorio
Laboratorios Vázquez Mellado
Toma de muestras a domicilio laboratorio
Laboratorio con servicio fines de semana
Laboratorio con personal competente
Laboratorio con servicio los sábados y domingos
Sucursales laboratorio corregidora
Costos análisis laboratorio corregidora
Laboratorios en Querétaro ISO
Laboratorio corregidora sucursales
Laboratorio con costos accesibles en Querétaro
Laboratorio con costos económicos en Querétaro
Laboratorio con costos bajos en Querétaro
Laboratorio entrega rápido resultados
Laboratorio resultados confiables
Laboratorio entrega resultados el mismo día
Laboratorio con mayor prestigio en Querétaro
Laboratorio más recomendado en Querétaro
Laboratorio con el mejor control de calidad
Laboratorio de análisis clínicos con mayor calidad
Laboratorio de calidad
Laboratorio con excelencia en los resultados
Laboratorio con resultados confiables
Laboratorio personal especialista en toma de muestra pediátrica
Análisis Clínicos
Análisis de sangre
Análisis de orina
Análisis generales
Análisis para infección
Análisis de laboratorio
Análisis  de VIH
Análisis de prueba de embarazo
Menú de análisis de laboratorio
Menú de análisis clínicos
Análisis clínicos
Análisis check up" />

    <!-- <link rel="stylesheet" href="estilo.css" /> -->
    <!-- <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"> -->

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/site.webmanifest">
    <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">
    <!-- favicon -->

    <!-- fuentes -->
    <link rel="preload" href="/fonts/Montserrat-Black.ttf" as="font" type="font/ttf" crossorigin>
    <link rel="preload" href="/fonts/Montserrat-Bold.ttf" as="font" type="font/ttf" crossorigin>
    <link rel="preload" href="/fonts/Montserrat-ExtraBold.ttf" as="font" type="font/ttf" crossorigin>
    <link rel="preload" href="/fonts/Montserrat-Medium.ttf" as="font" type="font/ttf" crossorigin>
    <link rel="preload" href="/fonts/Montserrat-Regular.ttf" as="font" type="font/ttf" crossorigin>
    <link rel="preload" href="/fonts/Montserrat-SemiBold.ttf" as="font" type="font/ttf" crossorigin>
    <!-- end fuentes -->


    <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
    <!-- Global site tag (gtag.js) - Google Ads: 691396176 -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691396176"></script> -->
    <!-- <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-691396176');
    </script> -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152013601-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-152013601-2');
    </script> -->
    <!-- end Global site tag (gtag.js) - Google Analytics -->
    <style>
        .background-banner {
            background-image: url('img/svg/fondo_banner.svg');
            background-size: cover;
            background-position: center;
        }


        .textos_banners {
            position: absolute;
            top: 59%;
            left: 28%;
            transform: translate(-50%, -50%);
        }

        .btn_clic_banner {
            background-color: #0a1e5b;
            color: #fff;
            padding: 20px 50px;
            border-radius: 20px;
            border: 3px solid #0a1e5b;
        }

        .btn_clic_banner:hover {
            cursor: pointer;
            background-color: #fff;
            color: #0a1e5b;
            transition: all .2s;
        }

        .azul_banner {
            color: #0a1e5b
        }
    </style>
</head>

<body>


    <section id="banner">

        <div class="relative w-full w-screen h-screen background-banner">
            <div class="text-4xl textos_banners">
                <a>
                    <span class="btn_clic_banner">
                        HAZ CLIC AQUÍ
                    </span>
                </a>
            </div>
        </div>

    </section>


    <script type="text/javascript" src="js/jquery.js"></script>
    <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6HJOhs7FrQM6xTGa6K6PvHAfSLu9Ky9M&callback=initMap"></script> -->

    <script src="js/script.js"></script>

</body>

</html>