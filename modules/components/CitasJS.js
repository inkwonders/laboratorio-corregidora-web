import { DatePicker } from 'v-calendar';

import "v-calendar/style.css";
import "../css/custom-calendar.css";




export default function CitasJS() {
    return {
        components: {
            DatePicker,
        },
        data() {
            return {
                months: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
                days: [ 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo' ],
                states: [],
                municipalities: [],
                taxes: [],
                cfdi_uses: [],
                symptoms_list: [],
                diseases_list: [],
                reasons_list: [],
                languages_list: [],

                patient_name: "",
                patient_father_last_name: "",
                patient_mother_last_name: "",
                gender: "female",
                birthdate: null,
                street: "",
                colony: "",
                no_ext: "",
                no_int: "",
                state: "",
                municipality: "",
                zip_code: "",
                passport: "",
                cellphone: "",
                phone: "",
                email: "",
                med_name: "",
                med_email: "",
                business_name: "",
                rfc: "",
                tax_residence: "",
                tax_regime: "",
                cfdi: "",
                symptoms: [],
                other_symptoms: "",
                chronic_disease: [],
                other_disease: "",
                question_a: false,
                question_b: false,
                question_c: false,
                test_reasons: [],
                other_reasons: "",
                languages: [ 1 ],

                payment_url: "",
                confirmation_number: "",
                show_birthdate: false,
                sections: 1,
                section: 0,
                type: null,
                selected_date: null,
                selected_time: null,
                confirmation: false,
                confirmation_patient: false,
                payment_confirmation: false,
                payment_confirmation_modal: false,
                accepted: false,
                early_schedules: [],
                late_schedules: [],
                msg_schedules: "",
                patients: [],
                get_headers: {
                    headers: {
                        "Accept": "application/json",
                    }
                }
            }
        },
        methods: {
            nextNav() {
                if (this.section < this.sections) {
                    this.section++
                }
                setTimeout(() => {
                    window.scrollTo({ top: 0, behavior: 'smooth' })
                }, 100)
            },
            backNav() {
                if (this.section > 1) {
                    this.section--
                }
                setTimeout(() => {
                    window.scrollTo({ top: 0, behavior: 'smooth' })
                }, 100)
            },
            select(type) {
                this.type = type
                this.selected_date = null
                this.nextNav()
            },
            getFormattedDate(date, separator = '-') {
                const year = date.getFullYear()
                const month = (date.getMonth() +1)
                const day = date.getDate()
                return [
                    year,
                    (month > 9? month : ('0' + month)),
                    (day > 9? day : ('0' + day))
                ].join(separator)
            },
            textDate(date) {
                const year = date.getFullYear()
                const month = date.getMonth()
                const day = date.getDate()
                const week_day = date.getDay()
                return [
                    this.days[week_day],
                    day,
                    'de',
                    this.months[month] + ',',
                    year
                ].join(' ')
            },
            formatedTimes({ start_time, end_time, available }) {
                const start         = new Date(start_time);
                const end           = new Date(end_time);
                // const current_date  = new Date();

                const hour_a    = (start.getHours() % 12) > 0? (start.getHours() % 12) : 12;
                const minute_a  = start.getMinutes() > 10? start.getMinutes() : ('0' + start.getMinutes());
                const hour_b    = (end.getHours() % 12) > 0? (end.getHours() % 12) : 12;
                const minute_b  = end.getMinutes() > 10? end.getMinutes() : ('0' + end.getMinutes());

                return {
                    is_available:   available, //current_date.getTime() < start.getTime(),
                    date:           this.getFormattedDate(start),
                    start_time:     `${hour_a}:${minute_a} ${start.getHours() < 12? 'am' : 'pm'}`,
                    start_datetime: start_time,
                    end_time:       `${hour_b}:${minute_b} ${end.getHours() < 12? 'am' : 'pm'}`,
                    end_datetime:   end_time,
                }
            },
            formatedPatientName(patient) {
                if (this.states.length) {
                    const [{ name }] = this.states.filter(({ id }) => id == patient.state_id)
                    return `${patient.patient_name} ${patient.patient_father_last_name}${patient.patient_mother_last_name? (' '+patient.patient_mother_last_name) : ''}, ${patient.municipality_name} ${name}`
                } else {
                    return `${patient.patient_name} ${patient.patient_father_last_name}${patient.patient_mother_last_name? (' '+patient.patient_mother_last_name) : ''}`
                }
            },
            deletePatient(id) {
                this.patients = this.patients.filter(
                    ({ index }) => (index != id)
                )
            },
            addingSchedules(date) {
                if (date) {
                    const selected = this.getFormattedDate(date)

                    fetch(`/citas/ajax/horario.ajax.php`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                        },
                        body: JSON.stringify({
                            fecha: selected
                        })
                    })
                    .then(this.returnJson)
                    .then((data) => {
                        if (data.success) {
                            this.early_schedules = data.early_schedules.map(this.formatedTimes)
                            this.late_schedules = data.late_schedules.map(this.formatedTimes)
                            this.msg_schedules = ""
                        } else {
                            console.log("bad:", JSON.stringify(data, null, 4));
                            this.early_schedules = []
                            this.late_schedules = []
                            this.msg_schedules = data.msg
                        }
                    })
                    .catch(this.cached)
                } else {
                    this.early_schedules = []
                    this.late_schedules = []
                }
            },
            onlyNumber(evt) {
                if(evt.charCode < 48 || evt.charCode > 57) {
                    evt.preventDefault()
                }
            },
            savePatient(evt) {
                evt.preventDefault();
                const [{ name }] = this.municipalities.filter(({ id }) => id == this.municipality)

                this.patients.push({
                    index:                      (this.patients.length +1),
                    patient_name:               this.patient_name,
                    patient_father_last_name:   this.patient_father_last_name,
                    patient_mother_last_name:   this.patient_mother_last_name,
                    gender:                     this.gender,
                    birthdate:                  this.getFormattedDate(this.birthdate),
                    street:                     this.street,
                    colony:                     this.colony,
                    no_ext:                     this.no_ext,
                    no_int:                     this.no_int,
                    state_id:                   this.state,
                    municipality_id:            this.municipality,
                    municipality_name:          name,
                    zip_code:                   this.zip_code,
                    passport:                   this.passport,
                    cellphone:                  this.cellphone,
                    phone:                      this.phone,
                    email:                      this.email,
                    med_name:                   this.med_name,
                    med_email:                  this.med_email,
                    business_name:              this.business_name,
                    rfc:                        this.rfc,
                    tax_residence:              this.tax_residence,
                    tax_id:                     this.tax_regime,
                    cfdi_use_id:                this.cfdi,
                    symptoms:                   this.symptoms,
                    other_symptoms:             this.other_symptoms,
                    chronic_diseases:           this.chronic_disease,
                    other_disease:              this.other_disease,
                    question_a:                 this.question_a,
                    question_b:                 this.question_b,
                    question_c:                 this.question_c,
                    test_reasons:               this.test_reasons,
                    other_reasons:              this.other_reasons,
                    languages:                  this.languages,
                });

                this.confirmation_patient = false;
                this.patient_name = ""
                this.patient_father_last_name = ""
                this.patient_mother_last_name = ""
                this.gender = "female"
                this.birthdate = null
                this.street = ""
                this.colony = ""
                this.no_ext = ""
                this.no_int = ""
                this.state = ""
                this.municipality = ""
                this.zip_code = ""
                this.passport = ""
                this.cellphone = ""
                this.phone = ""
                this.email = ""
                this.med_name = ""
                this.med_email = ""
                this.business_name = ""
                this.rfc = ""
                this.tax_residence = ""
                this.tax_regime = ""
                this.cfdi = ""
                this.symptoms = []
                this.other_symptoms = ""
                this.chronic_disease = []
                this.other_disease = ""
                this.question_a = false
                this.question_b = false
                this.question_c = false
                this.test_reasons = []
                this.other_reasons = ""
                this.languages = [ 1 ]
            },
            returnJson(response) {
                try {
                    return response.json()
                } catch (error) {
                    console.log("Catched:", error)
                    console.log("Error:", response.statusText)

                    return { success: response.ok }
                }
            },
            cached(th) {
                console.log("Error:", th)
            },
            getMunicipalities() {
                if (this.state) {
                    fetch(`/citas/ajax/municipalities.php`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                        },
                        body: JSON.stringify({
                            state: this.state
                        })
                    })
                    .then(this.returnJson)
                    .then(data => {
                        this.municipalities = Object.values(data)
                    })
                    .catch(this.cached)
                } else {
                    this.municipalities = []
                }
            },
            getComponentData() {
                fetch('/citas/ajax/states.php', this.get_headers)
                .then(this.returnJson)
                .then(data => {
                    this.states = Object.values(data)
                })
                .catch(this.cached)

                fetch(`/citas/ajax/taxes.php`, this.get_headers)
                .then(this.returnJson)
                .then(data => {
                    this.taxes = Object.values(data)
                })
                .catch(this.cached)

                fetch(`/citas/ajax/cfdi_uses.php`, this.get_headers)
                .then(this.returnJson)
                .then(data => {
                    this.cfdi_uses = Object.values(data)
                })
                .catch(this.cached)

                fetch(`/citas/ajax/symptoms.php`, this.get_headers)
                .then(this.returnJson)
                .then(data => {
                    this.symptoms_list = Object.values(data)
                })
                .catch(this.cached)

                fetch(`/citas/ajax/diseases.php`, this.get_headers)
                .then(this.returnJson)
                .then(data => {
                    this.diseases_list = Object.values(data)
                })
                .catch(this.cached)

                fetch(`/citas/ajax/test_reasons.php`, this.get_headers)
                .then(this.returnJson)
                .then(data => {
                    this.reasons_list = Object.values(data)
                })
                .catch(this.cached)

                fetch(`/citas/ajax/languages.php`, this.get_headers)
                .then(this.returnJson)
                .then(data => {
                    this.languages_list = Object.values(data)
                })
                .catch(this.cached)
            },
            getUrlError() {
                alert("Error")
            },
            getUrl() {
                if (this.payment_confirmation_modal) {
                    // console.log(JSON.stringify({
                    //     test: this.type,
                    //     time: this.selected_time,
                    //     patients: this.patients.map(({ patient_name }) => patient_name)
                    // }, null, 4));

                    fetch(`/citas/get_payment_url/index.php`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                        },
                        body: JSON.stringify({
                            test: this.type,
                            time: this.selected_time,
                            patients: this.patients
                        })
                    })
                    .then(this.returnJson)
                    .then(({ success, url, confirmation_number }) => {
                    // .then((data) => {
                        // console.log("data:", data)
                        if (success) {
                            this.payment_url = url
                            this.confirmation_number = confirmation_number
                        } else {
                            this.getUrlError()
                        }
                    })
                    .catch(this.cached)




                    // this.payment_url = "https://banregio.mitec.com.mx/p/i/6WQM829K"
                    // this.confirmation_number = "csb6-irj1"
                }
            },
            checkPayment(evt) {
                fetch('/api/appointment-status', {
                    method: 'POST',
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        confirmation_number: this.confirmation_number
                    })
                })
                .then(this.returnJson)
                .then(({ success, confirmation_number, payment_status }) => {
                    if (success) {
                        console.log(`check[${confirmation_number}]:`, payment_status);
                    } else {
                        this.getUrlError()
                    }
                })
                .catch(this.cached)
            },
            iframeLoad() {
                this.checkPayment()
            }
        },
        watch: {
            payment_confirmation_modal: 'getUrl',
            selected_date: 'addingSchedules',
            state: 'getMunicipalities'
        },
        mounted() {
            this.getComponentData();

            this.sections = 4;
            // this.section = 4;
            this.section = 1;




            // // Pruebas:
            // this.payment_confirmation = true;
            // // this.type = "antigenos"
            // this.type = "pcr"
            // this.selected_time = {
            //     "is_available": true,
            //     "date": "2023-12-21",
            //     "start_time": "7:30 am",
            //     "start_datetime": "2023-12-21 07:30:00",
            //     "end_time": "8:30 am",
            //     "end_datetime": "2023-12-21 08:30:00"
            // }
            this.patients = [
                {
                    "index": 1,
                    "patient_name": "Prueba",
                    "patient_father_last_name": "Aleatorio",
                    "patient_mother_last_name": "Ink",
                    "gender": "male",
                    "birthdate": "1995-01-25",
                    "street": "Centro sur",
                    "colony": "Colinas del Cimatario",
                    "no_ext": "342",
                    "no_int": "123",
                    "state_id": 22,
                    "municipality_id": 1799,
                    "municipality_name": "Querétaro",
                    "zip_code": "",
                    "passport": "",
                    "cellphone": "1234567890",
                    "phone": "",
                    "email": "mermin@protonmail.ch",
                    "med_name": "",
                    "med_email": "",
                    "business_name": "",
                    "rfc": "",
                    "tax_residence": "",
                    "tax_id": "",
                    "cfdi_use_id": "",
                    "symptoms": [
                        5,
                        6
                    ],
                    "other_symptoms": "",
                    "chronic_diseases": [
                        3,
                        4
                    ],
                    "other_disease": "",
                    "question_a": false,
                    "question_b": true,
                    "question_c": false,
                    "test_reasons": [
                        3,
                        4
                    ],
                    "other_reasons": "",
                    "languages": [
                        1
                    ]
                },
                { "index": 2, "patient_name": "Prueba2", "patient_father_last_name": "Aleatorio2", "patient_mother_last_name": "Ink2", "gender": "female", "birthdate": "1995-01-25", "street": "Centro sur", "colony": "Colinas del Cimatario", "no_ext": "123", "no_int": "123", "state_id": 22, "municipality_id": 1799, "municipality_name": "Querétaro", "zip_code": "12345", "passport": "1234567890", "cellphone": "1234567890", "phone": "1234567890", "email": "usuario2@ink.com", "med_name": "Médico Prueba", "med_email": "medico@prueba.com", "business_name": "Pruebas Ink", "rfc": "RARG2525253AA", "tax_residence": "Centro Sur", "tax_id": 1, "cfdi_use_id": 3, "symptoms": [ 1, 4, 8, 11, 15, 18 ], "other_symptoms": "Prueba Síntoma", "chronic_diseases": [ 1, 4, 8 ], "other_disease": "Prueba Enfermedad", "question_a": true, "question_b": false, "question_c": true, "test_reasons": [ 1, 5 ], "other_reasons": "Prueba Razón", "languages": [ 1, 2 ] },
            ];
        }
    }
}
