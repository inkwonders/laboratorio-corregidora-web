// const gApi = (g => {var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=`https://maps.${c}apis.com/maps/api/js?`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
//     key: "AIzaSyDtmAZBLkqZEWcMd6tZPElNrxIhT4k4tMI",
//     v: "alpha",
//     // Use the 'v' parameter to indicate the version to use (weekly, beta, alpha, etc.).
//     // Add other bootstrap parameters as needed, using camel case.
// });

// gApi();

// let map = null;

// const styles = [
// 	{ "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }] },
// 	{ "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] },
// 	{ "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
// 	{ "elementType": "labels.text.stroke", "stylers": [{ "color": "#f5f5f5" }] },
// 	{ "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#bdbdbd" }] },
// 	{ "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
// 	{ "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
// 	{ "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
// 	{ "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
// 	{ "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }] },
// 	{ "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
// 	{ "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#dadada" }] },
// 	{ "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
// 	{ "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
// 	{ "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
// 	{ "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
// 	{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#c9c9c9" }] },
// 	{ "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] }
// ];

// const markers = [
//     { id: 1, title: 'Laboratorio Corregidora El Marqués Matriz', name: 'Laboratorio Corregidora El Marqués Matriz', lat: 20.590621, lng: -100.330477, icon: "icon_covid", address: "Avenida Prolongación Constituyentes No. 32, Oriente, Colonia Villas del Marqués del Águila C.P. 76240, Altos del Marques, El Marqués, Querétaro.", schedule: "Lunes a Viernes de 7:00 hrs. a 19:30 hrs.<br/>Sábados de 7:00 hrs. a 13:30 hrs." },
//     { id: 2, title: 'Laboratorio Corregidora Sucursal Centro Histórico', name: 'Laboratorio Corregidora Sucursal Centro Histórico', lat: 20.595638, lng: -100.392729, icon: "icon", address: "Corregidora 75-101 Col. Centro, Querétaro, Qro.", schedule: "Lunes a Viernes de 7:00 hrs. a 20:00 hrs.<br/>Sábados de 7:00 hrs. a 13:30 hrs." },
//     { id: 3, title: 'Laboratorio Corregidora Sucursal Jurica', name: 'Laboratorio Corregidora Sucursal Jurica', lat: 20.645469, lng: -100.431589, icon: "icon", address: "Av. 5 de Febrero No. 2125 Local 9-10 Planta Baja, Condominio Plaza Norte, Zona Industrial Benito Juárez", schedule: "Lunes a sábado de 7:00 hrs. a 11:00 hrs." },
//     { id: 4, title: 'Laboratorio Corregidora Sucursal Juriquilla', name: 'Laboratorio Corregidora Sucursal Juriquilla', lat: 20.707342, lng: -100.447255, icon: "icon", address: "Boulevard Universitario 560, Centro Comercial Juriquilla, Col. Jurica Acueducto", schedule: "Lunes a sábado de 7:00 hrs. a 11:30 hrs." },
//     { id: 5, title: 'Laboratorio Corregidora Sucursal Tecnológico', name: 'Laboratorio Corregidora Sucursal Tecnológico', lat: 20.588998, lng: -100.404089, icon: "icon", address: "Av. Tecnológico Sur No. 2, local 103 planta baja, Col. Niños Héroes.", schedule: "Lunes a Sábado de 7:00 hrs. a 10:30 hrs." },
//     { id: 6, title: 'Laboratorio Corregidora Sucursal Pueblo Nuevo', name: 'Laboratorio Corregidora Sucursal Pueblo Nuevo', lat: 20.546885, lng: -100.428507, icon: "icon", address: "Av. Rufino Tamayo No. 37 Planta, baja interior 1 Col. Pueblo Nuevo, Corregidora, Qro.", schedule: "Lunes a sábado de 7:00 hrs. a 11:00 hrs." },
//     { id: 7, title: 'Laboratorio Corregidora Sucursal Milenio', name: 'Laboratorio Corregidora Sucursal Milenio', lat: 20.594070, lng: -100.349487, icon: "icon", address: "Camino Real de Carretas 416, local 105 Plaza Ubika, Fracc. Milenio III", schedule: "Lunes a Sábado de 7:00 hrs. a 10:30 hrs." },
//     { id: 8, title: 'Laboratorio Corregidora Sucursal El Refugio', name: 'Laboratorio Corregidora Sucursal Milenio', lat: 20.6421003, lng: -100.3461815, icon: "icon", address: "Anillo Fray Junípero Serra, Plaza Ubika El Refugio, Local. 124, El Refugio, C.P. 76148, Querétaro, Qro.", schedule: "Lunes a Sábado de 7:00 hrs. a 11:00 hrs." },
// ];

// const center = {
// 	lat: 20.609427580021816, lng: -100.39599799324692
// };

// async function initMap() {
//     const { Map } = await google.maps.importLibrary("maps");

//     map = new Map(document.getElementById('map'), {
//         zoom: 11,
//         center: center,
//         styles: styles
//     });

//     markers.map(location => {
// 		document.getElementById("dropdowns-list").innerHTML += mapNavItem(location)
//         Map.Marker({
//             position: location,
//             map: map,
//             icon: {
//                 url: `/img/${location.icon}.svg`,
//                 scaledSize: Map.Size(40, 40),
//             },
//             title: location.title,
// 			click: ({ domEvent }) => {
// 				const { target } = domEvent;
		
// 				console.log("target:", target);
// 			}
//         });
//     });

// 	markers.click(({ domEvent }) => {
// 		const { target } = domEvent;

// 		console.log("target:", target);
// 	});
// }

// initMap();
