import Swiper from 'swiper';
import "swiper/css";

document.addEventListener("DOMContentLoaded", () => {
    const swiper = new Swiper('.swiper');

    if (swiper.initialized) {
        document.getElementById('swiper-button-next').addEventListener('click', () => {
            swiper.slideNext();
        });

        document.getElementById('swiper-button-prev').addEventListener('click', () => {
            swiper.slidePrev();
        });
    }
});
